package vn.com.feliz.application;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import rx.Scheduler;
import rx.schedulers.Schedulers;
import vn.com.feliz.common.Constant;
import vn.com.feliz.network.services.ApiService;
/**
 * Created by Mr Son on 2016-07-27.
 */
public class BaseApplication extends Application {

    /**
     * Singleton Instance
     */
    private static BaseApplication mInstance;
    /**
     * API Service Instance
     */
    private ApiService mService;
    /**
     * Font Bold
     */
    private Typeface mFontBold;
    /**
     * Font Light
     */
    private Typeface mFontLight;
    /**
     * Font Medium
     */
    private Typeface mFontMedium;
    /**
     * Font Regular
     */
    private Typeface mFontRegular;
    /**
     * Font Thin
     */
    private Typeface mFontThin;
    /**
     * Font Italic
     */
    private Typeface mFontItalic;
    /**
     * mHeveltical
     */
//    private Typeface mHeveltical;
    /**
     * mAria
     */
    private Typeface mAria;

    //cuongnv
    private Scheduler mScheduler;

    private Tracker mTracker;

    /**
     * Default Constructor
     */
    public BaseApplication() {
        super();

        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Fabric.with(this, new Crashlytics());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //init facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        //creat realm
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);

        // Init context for Firebase
        if(!FirebaseApp.getApps(this).isEmpty()) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }

      /*  OkLogInterceptor okLogInterceptor = OkLogInterceptor.builder().build();

// create an instance of OkHttpClient builder
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

// add OkLogInterceptor to OkHttpClient's application interceptors
        okHttpBuilder.addInterceptor(okLogInterceptor);


// build
        OkHttpClient okHttpClient = okHttpBuilder.build();*/

        // Create Retrofit Builder
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(Constant.DOMAIN);
        builder.addConverterFactory(GsonConverterFactory.create());
        builder.addConverterFactory(ScalarsConverterFactory.create());
        // builder.client(okHttpClient);
        if (Constant.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient
                    .Builder()
                    .connectTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .addInterceptor(interceptor).build();
            builder.client(client);

        } else {
            OkHttpClient client = new OkHttpClient
                    .Builder()
                    .connectTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .readTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                    .writeTimeout(Constant.CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS).build();
            builder.client(client);
        }
        // Create Retrofit
        Retrofit retrofit = builder.build();
        // Set Log Enable for Retrofit

        // create an instance of OkLogInterceptor using a builder()


        // Create Service
        mService = retrofit.create(ApiService.class);
        //load fonts custom
        loadFonts();
        Tracker mTracker = getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("UX")
                .setAction("appstart")
                .build());
        mTracker = getDefaultTracker();
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.send(new HitBuilders.ScreenViewBuilder()
                .setNewSession()
                .build());
        AppEventsLogger.activateApp(this);
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            analytics.setLocalDispatchPeriod(1800);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker("UA-93481210-1");
            mTracker.enableExceptionReporting(true);
            mTracker.enableAdvertisingIdCollection(true);
            mTracker.enableAutoActivityTracking(true);
        }
        return mTracker;
    }

    /**
     * Load Fonts
     */
    private void loadFonts() {
        mFontBold = Typeface.createFromAsset(getAssets(), Constant.FONT_BOLD);
        mFontLight = Typeface.createFromAsset(getAssets(), Constant.FONT_LIGHT);
        mFontMedium = Typeface.createFromAsset(getAssets(), Constant.FONT_MEDIUM);
        mFontRegular = Typeface.createFromAsset(getAssets(), Constant.FONT_REGULAR);
        mFontThin = Typeface.createFromAsset(getAssets(), Constant.FONT_THIN);
        mFontItalic = Typeface.createFromAsset(getAssets(), Constant.FONT_ITALIC);
//        mHeveltical= Typeface.createFromAsset(getAssets(), Constant.HELVETICA);
        mAria= Typeface.createFromAsset(getAssets(), Constant.ARIA);
    }

    //cuongnv
    public Scheduler subscribeScheduler() {
        if (mScheduler == null){
            mScheduler = Schedulers.io();
        }

        return mScheduler;
    }

    public void setScheduler(Scheduler scheduler) {
        mScheduler = scheduler;
    }

    /**
     * Set the base context for this ContextWrapper.  All calls will then be
     * delegated to the base context.  Throws
     * IllegalStateException if a base context has already been set.
     *
     * @param base The new base context for this wrapper.
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        try {
            MultiDex.install(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Get Gcm Token
     *
     * @return String
     */
    public String getGcmToken() {
        return FirebaseInstanceId.getInstance().getToken();
    }
    // Initialize the facebook sdk and then callback manager will handle the login responses.

    /**
     * Get Singleton Instance
     *
     * @return BaseApplication
     */
    public static BaseApplication getInstance() {
        return mInstance;
    }

    /**
     * Get API Service
     *
     * @return ApiService
     */
    public ApiService getService() {
        return mService;
    }

    /**
     * Font Bold
     *
     * @return Typeface
     */
    public Typeface getFontBold() {
        return mFontBold;
    }

    /**
     * Font Light
     *
     * @return Typeface
     */
//    public Typeface getFontHelvetical() {
//        return mHeveltical;
//    }
    /**
     * Font Light
     *
     * @return Typeface
     */
    public Typeface getFontLight() {
        return mFontLight;
    }

    /**
     * Font Medium
     *
     * @return Typeface
     */
    public Typeface getFontMedium() {
        return mFontMedium;
    }

    /**
     * Font Regular
     *
     * @return Typeface
     */
    public Typeface getFontRegular() {
        return mFontRegular;
    }

    /**
     * Font Thin
     *
     * @return Typeface
     */
    public Typeface getFontThin() {
        return mFontThin;
    }

    /**
     * Font Italic
     *
     * @return Typeface
     */
    public Typeface getFontItalic() {
        return mFontItalic;
    }
    public Typeface getmAria() {
        return mAria;
    }

    public void setmAria(Typeface mAria) {
        this.mAria = mAria;
    }

}