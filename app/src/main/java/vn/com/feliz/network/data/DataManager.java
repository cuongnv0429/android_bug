package vn.com.feliz.network.data;

import android.content.Context;

import rx.Scheduler;
import vn.com.feliz.application.BaseApplication;

/**
 * Created by ITV01 on 12/26/16.
 */

public class DataManager {
    private Scheduler mScheduler;
    public static DataManager instance;
    public synchronized static DataManager getInstance(Context context) {
        if (instance == null) {
            instance = new DataManager(context.getApplicationContext());
        }
        return instance;
    }

    private DataManager(Context context) {
        mScheduler = BaseApplication.getInstance().subscribeScheduler();
    }

    public Scheduler getmScheduler() {
        return mScheduler;
    }

    public void setmScheduler(Scheduler mScheduler) {
        this.mScheduler = mScheduler;
    }
}
