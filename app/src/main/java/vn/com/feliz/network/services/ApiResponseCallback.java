package vn.com.feliz.network.services;

/**
 * Copyright Innotech Vietnam
 * Created by Mr Son on 6/24/2016.
 */
public interface ApiResponseCallback {
    /**
     * Process Response
     * @param task ApiTask
     * @return True if Task is finished and do next task
     */
    boolean onResponse(ApiTask task);
    boolean onFailure(ApiTask task);
}
