package vn.com.feliz.network.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.v.activity.MainActivity;


/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class NotificationListenerService extends FirebaseMessagingService/* implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener*/ {

    /*private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    @Override
    public void onCreate() {
            super.onCreate();
            createGoogleApiClient();
            }

    @Override
    public void onDestroy() {
            super.onDestroy();
            mGoogleApiClient.disconnect();
            }*/

    /**
     * Called when message is received.
     *
     * @param remoteMessage RemoteMessage
     */

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        BaseApplication mBaseApplication = (BaseApplication) getApplication();
        Tracker mTracker = mBaseApplication.getDefaultTracker();
        if (remoteMessage.getData() != null) {
//            Log.e("onMessageReceived", remoteMessage.getData().toString());
            int pushType = 0, update_app = 0;
            String gbCode = null,
                    couponId = null,
                    couponSubId = null,
                    adsId = null,
                    title = null,
                    content = null;

            try {
                Map<String, String> data = remoteMessage.getData();
                if(data != null) {
                    String push_type = data.get("push_type");
                    if(push_type != null) {
                        pushType = Integer.parseInt(push_type);
                    }

                    String updateApp = data.get("update_app");
                    if(updateApp != null) {
                        update_app = Integer.parseInt(updateApp);
                    }

                    gbCode = data.get("gb_code");
                    couponId = data.get("coupon_id");
                    couponSubId = data.get("coupon_sub_id");
                    adsId = data.get("ads_id");

                    title = data.get("title");
                    content = data.get("content");
                    mTracker.enableAdvertisingIdCollection(true);
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constant.CATEGORY_ANALYTISC_PUSH_NOTIFICATIONS)
                            .setAction(Constant.EVENT_ANALYTISC_NOTIFICATION_RECIEVED + "_" + pushType + "_" + adsId)
                            .build());

                }


                if(title == null) {
                    title = remoteMessage.getNotification().getTitle();
                }
                if(content == null) {
                    content = remoteMessage.getNotification().getBody();
                }
                sendNotification(title, content, pushType, couponId, couponSubId , gbCode, adsId, update_app);
                Log.d("HoangNM", "Đã nhận push_type = " + pushType);
//                if (pushType == 10) {
//                    Utils.saveAds(getApplicationContext(), remoteMessage.getData().get("ads_id"));
//                    Intent i = new Intent(getApplicationContext().getPackageName());
//                    i.putExtra(Constant.ACTION, Constant.ACTION_HAVE_ADS);
//                    sendBroadcast(i);
//                }
                /*if (pushType == 10) {
                    Utils.saveAds(getApplicationContext(), remoteMessage.getData().get("ads_id"));
                }else if (pushType == 5){//map
                    CouponAvailableModel.CouponAvailable couponAvailable = Utils.getCouponAvailable(getApplicationContext());
                    gbCode = remoteMessage.getData().get("gb_code");
                    couponId = remoteMessage.getData().get("coupon_id");
                    if (couponId != null) {
                        for (CouponModel.Coupon coupon : couponAvailable.getCoupons()){
                            if (couponId.equals(coupon.getCouponId())){
                                sendNotification(remoteMessage.getNotification().getTitle(),
                                        remoteMessage.getNotification().getBody(), pushType,
                                        couponId, gbCode);
                            }
                        }
                    }
                } else {
                    gbCode = remoteMessage.getData().get("gb_code");
                    couponId = remoteMessage.getData().get("coupon_id");
                    sendNotification(remoteMessage.getNotification().getTitle(),
                            remoteMessage.getNotification().getBody(), pushType,
                            couponId, gbCode);

                }*/
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    /*@Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    private void createGoogleApiClient(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }*/
    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String title, String message, int type, String couponId, String couponSubId, String gbCode, String adsId, int update_app) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("push_type", type);
        intent.putExtra("update_app", update_app);
        intent.putExtra("coupon_id", couponId);
        intent.putExtra("coupon_sub_id", couponSubId);
        intent.putExtra("gb_code", gbCode);
        intent.putExtra("ads_id", adsId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* request code */, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Bundle b = new Bundle();
        b.putInt("push_type", type);
        b.putInt("update_app", update_app);
        b.putString("coupon_id", couponId);
        b.putString("coupon_sub_id", couponSubId);
        b.putString("gb_code", gbCode);
        b.putString("ads_id", adsId);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder)
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icons_notifi_x)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setExtras(b);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(type, notificationBuilder.build());
    }

}
