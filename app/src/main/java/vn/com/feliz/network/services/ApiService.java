package vn.com.feliz.network.services;


import java.util.List;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.response.NotificationRespone;
import vn.com.feliz.m.response.QuestionDailyListResponse;
import vn.com.feliz.m.AddReviewResponse;
import vn.com.feliz.m.BaseResponseAddReview;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.ScanbarProductReviewResponse;
import vn.com.feliz.m.response.BuyCouponECResponse;
import vn.com.feliz.m.response.BuyCouponRespone;
import vn.com.feliz.m.response.InterestListResponse;
import vn.com.feliz.m.response.LoginResponse;
import vn.com.feliz.m.response.ProfileRespone;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.m.response.MessageGiftCoin;
import vn.com.feliz.m.response.MetaResponse;
import vn.com.feliz.m.response.RegisterResponse;
import vn.com.feliz.m.response.RequestCodeRespone;
import vn.com.feliz.m.response.VerifyResponse;

public interface ApiService {
    @POST("request-code")
    @Multipart
    Call<BaseResponse<RequestCodeRespone>>
    requestCode(@HeaderMap Map<String, String> headers,
                @PartMap Map<String, RequestBody> data);

    @POST("verify-code")
    @Multipart
    Call<BaseResponse<VerifyResponse>>
    verifyCode(@HeaderMap Map<String, String> headers,
               @PartMap Map<String, RequestBody> data);

    @POST("register")
    @Multipart
    Call<BaseResponse<RegisterResponse>>
    registerAccount(@HeaderMap Map<String, String> headers,
                    @PartMap Map<String, RequestBody> data);

    @POST("profile/edit")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    editProfile(@HeaderMap Map<String, String> headers,
                @PartMap Map<String, RequestBody> data);

    @POST("login")
    @Multipart
    Call<BaseResponse<LoginResponse>>
    login(@HeaderMap Map<String, String> headers,
          @PartMap Map<String, RequestBody> data);

    @POST("coin/gift")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    submitReferre(@HeaderMap Map<String, String> headers,
          @PartMap Map<String, RequestBody> data);

    @POST("profile/info")
    @Multipart
    Call<BaseResponse<ProfileRespone>>
    getProfile(@HeaderMap Map<String, String> headers,
               @PartMap Map<String, RequestBody> data);

    @POST("daily-question")
    @Multipart
    Call<BaseResponse<QuestionDailyListResponse>>
    getQuestionDaily(@HeaderMap Map<String, String> headers,
                     @PartMap Map<String, RequestBody> data);

    @POST("submit-answer")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    submitAnswer(@HeaderMap Map<String, String> headers,
                 @PartMap Map<String, RequestBody> data);

    @POST("user-interest")
    @Multipart
    Call<BaseResponse<InterestListResponse>>
    getInterest(@HeaderMap Map<String, String> headers,
                @PartMap Map<String, RequestBody> data);

    @POST("submit-interest")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    submitInterest(@HeaderMap Map<String, String> headers,
                   @PartMap Map<String, RequestBody> data);

    @GET("metadata")
    Call<BaseResponse<MetaResponse>>
    getMetadata(@HeaderMap Map<String, String> headers);

    @POST("coupon/submit-exchange")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    submitExchange(@HeaderMap Map<String, String> headers,
                   @PartMap Map<String, RequestBody> data);

    @POST("coupon/check-barcode")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    checkBarcode(@HeaderMap Map<String, String> headers,
                 @PartMap Map<String, RequestBody> data);

    @POST("review/list")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    getListReviews(@HeaderMap Map<String, String> headers,
                   @PartMap Map<String, RequestBody> data);

    @POST("review/list-by-user")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    getListByUser(@HeaderMap Map<String, String> headers,
                  @PartMap Map<String, RequestBody> data);

    @POST("review/helpful")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    submitHelpful(@HeaderMap Map<String, String> headers,
                  @PartMap Map<String, RequestBody> data);

    @POST("review/list")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    getListFriendReviews(@HeaderMap Map<String, String> headers,
                         @PartMap Map<String, RequestBody> data);

    @POST("review/list-by-product")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    getListReviewsByProduct(@HeaderMap Map<String, String> headers,
                            @PartMap Map<String, RequestBody> data);

    @POST("review/list-by-product")
    @Multipart
    Call<BaseResponse<ReviewAllItem>>
    getListReviewsByProducta(@HeaderMap Map<String, String> headers,
                            @PartMap Map<String, RequestBody> data);


    @POST("product/check-barcode")
    @Multipart
    Call<BaseResponse<ScanbarProductReviewResponse>>
    checkBarcodeProductReview(@HeaderMap Map<String, String> headers,
                              @PartMap Map<String, RequestBody> data);

    @POST("review/add")
    @Multipart
    Call<BaseResponse<AddReviewResponse>>
    addReview(@HeaderMap Map<String, String> headers,
              @PartMap Map<String, RequestBody> data);

    @POST("product/add-product-review")
    @Multipart
    Call<BaseResponse<BaseResponseAddReview>>
    addReviewUploadImage(@HeaderMap Map<String, String> headers,
                         @PartMap Map<String, RequestBody> data);

    @POST("coin/gift")
    @Multipart
    Call<BaseResponse<MessageGiftCoin>>
    getMessageGiftCoin(@HeaderMap Map<String, String> headers,
                       @PartMap Map<String, RequestBody> data);

    @POST("coupon/buy")
    @Multipart
    Call<BaseResponse<List<BuyCouponRespone>>>
    buyCoupon(@HeaderMap Map<String, String> headers,
              @PartMap Map<String, RequestBody> data);

    @POST("coupon/submit-escommerce-exchange")
    @Multipart
    Call<BaseResponse<List<BuyCouponECResponse>>>
    buyCouponEC(@HeaderMap Map<String, String> headers,
              @PartMap Map<String, RequestBody> data);

    @POST("coupon/submit-status")
    @Multipart
    Call<BaseResponse<BaseResponse>>
    submitStatusCoupon(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> data);

    @POST("review/add")
    @Multipart
    Call<BaseResponse>
    addReviewProductCoupon(@HeaderMap Map<String, String> headers, @PartMap Map<String, RequestBody> data);

    @POST("coupon/submit-home-exchange")
    @Multipart
    Call<BaseResponse>
    submitHomeExchange(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> data);

    @POST("notification/list")
    @Multipart
    Call<BaseResponse<List<NotificationRespone>>>
    getListNotifications(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> data);

    @POST("notification/submit-status")
    @Multipart
    Call<BaseResponse>
    submitStatusNotifications(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> data);

    @POST("issue/submit-issue")
    @Multipart
    Call<BaseResponse>
    submitIssue(@HeaderMap Map<String, String> header, @PartMap Map<String, RequestBody> data);


/*    @POST("review/list")
    @Multipart
    Call<BaseResponse<ListAllReviewsResponse>>
    getListReviewsFriends(@HeaderMap Map<String, String> headers,
                   @PartMap Map<String, RequestBody> data);*/
}
