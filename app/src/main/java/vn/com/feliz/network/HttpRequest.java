package vn.com.feliz.network;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.gbservice1.api.BaseService;
import vn.com.feliz.m.AdsModel;

import vn.com.feliz.m.AdsListModel;

/**
 * Created by Thomas on 2/19/17.
 */

public class HttpRequest extends BaseService {
    public AdsListModel getAdsList(Context context) {
        try {
            String url = Constant.DOMAIN + "ads/list";
            String device_id = Utils.generateUUID(context);
            HttpURLConnection connection = getConnection(device_id, url, RequestMethod.GET, null);
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
                Log.e("HttpRequest", result);
                return g.fromJson(result, AdsListModel.class);
            } else {
                result = readStream(connection.getErrorStream());
                Log.e("HttpRequest", result);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
