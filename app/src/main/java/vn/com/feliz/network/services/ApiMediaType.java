package vn.com.feliz.network.services;

/**
 * Copyright Innotech Vietnam
 * Created by Mr Son on 6/24/2016.
 */
public interface ApiMediaType {
    int FACE = 1;
    int NECK = 2;
    int VIDEO = 3;
    int AVATAR = 4;
}
