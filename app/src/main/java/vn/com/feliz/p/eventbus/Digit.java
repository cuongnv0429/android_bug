package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-11-30.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class Digit {

    String digit;

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }

    public Digit(String digit) {
        this.digit = digit;
    }
}
