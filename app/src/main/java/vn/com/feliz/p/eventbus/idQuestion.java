package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-11-24.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class idQuestion {
    String idQuestion;
    String idAnswer;

    public idQuestion(String idQuestion, String idAnswer) {
        this.idQuestion = idQuestion;
        this.idAnswer = idAnswer;
    }

    public String getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(String idAnswer) {
        this.idAnswer = idAnswer;
    }

    public idQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }

    public String getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(String idQuestion) {
        this.idQuestion = idQuestion;
    }
}
