package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-11.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SearchReviewFriend {
    public SearchReviewFriend(String keyWord) {
        this.keyWord = keyWord;
    }

    private String keyWord;

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
