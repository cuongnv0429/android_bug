package vn.com.feliz.p;

import android.content.Context;
import android.util.Log;

import retrofit2.Call;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.response.RegisterRespone;
import vn.com.feliz.m.response.RequestCodeRespone;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;

/**
 * Created by Hau on 2016-07-18.
 */
public class RegisterPresenter extends BasePresenter {
    private OnResponseListener mListener;
    private RequestCodeRespone mRequestCode;
    private RegisterRespone mRegisterRespone;

    public RegisterPresenter(Context context, OnResponseListener mListener) {
        super(context);
        this.mListener = mListener;
    }

    /**
     *
     * @param token String
     */
    public void loginRequest(final String token, final String accessToken) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return loginAccount(token, accessToken);
            }
        }, ApiTaskType.LOGIN, this);
    }
    /**
     * Login
     *
     * @param phone String
     */
    public void requestCode(final String token_user, final String phone, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return createRequestCode(token_user,phone, type);
            }
        }, ApiTaskType.REQUEST_CODE, this);
    }

    public void submitReferreCode(final String token, final String code) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitReferre(token, code);
            }
        }, ApiTaskType.SUBMIT_REFERRE_CODE, this);
    }

    /**
     * Login
     *
     * @param code String
     */
    public void verifyCode(final String code, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return createVerifyCode(code, type);
            }
        }, ApiTaskType.VERIFY_CODE, this);
    }

    /**
     * Login
     *
     * @param code String
     */
    public void verifyCode(final String code, final String type, final String token, final String phone) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return createVerifyCode(code, type, token, phone);
            }
        }, ApiTaskType.VERIFY_CODE, this);
    }

    /**
     * @param token           String
     * @param phone           String
     * @param address         String
     * @param progress_status String
     */
    public void changeProfile(final String birthday, final String token, final String phone,
                              final String address, final String progress_status, final String districtName, final String wardName ) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return editProfile(birthday,token, phone, address, progress_status,"","", districtName, wardName, "", "");
            }
        }, ApiTaskType.CHANGE_PROFILE, this);
    }

    public String id_Coupons() {
        return mRegisterRespone.getCoupon_id();
    }

    public String id_Coupons_sub() {
        return mRegisterRespone.getCoupon_sub_id();
    }
    /**
     * getMetaDataRequest
     */
    public void getMetaDataRequest() {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getMetadata();
            }
        }, ApiTaskType.GET_METADATA, this);
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Log.e("Register", "Register");
        if (task.getType() == ApiTaskType.REQUEST_CODE) {
            if (status == ApiResponseCode.SUCCESS) {
                BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                try {
                    mRequestCode = (RequestCodeRespone) baseResponse.getData();
                    Log.d("sssDEBUG", "mRequestCode " + mRequestCode.getVerify_code());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (task.getType() == ApiTaskType.GET_METADATA) {

        }
        return mListener.onResponse(task, status);
    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }

}
