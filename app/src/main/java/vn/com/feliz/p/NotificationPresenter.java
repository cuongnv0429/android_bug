package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;

import retrofit2.Call;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;

/**
 * Created by ITV01 on 2/24/17.
 */

public class NotificationPresenter extends BasePresenter{
    private OnResponseListener onResponseListener;

    public NotificationPresenter(Context context, OnResponseListener onResponseListener) {
        super(context);
        this.onResponseListener = onResponseListener;
    }

    /**
     * Default Constructor
     *
     * @param context Context
     */
    public NotificationPresenter(Context context) {
        super(context);
    }

    /**
     * Has User Info
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }

    public void getNotifications(final String token) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getNotification(token);
            }
        }, ApiTaskType.GET_NOTIFICATIONS, this);
    }

    public void submitStatus(final String token, final String pushType, final String pushStatus) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitStatusNotification(token, pushType, pushStatus);
            }
        }, ApiTaskType.SUBMIT_STATUS_NOTIFICATIONS, this);
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getType() == ApiTaskType.GET_NOTIFICATIONS || task.getType() == ApiTaskType.SUBMIT_STATUS_NOTIFICATIONS) {
            if(status == 200) {
                return onResponseListener.onResponse(task, status);
            }
        }
        return false;
    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }
}
