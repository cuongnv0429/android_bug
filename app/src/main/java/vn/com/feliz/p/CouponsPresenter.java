package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;

import retrofit2.Call;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;


/**
 * Created by Hau on 2016-07-18.
 */
public class CouponsPresenter extends BasePresenter {
    private OnResponseListener mListener;
    private BaseResponse mBaseResponse;


    public CouponsPresenter(Context context, OnResponseListener mListener) {
        super(context);
        this.mListener = mListener;
    }

    /**
     * requestSubmitCoupons
     * @param token String
     * @param couponId String
     * @param barCode String
     * @param shopId String
     */
    public void requestSubmitCoupons(final String token,final String couponId,
                                     final String barCode,final String shopId,final String quantity,final String coupons_sub_id) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitExchangeCoupons(token,couponId,barCode,shopId,quantity,coupons_sub_id);
            }
        }, ApiTaskType.SUBMIT_COUPONS, this);
    }

    /**
     *
     * @param token String
     * @param barCode String
     */
    public void checkBarcode(final String token, final String barCode, final String CouponsId) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return checkBarCodeScan(token,barCode, CouponsId);
            }
        }, ApiTaskType.CHECK_BARCODE, this);
    }

    //cuongnv
    public void buyCounpon(final String token, final String coupon_id, final String subIdCoupon) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return buy(token, coupon_id, subIdCoupon);
            }
        }, ApiTaskType.BUY_COUNPON, this);
    }
    //cuongnv
    public void submitStatus(final String token, final String coupon_id,final String couponSubId, final String value) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitStatusCoupon(token, coupon_id, couponSubId, value);
            }
        }, ApiTaskType.SUBMIT_STATUS_COUPON, this);
    }
    //cuongnv
    public void addReviewCoupon(final String token, final String barCode,final String point,
                                final String content, final String isShare) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return addReviewProductCoupon(token, barCode, point, content, isShare);
            }
        }, ApiTaskType.ADD_REVIEW_COUPON, this);
    }

    //cuongnv
    public void submitGiftHomeExchange(final String token, final String coupon_id, final String coupon_sub_id,
                                   final String status, final String address, final String city_name, final String district_name,
                                   final String ward_name) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitHomeExchange(token, coupon_id, coupon_sub_id, status, address, city_name, district_name, ward_name);
            }
        }, ApiTaskType.SUBMIT_HOME_EXCHANGE, this);
    }
    /**
     * Has User Info
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }

    /*
    * SonLam
    *Buy Coupon EC
     */
    public void buyCounponEC(final String token, final String coupon_id, final String subIdCoupon) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return buyEC(token, coupon_id, subIdCoupon);
            }
        }, ApiTaskType.BUY_COUNPON_EC, this);
    }


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getType() == ApiTaskType.CHECK_BARCODE) {
            if(!(status == ApiResponseCode.SUCCESS)){

            }
        }
        return mListener.onResponse(task, status);
    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }

    public void getListAllReviewByProductPresenta(final String token, final String product_id, final String offset,
                                                  final String limit, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAllReviewByProductA(token, product_id, offset,
                        limit, type);
            }
        }, ApiTaskType.GET_LIST_REVIEW_BY_PRODUCT_ONLY, this);
    }
}
