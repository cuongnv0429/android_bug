package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-12.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class BackEventFromDetail {
    public BackEventFromDetail(String screen) {
        this.screen = screen;
    }

    String screen;

    public String getScreen() {
        return screen;
    }

    public void setScreen(String screen) {
        this.screen = screen;
    }
}
