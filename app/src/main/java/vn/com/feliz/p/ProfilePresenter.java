package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.response.ProfileRespone;
import vn.com.feliz.m.response.QuestionDailyListResponse;
import vn.com.feliz.m.response.QuestionDailyResponse;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.m.response.RequestCodeRespone;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;

/**
 * Created by Hau on 2016-07-18.
 */
public class ProfilePresenter extends BasePresenter {
    private OnResponseListener mListener;
    private BaseResponse mBaseResponse;
    private ProfileRespone mRequestCodeProfile;
    private RequestCodeRespone mRequestCodeRespone;
    private QuestionDailyListResponse mQuestionDailyListResponse;
    private List<QuestionDailyResponse> mQuestionsResponse = new ArrayList<>();

    public ProfilePresenter(Context context, OnResponseListener mListener) {
        super(context);
        this.mListener = mListener;
    }

    /**
     * @param token           String
     * @param phone           String
     * @param address         String
     * @param progress_status String
     */
    public void changeProfile(final String  birthday, final String token, final String phone,
                              final String address, final String progress_status, final String allow_ads, final String size_ads,
                              final String districtName, final String wardName, final String cityName, final String countryName) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return editProfile(birthday,token, phone,
                        address,
                        progress_status,allow_ads,size_ads, districtName, wardName, cityName, countryName);
            }
        }, ApiTaskType.CHANGE_PROFILE, this);
    }

    /**
     * @param token String
     */
    public void requestProfile(final String token) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getProfileInfo(token);
            }
        }, ApiTaskType.GET_PROFILE, this);
    }

    /**
     * @param token String
     */
    public void requestQuestion(final String token) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getQuestion(token);
            }
        }, ApiTaskType.GET_QUESTION_DAILY, this);
    }
    /**
     * @param token String
     */
    public void requestInterest(final String token) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getInterest(token);
            }
        }, ApiTaskType.GET_INTEREST, this);
    }/**
     * @param token String
     */
    public void requestSubmitInterest(final String token, final String list) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitInterest(token,list);
            }
        }, ApiTaskType.SUBMIT_INTEREST, this);
    }
    /**
     *
     * @param token String
     */
    public void loginRequest(final String token, final String accessToken) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return loginAccount(token, accessToken);
            }
        }, ApiTaskType.LOGIN, this);
    }
    /**
     *
     * @param token
     * @param idAnswer
     */
    public void submitIdQuestion(final String token,  final String idAnswer) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitQuestion(token, idAnswer);
            }
        }, ApiTaskType.SUBMIT_ID_QUESTION, this);
    }

    /**
     * getBirthday
     *
     * @return String
     */
    public String getBirthday() {
        return mRequestCodeProfile.getBirthday();
    }

    /**
     * getBirthday
     *
     * @return String
     */
    public String getMobile() {
        return mRequestCodeProfile.getPhone();
    }
    /**
     * getAllow_ads
     *
     * @return String
     */
    public String getAllow_ads() {
        return mRequestCodeProfile.getAllow_ads();
    }
    /**
     * getAllow_ads
     *
     * @return String
     */
    public String getSize_ads() {
        return mRequestCodeProfile.getSize_ads();
    }
    /**
     * CheckInTime
     *
     * @return Date
     */
    public String getGender() {
        return mRequestCodeProfile.getGender();
    }

    /**
     * getAddress
     *
     * @return getAddress
     */
    public String getAddress() {
        return mRequestCodeProfile.getAddress();
    }

    /**
     * Has User Info
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getType() == ApiTaskType.REQUEST_CODE) {
            if (status == ApiResponseCode.SUCCESS) {
                try {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mRequestCodeRespone = (RequestCodeRespone) baseResponse.getData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (task.getType() == ApiTaskType.GET_PROFILE) {

            if (status == ApiResponseCode.SUCCESS) {
                mBaseResponse = (BaseResponse) task.getResponse().body();
                mRequestCodeProfile = (ProfileRespone) mBaseResponse.getData();
            }
        } else if (task.getType() == ApiTaskType.GET_QUESTION_DAILY) {

        } else if (task.getType() == ApiTaskType.SUBMIT_ID_QUESTION) {
            if (status == ApiResponseCode.SUCCESS) {
            }
        }
        return mListener.onResponse(task, status);
    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }
}
