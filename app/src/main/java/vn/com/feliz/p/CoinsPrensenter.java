package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;

import retrofit2.Call;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.network.OnResponseListener;

/**
 * Created by ITV01 on 12/27/16.
 */

public class CoinsPrensenter extends BasePresenter{
    private OnResponseListener onResponseListener;
    /**
     * Default Constructor
     *
     * @param context Context
     */
    public CoinsPrensenter(Context context, OnResponseListener onResponseListener) {
        super(context);
        this.onResponseListener = onResponseListener;
    }

    /**
     * Has User Info
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }

    public void getMessageGiftCoin(final String token, final String type, final String value) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getMessage(token, type, value);
            }
        }, ApiTaskType.GET_MESSAGE_GIFT_COINS, this);
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getType() == ApiTaskType.GET_MESSAGE_GIFT_COINS) {
            if(status == 200) {
                return onResponseListener.onResponse(task, status);
              /*  BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();*/
            }
        }
        return false;

    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }

}
