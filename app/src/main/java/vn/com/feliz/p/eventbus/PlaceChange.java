package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-11-11.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class PlaceChange {
    private String placeId;
    String place;
    private String district_name;
    private String ward_name;
    private String country_name;
    private String city_name;

    public PlaceChange() {
        this.district_name = "";
        this.ward_name = "";
        this.city_name = "";
        this.country_name = "";
        this.place = "";

    }
    public PlaceChange(String place) {
        this.place = place;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getWard_name() {
        return ward_name;
    }

    public void setWard_name(String ward_name) {
        this.ward_name = ward_name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
