package vn.com.feliz.p.eventbus.coins;

/**
 * Created by ITV01 on 12/25/16.
 */

public class DigitCoins {
    String digit;

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }

    public DigitCoins(String digit) {
        this.digit = digit;
    }
}
