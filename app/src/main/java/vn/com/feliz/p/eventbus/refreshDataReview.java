package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-06.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class refreshDataReview {
    String typeScreen;

    public refreshDataReview(String typeScreen) {
        this.typeScreen = typeScreen;
    }

    public String getTypeScreen() {
        return typeScreen;
    }

    public void setTypeScreen(String typeScreen) {
        this.typeScreen = typeScreen;
    }

}
