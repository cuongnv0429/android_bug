package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-12-16.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class refreshData {
    String nameFragment;

    public refreshData(String nameFragment) {
        this.nameFragment = nameFragment;
    }

    public String getNameFragment() {
        return nameFragment;
    }

    public void setNameFragment(String nameFragment) {
        this.nameFragment = nameFragment;
    }
}
