package vn.com.feliz.p;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.realm.Realm;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;
import vn.com.feliz.BuildConfig;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.MetaInfo;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.m.response.DeviceInfo;
import vn.com.feliz.m.response.LoginResponse;
import vn.com.feliz.m.response.MetaResponse;
import vn.com.feliz.m.response.RegisterResponse;
import vn.com.feliz.network.services.ApiResponseCallback;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiService;
import vn.com.feliz.network.services.ApiTask;


abstract class BasePresenter implements ApiResponseCallback {
    static Map<String, String> mHeaders;
    final Context mContext;
    /**
     * The Api Service
     */
    final ApiService mService;
    /**
     * Realm Instance
     */
    Realm mRealm;
    /**
     * User Info (Shared Between All of Presenters)
     */
    static UserInfo mUserInfo;
    static MetaInfo mMetaInfo;
    DeviceInfo mDeviceInfo;
    private String mMessageError;

    /**
     * Default Constructor
     *
     * @param context Context
     */
    public BasePresenter(Context context) {
        mContext = context;
        mRealm = Realm.getDefaultInstance();
        mService = BaseApplication.getInstance().getService();
        if (mHeaders == null) {
            initDevice(context);
            loadDeviceInfo();
        } else {
            mHeaders.clear();
            initDevice(context);
            loadDeviceInfo();
        }
    }

    /**
     *
     * @return
     */
    public DeviceInfo getDeviceInfo() {
        return mRealm.where(DeviceInfo.class).findFirst();
    }

    /**
     *
     */
    public void initDevice(Context context) {
        mDeviceInfo = getDeviceInfo();

        String uuid;

        if (mDeviceInfo == null || mDeviceInfo.getmDeviceId().equals("") || (mDeviceInfo!=null && !mDeviceInfo.isValid())) {

            mRealm.beginTransaction();

            uuid = Utils.generateUUID(context);

            if (mDeviceInfo != null) {
                mRealm.where(DeviceInfo.class).findAll().clear();
                mDeviceInfo = null;
            }

            if (mDeviceInfo == null) {
                mDeviceInfo = mRealm.createObject(DeviceInfo.class);
            }
            if (uuid != null) {
                mDeviceInfo.setmDeviceId(uuid);
            } else {
                mDeviceInfo.setmDeviceId(Utils.md5(System.currentTimeMillis() + Math.random() + "RandomUUID"));
            }

            mRealm.commitTransaction();
        }
    }

    public void initDevice(Context context, final Location mLocaiton) {
        mDeviceInfo = getDeviceInfo();

        String uuid;

        if (mDeviceInfo == null || mDeviceInfo.getmDeviceId().equals("") || (mDeviceInfo!=null && !mDeviceInfo.isValid())) {

            mRealm.beginTransaction();

            uuid = Utils.generateUUID(context);

            if (mDeviceInfo != null) {
                mRealm.where(DeviceInfo.class).findAll().clear();
                mDeviceInfo = null;
            }

            if (mDeviceInfo == null) {
                mDeviceInfo = mRealm.createObject(DeviceInfo.class);
            }
            if (uuid != null) {
                mDeviceInfo.setmDeviceId(uuid);
            } else {
                mDeviceInfo.setmDeviceId(Utils.md5(System.currentTimeMillis() + Math.random() + "RandomUUID"));
            }

            if (mLocaiton != null) {
                mDeviceInfo.setmLat(mLocaiton.getLatitude() + "");
                mDeviceInfo.setmLong(mLocaiton.getLongitude() + "'");
//                try {
//                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
//                    List<Address> addressList = new ArrayList<>();
//                    addressList = geocoder.getFromLocation(mLocaiton.getLatitude(), mLocaiton.getLongitude(), 1);
//                    if (addressList.size() > 0) {
//                        String city = addressList.get(0).getLocality();
//                        String area = addressList.get(0).getSubAdminArea();
//                        mDeviceInfo.setCity(BaseActivity.removeAccent(city));
//                        mDeviceInfo.setArea(BaseActivity.removeAccent(area));
//                    } else {
//                        mDeviceInfo.setCity("");
//                        mDeviceInfo.setArea("");
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    e.printStackTrace();
//                    mDeviceInfo.setCity("");
//                    mDeviceInfo.setArea("");
//                }

            }

            mRealm.commitTransaction();
        } else {
            if (mLocaiton != null) {
//                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
//                String city = "";
//                String area = "";
//                try {
//                    List<Address> addressList = geocoder.getFromLocation(mLocaiton.getLatitude(), mLocaiton.getLongitude(), 1);
//                    if (!addressList.isEmpty()) {
//                        city = addressList.get(0).getLocality();
//                        area = addressList.get(0).getSubAdminArea();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

                mRealm = Realm.getDefaultInstance();
//                final String finalCity = city;
//                final String finalArea = area;
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        mDeviceInfo.setmLat(mLocaiton.getLatitude() + "");
                        mDeviceInfo.setmLong(mLocaiton.getLongitude() + "'");
//                        mDeviceInfo.setCity(BaseActivity.removeAccent(finalCity));
//                        mDeviceInfo.setArea(BaseActivity.removeAccent(finalArea));
                    }
                });
            }
            loadDeviceInfo();
        }
    }

    /**
     * Load Device Info
     */
    private void loadDeviceInfo() {

        mHeaders = new HashMap<>();

        String device_id = "no_device_id";
        String lat = "";
        String lng = "";
//        String city = "";
//        String area = "";

        if (mDeviceInfo != null) {
            device_id = mDeviceInfo.getmDeviceId();
            try {
                lat = mDeviceInfo.getmLat();
                lng = mDeviceInfo.getmLong();
//                city = mDeviceInfo.getCity();
//                area = mDeviceInfo.getArea();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mHeaders.put("X-DEVICE-ID", device_id);
        mHeaders.put("X-OS-TYPE", "ANDROID");
        mHeaders.put("X-OS-VERSION", "Android " + Build.VERSION.RELEASE);
        mHeaders.put("X-API-ID", "ID_ITV_GB");
        mHeaders.put("X-API-KEY", "KEY_ITV_GB");
        mHeaders.put("X-APP-VERSION", BuildConfig.VERSION_NAME);
        mHeaders.put("X-LATITUDE", lat);
        mHeaders.put("X-LONGITUDE", lng);
//        mHeaders.put("X-CITY", BaseActivity.removeAccent(city));
//        mHeaders.put("X-AREA", BaseActivity.removeAccent(area));
        if (FirebaseInstanceId.getInstance().getToken() != null) {
            mHeaders.put("X-PUSH-TOKEN", FirebaseInstanceId.getInstance().getToken());
        }
        Log.d("sssDEBUG", mHeaders.toString());
    }

    /**
     * Has User Info
     */
    public MetaInfo hasMetaInfo() {
        if (mMetaInfo == null) {
            mMetaInfo = mRealm.where(MetaInfo.class).findFirst();
        }
        return mMetaInfo;
    }

    /**
     * Create Code Request
     *
     * @param phone String
     * @return Call
     */
    protected Call createRequestCode(String token, String phone, String type) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("phone", RequestBody.create(MediaType.parse("text/plain"), phone));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        return mService.requestCode(mHeaders, data);
    }

    public void setDeviceId(String device_id) {
        mHeaders.put("X-DEVICE-ID", device_id);
    }

    /**
     * Create Code Request
     *
     * @param code String
     * @return Call
     */
    protected Call createVerifyCode(String code, String type) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("code", RequestBody.create(MediaType.parse("text/plain"), code));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));
        return mService.verifyCode(mHeaders, data);
    }

    /**
     * Create Code Request
     *
     * @param code String
     * @return Call
     */
    protected Call createVerifyCode(String code, String type, String token, String phone) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("code", RequestBody.create(MediaType.parse("text/plain"), code));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));
        data.put("phone", RequestBody.create(MediaType.parse("text/plain"), phone));
        return mService.verifyCode(mHeaders, data);
    }

    /**
     * @param fullname fullname
     * @param email    String
     * @param fb_id    String
     * @param phone    String
     * @param gender   String
     * @param address  String
     * @param avatar   String
     * @param password String
     * @param uid      String
     * @param birthday String
     * @return registerAccount
     */
    protected Call createRegisterAccount(String accessToken, String fullname, String email, String fb_id, String phone,
                                         String gender, String address, String avatar, String password, String uid,
                                         String birthday) {
        Map<String, RequestBody> data = new HashMap<>();

        Log.e("mHeaders", mHeaders.toString());

        data.put("fb_access_token", RequestBody.create(MediaType.parse("text/plain"), accessToken));

        data.put("fullname", RequestBody.create(MediaType.parse("text/plain"), fullname));
        data.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
        data.put("fb_id", RequestBody.create(MediaType.parse("text/plain"), fb_id));
        data.put("phone", RequestBody.create(MediaType.parse("text/plain"), phone));
        data.put("gender", RequestBody.create(MediaType.parse("text/plain"), gender));
        data.put("address", RequestBody.create(MediaType.parse("text/plain"), address));
        data.put("avatar", RequestBody.create(MediaType.parse("text/plain"), avatar));
        data.put("password", RequestBody.create(MediaType.parse("text/plain"), password));
        data.put("uid", RequestBody.create(MediaType.parse("text/plain"), uid));
        data.put("birthday", RequestBody.create(MediaType.parse("text/plain"), birthday));
        return mService.registerAccount(mHeaders, data);
    }

    /**
     * @param token           String
     * @param phone           String
     * @param address         String
     * @param progress_status String
     * @return editProfile
     */
    protected Call editProfile(String birthday, String token, String phone,
                               String address,
                               String progress_status, String allow_ads, String size_ads,
                               String districtName, String wardName, String cityName, String countryName) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        //data.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
        data.put("phone", RequestBody.create(MediaType.parse("text/plain"), phone));
        //data.put("gender", RequestBody.create(MediaType.parse("text/plain"), gender));
        data.put("address", RequestBody.create(MediaType.parse("text/plain"), address));
        data.put("birthday", RequestBody.create(MediaType.parse("text/plain"), birthday));
        data.put("progress_status", RequestBody.create(MediaType.parse("text/plain"), progress_status));
        data.put("allow_ads", RequestBody.create(MediaType.parse("text/plain"), allow_ads));
        data.put("size_ads", RequestBody.create(MediaType.parse("text/plain"), size_ads));
        data.put("district_name", RequestBody.create(MediaType.parse("text/plain"), districtName));
        data.put("ward_name", RequestBody.create(MediaType.parse("text/plain"), wardName));
        data.put("city_name", RequestBody.create(MediaType.parse("text/plain"), cityName));
        data.put("country_name", RequestBody.create(MediaType.parse("text/plain"), countryName));
        return mService.editProfile(mHeaders, data);
    }

    /**
     * @param token           String
     * @param progress_status String
     * @return editProfile
     */
    protected Call editProfileFromsetting(String token, String birthday,

                                          String progress_status) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        //data.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
        //data.put("gender", RequestBody.create(MediaType.parse("text/plain"), gender));
        data.put("birthday", RequestBody.create(MediaType.parse("text/plain"), birthday));
        data.put("progress_status", RequestBody.create(MediaType.parse("text/plain"), progress_status));
        return mService.editProfile(mHeaders, data);
    }

    /**
     * @param token String
     * @return status code
     */
    protected Call loginAccount(String token, String accessToken) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("fb_access_token", RequestBody.create(MediaType.parse("text/plain"), accessToken));
        Log.e("mHeaders", mHeaders.toString());
        return mService.login(mHeaders, data);
    }

    protected Call submitReferre(String token, String code) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("code", RequestBody.create(MediaType.parse("text/plain"), code));
        return mService.submitReferre(mHeaders, data);
    }

    /**
     * /* *
     *
     * @param token String
     * @return Profile info
     */
    protected Call getProfileInfo(String token) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        return mService.getProfile(mHeaders, data);
    }

    /**
     * /* *
     *
     * @param token String
     * @return Profile info
     */
    protected Call getQuestion(String token) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        return mService.getQuestionDaily(mHeaders, data);
    }

    /**
     * /* *
     *
     * @param token String
     * @return Profile info
     */
    protected Call getInterest(String token) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        return mService.getInterest(mHeaders, data);
    }

    /**
     * @param token
     * @param list
     * @return
     */
    protected Call submitInterest(String token, String list) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("list", RequestBody.create(MediaType.parse("text/plain"), list));
        return mService.submitInterest(mHeaders, data);
    }

    /**
     * @param token    String
     * @param couponId String
     * @param barCode  String
     * @param shopId   String
     * @return
     */
    protected Call submitExchangeCoupons(String token, String couponId, String barCode, String shopId, String quantity, String coupons_sub_id) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), couponId));
        data.put("barcode", RequestBody.create(MediaType.parse("text/plain"), barCode));
        data.put("shop_id", RequestBody.create(MediaType.parse("text/plain"), shopId));
        data.put("quantity", RequestBody.create(MediaType.parse("text/plain"), quantity));
        data.put("coupon_sub_id", RequestBody.create(MediaType.parse("text/plain"), coupons_sub_id));
        return mService.submitExchange(mHeaders, data);
    }

    protected Call checkBarCodeScan(String token, String barCode, String CouponsId) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("barcode", RequestBody.create(MediaType.parse("text/plain"), barCode));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), CouponsId));

        return mService.checkBarcode(mHeaders, data);
    }


    /**
     * @return getMetadata
     */
    protected Call getMetadata() {
        /*Map<String, RequestBody> data = new HashMap<>();*/
        return mService.getMetadata(mHeaders);
    }

    /**
     * @param token
     * @param idAnser
     * @return
     */
    protected Call submitQuestion(String token, String idAnser) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("answer", RequestBody.create(MediaType.parse("text/plain"), idAnser));
        return mService.submitAnswer(mHeaders, data);
    }

    /*Review Screen----------------------------------------------------------------------*/
    protected Call getListAll(String token, String review_type, String offset, String limit) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), review_type));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("keyword", RequestBody.create(MediaType.parse("text/plain"), ""));
        return mService.getListReviews(mHeaders, data);
    }

    /**
     * submitHelpfulPresenter
     *
     * @param token     token
     * @param review_id review_id
     * @param type      type
     * @return
     */
    protected Call submitHelpfulPresenter(String token, String review_id, String type) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("review_id", RequestBody.create(MediaType.parse("text/plain"), review_id));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));
        return mService.submitHelpful(mHeaders, data);
    }

    protected Call getListAllSearch(String token, String review_type, String offset, String limit, String keyword) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), review_type));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("keyword", RequestBody.create(MediaType.parse("text/plain"), keyword));

        return mService.getListReviews(mHeaders, data);
    }

    /**
     * getListByProductUser
     * @param token token
     * @param offset offset
     * @param limit limit
     * @param user_id user_id
     * @return
     */
    protected Call getListByProductUser(String token, String offset, String limit, String user_id) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("user_id", RequestBody.create(MediaType.parse("text/plain"), user_id));


        return mService.getListByUser(mHeaders, data);
    }
    protected Call getListSearchByProductUser(String token, String offset, String limit, String user_id, String keyword) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("user_id", RequestBody.create(MediaType.parse("text/plain"), user_id));
        data.put("keyword", RequestBody.create(MediaType.parse("text/plain"), keyword));


        return mService.getListByUser(mHeaders, data);
    }

    /**
     * api get list friend
     *
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     * @param keyword
     * @return
     */
    protected Call getListFriendReviewsBase(String token, String review_type, String offset, String limit, String keyword) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), review_type));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("keyword", RequestBody.create(MediaType.parse("text/plain"), keyword));

        return mService.getListFriendReviews(mHeaders, data);
    }

    /**
     * getListAllReviewByProduct
     *
     * @param token
     * @param product_id
     * @param offset
     * @param limit
     * @param type
     * @return
     */
    protected Call getListAllReviewByProduct(String token, String product_id, String offset, String limit, String type) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("product_id", RequestBody.create(MediaType.parse("text/plain"), product_id));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));

        return mService.getListReviewsByProduct(mHeaders, data);
    }
    protected Call getListAllReviewByProductA(String token, String product_id, String offset, String limit, String type) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("product_id", RequestBody.create(MediaType.parse("text/plain"), product_id));
        data.put("offset", RequestBody.create(MediaType.parse("text/plain"), offset));
        data.put("limit", RequestBody.create(MediaType.parse("text/plain"), limit));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));

        return mService.getListReviewsByProducta(mHeaders, data);
    }

    /*Coin Screen*/
    protected Call getMessage(String token, String type, String value) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("type", RequestBody.create(MediaType.parse("text/plain"), type));
        data.put("value", RequestBody.create(MediaType.parse("text/plain"), value));
        return mService.getMessageGiftCoin(mHeaders, data);
    }

    /*Notification Screen*/
    protected Call getNotification(String token) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        return mService.getListNotifications(mHeaders, data);
    }

    protected Call submitStatusNotification(String token, String pushId, String pushStatus) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("push_type", RequestBody.create(MediaType.parse("text/plain"), pushId));
        data.put("push_status", RequestBody.create(MediaType.parse("text/plain"), pushStatus));
        return mService.submitStatusNotifications(mHeaders, data);
    }

    /*Scanbar Review*/
    protected Call ScanbarProductReview(String token, String bar_code) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("bar_code", RequestBody.create(MediaType.parse("text/plain"), bar_code));
        return mService.checkBarcodeProductReview(mHeaders, data);
    }

    /**
     * addReview
     *
     * @param token
     * @param bar_code
     * @return
     */
    protected Call addReviewCreate(String token, String bar_code, String point, String content, String is_share) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("bar_code", RequestBody.create(MediaType.parse("text/plain"), bar_code));
        data.put("point", RequestBody.create(MediaType.parse("text/plain"), point));
        data.put("content", RequestBody.create(MediaType.parse("text/plain"), content));
        data.put("is_share", RequestBody.create(MediaType.parse("text/plain"), is_share));
        return mService.addReview(mHeaders, data);
    }

    /**
     * addReviewUploadCreate
     *
     * @param token
     * @param bar_code
     * @param point
     * @param content
     * @param is_share
     * @param file
     * @return
     */
    protected Call addReviewUploadCreate(String token, String bar_code, String point, String title,
                                         String content, String is_share, File file) {
//        String key = String.format("file\"file; filename=\"%s\" ", file.getName());
        String key = "file\"; filename=\"" + file.getName() + "\" ";
        String fileType;
        if (file.getPath().endsWith(".mp4")) {
            fileType = "video/*";
        } else if (file.getPath().endsWith(".mp3")) {
            fileType = "audio/*";
        } else {
            fileType = "image/jpeg";
        }

        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("bar_code", RequestBody.create(MediaType.parse("text/plain"), bar_code));
        data.put("point", RequestBody.create(MediaType.parse("text/plain"), point));
        data.put("title", RequestBody.create(MediaType.parse("text/plain"), title));
        data.put("content", RequestBody.create(MediaType.parse("text/plain"), content));
        data.put("is_share", RequestBody.create(MediaType.parse("text/plain"), is_share));
        data.put(key, RequestBody.create(MediaType.parse(fileType), file));
        Log.d("token", "[createUploadMediaRequest] token = " + token + " \nfile = " + file.getPath());
        Log.d("bar_code", "[createUploadMediaRequest] bar_code = " + bar_code + " \nfile = ");
        Log.d("point", "[createUploadMediaRequest] token = " + point);
        Log.d("content", "[createUploadMediaRequest] token = " + content);
        Log.d("is_share", "[createUploadMediaRequest] token = " + is_share);
        Log.d("file", "[createUploadMediaRequest] token = " + token + " \nfile = " + file.getUsableSpace());
        return mService.addReviewUploadImage(mHeaders, data);
    }
    protected Call buy(String token, String CouponsId, String subIdCoupon) {
        Map<String, RequestBody> data = new HashMap<>();
        Log.e("buy", token);
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), CouponsId));
        data.put("coupon_sub_id", RequestBody.create(MediaType.parse("text/plain"), subIdCoupon));

        return mService.buyCoupon(mHeaders, data);
    }

    //cuongnv
    protected Call submitStatusCoupon(String token, String couponsId, String couponSupId, String value) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), couponsId));
        data.put("coupon_sub_id", RequestBody.create(MediaType.parse("text/plain"), couponSupId));
        data.put("value", RequestBody.create(MediaType.parse("text/plain"), value));
        return mService.submitStatusCoupon(mHeaders, data);
    }

    //cuongnv
    protected Call addReviewProductCoupon(String token, String barCode, String point,
                                          String content, String isShare) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("bar_code", RequestBody.create(MediaType.parse("text/plain"), barCode));
        data.put("point", RequestBody.create(MediaType.parse("text/plain"), point));
        data.put("content", RequestBody.create(MediaType.parse("text/plain"), content));
        data.put("is_share", RequestBody.create(MediaType.parse("text/plain"), isShare));
        return mService.addReviewProductCoupon(mHeaders, data);
    }

    //cuongnv
    protected Call submitHomeExchange(String token, String coupon_id, String coupon_sub_id,
                                      String status, String address, String city_name, String district_name,
                                      String ward_name) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), coupon_id));
        data.put("coupon_sub_id", RequestBody.create(MediaType.parse("text/plain"), coupon_sub_id));
        data.put("status", RequestBody.create(MediaType.parse("text/plain"), status));
        data.put("address", RequestBody.create(MediaType.parse("text/plain"), address));
        data.put("city_name", RequestBody.create(MediaType.parse("text/plain"), city_name));
        data.put("district_name", RequestBody.create(MediaType.parse("text/plain"), district_name));
        data.put("ward_name", RequestBody.create(MediaType.parse("text/plain"), ward_name));
        return mService.submitHomeExchange(mHeaders, data);
    }

    /*
    * Buy coupon EC
     */
    protected Call buyEC(String token, String CouponsId, String subIdCoupon) {
        Map<String, RequestBody> data = new HashMap<>();
        Log.e("buyEC", token);
        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("coupon_id", RequestBody.create(MediaType.parse("text/plain"), CouponsId));
        data.put("coupon_sub_id", RequestBody.create(MediaType.parse("text/plain"), subIdCoupon));

        return mService.buyCouponEC(mHeaders, data);
    }
    //error login facebook
    protected Call submitError(String error) {
        Log.e("submitError: ", mHeaders.toString());
        Map<String, RequestBody> data = new HashMap<>();
//        data.put("token", RequestBody.create(MediaType.parse("text/plain"), token));
        data.put("error", RequestBody.create(MediaType.parse("text/plain"), error));
        return mService.submitIssue(mHeaders, data);
    }

    /**
     * Get User Info
     *
     * @return Info
     */
    public UserInfo getUserInfo() {
        return mUserInfo;
    }

    /**
     * Clear Invalid Token
     */
    public void clearInvalidToken() {
        mRealm.beginTransaction();
        mUserInfo.setToken("");
        Utils.saveToken(mContext, "");
        mRealm.commitTransaction();
        Log.d("DEBUG", "[BasePresenter] Clear Token");
    }

    /**
     *
     * @param status
     */
    public void updateIsVerify(String status) {
        mRealm.beginTransaction();
        mUserInfo.setIs_verify(status);
        mRealm.commitTransaction();
    }

    public void updatePhone(String phone) {
        mRealm.beginTransaction();
        mUserInfo.setPhone(phone);
        mRealm.commitTransaction();
    }

    /**
     * Parse Response
     *
     * @param response BaseResponse
     */
    @SuppressLint("LongLogTag")
    protected void parseResponse(BaseResponse response) {
        Log.d("sssDEBUG full json => ", new Gson().toJson(response.getData()));

        if (response.getCode() == 416) {
            // Token not valid
        }

        Object data = response.getData();
        if (data instanceof RegisterResponse) {
            processResponse((RegisterResponse) data);
        } else if (data instanceof LoginResponse) {
            processResponse((LoginResponse) data);
        } else if (data instanceof MetaResponse) {
            processResponse((MetaResponse) data);
        }

    }

    /**
     * Process Response
     *
     * @param response RegisterResponse
     */
    private void processResponse(RegisterResponse response) {
        mRealm.beginTransaction();

        if (mUserInfo != null && !mUserInfo.isValid()) {
            mUserInfo = null;
        }

        if (mUserInfo != null) {
            mRealm.where(UserInfo.class).findAll().clear();
            mUserInfo = null;
            Log.e("e BasePresenter", "NULL");
        }

        if (mUserInfo == null) {
            mUserInfo = mRealm.createObject(UserInfo.class);
        }
        mUserInfo.setToken(response.getToken());
        mUserInfo.setLogin_type(response.getLogin_type());
        mUserInfo.setUid(response.getUid());
        mUserInfo.setIs_verify(response.getIsVerify());
        mUserInfo.setFb_id(response.getFacebookId());
        mUserInfo.setPhone(response.getPhone());
        mUserInfo.setAllow_ads(response.getAllow_ads());
        Utils.saveToken(mContext, response.getToken());
        Utils.saveUid(mContext, response.getUid());

        mRealm.commitTransaction();
    }

    /**
     * Process Response
     *
     * @param response LoginResponse
     */
    private void processResponse(LoginResponse response) {
        mRealm.beginTransaction();

        if (mUserInfo != null && !mUserInfo.isValid()) mUserInfo = null;

        if (mUserInfo != null) {
            mRealm.where(UserInfo.class).findAll().clear();
            mUserInfo = null;
        }

        if (mUserInfo == null) {
            mUserInfo = mRealm.createObject(UserInfo.class);
        }

        Log.e("token", response.getToken());
        Log.e("LoginResponse", response.getUid());
        mUserInfo.setToken(response.getToken());
        mUserInfo.setLogin_type(response.getLogin_type());
        mUserInfo.setUid(response.getUid());
        mUserInfo.setIs_verify(response.getIsVerify());
        mUserInfo.setFb_id(response.getFacebookId());
        mUserInfo.setPhone(response.getPhone());
        mUserInfo.setAllow_ads(response.getAllow_ads());
        Utils.saveToken(mContext, response.getToken());
       /* mUserInfo.setEmail(response.getEmail());
        mUserInfo.setFullname(response.getFullname());
        mUserInfo.setFirebase_password(response.getFirebase_password());*/

        mRealm.commitTransaction();
    }

    /**
     * Process Response
     *
     * @param response LoginResponse
     */
    private void processResponse(MetaResponse response) {

        mRealm.beginTransaction();

        if (mMetaInfo != null && !mMetaInfo.isValid()) mMetaInfo = null;

        if (mMetaInfo != null) {
            mRealm.where(MetaInfo.class).findAll().clear();
            mMetaInfo = null;
        }

        if (mMetaInfo == null) {
            mMetaInfo = mRealm.createObject(MetaInfo.class);
        }

        mMetaInfo.setTerm(response.getTerm());
        mMetaInfo.setApp_info(response.getApp_info());
        mMetaInfo.setApp_intro(response.getApp_intro());
        mMetaInfo.setCondition(response.getCondition());

        if(response.getAds_review() != null) {
            mMetaInfo.setAds_review(response.getAds_review());
        }

        if(response.getEvent_mode() != null) {
            mMetaInfo.setEvent_mode(response.getEvent_mode());
        } else {
            mMetaInfo.setEvent_mode("0");
        }

        if(response.getScan_timeout() != null) {
            mMetaInfo.setScan_timeout(response.getScan_timeout());
        } else {
            mMetaInfo.setScan_timeout("20000");
        }

        if (response.getForce_update() != null) {
            mMetaInfo.setForce_update(response.getForce_update());
        }

        if (response.getForce_message() != null) {
            mMetaInfo.setForce_message(response.getForce_message());
        }

        if (response.getForce_version() != null) {
            mMetaInfo.setForce_version(response.getForce_version());
        }

        if (response.getGame_link() != null) {
            mMetaInfo.setGame_link(response.getGame_link());
        }

        if (response.getGame_message() != null) {
            mMetaInfo.setGame_message(response.getGame_message());
        }

        if (response.getGame_image_url() != null) {
            mMetaInfo.setGame_image_url(response.getGame_image_url());
        }

        mMetaInfo.setVerify_code_limit(Integer.parseInt(response.getVerify_code_limit()));
        mRealm.commitTransaction();
    }


    /**
     * message err base
     *
     * @return
     */
    public String messageErr() {
        return mMessageError;
    }
    /**
     * Process Response
     *
     * @param response LoginResponse
     */

    /**
     * Process Response
     *
     * @param task ApiTask
     * @return True if Task is finished and do next task
     */
    @Override
    public final boolean onResponse(ApiTask task) {

        Log.e("eBasePresenter", "333");

        int status = ApiResponseCode.CANNOT_CONNECT_TO_SERVER;
        Response response = task.getResponse();

        if (response != null) {
            if (response.code() == 200) {

                status = ((BaseResponse) response.body()).getCode();

                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse.getMessage() != null) {
                        mMessageError = baseResponse.getMessage();
                    } else {
                        mMessageError = null;
                    }
                    parseResponse((BaseResponse) response.body());

                } else {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse.getMessage() != null) {
                        mMessageError = baseResponse.getMessage();
                    } else {
                        mMessageError = null;
                    }
                }
            }
        }

        return onPostResponse(task, status);
    }

    /**
     * Process Response
     *
     * @param task ApiTask
     * @return True if Task is finished and do next task
     */
    @Override
    public final boolean onFailure(ApiTask task) {
        return onFailureResponse(task);
    }


   /* *//**
     * Create Login Request
     *
     * @param email String
     * @param pass  String
     * @return Call
     *//*
    protected Call createLoginRequest(String email, String pass) {
        Map<String, RequestBody> data = new HashMap<>();
        data.put("username", RequestBody.create(MediaType.parse("text/plain"), email));
        data.put("password", RequestBody.create(MediaType.parse("text/plain"), pass));

        return mService.login(mHeaders, data);
    }*/

    /**
     * Process Response
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Task is finished and do next task
     */
    public abstract boolean onPostResponse(ApiTask task, int status);

    public abstract boolean onFailureResponse(ApiTask task);
}