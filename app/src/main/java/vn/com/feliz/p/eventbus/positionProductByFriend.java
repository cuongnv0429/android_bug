package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-14.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class positionProductByFriend {
  int position;

    public positionProductByFriend(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
