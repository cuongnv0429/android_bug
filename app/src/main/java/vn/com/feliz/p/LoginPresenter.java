package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;

import retrofit2.Call;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiTaskType;

/**
 *
 */
public class LoginPresenter extends BasePresenter {
    private OnResponseListener mListener;

    public LoginPresenter(Context context, OnResponseListener mListener) {
        super(context);
        this.mListener = mListener;
    }

    /**
     *
     * @param token String
     */
    public void loginRequest(final String token, final String accessToken) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return loginAccount(token, accessToken);
            }
        }, ApiTaskType.LOGIN, this);
    }

    /**
     * Login
     *
     * @param phone String
     */
    public void requestCode(final String token_user, final String phone, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return createRequestCode(token_user,phone, type);
            }
        }, ApiTaskType.REQUEST_CODE, this);
    }

    /**
     * @param fullname fullname
     * @param email    String
     * @param fb_id    String
     * @param phone    String
     * @param gender   String
     * @param address  String
     * @param avatar   String
     * @param password String
     * @param uid      String
     * @param birthday String
     */
    public void registerAccount2( final String accessToken, final String fb_id, final String fullname, final String email, final String phone,
                                 final String gender, final String address, final String avatar, final String password, final String uid,
                                 final String birthday) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return createRegisterAccount(accessToken, fullname, email, fb_id, phone, gender, address, avatar, password, uid, birthday);
            }
        }, ApiTaskType.REGISTER, this);
    }


    public void submitErrorLoginFB(final String error) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitError(error);
            }
        }, ApiTaskType.SUBMIT_ERROR, this);
    }

    /**
     *
     * @return
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }


    public void clearUser() {
        mRealm.beginTransaction();
        mRealm.where(UserInfo.class).findAll().clear();
        mRealm.commitTransaction();
    }

    /**
     *
     * @param task   ApiTask
     * @param status int
     * @return
     */

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        return mListener.onResponse(task, status);
    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        return false;
    }
}
