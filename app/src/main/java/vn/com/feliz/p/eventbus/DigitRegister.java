package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-12-03.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class DigitRegister {
    String digit;
    public DigitRegister(String digit) {
        this.digit = digit;
    }

    public String getDigit() {
        return digit;
    }

    public void setDigit(String digit) {
        this.digit = digit;
    }


}
