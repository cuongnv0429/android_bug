package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-09-27.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SetTitleMessage {
    private final String mTitleText;
    public String getmTitleText() {
        return mTitleText;
    }
    public SetTitleMessage(String mTitleText) {
        this.mTitleText = mTitleText;
    }
}
