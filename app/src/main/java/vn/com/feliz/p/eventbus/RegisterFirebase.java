package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-11-18.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RegisterFirebase {
    public RegisterFirebase(String idFb) {
        this.idFb = idFb;
    }

    String idFb;

    public String getIdFb() {
        return idFb;
    }

    public void setIdFb(String idFb) {
        this.idFb = idFb;
    }
}
