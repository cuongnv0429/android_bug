package vn.com.feliz.p;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.search.SearchAuth;

import java.io.File;

import retrofit2.Call;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.TempImageInfo;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;

/**
 * Created by Hau on 2016-07-18.
 */
public class ReviewsPrensenter extends BasePresenter {
    private OnResponseListener mListener;
    private BaseResponse mBaseResponse;
    /*private ListAllReviewsResponse mReviewAllResponse;*/

    public ReviewsPrensenter(Context context, OnResponseListener mListener) {
        super(context);
        this.mListener = mListener;
    }

    /**
     *
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     * @param keyword
     */
    public void getListAllReviewsSearch(final String token, final String review_type, final String offset,
                                        final String limit, final String keyword) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAllSearch(token, review_type, offset,
                        limit, keyword);
            }
        }, ApiTaskType.GET_LIST_ALL, this);
    }

    public void getListByUser(final String token, final String offset,
                                        final String limit, final String user_id) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListByProductUser(token, offset,
                        limit, user_id);
            }
        }, ApiTaskType.GET_LIST_PRODUCT_BY_USER, this);
    }

    /**
     *
     * @param token
     * @param offset
     * @param limit
     * @param user_id
     */
    public void getListByUserSearch(final String token, final String offset,
                              final String limit, final String user_id , final  String key_word) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListSearchByProductUser(token, offset,
                        limit, user_id,key_word);
            }
        }, ApiTaskType.GET_LIST_PRODUCT_BY_USER, this);
    }
    /**
     *
     * @param token
     * @param review_id
     * @param type
     */
    public void submitHelpful(final String token, final String review_id, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return submitHelpfulPresenter(token, review_id, type);
            }
        }, ApiTaskType.SUBMIT_HELP_FULL, this);
    }

    /**
     *
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     */
    public void getListAllReviews(final String token, final String review_type, final String offset,
                                  final String limit) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAll(token, review_type, offset,
                        limit);
            }
        }, ApiTaskType.GET_LIST_ALL, this);
    }

    /**
     * get list me
     *
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     */
    public void getListMeReviews(final String token, final String review_type, final String offset,
                                 final String limit) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAll(token, review_type, offset,
                        limit);
            }
        }, ApiTaskType.GET_LIST_ME, this);
    }

    /**
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     * @param keyword
     */
    public void getListMeReviewsSearch(final String token, final String review_type, final String offset,
                                       final String limit, final String keyword) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAllSearch(token, review_type, offset,
                        limit, keyword);
            }
        }, ApiTaskType.GET_LIST_ME, this);
    }

    /**
     * get list friend
     *
     * @param token
     * @param review_type
     * @param offset
     * @param limit
     * @param keyword
     */
    public void getListFriendReviews(final String token, final String review_type, final String offset,
                                     final String limit, final String keyword) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListFriendReviewsBase(token, review_type, offset,
                        limit, keyword);
            }
        }, ApiTaskType.GET_LIST_FRIEND, this);
    }

    public void getListAllReviewByProductPresent(final String token, final String product_id, final String offset,
                                                 final String limit, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAllReviewByProduct(token, product_id, offset,
                        limit, type);
            }
        }, ApiTaskType.GET_LIST_REVIEW_BY_PRODUCT, this);
    }
    public void getListAllReviewByProductPresenta(final String token, final String product_id, final String offset,
                                                 final String limit, final String type) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return getListAllReviewByProductA(token, product_id, offset,
                        limit, type);
            }
        }, ApiTaskType.GET_LIST_REVIEW_BY_PRODUCT_ONLY, this);
    }
    /**
     * requestScanbarReview
     *
     * @param token
     * @param bar_code
     */
    public void requestScanbarReview(final String token, final String bar_code) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return ScanbarProductReview(token, bar_code);
            }
        }, ApiTaskType.SCAN_BARCODE_REVIEW, this);
    }

    /**
     * addReviewProduct
     *
     * @param token
     * @param bar_code
     * @param point
     * @param content
     * @param isShare
     */
    public void addReviewProduct(final String token, final String bar_code, final String point, final String content, final String isShare) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return addReviewCreate(token, bar_code, point, content, isShare);
            }
        }, ApiTaskType.ADD_REVIEW, this);
    }

    /**
     * addReviewUploadProduct
     *
     * @param token
     * @param bar_code
     * @param point
     * @param content
     * @param isShare
     * @param file
     */
    public void addReviewUploadProduct(final String token, final String bar_code,
                                       final String point, final String title, final String content,
                                       final String isShare, final String file) {
        ApiTask.execute(new ApiTask.OnCreateCallCallback() {
            @Override
            public Call onCreateCall() {
                return addReviewUploadCreate(token, bar_code, point, title, content, isShare, new File(file));
            }
        }, ApiTaskType.ADD_REVIEW_UPLOAD, this);
    }

    /**
     * \
     *
     * @param path
     */
    public void addTempImage(String path) {
        mRealm.beginTransaction();
        TempImageInfo info = mRealm.createObject(TempImageInfo.class);
        info.setPath(path);
        mRealm.commitTransaction();
    }
   /*

    *//**
     * getBirthday
     *
     * @return String
     *//*
    public String getMobile() {
        return mRequestCodeProfile.getPhone();
    }

    *//**
     * CheckInTime
     *
     * @return Date
     *//*
    public String getGender() {
        return mRequestCodeProfile.getGender();
    }

    *//**
     * getAddress
     *
     * @return getAddress
     *//*
    public String getAddress() {
        return mRequestCodeProfile.getAddress();
    }
*/

    /**
     * Has User Info
     */
    public boolean hasUserInfo() {
        mUserInfo = mRealm.where(UserInfo.class).findFirst();
        return mUserInfo != null && !TextUtils.isEmpty(mUserInfo.getToken());
    }

    /**
     * get List Review
     *
     * @return List<ReviewAllItem>
     */
    /*public List<ReviewAllItem> getReviewAllItemList() {
        return mReviewAllResponse.getListReviews();
    }*/
    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getThrowable() != null) {
            Log.e("onPostResponse", task.getThrowable().toString() + "__" + task.getType());
        }
        if (task.getType() == ApiTaskType.GET_LIST_ALL) {
            if (status == SearchAuth.StatusCodes.SUCCESS) {
              /*  BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();*/
            }

        }
        return mListener.onResponse(task, status);

    }

    @Override
    public boolean onFailureResponse(ApiTask task) {
        Log.e("onFailureResponse", task.getThrowable().toString());
        return false;
    }

    /**
     * Create File Path
     *
     * @param ext String
     * @return String
     */
    private String getRandomPath(String ext) {
        File file;
        long time = System.currentTimeMillis();
        int index = 0;

        do {
            file = new File(mContext.getExternalFilesDir(null), time + "_" + index + "." + ext);
        } while (file.exists());

        return file.getPath();
    }
}

