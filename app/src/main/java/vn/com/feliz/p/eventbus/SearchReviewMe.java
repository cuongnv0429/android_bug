package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-08.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SearchReviewMe {
  private String keyWord;

    public SearchReviewMe(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
