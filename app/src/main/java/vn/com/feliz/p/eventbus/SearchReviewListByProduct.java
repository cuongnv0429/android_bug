package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-10.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SearchReviewListByProduct {
    private String keyWord;

    public SearchReviewListByProduct(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
}
