package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-09.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class HelpfulModle {
    String review_id;
    String type;

    public HelpfulModle(String review_id, String type) {
        this.review_id = review_id;
        this.type = type;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
