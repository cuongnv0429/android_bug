package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2016-12-23.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class CancleGps {
    String screenName;

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public CancleGps(String screenName) {
        this.screenName = screenName;
    }
}
