package vn.com.feliz.p.eventbus;

/**
 * Created by Nguyen Thai Son on 2017-01-02.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SearchReviewAll {
    String keySearch;

    public SearchReviewAll(String keySearch) {
        this.keySearch = keySearch;
    }

    public String getKeySearch() {
        return keySearch;
    }

    public void setKeySearch(String keySearch) {
        this.keySearch = keySearch;
    }
}
