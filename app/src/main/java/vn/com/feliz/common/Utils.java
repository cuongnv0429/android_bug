package vn.com.feliz.common;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Pair;
import android.util.Patterns;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

import vn.com.feliz.R;
import vn.com.feliz.m.CouponAvailableModel;
import vn.com.feliz.m.response.PushResponse;

/**
 * Created by macbook on 4/11/16.
 */
public class Utils {

    private static final String REGEX_EMAIL = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    private static final Pattern PATTERN_EMAIL = Pattern.compile(REGEX_EMAIL, Pattern.CASE_INSENSITIVE);
    private static final int next_show_time = 1800; // 30 minutes

    /**
     * A safe way to get an instance of the Camera object.
     */
    @SuppressWarnings("deprecation")
    public static Pair<Camera, Integer> getCameraInstance(int facingValue) {
        Camera c = null;
        int cameraId = 0;
        int count = Camera.getNumberOfCameras();
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();

        for (int i = 0; i < count; ++i) {
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == facingValue) {
                try {
                    c = Camera.open(i); // attempt to get a Camera instance
                    cameraId = i;
                    break;
                } catch (RuntimeException e) {
                    // Camera is not available (in use or does not exist)
                    e.printStackTrace();
                }
            }
        }

        return new Pair<>(c, cameraId); // returns null if camera is unavailable
    }

    /**
     * Set Camera Display Orientation
     *
     * @param activity Activity
     * @param cameraId int
     * @param camera   Camera
     */
    @SuppressWarnings("deprecation")
    public static void setCameraDisplayOrientation(Activity activity, int cameraId, Camera camera) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);

        Camera.Parameters params = camera.getParameters();
        params.setRotation(result);
        camera.setParameters(params);
    }

    /**
     * Write byte[] to File
     *
     * @param data     bytes
     * @param filePath String
     * @throws IOException
     * @throws FileNotFoundException
     */
    public static void writeBytesIntoFile(byte[] data, String filePath) throws IOException {
        File file = new File(filePath);
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(data);
        fos.close();
    }

    /**
     * has navbar
     *
     * @param context
     * @return
     */
    public static boolean hasNavBar(Context context) {
        Resources resources = context.getResources();
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            return resources.getBoolean(id);
        } else {    // Check for keys
            boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            return !hasMenuKey && !hasBackKey;
        }
    }

    public static int getNavBarHeight(Context c) {
        int result = 0;
        boolean hasMenuKey = ViewConfiguration.get(c).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if (!hasMenuKey && !hasBackKey) {
            //The device has a navigation bar
            Resources resources = c.getResources();

            int orientation = c.getResources().getConfiguration().orientation;
            int resourceId;
            if (isTablet(c)) {
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape", "dimen", "android");
            } else {
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_width", "dimen", "android");
            }

            if (resourceId > 0) {
                return c.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }


    private static boolean isTablet(Context c) {
        return (c.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Write Bitmap into File
     *
     * @param bm       Bitmap
     * @param filePath String
     */
    public static void writeBitmapIntoFile(Bitmap bm, String filePath) throws IOException {
        FileOutputStream out = new FileOutputStream(filePath);
        bm.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
        // PNG is a lossless format, the compression factor (100) is ignored
        out.close();
    }

    /**
     * hideKeyBoardWhenfocus
     */
    public static void hideKeyBoardWhenFocus(EditText mEdt, Context mContext) {
        InputMethodManager im = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        im.hideSoftInputFromWindow(mEdt.getWindowToken(), 0);
    }

    /**
     * gene password
     *
     * @return String
     */
    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(Constant.MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public static void vibarte(Context context) {
        if(context != null) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(Constant.VIBRATE);
        }
    }

    /**
     * Close keyboard when tap outside
     *
     * @param context context
     * @param view    view
     */
    public static void setupKeyboard(final Context context, final View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager)
                            context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    return false;
                }
            });
        }

        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;

            for (int i = 0; i < group.getChildCount(); ++i) {
                setupKeyboard(context, group.getChildAt(i));
            }
        }
    }

    /**
     * gen md5 pass
     *
     * @param s
     * @return
     */
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Check email syntax
     *
     * @param email String
     * @return TRUE if correct
     */
    public static boolean checkEmailSyntax(String email) {
        if (TextUtils.isEmpty(email)) {
            return false;
        }

        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
                && PATTERN_EMAIL.matcher(email).matches();
    }


    /**
     * Create Animation
     *
     * @param v View
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static void animateWobble(View v) {
        ObjectAnimator animator = createBaseWobble(v);
        animator.setFloatValues(-10, 10);
        animator.start();
    }

    /**
     * Create Animation
     *
     * @param v View
     * @return ObjectAnimator
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static ObjectAnimator createBaseWobble(final View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            v.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        ObjectAnimator animator = new ObjectAnimator();
        animator.setDuration(100);
        animator.setRepeatMode(ValueAnimator.REVERSE);
        animator.setRepeatCount(5);
        animator.setPropertyName("rotation");
        animator.setTarget(v);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                v.setLayerType(View.LAYER_TYPE_NONE, null);
                v.setRotation(0);
            }
        });
        return animator;
    }

    /**
     * Align Center Text View
     *
     * @param view View
     */
    public static void alignCenterTextView(View view) {
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            textView.setGravity(Gravity.CENTER);

            ViewGroup.LayoutParams params = textView.getLayoutParams();
            params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            textView.setLayoutParams(params);

            View parent = (View) textView.getParent();
            if (parent instanceof LinearLayout) {
                ((LinearLayout) parent).setGravity(Gravity.CENTER);
            }
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;

            for (int i = 0; i < viewGroup.getChildCount(); ++i) {
                alignCenterTextView(viewGroup.getChildAt(i));
            }
        }
    }

    public static Dialog showInfoDialog(Context context,
                                        @StringRes int title, @StringRes int message,
                                        @StringRes int btnLabel, DialogInterface.OnClickListener btnListener,
                                        DialogInterface.OnShowListener onShowListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setNegativeButton(btnLabel, btnListener);
        builder.setCancelable(false);

        Dialog dialog = builder.create();
        if (title == 0) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        dialog.setOnShowListener(onShowListener);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }

    public static Dialog showConfirmDialog(Context context,
                                           @StringRes int title, @StringRes int message,
                                           @StringRes int leftLabel, DialogInterface.OnClickListener leftListener,
                                           @StringRes int rightLabel, DialogInterface.OnClickListener rightListener,
                                           DialogInterface.OnShowListener onShowListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setNegativeButton(leftLabel, leftListener);
        builder.setPositiveButton(rightLabel, rightListener);
        builder.setCancelable(false);

        Dialog dialog = builder.create();
        if (title == 0) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        dialog.setOnShowListener(onShowListener);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }

    public static Dialog showConfirmDialog(Context context,
                                           @StringRes int title, SpannableString message,
                                           @StringRes int leftLabel, DialogInterface.OnClickListener leftListener,
                                           @StringRes int rightLabel, DialogInterface.OnClickListener rightListener,
                                           DialogInterface.OnShowListener onShowListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (title != 0) {
            builder.setTitle(title);
        }
        builder.setMessage(message);
        builder.setNegativeButton(leftLabel, leftListener);
        builder.setPositiveButton(rightLabel, rightListener);
        builder.setCancelable(false);

        Dialog dialog = builder.create();
        if (title == 0) {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        }
        dialog.setOnShowListener(onShowListener);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        return dialog;
    }

    /**
     * Show Confirm Dialog
     *
     * @param context    Context
     * @param message    String Res ID
     * @param leftBtn    String res ID
     * @param leftClick  Listener
     * @param rightBtn   String res ID
     * @param rightClick Listener
     * @return Dialog
     */
    public static Dialog showConfirmDialog(Context context,
                                           @StringRes int message,
                                           @StringRes int leftBtn, DialogInterface.OnClickListener leftClick,
                                           @StringRes int rightBtn, DialogInterface.OnClickListener rightClick) {

        if(context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message);
            builder.setPositiveButton(rightBtn, rightClick);
            builder.setNegativeButton(leftBtn, leftClick);
            builder.setCancelable(false);

            Dialog dialog = builder.create();
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();

            return dialog;
        } else {
            return null;
        }
    }

    public static Dialog showConfirmDialogScan(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(message);
        Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        return dialog;
    }

    /**
     * Show Retry Dialog
     *
     * @param context Context
     */

    public static Dialog showRetryDialog(Context context,
                                         DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        return showConfirmDialog(context,
                R.string.dialog_change,
                R.string.dialog_retry_yes, okListener,
                R.string.dialog_retry_no, cancelListener);
    }
/*    public static Dialog showRetryDialogScan(Context context,

                                         DialogInterface.OnClickListener cancelListener,
                                         DialogInterface.OnClickListener okListener, String message) {
        return showConfirmDialogScan(context,
               message,
                R.string.dialog_retry_rescan, okListener,
                R.string.dialog_retry_exit, cancelListener);
    }*/

    /**
     * Show Retry Dialog
     *
     * @param context Context
     */

    public static Dialog showRetryDialogLocation(Context context,
                                                 DialogInterface.OnClickListener okListener,
                                                 DialogInterface.OnClickListener cancelListener) {
        return showConfirmDialog(context,
                R.string.dialog_change,
                R.string.dialog_retry_no, cancelListener,
                R.string.dialog_retry_yes, okListener);
    }

    //cuongnv
    public static Dialog showDialogConfirmBuyCoupon(Context context, int messageConfirm,
                                                    DialogInterface.OnClickListener okListener,
                                                    DialogInterface.OnClickListener cancelListener) {
        return showConfirmDialog(context,
                messageConfirm,
                R.string.dialog_retry_no, cancelListener,
                R.string.dialog_retry_yes, okListener);
    }

    public static Dialog showProgressDialog(Context context,
                                            @StringRes int message) {
        if(context != null) {
            ProgressDialog dialog = new ProgressDialog(context);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.setMessage(context.getString(message));
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.show();

            return dialog;
        } else {
            return null;
        }
    }

    /**
     * Show Retry Dialog
     *
     * @param context Context
     */

    public static Dialog showRetryDialogVeryficodeRegister(Context context,
                                                           DialogInterface.OnClickListener okListener,
                                                           DialogInterface.OnClickListener cancelListener) {
        return showConfirmDialog(context,
                R.string.dialog_change_mobile_veryficode,
                R.string.dialog_retry_no, cancelListener,
                R.string.dialog_retry_yes, okListener);
    }

    /**
     * Show Toast Message
     *
     * @param id int
     */
    public static void showMessage(@StringRes int id, Context context) {
        Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
    }

//    /**
//     * Show Retry Dialog
//     *
//     * @param context Context
//     * @param message
//     */
//    public static Dialog showMessageConfirm(Context context, @StringRes String message,
//                                            DialogInterface.OnClickListener okListener) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setPositiveButton(R.string.profile_dialog_ok, okListener);
//        builder.setCancelable(false);
//        builder.setMessage(message);
//
//        Dialog dialog = builder.create();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.setCancelable(false);
//        dialog.show();
//
//        return dialog;
//    }

    public static void showMessageNoTitle(Context context, Object message, DialogInterface.OnClickListener okListener) {
        if(context != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            if (message instanceof String)
                builder.setMessage((String) message);
            else if (message instanceof Integer) {
                builder.setMessage((Integer) message);
            }
            builder.setCancelable(false);
            builder.setPositiveButton(context.getResources().getString(android.R.string.ok), okListener);
            AlertDialog dialog = builder.create();
            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * check nextwork
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void saveAds(Context context, String adsId) {//adsId == null or "" then behave as remove
        Log.e(">>>>> Save ads", adsId);
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Gson g = new Gson();
        String old = preferences.getString("data", "");
        ArrayList<String> idList = new ArrayList<>();
        if (!TextUtils.isEmpty(old)) {
            idList = g.fromJson(old, new TypeToken<ArrayList<String>>() {
            }.getType());
        }
        if (TextUtils.isEmpty(adsId)) {
            if (idList.size() > 0) idList.remove(0);
        } else {
            idList.add(adsId);
        }
        editor.putString("data", g.toJson(idList));
        editor.apply();
    }

    public static void deleteAds(Context context, String ads_id) {
        if(ads_id == null) {
            saveAds(context, "");
        } else {
            SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            Gson g = new Gson();
            String old = preferences.getString("data", "");
            ArrayList<String> idList = new ArrayList<>();
            if (!TextUtils.isEmpty(old)) {
                idList = g.fromJson(old, new TypeToken<ArrayList<String>>() {
                }.getType());
            }

            for (int i = 0; i < idList.size(); i++) {
                PushResponse pushResponse = new Gson().fromJson(idList.get(i), PushResponse.class);
                if (pushResponse != null) {
                    if (pushResponse.getAds_id().equals(ads_id)) {
                        idList.remove(i);
                        break;
                    }
                }
            }

            editor.putString("data", g.toJson(idList));
            editor.apply();
        }
    }

    public static void updateNextShowTimeAds(Context context, String ads_id) {
        if(ads_id != null) {
            SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            Gson g = new Gson();
            String old = preferences.getString("data", "");
            ArrayList<String> idList = new ArrayList<>();
            if (!TextUtils.isEmpty(old)) {
                idList = g.fromJson(old, new TypeToken<ArrayList<String>>() {}.getType());
            }

            for (int i = 0; i < idList.size(); i++) {
                PushResponse pushResponse = new Gson().fromJson(idList.get(i), PushResponse.class);
                if (pushResponse != null) {
                    if (pushResponse.getAds_id().equals(ads_id)) {
                        pushResponse.setNext_show_time((System.currentTimeMillis() / 1000 + next_show_time) + "");
                        idList.set(i, g.toJson(pushResponse));
                        break;
                    }
                }
            }

            editor.putString("data", g.toJson(idList));
            editor.apply();
        }
    }

    public static void setLastGetAds(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong("last_get", System.currentTimeMillis());
        editor.apply();
    }

    public static long getLastGetAds(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        return preferences.getLong("last_get", 0);
    }

    public static PushResponse getAds(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        Gson g = new Gson();
        String old = preferences.getString("data", "");
        Log.e("GET ADS", ">>"+old);
        ArrayList<String> idList = new ArrayList<>();
        if (!TextUtils.isEmpty(old)) {
            idList = g.fromJson(old, new TypeToken<ArrayList<String>>() {}.getType());
        }
        if(idList.size() > 0) {
            return new Gson().fromJson(idList.get(0), PushResponse.class);
        }
        return null;
    }

    public static void clearAds(Context context) {//adsId == null or "" then behave as remove
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("data", "");
        editor.apply();
    }

    public static void saveToken(Context context, String token) {
        SharedPreferences.Editor editor = context.getSharedPreferences("token", Context.MODE_PRIVATE).edit();
        editor.putString("token", token);
        editor.apply();
    }

    public static boolean getLockScreenIsOpen(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE);
        return preferences.getBoolean("status", false);
    }
    public static void setLockScreenIsOpen(Context context, boolean status) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Constant.LOCKSCREEN_ADS, Context.MODE_PRIVATE).edit();
        editor.putBoolean("status", status);
        editor.apply();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);
        return preferences.getString("token", "");
    }

    public static void saveDeviceId(Context context, String deviceId) {
        SharedPreferences.Editor editor = context.getSharedPreferences("device_id", Context.MODE_PRIVATE).edit();
        editor.putString("device_id", deviceId);
        editor.apply();
    }

    public static String getDeviceId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("device_id", Context.MODE_PRIVATE);
        return preferences.getString("device_id", "");
    }

    public static void saveLockScreenStatus(Context context, boolean status) {
        SharedPreferences.Editor editor = context.getSharedPreferences("lockscreen_status", Context.MODE_PRIVATE).edit();
        editor.putBoolean("lockscreen_status", status);
        editor.apply();
    }

    public static void saveTimesEnterCouponDetail(Context context, boolean stop) {
        SharedPreferences.Editor editor = context.getSharedPreferences("times_enter_coupon_detail", Context.MODE_PRIVATE).edit();
        editor.putBoolean("times_enter_coupon_detail", stop);
        editor.apply();
    }

    public static boolean getTimesEnterCouponDetail(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("times_enter_coupon_detail", Context.MODE_PRIVATE);
        return preferences.getBoolean("times_enter_coupon_detail", false);
    }

    public static boolean getLockScreenStatus(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("lockscreen_status", Context.MODE_PRIVATE);
        return preferences.getBoolean("lockscreen_status", true);
    }

    public static void saveUid(Context context, String uid) {
        SharedPreferences.Editor editor = context.getSharedPreferences("uid", Context.MODE_PRIVATE).edit();
        editor.putString("uid", uid);
        editor.apply();
    }

    public static String getUid(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("uid", Context.MODE_PRIVATE);
        return preferences.getString("uid", "");
    }

    public static void showNoInternetMessage(Context context, DialogInterface.OnClickListener okListener) {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setMessage("Have a problem with internet connection, please check your internet and try again!");
        builder.setCancelable(false);
        builder.setNegativeButton(context.getResources().getString(android.R.string.ok), okListener);
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void saveCouponAvailable(Context context, CouponAvailableModel.CouponAvailable couponAvailable) {
        SharedPreferences.Editor editor = context.getSharedPreferences("coupon_available", Context.MODE_PRIVATE).edit();
        editor.putString("coupon_available", new Gson().toJson(couponAvailable));
        editor.apply();
    }

    public static CouponAvailableModel.CouponAvailable getCouponAvailable(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("coupon_available", Context.MODE_PRIVATE);
        return new Gson().fromJson(preferences.getString("coupon_available", ""), CouponAvailableModel.CouponAvailable.class);
    }

    public static void saveReferreCode(Context context, String code) {
        SharedPreferences.Editor editor = context.getSharedPreferences("referre_code", Context.MODE_PRIVATE).edit();
        editor.putString("referre_code", code);
        editor.apply();
    }
    public static String getReferreCode(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("referre_code", Context.MODE_PRIVATE);
        return preferences.getString("referre_code", "");
    }

    public static void saveOverlayCoins(Context context, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("overlay_coins", Context.MODE_PRIVATE).edit();
        editor.putBoolean("overlay_coins", value);
        editor.apply();
    }

    public static Boolean getOverlayCoins(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("overlay_coins", Context.MODE_PRIVATE);
        return preferences.getBoolean("overlay_coins", false);
    }

    public static void saveOverlaySetting(Context context, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("overlay_setting", Context.MODE_PRIVATE).edit();
        editor.putBoolean("overlay_setting", value);
        editor.apply();
    }

    public static Boolean getOverlaySetting(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("overlay_setting", Context.MODE_PRIVATE);
        return preferences.getBoolean("overlay_setting", false);
    }

    public static void saveOverlayReview(Context context, boolean value) {
        SharedPreferences.Editor editor = context.getSharedPreferences("overlay_review", Context.MODE_PRIVATE).edit();
        editor.putBoolean("overlay_review", value);
        editor.apply();
    }

    public static Boolean getOverlayReview(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("overlay_review", Context.MODE_PRIVATE);
        return preferences.getBoolean("overlay_review", false);
    }

    public static String convertLongTimeToString(int timeMs) {
        if (timeMs <= 0 || timeMs >= 24 * 60 * 60 * 1000) {
            return "00:00";
        }
        int totalSeconds = timeMs / 1000;
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    /**
     * Hide soft keyboard.
     *
     * @param activity the activity
     */
    public static void hideSoftKeyboard(Activity activity) {
        if (activity == null)
            return;
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager == null || activity.getCurrentFocus() == null
                || activity.getCurrentFocus().getWindowToken() == null)
            return;

        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void hideSoftKeyboard(Activity activity, EditText editText) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromInputMethod(editText.getWindowToken(), 0);
    }

    /**
     * Hide soft keyboard.
     *
     * @param activity the activity
     */
    public static void ShowKeyboard(Activity activity) {
        if (activity == null)
            return;
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_UNSPECIFIED);

    }

    /**
     * Drop image
     *
     * @param uri Uri
     */
    private void getDropboxIMGSize(Uri uri) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(new File(uri.getPath()).getAbsolutePath(), options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

    }

    /**
     * crop image To Square
     *
     * @param bitmap
     * @return
     */
    public static Bitmap cropToSquare(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int newWidth = (height > width) ? width : height;
        int newHeight = (height > width) ? height - (height - width) : height;
        int cropW = (width - height) / 2;
        cropW = (cropW < 0) ? 0 : cropW;
        int cropH = (height - width) / 2;
        cropH = (cropH < 0) ? 0 : cropH;
        Bitmap cropImg = Bitmap.createBitmap(bitmap, cropW, cropH, newWidth, newHeight);

        return cropImg;
    }

    /**
     * get bitmap from url
     *
     * @param src String
     * @return myBitmap
     */
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    /**
     * @param context showLoadding
     */
    public static ProgressDialog showLoading(Context context) {
        ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.element_progress_splash);
        if(progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        progressDialog.show();
        return progressDialog;
    }

    public static void hideLoading(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /**
     * disable back Button
     *
     * @param rootView View
     */
    public static void handlerBackButton(View rootView) {
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        return true;
                    }
                }
                return false;
            }
        });
    }

    /**
     * animate wrong input
     *
     * @param mImg ImageView
     */
    public static void notifyInput(ImageView mImg) {
        //  mImg.setImageResource(R.drawable.bg_sign_icon_email_error);
        Utils.animateWobble(mImg);
    }

    public static void addFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag, AppCompatActivity mActivity) {
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .add(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    public static void replaceFragment(@IdRes int containerViewId,
                                       @NonNull Fragment fragment,
                                       String fragmentTag,
                                       @Nullable String backStackStateName, AppCompatActivity mActivity) {
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                // .addToBackStack(backStackStateName)
                .commit();
    }

    /**
     * snackBar with x
     *
     * @param snackbar
     * @param errParams
     * @param activity
     */
    public static void showMessageTopbar(final TSnackbar snackbar, String errParams, Activity activity) {
        String err = "gibi" + "\n" + "" + errParams;

        if(activity != null) {

            TSnackbar.SnackbarLayout layout = (TSnackbar.SnackbarLayout) snackbar.getView();

            // Hide the text
            TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
            textView.setVisibility(View.INVISIBLE);
            textView.setHeight(80);
            // Inflate our custom view
            View snackView = activity.getLayoutInflater().inflate(R.layout.snack_layout, null);
            // Configure the view

            TextView textViewTop = (TextView) snackView.findViewById(R.id.tvSnack);
            textViewTop.setText(err);
            textViewTop.setTextColor(Color.WHITE);
            ImageView imgHideTopbar = (ImageView) snackView.findViewById(R.id.img_delete_ic);
            imgHideTopbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    snackbar.dismiss();
                }
            });
            // Add the view to the Snackbar's layout
            layout.addView(snackView, 0);
            if (errParams != null && errParams != "") {
                snackbar.show();
            }
        }
    }

    public static void showMessageTopbarNextWork(final TSnackbar snackbar, String errParams, Activity activity) {
        String err = "gibi" + "\n" + "" + errParams;
        //error
        //setup snackBar
   /* snackbar = TSnackbar
            .make(view, " " + err, TSnackbar.LENGTH_INDEFINITE);*/
        TSnackbar.SnackbarLayout layout = (TSnackbar.SnackbarLayout) snackbar.getView();

        // Hide the text
        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        textView.setHeight(80);
        // Inflate our custom view
        View snackView = activity.getLayoutInflater().inflate(R.layout.snack_layout, null);
        // Configure the view

        TextView textViewTop = (TextView) snackView.findViewById(R.id.tvSnack);
        textViewTop.setText(err);
        textViewTop.setTextColor(Color.WHITE);
        ImageView imgHideTopbar = (ImageView) snackView.findViewById(R.id.img_delete_ic);
        imgHideTopbar.setVisibility(View.INVISIBLE);
      /*  imgHideTopbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        */
        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
        if (errParams != null) {
            snackbar.show();
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static boolean isKeyboardShown(View rootView) {
    /* 128dp = 32dp * 4, minimum button height 32dp and generic 4 rows soft keyboard */
        final int SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD = 128;

        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
    /* heightDiff = rootView height - status bar height (r.top) - visible frame height (r.bottom - r.top) */
        int heightDiff = rootView.getBottom() - r.bottom;
    /* Threshold size: dp to pixels, multiply with display density */
        boolean isKeyboardShown = heightDiff > SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD * dm.density;


        return isKeyboardShown;
    }

    /**
     * check location
     *
     * @param context
     * @return
     */
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    public static String generateUUID(Context context) {
        String uniqueId;
        try {
            uniqueId = getDeviceId(context);
            if(uniqueId == null || uniqueId.equals("")) {

                uniqueId = UUID.randomUUID().toString();

//                TelephonyManager teleManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
//                String tmSerial = teleManager.getSimSerialNumber();
//                String tmDeviceId = teleManager.getDeviceId();
//                String androidId = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
//                if (tmSerial == null) tmSerial = "1";
//                if (tmDeviceId == null) tmDeviceId = "1";
//                if (androidId == null) androidId = "1";
//                UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDeviceId.hashCode() << 32) | tmSerial.hashCode());
//                uniqueId = deviceUuid.toString();

                saveDeviceId(context, uniqueId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            uniqueId = md5(Math.random() + "" + System.currentTimeMillis());
            saveDeviceId(context, uniqueId);
        }
        return uniqueId;
    }

    /**
     * show dialog confirm enable gps
     *
     * @param activity
     */
    public static void displayPromptForEnablingGPS(final Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = activity.getString(R.string.message_location);

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(activity.getString(R.string.message_action_gb_later)); // De sau
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(activity.getString(R.string.message_action_gb_config)); // Vao config
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(activity.getString(R.string.message_location_gb));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(action));
                dialog.dismiss();
            }
        });

//        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
//        final String message = activity.getString(R.string.message_location);
//
//        builder.setMessage(message)
//                .setPositiveButton("OK",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface d, int id) {
//                                activity.startActivity(new Intent(action));
//                                d.dismiss();
//                            }
//                        })
//                .setNegativeButton("Cancel",
//                        new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface d, int id) {
//                                d.cancel();
//                            }
//                        });
//        builder.create().show();
    }

    public static void displayPromptForCheckGPS(final Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(activity.getString(R.string.message_action_gb_skip)); // De sau
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(activity.getString(R.string.message_action_gb_retry)); // Vao config
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(activity.getString(R.string.message_location_gb_latlng));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
    }

    public static float getDistance(Location loc1, Location loc2) {
        Location locationA = new Location("point A");

        locationA.setLatitude(loc1.getLatitude());
        locationA.setLongitude(loc1.getLongitude());

        Location locationB = new Location("point B");

        locationB.setLatitude(loc2.getLatitude());
        locationB.setLongitude(loc2.getLongitude());

        return locationA.distanceTo(locationB);
    }
    public static String getDistance2(Location loc1, double lat2, double lng2) {
        Location locationA = new Location("point A");

        locationA.setLatitude(loc1.getLatitude());
        locationA.setLongitude(loc1.getLongitude());

        Location locationB = new Location("point B");

        locationB.setLatitude(lat2);
        locationB.setLongitude(lng2);

        float d = locationA.distanceTo(locationB);
        if(d < 1000) {
            return Math.round(d)+"m";
        } else {
            return (Math.round(d/10)/100)+"km";
        }
    }
}