package vn.com.feliz.common;

import android.Manifest;
import android.annotation.SuppressLint;
/**
 * Created by macbook on 4/11/16.
 */
public class Constant {

    public static final boolean DEBUG = true;

    // For Dev
    private static final String API_URL_DEBUG = "http://gb.itvsoft.asia/api/";
    // For Live
    private static final String API_URL_RELEASE = "http://api.goldbeetle.vn/";

    // For Firebase Dev
    private static final String API_FIREBASE_URL_DEBUG = "https://gibi-dev.firebaseio.com/";
    // For Firebase Live
    private static final String API_FIREBASE_URL_RELEASE = "https://gibi-fcdb4.firebaseio.com/";

    // Switch api mode
    public static final String DOMAIN = API_URL_RELEASE;
    public static final String FIREBASE_URL = API_FIREBASE_URL_RELEASE;
    public static final float CAMERA_EXTRA = 1.5f;
    public static final int CAMERA_MAX_DURATION = 30000; //miliseconds
    public static final int CAMERA_VIDEO_W_DEFAULT = 800;
    public static final int CAMERA_VIDEO_H_DEFAULT = 480;
    public static final int CAMERA_AUDIO_ENCODING_BIT_RATE = 16;
    public static final int CAMERA_AUDIO_SAMPLING_RATE = 44100;
    public static final int CAMERA_VIDEO_ENCODING_BIT_RATE = 10000000;
    public static final int CAMERA_VIDEO_FRAME_RATE = 30;
    public static final int IMAGE_FIRST = 0;

    public static final int CONNECTION_TIMEOUT = 15000; // Using miliseconds

    public static final int REQUEST_PERMISSIONS_CODE = 1;
    public static final int REQUEST_PERMISSIONS_CAMERA = 2;
    public static final int REQUEST_PERMISSIONS_LOCATION = 3;
    public static final int REQUEST_PERMISSIONS_UUID = 4;
    @SuppressLint("InlinedApi")
    public static final String[] PERMISSIONS = {
            Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final String[] PERMISSIONSCAMERA = {
            Manifest.permission.CAMERA};
    //SplashScreen Constanst
    public static final int SPLASH_TIME_OUT = 500;
    public static final int SPLASH_TIME_OUT_SUBMIT = 500;
    public static final String PREF_EMAIL = "PREF_EMAIL";
    public static final String PREF_PASSWORD = "PREF_PASSWORD";
    public static final int SCHEMA_VERSION_4 = 4;
    //Register Screen
    public static final int GB_IC_SHOW = 300;
    public static final int GB_IC_HIDE = 300;

    // FONT
    public static final String FONT_BOLD = "SF-UI-Display-Bold.otf";
    public static final String FONT_LIGHT = "SF-UI-Display-Light.otf";
    public static final String FONT_MEDIUM = "SF-UI-Display-Medium.otf";
    public static final String FONT_REGULAR = "SF-UI-Display-Regular.otf";
    public static final String FONT_THIN = "SF-UI-Display-Thin.otf";
    public static final String FONT_ITALIC = "SF-UI-Text-Italic.otf";
    public static final String FONT_VARI = "12-10071-VAGRB.TTF";
    //    public static final String HELVETICA = "HelveticaNeueUltraLight.ttf";
    public static final String ARIA = "Aria.ttf";

    //FACEBOOK SDK
    public static final String USER_FRIENDS = "user_friends";
    public static final String PUBLIC_PROFILE = "public_profile";
    public static final String EMAIL = "email";
    public static final String USER_BIRTHDAY = "user_birthday";
    public static final String NAME = "name";
    public static final String ID = "id";
    public static final String FIELDS = "fields";
    public static final String GENDER = "gender";
    public static final String BIRTHDAY = "birthday";
    //adapter InterRest
    public static final int TYPE_HEADER = 1000;
    public static final int TYPE_CONTENT_INTEREST = 1002;
    public static final int TYPE_CONTENT_TODAY_QUESTIONS = 1003;
    //RUN APP FIRST TIME
    public static final String FIRST_RUN = "FIRST_RUN";
    public static final String MYPREFSFILE = "MyPrefsFile";
    //Search Place
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    public static final String FACEBOOK_OBJECT = "FaceBookObject";
    //CHANGE PROFILE
    public static final String MYPREFSFILE_MOBILE_CHANGE = "MyPrefsFile_mobile";
    public static final String MYPREFSFILE_ACCESSTOKEN = "MyPrefsFile_mobile";
    public static final String IS_CHANGE = "IS_CHANGE";
    //KEYBOARD
    public static final String KEY_NULL = "";
    //COUPONS
    public static final int LIMIT_COUPONS = 5;
    public static final String F_ARG_COUPON_ID = "F_ARG_COUPON_ID";
    public static final String F_ARG_SUB_COUPON_ID = "coupon_sub_id ";
    public static final String F_ARG_COUPON_TOTAL = "F_ARG_COUPON_TOTAL";
    public static final String F_ARG_COUPON_ID_TERM = "F_ARG_COUPON_ID_TERM";
    public static final String F_ARG_COUPON_BARCODE = "F_ARG_COUPON_BARCODE";
    //REQUEST CODE
    public static final String TYPE_REQUEST = "1";
    //CHANGE PROFILE
    public static final String PROGRESS_STATUS = "2";
    //VIBRATE EVENT
    public static final int VIBRATE = 500;
    public static final int MAX_LENGTH = 4;
    public static final int MAX_LENGTH_PHONE = 15;
    public static final int MAX_LENGTH_FB = 3;
    //LOCK EVENT TIME
    public static final int LOCK_EVENT_TIME = 200;
    //INTEREST
    public static final String CHECKED_INTEREST = "1";

    //lockscreen
    public static final String ACTION = "action";
    public static final String CODE_SUCCESS = "200";
    public static final String CODE_ERROR_EXCEPTION = "2000";
    public static final int NOTIFICATION_RADIUS = 500;
    public static final int ACTION_GET_ADS_LIST = 12;
    public static final int ACTION_GET_ADS = 1;
    public static final int ACTION_GET_SYSTEM_NOTIFICATION_LIST = 2;
    public static final int ACTION_SUBMIT_ADS = 3;
    public static final int ACTION_REMOVE_SYSTEM_NOTIFICATION = 4;
    public static final int ACTION_CLOSE_SYSTEM_NOTIFICATION = 100;
    public static final int ACTION_GET_SYSTEM_NOTIFICATION_POSTED = 5;
    public static final int ACTION_GET_COUPON_DETAIL = 6;
    public static final int ACTION_SUBMIT_CONFIRM_PRODUCT = 7;
    public static final int ACTION_GET_SHOP_LIST = 8;
    public static final int ACTION_GET_COUPON_AVAILABLE_LIST = 9;
    public static final int ACTION_GET_COUPON_LIST = 10;
    public static final int ACTION_HAVE_ADS = 11;
    public static final String COUPON_STATUS_NEW = "1";
    public static final String COUPON_STATUS_NOT_EXCHANGE_YET = "2";
    public static final String COUPON_STATUS_EXPIRED = "3";
    public static final String COUPON_STATUS_EXCHANGED = "4";
    public static final String COUPON_STATUS_CAN_BUY = "5";
    public static final String DATE_FORMAT = "dd/MM/yy";
    //LOGIN
    public static final String NEW_LOGIN = "0";
    public static final String OLD_LOGIN = "1";
    //SEARCH FRAGMENT
    public static final int TIME_DELAY_HIDE_KEY_BOARD = 300;
    //REVIEW SCREEN
    public static final int REVIEWS_SCREEN_TAB_ONE = 0;
    public static final int REVIEWS_SCREEN_TAB_TWO = 1;
    public static final int REVIEWS_SCREEN_TAB_THREE = 2;
    public static final String TYPE_REVIEWS = "all";
    ;
    public static final String F_ARG_PRODUCT_IMG = "F_ARG_PRODUCT_IMG";
    public static final String F_ARG_PRODUCT_NAME = "F_ARG_PRODUCT_NAME";
    public static final String F_ARG_PRODUCT_CONTENT = "F_ARG_PRODUCT_CONTENT";
    public static final String F_ARG_PRODUCT_RATING = "F_ARG_PRODUCT_RATING";
    public static final String F_ARG_PRODUCT_RATING_5 = "F_ARG_PRODUCT_RATING_5";
    public static final String F_ARG_PRODUCT_RATING_4 = "F_ARG_PRODUCT_RATING_4";
    public static final String F_ARG_PRODUCT_RATING_3 = "F_ARG_PRODUCT_RATING_3";
    public static final String F_ARG_PRODUCT_RATING_2 = "F_ARG_PRODUCT_RATING_2";
    public static final String F_ARG_PRODUCT_RATING_1 = "F_ARG_PRODUCT_RATING_1";
    public static final String F_ARG_PRODUCT_REVIEWS = "F_ARG_PRODUCT_REVIEWS";
    public static final String F_ARG_PRODUCT_ID = "F_ARG_PRODUCT_ID";
    public static final String F_ARG_PRODUCT_BARCODE = "F_ARG_PRODUCT_ID";
    public static final String F_ARG_REFER_FROM_COUPON = "F_ARG_REFER_FROM_COUPON";
    public static final String F_ARG_USER_ID= "F_ARG_USER_ID";
    public static final String F_ARG_USER_NAME= "F_ARG_USER_NAME";
    public static final String F_ARG_COUNT_PRODUCT_BY_USER= "F_ARG_COUNT_PRODUCT_BY_USER";

    //Gps
    public static final String SPlASH_NAME = "splash";
    public static final String WELLCOME = "WELLCOME";
    public static final String MYPREFSFILE_WELLCOME = "MyPrefsFilewellcome";
    //COUPON
    public static final String MYPREFSFILE_GUIDE_CHANGE = "MYPREFSFILE_GUIDE_CHANGE";
    public static final String CHANGE_COUPON = "CHANGE_COUPON";
    public static final int MAX_ENTER_COUPON_DETAIL = 3;
    //Profile
    public static final String SIZE_ADS_LITTLE = "0";
    public static final String SIZE_ADS_MORE = "1";
    public static final String allow_ads_OFF = "0";
    public static final String allow_ads_ON = "1";
    //CREATE NEW REVIEW
    public static final String F_ARG_PRODUCT_NAME_REVIEWS_CREATE = "F_ARG_PRODUCT_NAME_REVIEWS_CREATE";
    public static final String F_ARG__REVIEWS_RATING_VALUE_CREATE = "F_ARG__REVIEWS_RATING_VALUE_CREATE";
    public static final String F_ARG_COMMENT_CREATE = "F_ARG_COMMENT_CREATE";
    public static final String F_ARG_BARCODE_REVIEW_CREATE = "F_ARG_BARCODE_REVIEW_CREATE";
    public static final String F_ARG_MEDIA_URL = "F_ARG_MEDIA_URL";
    public static final String F_ARG_IS_HAS_IMAGE = "F_ARG_IS_HAS_IMAGE";
    public static final String F_ARG_IS_HAS_IMAGE_VALUE = "1";
    public static final String F_ARG_IS_HAS_IMAGE_NO_VALUE = "0";

    public static final String CONNECT_STATUS = "connect_status";
    public static final int CANNOT_CONNECT_SERVER = 0;
    //cuongnv
    public static final String COUPON_IS_REVIEW_DO_NOT_OR_ALREADY = "2";
    public static final String COUPON_IS_REVIEW = "1";
    public static final String COUPON_IS_REVIEW_ALREADY = "3";
    public static final int STATUS_SUCCESS = 200;
    public static final String TYPE_IMAGE_CAPTURE = "2";

    public static final String FIREBASE_EMAIL_PREFIX = "@gmail.com";

    public static final int MAX_LENGTH_CONTENT_REVIEW = 90;
    public static final String LOCKSCREEN_ADS = "lockscreen_ads";
    public static final String CATEGORY_ANALYTISC_PUSH_NOTIFICATIONS = "PushNotifications";
    public static final String EVENT_ANALYTISC_NOTIFICATION_RECIEVED = "NotificationRecieved";
    public static final String EVENT_ANALYTISC_NOTIFICATION_CLICK = "NotificationClick";

    public static final String CATEGORY_ANALYTISC_COUPONS = "Coupons";
    public static final String EVENT_ANALYTISC_COUPON_DETAIl = "CouponDetail";

}