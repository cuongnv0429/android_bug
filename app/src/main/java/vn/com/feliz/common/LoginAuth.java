package vn.com.feliz.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.androidadvance.topsnackbar.TSnackbar;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.util.Arrays;

import vn.com.feliz.p.LoginPresenter;
import vn.com.feliz.v.activity.LoginActivity;
import vn.com.feliz.v.widget.PhoneEditText;
import vn.com.feliz.m.LoginModel;
import vn.com.feliz.BuildConfig;

/**
 * Created by user on 1/16/17.
 */

public class LoginAuth {

    //facebook
    private LoginButton mLoginButtonFacebook;
    private static LoginManager mLoginManagerFb;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;
    private ProfileTracker profileTracker;
    public static CallbackManager callbackManager;
    private String mFBAccessToken;

    private FirebaseAuth mFireBaseAuth;

    private LoginPresenter mLoginPresenter;

    private RelativeLayout mLoginView;

    private ProgressDialog mProgressDialog;
    private TSnackbar snackbar;
    private NetworkStateReceiver mNetworkStateReceiver;

    private LinearLayout loginGBLogoWP;
    private PhoneEditText mPhoneNumber;
    private Button mButtonPhoneConfirm;
    private String mPhoneNumberText;
    private LoginModel mLoginModel;

    private boolean isSideTop = false;

    private LoginActivity.LoginCallback mLoginCallback;

    private Context mContext;

    public LoginAuth(Context context) {
        FacebookSdk.sdkInitialize(context);
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        mContext = context;
        mLoginModel = new LoginModel();
    }

    public void nextStepLoginFB(LoginActivity.LoginCallback loginCallback) {

        mLoginCallback = loginCallback;

        callbackManager = CallbackManager.Factory.create();
        mLoginManagerFb = LoginManager.getInstance();

        try {

            mLoginManagerFb.registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(final LoginResult loginResult) {
                            // Login facebook ok, get user info
                            mFBAccessToken = loginResult.getAccessToken().getToken();
                            mLoginModel.setAccessToken(mFBAccessToken);
                            GraphRequest request = GraphRequest.newMeRequest(
                                    loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response) {
                                            Log.v("LoginActivity", response.toString());

                                            String fbID = "",
                                                    name = "",
                                                    email = "",
                                                    gender = "",
                                                    birthday = "",
                                                    address = "",
                                                    avatar = "";

                                            try {
                                                String id = null;
                                                id = object.getString("id");
                                                if (id != null) {
                                                    fbID = id;
                                                } else {
                                                    Profile profile = Profile.getCurrentProfile();
                                                    id = profile.getId();
                                                    fbID = id;
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            try {
                                                name = object.getString(Constant.NAME);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                email = object.getString(Constant.EMAIL);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                gender = object.getString(Constant.GENDER);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                birthday = object.getString(Constant.BIRTHDAY);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                JSONObject locationObj = object.getJSONObject("location");
                                                address = locationObj.getString("name");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                JSONObject pictureObj = object.getJSONObject("picture");
                                                JSONObject data = pictureObj.getJSONObject("data");
                                                avatar = data.getString("url");
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }

                                            mLoginModel.setFirebaseUID(fbID);
                                            mLoginModel.setFbID(fbID);
                                            mLoginModel.setName(name);
                                            mLoginModel.setEmail(email);
                                            mLoginModel.setGender(gender);
                                            mLoginModel.setBirthday(birthday);
                                            mLoginModel.setAvatar(avatar);
                                            mLoginModel.setAddress(address);

                                            Log.e("fbx", "1");

                                            mLoginCallback.processFB(mLoginModel);

                                        }
                                    });
                            Bundle parameters = new Bundle();
                            parameters.putString(Constant.FIELDS, "id,name,email,gender,birthday,picture,location");
                            request.setParameters(parameters);
                            request.executeAsync();
                        }

                        @Override
                        public void onCancel() {
                            // App code
                            Log.e("FBLOGIN", "Cancel");
                            mLoginCallback.cancelFB();
                        }

                        @Override
                        public void onError(FacebookException exception) {
                            // App code
                            Log.e("FBLOGIN", exception.toString());
                            mLoginCallback.fbLoginOther();
                        }
                    });

            mLoginManagerFb.logInWithReadPermissions((Activity) mContext, Arrays.asList("public_profile"));
        } catch (Exception e) {
            e.printStackTrace();
            //call api error
            mLoginCallback.errorFB(e.getMessage().toString());
        }
    }

}
