package vn.com.feliz.common.ResizeAnimation;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created by user on 1/15/17.
 */

public class ResizeAnimation extends Animation {
    final float targetHeight;
    View view;
    float startHeight;
    boolean fadeIn;

    public ResizeAnimation(View view, float targetHeight, float startHeight, boolean fadeIn) {
        this.view = view;
        this.targetHeight = targetHeight;
        this.startHeight = startHeight;
        this.fadeIn = fadeIn;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        float w = (startHeight+(targetHeight - startHeight) * interpolatedTime);
        //to support decent animation, change new heigt as Nico S. recommended in comments
        //int newHeight = (int) (startHeight+(targetHeight - startHeight) * interpolatedTime);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, 0);
        params.weight = w;

        if(w == targetHeight && fadeIn) {
            view.setVisibility(View.GONE);
        }

        view.setLayoutParams(params);
        view.requestLayout();
    }

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}