package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.eventbus.BackListAll;
import vn.com.feliz.p.eventbus.CreateReview;
import vn.com.feliz.p.eventbus.RefreshListFriend;
import vn.com.feliz.p.eventbus.RefreshListMe;
import vn.com.feliz.p.eventbus.SearchReviewAll;
import vn.com.feliz.p.eventbus.SearchReviewFriend;
import vn.com.feliz.p.eventbus.SearchReviewMe;
import vn.com.feliz.p.eventbus.SetTitleMessage;
import vn.com.feliz.v.adapter.PagerReviewsAdapter;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.v.widget.AutoResizeTextView;
import vn.com.feliz.v.widget.CustomViewPager;

import static vn.com.feliz.R.id.img_x_review_all;
import static vn.com.feliz.v.fragment.CouponsDetailScreen.mMediaPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsScreen extends BaseFragment implements OnPostResponseListener, OnMenuItemSelected, View.OnClickListener{

    public static TabLayout mTabLayout;
    @BindView(R.id.reviews_pager)
    CustomViewPager mViewPager;
    //@BindView(R.id.main_top_bar_title_review)
    public static TextView mTvTitleBar;
    @BindView(R.id.edt_search_reviews)
    EditText mEdtSearchBottom;
    public static TextView tvTotal;
    public static TextView tvTotalFriend;
    public static TextView tvTotalMe;
    private CountDownTimer waitTimer;
    public static ImageView imgBackReview;
    @BindView(R.id.line_below_tab_bar)
    View mViewLine;
    @BindView(img_x_review_all)
    ImageButton imgX;
    @BindView(R.id.linear_search_reviews)
    LinearLayout linearSearchReviews;
    @BindView(R.id.img_x_bar_top)
    ImageButton imgXbarTop;
    @BindView(R.id.linear_search_bar_top)
    LinearLayout linearSearchBarTop;
    @BindView(R.id.img_back_search)
    ImageButton imgBackSearch;
    public static EditText mEdtSearchBarTop;

    public static ImageView imgBackMain;

    public static RelativeLayout mTopBarReview;
    public static int mTabPos;

    public static RelativeLayout mRlNewReview;

    public static RelativeLayout mRlViewpager;
    private boolean isShowKeyBoard = false;
    View rootView;

    public ReviewsScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        // Set Title
        EventBus.getDefault().post(new SetTitleMessage(getString(R.string.reviews_Screen)));
        EventBus.getDefault().post(this);
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_reviews_screen, container, false);
            // Bind Resources
            mUnbinder = ButterKnife.bind(this, rootView);

            mTvTitleBar = (TextView) rootView.findViewById(R.id.main_top_bar_title_review);
            mTabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
            mTopBarReview = (RelativeLayout) rootView.findViewById(R.id.rl_top_bar_reviews_main);
            mEdtSearchBarTop = (EditText) rootView.findViewById(R.id.edt_search_bar_top);
            imgBackReview = (ImageView) rootView.findViewById(R.id.imgBackReview);
            imgBackMain = (ImageView) rootView.findViewById(R.id.imgBackReviewMain);

            mRlViewpager = (RelativeLayout) rootView.findViewById(R.id.rlViewpager);
            mRlNewReview = (RelativeLayout) rootView.findViewById(R.id.rlNewReview);
            //setup tavlayout
            mTabLayout.addTab(mTabLayout.newTab().setText("Tab 1"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Tab 2"));
            mTabLayout.addTab(mTabLayout.newTab().setText("Tab 3"));
            mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
            mTabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.black)));
            mTabLayout.setSelectedTabIndicatorColor(Color.parseColor("#fff4511e"));
            imgX.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEdtSearchBottom.setText("");
                }
            });

            imgXbarTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEdtSearchBarTop.setText("");
                }
            });
            imgBackSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideSoftKeyboard(mActivity);
                }
            });
            //setup view pager
            final PagerReviewsAdapter adapter = new PagerReviewsAdapter
                    (mActivity.getSupportFragmentManager(), mTabLayout.getTabCount());
            mViewPager.setAdapter(adapter);
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setCurrentItem(0);
            mTabPos = 1;
            mViewPager.setPagingEnabled(false);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
            mTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    mViewPager.setCurrentItem(tab.getPosition());
                    //setTing view
                    if (tab.getPosition() == Constant.REVIEWS_SCREEN_TAB_THREE) {
                        mTabPos = 3;
                        mTvTitleBar.setVisibility(View.GONE);
                        linearSearchBarTop.setVisibility(View.VISIBLE);
                        linearSearchReviews.setVisibility(View.GONE);
                        Utils.hideSoftKeyboard(mActivity);
                        imgBackMain.setVisibility(View.VISIBLE);
                        mActivity.lockEventIn(600);

                    } else if (tab.getPosition() == Constant.REVIEWS_SCREEN_TAB_TWO) {
                        mTabPos = 2;
                        mTvTitleBar.setVisibility(View.GONE);
                        linearSearchBarTop.setVisibility(View.VISIBLE);
                        linearSearchReviews.setVisibility(View.GONE);
                        Utils.hideSoftKeyboard(mActivity);
                        imgBackMain.setVisibility(View.VISIBLE);
                        mActivity.lockEventIn(600);
                    } else if (tab.getPosition() == 0) {
                        mTabPos = 1;
                        mTvTitleBar.setVisibility(View.VISIBLE);
                        linearSearchBarTop.setVisibility(View.GONE);
                        linearSearchReviews.setVisibility(View.VISIBLE);
                        Utils.hideSoftKeyboard(mActivity);
                        imgBackMain.setVisibility(View.GONE);
                        mActivity.lockEventIn(600);
                    }

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
            //setup custom tab
            customTab();
            setListener();
            RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
                @Override
                public void call(Object o) {
                    if (o instanceof String) {
                        if (((String) o).equalsIgnoreCase("back")) {
                            if (mTabPos == 2) {
                                mViewPager.setCurrentItem(0);
                            } else if (mTabPos == 3) {
                                mViewPager.setCurrentItem(0);
                            }
                        }
                    }
                }
            });
        }
        return rootView;
    }

    private void removeFragment(final int mTabPosInput) {
        final FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (fragmentManager.getBackStackEntryCount() == 1 && mTabPos == 1) {
                            mActivity.showDialogExitApp(mActivity);
                        } else if (mTabPos == 2) {
                            mViewPager.setCurrentItem(0);
                        } else if (mTabPos == 3) {
                            mViewPager.setCurrentItem(0);
                        }
                        return true;
                    }
                    mActivity.lockEventIn(500);
                }
                return false;

            }


        });
    }

    private void setListener() {
        //search event tab me
        mEdtSearchBarTop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.toString().equalsIgnoreCase("")){
                    imgXbarTop.setVisibility(View.GONE);
                } else {
                    imgXbarTop.setVisibility(View.VISIBLE);
                }
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(200, 200) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }

                    public void onFinish() {
                        if (mTabPos == 3) {
                            //After 60000 milliseconds (60 sec) finish current
                            EventBus.getDefault().post(new SearchReviewMe(s.toString()));
                        } else if (mTabPos == 2) {
                            EventBus.getDefault().post(new SearchReviewFriend(s.toString()));

                        }

                    }
                }.start();
            }
        });
        //search event tab all
        mEdtSearchBottom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.toString().equalsIgnoreCase("")){
                    imgX.setVisibility(View.GONE);
                } else {
                    imgX.setVisibility(View.VISIBLE);
                }
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(200, 200) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }
                    public void onFinish() {
                        EventBus.getDefault().post(new SearchReviewAll(s.toString()));
                    }
                }.start();
            }
        });

        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                rootView.getWindowVisibleDisplayFrame(rect);
                int screenHeight = rootView.getRootView().getHeight();
                int heightDifference = screenHeight - rect.bottom + rect.top;
                if (heightDifference > screenHeight * 0.15) {
                    if (!isShowKeyBoard) {
                        isShowKeyBoard = true;
                        if (mTabPos == 1 ) {
                            imgBackSearch.setVisibility(View.VISIBLE);
                        } else {
                            imgBackSearch.setVisibility(View.GONE);
                        }
                    }
                } else {
                    if (isShowKeyBoard) {
                        isShowKeyBoard = false;
                        imgBackSearch.setVisibility(View.GONE);
                        if (mTabPos == 1) {
                            RxBus.getInstance().post("back_review_all");
                        }
                        if (mTabPos == 2) {
                            RxBus.getInstance().post("back_review_friend");
                        }
                        if (mTabPos == 3) {
                            RxBus.getInstance().post("back_review_me");
                        }
                    }
                }

            }
        });
    }

    /**
     * customTab
     */
    private void customTab() {
        View v = LayoutInflater.from(mActivity).inflate(R.layout.view_custom_tab_active, null);
        LinearLayout tv = (LinearLayout) v.findViewById(R.id.tvTabActive);
        tvTotal = (AutoResizeTextView) v.findViewById(R.id.tvTotalAll);
        //   tvTotal.setTextSize(7 * mActivity.getResources().getDisplayMetrics().density);
        AutoResizeTextView tb_one = (AutoResizeTextView) v.findViewById(R.id.tb_one);
        // tb_one.setTextSize(8 * mActivity.getResources().getDisplayMetrics().density);
        tv.setSelected(true);
        v.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mTabLayout.getTabAt(0).setCustomView(v);

        View v2 = LayoutInflater.from(mActivity).inflate(R.layout.view_custom_tab_active_2, null);
        tvTotalFriend = (AutoResizeTextView) v2.findViewById(R.id.tvTotalFriend);
        // tvTotalFriend.setTextSize(7 * mActivity.getResources().getDisplayMetrics().density);
        AutoResizeTextView tb_two = (AutoResizeTextView) v2.findViewById(R.id.tb_two);
        // tb_two.setTextSize(8 * mActivity.getResources().getDisplayMetrics().density);

        LinearLayout tv2 = (LinearLayout) v2.findViewById(R.id.tvTabActive2);
        tv2.setSelected(true);
        v2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mTabLayout.getTabAt(1).setCustomView(v2);

        View v3 = LayoutInflater.from(mActivity).inflate(R.layout.view_custom_tab_active_3, null);
        tvTotalMe = (AutoResizeTextView) v3.findViewById(R.id.tvTotalMe);
        // tvTotalMe.setTextSize(7 * mActivity.getResources().getDisplayMetrics().density);
        AutoResizeTextView tb_three = (AutoResizeTextView) v3.findViewById(R.id.tb_three);
        //  tb_three.setTextSize(8 * mActivity.getResources().getDisplayMetrics().density);
        LinearLayout tv3 = (LinearLayout) v3.findViewById(R.id.tvTabActive3);
        tv3.setSelected(true);
        v3.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mTabLayout.getTabAt(2).setCustomView(v3);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            try {
                if (mMediaPlayer != null) {
                    mMediaPlayer.pause();
                    mMediaPlayer.setVolume(0f, 0f);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final FragmentManager fragmentManager = mActivity.getSupportFragmentManager();

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    //  Log.i(tag, "keyCode: " + keyCode);
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        if (fragmentManager.getBackStackEntryCount() == 1 && mTabPos == 1) {
                            mActivity.showDialogExitApp(mActivity);
                        } else if (mTabPos == 2) {

                            mViewPager.setCurrentItem(0);
                        } else if (mTabPos == 3 ) {
                            mViewPager.setCurrentItem(0);
                        }
                        return true;
                    }
                    mActivity.lockEventIn(500);
                }
                return false;

            }


        });
    }

    @Override
    public void onResume() {
        super.onResume();

        final FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    //  Log.i(tag, "keyCode: " + keyCode);
                    if (keyCode == KeyEvent.KEYCODE_BACK) {

                        if (fragmentManager.getBackStackEntryCount() == 1 && mTabPos == 1) {
                            mActivity.showDialogExitApp(mActivity);
                        } else if (mTabPos == 2) {

                            mViewPager.setCurrentItem(0);
                        } else if (mTabPos == 3 ) {
                            mViewPager.setCurrentItem(0);
                        }
                        return true;
                    }
                    mActivity.lockEventIn(500);
                }
                return false;

            }


        });
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_REVIEWS;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * btnCreateReview
     */
    @OnClick(R.id.btnCreateReview)
    public void createReview() {
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.add(R.id.rlNewReview, new ReviewsCreateScreen(), "ReviewsCreateScreen");
        transaction.addToBackStack(null);
        transaction.commit();
        mRlNewReview.setVisibility(View.VISIBLE);
    }

    /**
     * click back btn
     */
    @OnClick(R.id.imgBackReview)
    public void clickBack() {
        ReviewsScreen.mTopBarReview.setVisibility(View.VISIBLE);
        Fragment fragment = mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.rlNewReview);
        //back from detail tab one
        switch (mTabPos) {
            case 1:
                backOfTabOne();
                break;
            case 2:
                ReviewsScreen.imgBackReview.setVisibility(View.GONE);
                ReviewsScreen.imgBackMain.setVisibility(View.VISIBLE);
                ReviewsScreen.mTopBarReview.setVisibility(View.VISIBLE);
                if (fragment != null) {
                    mActivity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                    mActivity.getSupportFragmentManager().popBackStack();
                }
                EventBus.getDefault().post(new RefreshListFriend());
                mActivity.mBottom().setVisibility(View.VISIBLE);
                mTabLayout.setVisibility(View.VISIBLE);
                mViewLine.setVisibility(View.VISIBLE);
                imgBackReview.setVisibility(View.GONE);
                linearSearchBarTop.setVisibility(View.VISIBLE);
                mTvTitleBar.setVisibility(View.GONE);
                linearSearchReviews.setVisibility(View.GONE);
                //----
                mRlNewReview.setVisibility(View.GONE);
                mRlViewpager.setVisibility(View.VISIBLE);
                //--------
                break;
            case 3:
                ReviewsScreen.imgBackReview.setVisibility(View.GONE);
                ReviewsScreen.imgBackMain.setVisibility(View.VISIBLE);
                backOfTabthree();
                break;
        }
    }

    /**
     * backOfTabthree
     */
    private void backOfTabthree() {
        Fragment fragment = mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.rlNewReview);
        if (fragment != null && mTabPos == 3) {
            mActivity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            mActivity.getSupportFragmentManager().popBackStack();
            //----
            mRlNewReview.setVisibility(View.GONE);
            mRlViewpager.setVisibility(View.VISIBLE);
            mTopBarReview.setVisibility(View.VISIBLE);
            //--------
            EventBus.getDefault().post(new RefreshListMe());
            //----
            mActivity.mBottom().setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            mViewLine.setVisibility(View.VISIBLE);
            imgBackReview.setVisibility(View.GONE);
            linearSearchBarTop.setVisibility(View.VISIBLE);
            mTvTitleBar.setVisibility(View.GONE);
            linearSearchReviews.setVisibility(View.GONE);
        }
    }


    /**
     * backOfTabOne
     */
    private void backOfTabOne() {
        Fragment fragment = mActivity.getSupportFragmentManager()
                .findFragmentById(R.id.rlNewReview);
        Fragment fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsDetailProductScreen");
        if (fragmentremove != null) {
            mActivity.getSupportFragmentManager().beginTransaction().remove(fragmentremove).commit();

        }
        if (mViewPager.getCurrentItem() == 0) {
            mTopBarReview.setVisibility(View.VISIBLE);
            mActivity.mBottom().setVisibility(View.VISIBLE);
            linearSearchReviews.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            mViewLine.setVisibility(View.VISIBLE);
            imgBackReview.setVisibility(View.GONE);
            linearSearchBarTop.setVisibility(View.GONE);
            mTvTitleBar.setVisibility(View.VISIBLE);
            ReviewsAllScreen.mtvNodataAll.setVisibility(View.GONE);
        }
        if (fragment != null && mTabPos == 1) {
            //----
            mRlNewReview.setVisibility(View.GONE);
            mRlViewpager.setVisibility(View.VISIBLE);
            //--------
            mTopBarReview.setVisibility(View.VISIBLE);
            mActivity.mBottom().setVisibility(View.VISIBLE);
            linearSearchReviews.setVisibility(View.VISIBLE);
            mTabLayout.setVisibility(View.VISIBLE);
            mViewLine.setVisibility(View.VISIBLE);
            imgBackReview.setVisibility(View.GONE);
            linearSearchBarTop.setVisibility(View.GONE);
            mTvTitleBar.setVisibility(View.VISIBLE);
            ReviewsAllScreen.mtvNodataAll.setVisibility(View.GONE);
            EventBus.getDefault().post(new BackListAll());
            if (fragment instanceof ReviewsDetailProductScreen) {
                android.support.v4.app.FragmentManager fm = mActivity.getSupportFragmentManager();
                int stack = fm.getBackStackEntryCount();
                if (stack > 1) {
                    while (stack > 1) {
                        fm.popBackStack();
                        stack--;
                    }
                }
            }
        }
    }

    @Subscribe
    public void createReviewFromList(CreateReview createReview) {
        createReview();
    }

    @OnClick(R.id.imgBackReviewMain)
    public void clickBackMain() {
        if (mTabPos == 2) {
            mViewPager.setCurrentItem(0);
        } else if (mTabPos == 3) {
            mViewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onClick(View v) {
    }
}
