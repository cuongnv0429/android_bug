package vn.com.feliz.v.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ResolveInfo;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import vn.com.feliz.R;

/**
 * Created by ITV01 on 12/27/16.
 */

public class ShareIntentListAdapter extends ArrayAdapter{
    private Context mContext;
    Object[] items;

    public ShareIntentListAdapter(Context mContext, Object[] items) {
        super(mContext, R.layout.social_share, items);
        this.mContext = mContext;
        this.items = items;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View view = inflater.inflate(R.layout.social_share, null, false);
        ImageView img_share = (ImageView) view.findViewById(R.id.img_share);
        TextView txt_share = (TextView) view.findViewById(R.id.txt_share);
        // share native image of the App to share
        img_share.setImageDrawable(((ResolveInfo)items[position])
                .activityInfo.applicationInfo.loadIcon(mContext.getPackageManager()));
        txt_share.setText(((ResolveInfo) items[position]).activityInfo.applicationInfo.loadLabel(mContext.getPackageManager()));

        return view;
    }
}
