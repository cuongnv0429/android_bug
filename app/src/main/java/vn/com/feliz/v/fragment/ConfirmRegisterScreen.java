package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.com.feliz.common.Constant;
import vn.com.feliz.m.ProfileConfirmInfo;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.eventbus.SetTitleMessage;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.R;
import vn.com.feliz.network.OnPostResponseListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmRegisterScreen extends BaseFragment implements OnPostResponseListener, OnMenuItemSelected {
    @BindView(R.id.tv_birthDay_confirm)
    TextView mBirthdayConfirm;
    @BindView(R.id.tv_gender_confirm)
    TextView mGenderConfirm;
    @BindView(R.id.tv_mobile_confirm)
    TextView mMobileConfirm;

    public ConfirmRegisterScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        // Set Title
        EventBus.getDefault().post(new SetTitleMessage(getString(R.string.profile_Screen_confirm)));
        EventBus.getDefault().post(this);

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_confirm_screen, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        InitData();
        return rootView;
    }

    /**
     * init Data
     */
    private void InitData() {
        try {
            Intent i = mActivity.getIntent();
            if (i != null) {
                ProfileConfirmInfo mProfile = (ProfileConfirmInfo) i.getSerializableExtra(Constant.FACEBOOK_OBJECT);
                mBirthdayConfirm.setText(mProfile.getBirthday());
                if(mProfile.getGender().trim().toLowerCase().equals(getString(R.string.gender_male_eng).toLowerCase())){
                    mGenderConfirm.setText(getString(R.string.gender_male));
                }else if((mProfile.getGender().trim().toLowerCase().equals(getString(R.string.gender_f_male_eng).toLowerCase()))){
                    mGenderConfirm.setText(getString(R.string.gender_f_male));
                }
                //convert date
                SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
                SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");

                Date date = inputFormat.parse(mProfile.getBirthday());
                String outputDateStr = outputFormat.format(date);
                mBirthdayConfirm.setText(outputDateStr);

                mMobileConfirm.setText(mProfile.getMobile());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_read_more)
    public void readMore() {
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_frame, new TermsAndConditionsScreen())
                .commit();
    }

    @OnClick(R.id.root_frame)
    public void confirmTerm() {
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_frame, new SettingScreen())
                .commit();
    }

    @Override

    public boolean onPostResponse(ApiTask task, int status) {
        if (status == ApiResponseCode.SUCCESS) {
            if (task.getType() == ApiTaskType.LOGIN) {

            }
        }
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }



    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_SETTING_CONFIRM_REGISTER;
    }
}
