package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.NetworkStateReceiver;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.m.response.AnswerResponse;
import vn.com.feliz.m.response.InterestListResponse;
import vn.com.feliz.m.response.InterestResponse;
import vn.com.feliz.m.response.InterestResponseItem;
import vn.com.feliz.m.response.QuestionDailyListResponse;
import vn.com.feliz.m.response.QuestionDailyResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ProfilePresenter;
import vn.com.feliz.p.eventbus.PlaceChange;
import vn.com.feliz.p.eventbus.idAnswer;
import vn.com.feliz.p.eventbus.idInterest;
import vn.com.feliz.p.eventbus.refreshData;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.activity.RegisterActivity;
import vn.com.feliz.v.adapter.ExpandableListInterestAdapter;
import vn.com.feliz.v.adapter.ExpandableListQuestionAdapter;
import vn.com.feliz.v.interfaces.OnBirthdaySetListener;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.v.widget.DatePickerFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingScreen extends BaseFragment implements OnPostResponseListener,
        OnMenuItemSelected, OnBirthdaySetListener, CompoundButton.OnCheckedChangeListener,
        NetworkStateReceiver.NetworkStateReceiverListener, GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener{
    @BindView(R.id.main_recycler_question)
    RecyclerView mQuestionRecycler;
    List<ExpandableListQuestionAdapter.Item> mQuestions = new ArrayList<>();
    //interest
    @BindView(R.id.main_recycler_interest)
    RecyclerView mInterestRecycler;
    List<ExpandableListInterestAdapter.Item> mInterests = new ArrayList<>();
    //Web view show term
    @BindView(R.id.wv_content_term)
    WebView mWvTerm;
    //Guild block
    @BindView(R.id.guide_ln)
    LinearLayout mBlockGuide;
    //Web view show guide
    @BindView(R.id.wv_content_guide)
    WebView mWvGuide;
    //WebView info
    @BindView(R.id.wv_content_info)
    TextView mWvInfo;
    @BindView(R.id.fragment_profile_ln)
    LinearLayout mProfileLn;
    @BindView(R.id.question_ln)
    LinearLayout mQuestionLn;
    @BindView(R.id.interest_ln)
    LinearLayout mInterestLn;
    @BindView(R.id.function_ln)
    LinearLayout mFunctionLn;
    @BindView(R.id.function_main)
    RelativeLayout mMainFunctionRl;
    @BindView(R.id.seek)
    View seek;
    @BindView(R.id.function_main_below)
    RelativeLayout mMainFunctionRl_below;
    @BindView(R.id.adress)
    TextView mEdtAdress;
    @BindView(R.id.setting_more)
    TextView mSetting_more;
    @BindView(R.id.setting_little)
    TextView mSetting_little;
    @BindView(R.id.birthday_setting)
    TextView mTvBirthDay;
    @BindView(R.id.gender_setting)
    TextView mTvGender;
    @BindView(R.id.mobile_setting)
    TextView mTvMobile;
    @BindView(R.id.scroll_container)
    ScrollView mScrollContainer;
    // @BindView(R.id.view)
    //View mViewLine;
    @BindView(R.id.progress_toggle_top_more)
    ImageView mSeekBarTopMore;
    @BindView(R.id.progress_toggle_top_little)
    ImageView mSeekBarTopLittle;
    @BindView(R.id.linear_top_more)
    LinearLayout linearTopMore;
    @BindView(R.id.linear_top_litte)
    LinearLayout linearTopLitte;
    private boolean isShowGuide;
    private boolean isShowInfo;
    private boolean isShowTerm;
    private boolean isShowProfile = true;
    private boolean isShowFunction;
    private String mDateChange;
    private ProfilePresenter mProfilePresenter;
    private String mToken_user;
    private String mListInterest = "";
    public static String listId = ",-1,";
    public static String listIdQuestion = ",-1,";
    private boolean mIsVisibleQuestion;
    private TSnackbar snackbar;
    @BindView(R.id.rl_container_setting)
    RelativeLayout mRootView;
    @BindView(R.id.sb_text)
    SwitchCompat mSwitchLockScreen;
    private ProgressDialog mProgressDialog;
    @BindView(R.id.main_top_bar_title)
    TextView mTitleBar;
    @BindView(R.id.main_topbar_back)
    ImageView mBack;

    private String token_user;
    CountDownTimer waitTimer;
    private String idAnswerSubmit;
    private String isMoreOrTitle;
    private boolean mNetworkDisconnected = false;

    private NetworkStateReceiver mNetworkStateReceiver;
    private GoogleApiClient mGoogleApiClient;
    private boolean isSetting = true;//check if settingscreen display overlay

    public SettingScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfilePresenter = new ProfilePresenter(mActivity, this);
        //  EventBus.getDefault().post(this);
    /*    // Set Title
        EventBus.getDefault().post(new SetTitleMessage(getString(R.string.profile_Screen)));
        EventBus.getDefault().post(this);*/
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);

        //check nextwork connect create
        try {
            mNetworkStateReceiver = new NetworkStateReceiver();
            mNetworkStateReceiver.addListener(this);
            mActivity.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_setting_screen, container, false);

        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        mActivity.getmViewOverlayBottomBar().setVisibility(View.GONE);
        mQuestionRecycler.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));
        mInterestRecycler.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false));

        // mLoadQuestion.setImageResource(R.drawable.loadding_ic_gif);

        mTitleBar.setText(getString(R.string.profile_Screen));
        mBack.setVisibility(View.GONE);
        //mSeekBarTop.setProgress(100);
        // mInterestRecycler.setAdapter(new ExpandableListInterestAdapter(mInterests));
        //set data for term/guide/info
        setDataTerm();
        setDataGuide();
        setDatainfo();
        //auto scroll when touch
        if (mProfilePresenter.hasUserInfo()) {
            mToken_user = mProfilePresenter.getUserInfo().getToken();
            loadData();
            /* Insertest not used */
            //mProfilePresenter.requestInterest(mToken_user);
            //mLoadQuestion.setVisibility(View.VISIBLE);

        }
        //init seekbar
        mSeekBarTopMore.setVisibility(View.VISIBLE);
        mSeekBarTopLittle.setVisibility(View.GONE);
        //set switch event for lockscreen
        //mSwitchLockScreen.setChecked(Utils.getLockScreenStatus(SettingScreen.this.getActivity()));
        //mSwitchLockScreen.setOnClickListener(new SwitchLockScreenListener());
        mSwitchLockScreen.setOnCheckedChangeListener(this);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        try {

            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(mActivity, 1, this)
                    .addConnectionCallbacks(this)
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }

        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("setting")) {
                        if (isAdded()) {
                            mTitleBar.setText(getString(R.string.profile_Screen));
                            mBack.setVisibility(View.GONE);
                        }
                    }

                    if (((String) o).equalsIgnoreCase("title")) {
                        if (isAdded()) {
                            mTitleBar.setText(getString(R.string.text_adress_change));
                        }
                    }
                }
            }
        });

        return rootView;
    }

    private void loadData() {
        if(mActivity.isConnected(getContext())) {
            if (mToken_user != null) {
                mProgressDialog = Utils.showLoading(mActivity);
                //get Profile info
                mProfilePresenter.requestProfile(mToken_user);
                //get Question info
                if(MainActivity.isFromPush) {
                    MainActivity.isFromPush = false;
                    mProfilePresenter.requestQuestion(mToken_user);
                }
            }
        } else {
            mNetworkDisconnected = true;
        }
    }

    private void updateStatusSwitch(boolean statusSwitch, String valueLittleMore) {
        if (!statusSwitch) {
            linearTopLitte.setClickable(false);
            linearTopMore.setClickable(false);
            mSetting_little.setClickable(false);
            mSetting_more.setClickable(false);
            if (valueLittleMore.equals("1")) {
                mSeekBarTopLittle.setVisibility(View.INVISIBLE);
                mSeekBarTopMore.setVisibility(View.VISIBLE);
                mSeekBarTopMore.setBackgroundResource(R.drawable.draw_cicrle_disible);
            } else if (valueLittleMore.equals("0")) {
                mSeekBarTopMore.setVisibility(View.INVISIBLE);
                mSeekBarTopLittle.setVisibility(View.VISIBLE);
                mSeekBarTopLittle.setBackgroundResource(R.drawable.draw_cicrle_disible);
            }
        } else {
            linearTopLitte.setClickable(true);
            linearTopMore.setClickable(true);
            mSetting_little.setClickable(true);
            mSetting_more.setClickable(true);
            if (valueLittleMore.equals("1")) {
                mSeekBarTopLittle.setVisibility(View.INVISIBLE);
                mSeekBarTopMore.setVisibility(View.VISIBLE);
                mSeekBarTopMore.setBackgroundResource(R.drawable.draw_circle);
            } else if (valueLittleMore.equals("0")) {
                mSeekBarTopMore.setVisibility(View.INVISIBLE);
                mSeekBarTopLittle.setVisibility(View.VISIBLE);
                mSeekBarTopLittle.setBackgroundResource(R.drawable.draw_circle);
            }
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (RegisterActivity.messageChangeMobile != null) {
            snackbar = TSnackbar
                    .make(mRootView, " " + RegisterActivity.messageChangeMobile, TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, RegisterActivity.messageChangeMobile, mActivity);
            RegisterActivity.messageChangeMobile = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        if (mGoogleApiClient != null) {
            if(mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    /**
     * set data term for test
     */
    private void setDataTerm() {
        try {
            String textContent;
            if(mProfilePresenter.hasMetaInfo() != null) {
                if(mProfilePresenter.hasMetaInfo().getTerm() != null) {
                    textContent = "<html><head><meta http-equiv=\"Content-Type\" \" +\n" +
                            "\" +\"\"content=\"text/html; charset=utf-8\"></head><body><p align=\"justify\">";
                    textContent += mProfilePresenter.hasMetaInfo().getTerm();
                    textContent += "</p></body></html>";
                    //  mWvGuide.loadData(textContent, "text/html", "UTF-8");
                    mWvTerm.getSettings().setJavaScriptEnabled(true);
                    mWvTerm.loadDataWithBaseURL("", textContent, "text/html", "UTF-8", "");
                } else {
//                    mBlockGuide.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set data term for test
     */
    private void setDataGuide() {
        try {
            String textContent;
            if(mProfilePresenter.hasMetaInfo() != null) {
                if(mProfilePresenter.hasMetaInfo().getApp_intro() != null) {
                    textContent = "<html><head><meta http-equiv=\"Content-Type\" \" +\n" +
                            "\" +\"\"content=\"text/html; charset=utf-8\"></head><body><p align=\"justify\">";
                    textContent += mProfilePresenter.hasMetaInfo().getApp_intro();
                    textContent += "</p></body></html>";
                    //  mWvGuide.loadData(textContent, "text/html", "UTF-8");
                    mWvGuide.getSettings().setJavaScriptEnabled(true);
                    mWvGuide.loadDataWithBaseURL("", textContent, "text/html", "UTF-8", "");
                } else {
                    mBlockGuide.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set data term for test
     */
    private void setDatainfo() {
        try {
//            String textContent;
//            textContent = "<html><head><meta http-equiv=\"Content-Type\" \" +\n" +
//                    "\" +\"\"content=\"text/html; charset=utf-8\"></head><body><p align=\"justify\">";
//            textContent += mProfilePresenter.hasMetaInfo().getApp_info();
//            textContent += "</p></body></html>";
//            Log.e("setDatainfo", textContent);
            mWvInfo.setText(mProfilePresenter.hasMetaInfo().getApp_info());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Event show/hide content guide
     */
    @OnClick(R.id.tvGuide)
    public void openCloseGuide() {
        //if guide is close
        if (!isShowGuide) {
            isShowGuide = true;
            mWvGuide.setVisibility(View.VISIBLE);
            mWvTerm.setVisibility(View.GONE);
            mWvInfo.setVisibility(View.GONE);
            mMainFunctionRl.setVisibility(View.GONE);
            seek.setVisibility(View.GONE);
            //mViewLine.setVisibility(View.GONE);
            mMainFunctionRl_below.setVisibility(View.GONE);
            mProfileLn.setVisibility(View.GONE);
            mInterestLn.setVisibility(View.GONE);
            mQuestionLn.setVisibility(View.GONE);
            isShowTerm = false;
            isShowFunction = false;
            isShowProfile = false;
            isShowInfo = false;
        } else {
            isShowGuide = false;
            mWvGuide.setVisibility(View.GONE);
        }
    }

    /**
     * network
     */
    @Subscribe
    public void feFreshData(refreshData refreshData) {
        if (refreshData.getNameFragment().toLowerCase().equals("settingfragment")) {
            //auto scroll when touch
            if (mProfilePresenter.hasUserInfo()) {
                // mProgressDialog = Utils.showLoading(mActivity);
                mToken_user = mProfilePresenter.getUserInfo().getToken();
                //get Profile info
                //mProfilePresenter.requestProfile(mToken_user);
                //get Question info
                //mProfilePresenter.requestQuestion(mToken_user);

                /* Interest not used */
                //mProfilePresenter.requestInterest(mToken_user);
                // mLoadQuestion.setVisibility(View.VISIBLE);

            }
        }
    }

    /**
     * Event show/hide content guide
     */
    @OnClick(R.id.tvInfo)
    public void openCloseInfo() {
        //if guide is close
        if (!isShowInfo) {
            isShowInfo = true;
            mWvInfo.setVisibility(View.VISIBLE);
            mWvGuide.setVisibility(View.GONE);
            mWvTerm.setVisibility(View.GONE);
            mMainFunctionRl.setVisibility(View.GONE);
            seek.setVisibility(View.GONE);
            //mViewLine.setVisibility(View.GONE);
            mMainFunctionRl_below.setVisibility(View.GONE);
            mProfileLn.setVisibility(View.GONE);
            mInterestLn.setVisibility(View.GONE);
            mQuestionLn.setVisibility(View.GONE);
            isShowTerm = false;
            isShowFunction = false;
            isShowProfile = false;
            isShowGuide = false;
        } else {
            isShowInfo = false;
            mWvInfo.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.setting_more, R.id.linear_top_more})
    public void moreSetting() {
        mSeekBarTopLittle.setVisibility(View.INVISIBLE);
        mSeekBarTopMore.setVisibility(View.VISIBLE);
        isMoreOrTitle = "1";
        if (mProfilePresenter.hasUserInfo()) {
            token_user = mProfilePresenter.getUserInfo().getToken();
            //API CHANGE PROFILE
            if (waitTimer != null) {
                waitTimer.cancel();
                waitTimer = null;
            }
            waitTimer = new CountDownTimer(3000, 300) {

                public void onTick(long millisUntilFinished) {
                    //called every 300 milliseconds, which could be used to
                    //send messages or some other action
                }

                public void onFinish() {
                    //After 60000 milliseconds (60 sec) finish current
                    //if you would like to execute something when time finishes
                    mProfilePresenter.changeProfile("", token_user, "", "", Constant.PROGRESS_STATUS, "",
                            Constant.SIZE_ADS_MORE, "", "", "", "");
                }
            }.start();
        }

    }

    @OnClick({R.id.setting_little, R.id.linear_top_litte})
    public void littleSetting() {
        mSeekBarTopLittle.setVisibility(View.VISIBLE);
        mSeekBarTopMore.setVisibility(View.INVISIBLE);
        isMoreOrTitle = "0";
        if (mProfilePresenter.hasUserInfo()) {
            token_user = mProfilePresenter.getUserInfo().getToken();

            if (waitTimer != null) {
                waitTimer.cancel();
                waitTimer = null;
            }
            waitTimer = new CountDownTimer(3000, 300) {

                public void onTick(long millisUntilFinished) {
                    //called every 300 milliseconds, which could be used to
                    //send messages or some other action
                }

                public void onFinish() {
                    //After 60000 milliseconds (60 sec) finish current
                    //if you would like to execute something when time finishes
                    mProfilePresenter.changeProfile("" +
                                    "", token_user, "", "", Constant.PROGRESS_STATUS, "",
                            Constant.SIZE_ADS_LITTLE, "", "", "'", "");
                }
            }.start();
        }

    }

    /**
     * Event show/hide content term
     */
    @OnClick(R.id.tvTerm)
    void openCloseTerm() {
        if (!isShowTerm) {
            isShowTerm = true;
            isShowGuide = false;
            isShowFunction = false;
            isShowProfile = false;
            isShowInfo = false;
            mWvTerm.setVisibility(View.VISIBLE);
            mWvGuide.setVisibility(View.GONE);
            mWvInfo.setVisibility(View.GONE);
            mMainFunctionRl.setVisibility(View.GONE);
            mMainFunctionRl_below.setVisibility(View.GONE);
            seek.setVisibility(View.GONE);
            mInterestLn.setVisibility(View.GONE);
            mQuestionLn.setVisibility(View.GONE);
            mProfileLn.setVisibility(View.GONE);

        } else {
            isShowTerm = false;
            mWvTerm.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.main_topbar_back)
    public void backAdressChange() {
        Utils.hideSoftKeyboard(mActivity);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mActivity.getSupportFragmentManager().popBackStack();
                mBack.setVisibility(View.GONE);
                mTitleBar.setText(getResources().getString(R.string.profile_Screen));
                isSetting = true;
            }
        }, 100);

    }


    /**
     * Event show/hide content function
     */
    @OnClick(R.id.function_tv)
    public void openCloseFunction() {
        if (!isShowFunction) {
            isShowFunction = true;
            isShowGuide = false;
            isShowTerm = false;
            isShowProfile = false;
            isShowInfo = false;
            mWvTerm.setVisibility(View.GONE);
            mWvGuide.setVisibility(View.GONE);
            mWvInfo.setVisibility(View.GONE);
            mProfileLn.setVisibility(View.GONE);
            mInterestLn.setVisibility(View.GONE);
            mQuestionLn.setVisibility(View.GONE);
            //mViewLine.setVisibility(View.VISIBLE);

            mMainFunctionRl.setVisibility(View.VISIBLE);
            //mViewLine.setVisibility(View.VISIBLE);
            mMainFunctionRl_below.setVisibility(View.VISIBLE);
            seek.setVisibility(View.VISIBLE);

        } else {
            isShowFunction = false;
            mMainFunctionRl.setVisibility(View.GONE);
            mMainFunctionRl_below.setVisibility(View.GONE);
            //mViewLine.setVisibility(View.GONE);
            seek.setVisibility(View.GONE);
            //mViewLine.setVisibility(View.GONE);
        }
    }

    /**
     * start auto completeTextView
     */
    @OnClick(R.id.adress)
    public void ChangePlace() {
        //  if (Utils.isNetworkAvailable(mActivity)) {
       /* Intent intent = new Intent(mActivity,SearchActivity.class);
        startActivity(intent);*/
        isSetting = false;
        Bundle bundle = new Bundle();
        if (mProfilePresenter.getAddress() != null && !mProfilePresenter.getAddress().equals("")) {
            bundle.putString("address", mEdtAdress.getText().toString());
        } else {
            bundle.putString("address", "");
        }
        SearchPlaceScreen mSearchPlaceScreen = new SearchPlaceScreen();
        mSearchPlaceScreen.setArguments(bundle);
        mTitleBar.setVisibility(View.VISIBLE);
        mTitleBar.setText(getResources().getString(R.string.text_adress_change));
        mBack.setVisibility(View.VISIBLE);
        Utils.showSoftKeyboard(mActivity);
  /*      mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_frame, mSearchPlaceScreen)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commit();*/

        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.containerSearch, mSearchPlaceScreen)
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        getChildFragmentManager().executePendingTransactions();

//        getActivity().getSupportFragmentManager()
//                .beginTransaction()
//                .setCustomAnimations(R.anim.fragment_move_in_from_right_to_left, R.anim.fragment_move_out_from_right_to_left,
//                        R.anim.fragment_move_in_from_left_to_right, R.anim.fragment_move_out_from_left_to_right)
//                .replace(R.id.root_frame, mSearchPlaceScreen)
//                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
//                .addToBackStack(null)
//                .commit();
    }

    /**
     * changeBirthDay
     */
    @OnClick(R.id.birthday_setting)
    public void changeBirthDay() {
        if (mTvBirthDay.getText().toString().toLowerCase().equals("cáº­p nháº­t")) {
            //open DatePicker
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setListener(this);
            newFragment.show(mActivity.getSupportFragmentManager(),
                    DatePickerFragment.class.getSimpleName());
        }
    }

    /**
     * chaneg Gender
     */
    @OnClick(R.id.gender_setting)
    public void changeGender() {
        // custom dialog gender
       /* final Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.custom_dialog_gender);
        dialog.setTitle(getString(R.string.gender_chose));
        RadioButton rdMale = (RadioButton) dialog.findViewById(R.id.male);
        RadioButton rdFeMale = (RadioButton) dialog.findViewById(R.id.female);
        rdMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTvGender.setText(getString(R.string.custom_dialog_male));
                dialog.dismiss();
            }
        });
        rdFeMale.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mTvGender.setText(getString(R.string.custom_dialog_female));
                dialog.dismiss();
            }
        });
        dialog.show();*/
    }

    /**
     * change Mobile Phone
     */
    @OnClick(R.id.mobile_setting)
    public void changeMobile() {

        UserInfo userInfo = mProfilePresenter.getUserInfo();
        if(userInfo != null) {
            mProfilePresenter.loginRequest(userInfo.getToken(), userInfo.getAccess_token());
            //set flag for register
            SharedPreferences mSettings_mobile;
            mSettings_mobile = mActivity.getSharedPreferences(Constant.MYPREFSFILE_MOBILE_CHANGE, 0);
            SharedPreferences.Editor editorMobile = mSettings_mobile.edit();
            editorMobile.putBoolean(Constant.IS_CHANGE, true);
            editorMobile.commit();
        }

        // Start your app main activity
        Intent i = new Intent(mActivity, RegisterActivity.class);
        startActivity(i);
//        mActivity.finish();
        // close this activity
    }

    /**
     * get Event bus from activity after search
     *
     * @param placeChange PlaceChange
     */
    @Subscribe
    public void getChangePlace(PlaceChange placeChange) {
        Log.e("getChangePlace", mGoogleApiClient.isConnected() + "");
        try {
            if (placeChange.getPlace() != null) {
                mEdtAdress.setText(placeChange.getPlace());
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    Log.e("onResult", placeChange.getPlaceId());
                    Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeChange.getPlaceId())
                            .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                @Override
                                public void onResult(PlaceBuffer places) {
                                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                        final Place myPlace = places.get(0);
                                        Log.e("onResult", myPlace.getAddress().toString());
                                        Geocoder geocoder = new Geocoder(mActivity, Locale.getDefault());
                                        try {
                                            List<Address> addressList = geocoder.getFromLocation(myPlace.getLatLng().latitude,
                                                    myPlace.getLatLng().longitude, 1);
                                            Log.e("onResult", addressList.size() + "");
                                            Address add = addressList.get(0);
                                            Log.e("onResult", new Gson().toJson(add));
                                            PlaceChange pl;
                                            if (add != null) {
                                                pl = parseJsonLocate(new Gson().toJson(add));
                                                //if is change mobile
                                                String token_user = null;
                                                if (mProfilePresenter.hasUserInfo()) {
                                                    token_user = mProfilePresenter.getUserInfo().getToken();
//                                                    Log.e("setting", token_user);
                                                    //API CHANGE PROFILE
                                                    mProfilePresenter.changeProfile("", token_user, mProfilePresenter.getMobile(), myPlace.getAddress().toString(),
                                                            Constant.PROGRESS_STATUS, "", "", pl.getDistrict_name(),
                                                            pl.getWard_name(), pl.getCity_name(), pl.getCountry_name());
                                                }
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    places.release();
                                }
                            });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PlaceChange parseJsonLocate(String json) {
        PlaceChange placeChange = new PlaceChange();
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has("mAddressLines")) {
                JSONObject mAddressLines = jsonObject.getJSONObject("mAddressLines");
                if (mAddressLines.has("1")) {
                    placeChange.setWard_name(mAddressLines.getString("1"));
                }
                if (mAddressLines.has("2")) {
                    placeChange.setDistrict_name(mAddressLines.getString("2"));
                }
                if (mAddressLines.has("3")) {
                    placeChange.setCity_name(mAddressLines.getString("3"));
                }
                if (mAddressLines.has("4")) {
                    placeChange.setCountry_name(mAddressLines.getString("4"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return placeChange;
    }

    /**
     * get Event bus from activity after search
     */
    @Subscribe
    public void getId(idAnswer idAnswer) {
        try {
            idAnswerSubmit = idAnswer.getIdAnswer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("Setting", "onResume");
        firstIntance();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            mActivity.unregisterReceiver(mNetworkStateReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param idInterest
     */
    @Subscribe
    public void getIdInterest(idInterest idInterest) {
        /* Interset not used */
        /*
        if (idInterest.getId() != null) {
            mListInterest = idInterest.getId();
            mProfilePresenter.requestSubmitInterest(mToken_user, mListInterest);
            listId = ",-1,";
        }
        */
    }

    @OnClick(R.id.profile_tv)
    public void openCloseProfile() {
        try {
            if (isShowProfile) {
                //reset instance layout
                isShowProfile = false;
                isShowGuide = false;
                isShowTerm = false;
                isShowFunction = false;


                //hide all view when isShowProfile = false click titleBar
                mProfileLn.setVisibility(View.GONE);
                mInterestLn.setVisibility(View.GONE);
                mQuestionLn.setVisibility(View.GONE);

            } else {
                isShowProfile = true;
                isShowGuide = false;
                isShowTerm = false;
                isShowFunction = false;
                isShowInfo = false;
                mProfileLn.setVisibility(View.VISIBLE);

                //TODO will apply
              /*  mInterestLn.setVisibility(View.VISIBLE);
                mQuestionLn.setVisibility(View.VISIBLE);*/
                ///
                if (mIsVisibleQuestion) {
                    mQuestionLn.setVisibility(View.VISIBLE);
                }
                mWvTerm.setVisibility(View.GONE);
                mMainFunctionRl.setVisibility(View.GONE);
                mMainFunctionRl_below.setVisibility(View.GONE);
                seek.setVisibility(View.GONE);
                mWvGuide.setVisibility(View.GONE);

                mWvTerm.setVisibility(View.GONE);
                mWvInfo.setVisibility(View.GONE);
                mMainFunctionRl.setVisibility(View.GONE);
                mMainFunctionRl_below.setVisibility(View.GONE);
                mWvGuide.setVisibility(View.GONE);
                seek.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Log.e("onPostResponse", status + "");

        Utils.hideLoading(mProgressDialog);
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mNetworkDisconnected = true;
            TSnackbar snackbar = TSnackbar
                    .make(mRootView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), mActivity);
        } else {
            BaseResponse mBaseRespone;
            try {
                mBaseRespone = (BaseResponse) task.getResponse().body();
            } catch (Exception ex) {
                snackbar = TSnackbar
                        .make(getView(), getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                Utils.vibarte(mActivity);
                return false;
            }
            if (task.getType() == ApiTaskType.GET_PROFILE) {
                if (status == ApiResponseCode.SUCCESS) {
                    try {
                        if (mProfilePresenter.getAddress() == null || mProfilePresenter.getAddress().equals("")) {
                            mEdtAdress.setText(getString(R.string.setting_adress_update));
                        } else {
                            mEdtAdress.setText(mProfilePresenter.getAddress());
                        }
                        mTvGender.setText(mProfilePresenter.getGender());
                        if (mProfilePresenter.getBirthday() != "") {
                            mTvBirthDay.setClickable(false);
                            mTvBirthDay.setText(mProfilePresenter.getBirthday());
                        } else {
                            mTvBirthDay.setText(getString(R.string.setting_adress_update));
                            mTvBirthDay.setTextColor(getResources().getColor(R.color.black));
                        }

                        mTvMobile.setText(mProfilePresenter.getMobile());
                        mProfilePresenter.updatePhone(mProfilePresenter.getMobile());

                        isMoreOrTitle = mProfilePresenter.getSize_ads();
                        //little/more ads
                        if (mProfilePresenter.getSize_ads().equals("1")) {
                            mSeekBarTopLittle.setVisibility(View.INVISIBLE);
                            mSeekBarTopMore.setVisibility(View.VISIBLE);
                        } else if (mProfilePresenter.getSize_ads().equals("0")) {
                            mSeekBarTopLittle.setVisibility(View.VISIBLE);
                            mSeekBarTopMore.setVisibility(View.INVISIBLE);
                        }
                        //ads check
                        if (mProfilePresenter.getAllow_ads().equals("1")) {
                            mSwitchLockScreen.setChecked(true);

                            Intent intent = new Intent();
                            //BYME
                            //intent.setClass(mActivity, LockScreenService.class);
                            //mActivity.startService(intent);
                        } else if (mProfilePresenter.getAllow_ads().equals("0")) {
                            mSwitchLockScreen.setChecked(false);

                            Intent intent = new Intent();
                            //BYME
                            //intent.setClass(mActivity, LockScreenService.class);
                            //mActivity.stopService(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (task.getType() == ApiTaskType.GET_QUESTION_DAILY) {
                if (status == ApiResponseCode.SUCCESS) {
                    if (mBaseRespone.getData() != null) {
                        //pare question daily from json;
                        QuestionDailyListResponse mQuestionDailyListResponse = (QuestionDailyListResponse) mBaseRespone.getData();
                        List<QuestionDailyResponse> mListQuestion = new ArrayList<>();
                        mListQuestion = mQuestionDailyListResponse.getList();

                        ArrayList<ExpandableListQuestionAdapter.Item> mQuestionArayObj = new ArrayList<>();
                        // obj Question
                        if (mListQuestion != null) {
                            if (mQuestionLn != null) {
                                mQuestionLn.setVisibility(View.VISIBLE);
                                mIsVisibleQuestion = true;
                            }
                            for (int inQs = 0; inQs <= mListQuestion.size() - 1; inQs++) {
                                mQuestionArayObj.add(new ExpandableListQuestionAdapter.Item(ExpandableListQuestionAdapter.HEADER, mListQuestion.get(inQs).getQuestion(), "-1", mListQuestion.get(inQs).getQuestion_id()));
                                List<AnswerResponse> mAnswerList = new ArrayList<>();
                                mAnswerList.clear();
                                mAnswerList = mListQuestion.get(inQs).getListAnser();
                                //Obj answer
                                for (int itemAn = 0; itemAn <= mAnswerList.size() - 1; itemAn++) {
                                    mQuestionArayObj.add(new ExpandableListQuestionAdapter.Item(
                                            ExpandableListQuestionAdapter.QUESTION, mAnswerList.get(itemAn).getValue(), mAnswerList.get(itemAn).getAnswer_id(), "-2"));
                                }
                            }
                        } else {
                            if (mQuestionLn != null) {
                                mQuestionLn.setVisibility(View.GONE);
                                mIsVisibleQuestion = false;
                            }
                        }
                        if (mQuestionArayObj != null && mQuestionRecycler != null) {
                            mQuestionRecycler.setAdapter(new ExpandableListQuestionAdapter(mQuestionArayObj));
                        }
                    } else {
                        snackbar = TSnackbar
                                .make(getView(), getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                        Utils.vibarte(mActivity);
                        return false;
                    }
                }
            } else if (task.getType() == ApiTaskType.SUBMIT_ID_QUESTION) {
                if (status == ApiResponseCode.SUCCESS) {
                    snackbar = TSnackbar
                            .make(mRootView, " " + mProfilePresenter.messageErr(), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, mProfilePresenter.messageErr(), mActivity);
                    mProfilePresenter.requestQuestion(mToken_user);
                }
            } else if (task.getType() == ApiTaskType.GET_INTEREST) {
                if (status == ApiResponseCode.SUCCESS) {
                    //try {
                    if (mBaseRespone.getData() != null) {
                        InterestListResponse mInListResponse = (InterestListResponse) mBaseRespone.getData();
                        List<InterestResponse> mListInterestResponses = new ArrayList<>();
                        mListInterestResponses = mInListResponse.getList();
                        ArrayList<ExpandableListInterestAdapter.Item> mTitleArayObj = new ArrayList<>();
                        List<InterestResponseItem> mInterestResponseItemList = new ArrayList<>();
                        for (int inTitle = 0; inTitle < mListInterestResponses.size(); inTitle++) {
                            mTitleArayObj.add(new ExpandableListInterestAdapter.Item(ExpandableListInterestAdapter.HEADER, mListInterestResponses.get(inTitle).getName(),
                                    0));

                        }
                        ArrayList<String> itemInterest = new ArrayList<String>();
                        ArrayList<String> itemInterestValue = new ArrayList<String>();
                        ArrayList<String> itemInterestId = new ArrayList<String>();
                        for (int i = 0; i <= mTitleArayObj.size() - 1; i++) {
                            itemInterest.clear();
                            itemInterestValue.clear();
                            itemInterestId.clear();
                            mInterestResponseItemList = mListInterestResponses.get(i).getList();
                            if (mInterestResponseItemList != null) {
                                for (int iTem = 0; iTem <= mInterestResponseItemList.size() - 1; iTem++) {
                                    itemInterest.add(mInterestResponseItemList.get(iTem).getName());
                                    itemInterestValue.add(String.valueOf(mInterestResponseItemList.get(iTem).getValue()));
                                    itemInterestId.add(String.valueOf(mInterestResponseItemList.get(iTem).getInterest_id()));
                                    for (int idPos = 0; idPos <= itemInterestId.size() - 1; idPos++) {
                                        if (itemInterestValue.get(idPos) == Constant.CHECKED_INTEREST) {
                                            if (!listId.contains("," + itemInterestId.get(idPos) + ",")) {
                                                listId = listId + "," + itemInterestId.get(idPos) + ",";
                                            }
                                        }
                                    }
                                    mTitleArayObj.get(i).invisibleChildren = new ArrayList<>();
                                    if (mTitleArayObj.get(i) != null) {
                                        for (int itemInterestPos = 0; itemInterestPos <= itemInterest.size() - 1; itemInterestPos++) {
                                            if (itemInterestPos == itemInterestValue.size() - 1) {
                                                mTitleArayObj.get(i).
                                                        invisibleChildren.add(new ExpandableListInterestAdapter.
                                                        Item(ExpandableListInterestAdapter.INTEREST, itemInterest.get(itemInterestPos), Integer.parseInt(itemInterestValue.get(itemInterestPos)),
                                                        itemInterestId.get(itemInterestPos), true));
                                            } else {
                                                mTitleArayObj.get(i).
                                                        invisibleChildren.add(new ExpandableListInterestAdapter.
                                                        Item(ExpandableListInterestAdapter.INTEREST, itemInterest.get(itemInterestPos), Integer.parseInt(itemInterestValue.get(itemInterestPos))
                                                        , itemInterestId.get(itemInterestPos), false));
                                            }


                                        }
                                    }
                                }
                                mInterests.add(mTitleArayObj.get(i));

                            }


                        }
                        if (mInterests != null && mInterestRecycler != null) {
                            mInterestRecycler.setAdapter(new ExpandableListInterestAdapter(mInterests));
                        }
                    } else {
                        snackbar = TSnackbar
                                .make(getView(), getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                        Utils.vibarte(mActivity);
                        return false;
                    }
                }
            } else if (task.getType() == ApiTaskType.SUBMIT_INTEREST) {
                if (status == ApiResponseCode.SUCCESS) {

                }

            } else if (task.getType() == ApiTaskType.CHANGE_PROFILE) {
                if (status == ApiResponseCode.SUCCESS) {
                    try {
                        // mProfilePresenter.requestProfile(mToken_user);
                        snackbar = TSnackbar
                                .make(mRootView, " " + mProfilePresenter.messageErr(), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, mProfilePresenter.messageErr(), mActivity);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_SETTING;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

    }


    @Override
    public void onBirthdaySet(int year, int monthOfYear, int dayOfMonth) {
        try {

            //format day
            String month_input;
            String day_input;
            if ((monthOfYear + 1) < 10) {
                month_input = "0" + String.valueOf(monthOfYear + 1);
            } else {
                month_input = String.valueOf(monthOfYear + 1);
            }
            if (dayOfMonth < 10) {
                day_input = "0" + String.valueOf(dayOfMonth);
            } else {
                day_input = String.valueOf(dayOfMonth);
            }
            mDateChange = String.valueOf(month_input + "/" + day_input + "/" + +year);
            //set date for textView
            mTvBirthDay.setText(String.valueOf(day_input + "/" + month_input + "/" + year));
            String token_user = null;
            if (mProfilePresenter.hasUserInfo()) {
                token_user = mProfilePresenter.getUserInfo().getToken();
                //API CHANGE PROFILE
                mProfilePresenter.changeProfile(mDateChange, token_user, "", "",
                        Constant.PROGRESS_STATUS, "", "", "", "", "", "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        updateStatusSwitch(isChecked, isMoreOrTitle + "");
        if (isChecked) {
                    /*Intent intent = new Intent();
                    intent.setClass(SettingScreen.this.getActivity(), LockScreenService.class);
                    SettingScreen.this.getActivity().startService(intent); */
                    Utils.saveLockScreenStatus(SettingScreen.this.getActivity(), true);

            Intent intent = new Intent();
            // BYME
            // intent.setClass(mActivity, LockScreenService.class);
            //mActivity.startService(intent);

            if (mProfilePresenter.hasUserInfo()) {
                token_user = mProfilePresenter.getUserInfo().getToken();
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(3000, 300) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }

                    public void onFinish() {
                        //After 60000 milliseconds (60 sec) finish current
                        //if you would like to execute something when time finishes
                        mProfilePresenter.changeProfile("", token_user, "", "", "1",
                                Constant.allow_ads_ON, "", "", "", "", "");
                    }
                }.start();
            }


        } else {

            Intent intent = new Intent();
            // BYME
            // intent.setClass(mActivity, LockScreenService.class);
            //mActivity.stopService(intent);

            if (mProfilePresenter.hasUserInfo()) {
                token_user = mProfilePresenter.getUserInfo().getToken();
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(3000, 300) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }

                    public void onFinish() {
                        //After 60000 milliseconds (60 sec) finish current
                        //if you would like to execute something when time finishes
                        mProfilePresenter.changeProfile("", token_user, "", "", "0",
                                Constant.allow_ads_OFF, "", "", "", "", "");
                    }
                }.start();
            }

                 /*   Intent intent = new Intent();
                    intent.setClass(SettingScreen.this.getActivity(), LockScreenService.class);
                    SettingScreen.this.getActivity().stopService(intent); */
                    Utils.saveLockScreenStatus(SettingScreen.this.getActivity(), false);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onClick(View v) {
    }

    private class SwitchLockScreenListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (view != null && view.getId() == R.id.sb_text) {
      /*          if (mSwitchLockScreen.isChecked()) {
                    *//*Intent intent = new Intent();
                    intent.setClass(SettingScreen.this.getActivity(), LockScreenService.class);
                    SettingScreen.this.getActivity().startService(intent);
                    Utils.saveLockScreenStatus(SettingScreen.this.getActivity(), false);*//*
                    if (mProfilePresenter.hasUserInfo()) {
                        token_user = mProfilePresenter.getUserInfo().getToken();
                        //API CHANGE PROFILE
                        mProfilePresenter.changeProfile("", token_user, "", "", Constant.PROGRESS_STATUS, Constant.allow_ads_OFF, "");

                    }
                } else {
                    if (mProfilePresenter.hasUserInfo()) {
                        token_user = mProfilePresenter.getUserInfo().getToken();
                        //API CHANGE PROFILE
                        mProfilePresenter.changeProfile("", token_user, "", "", Constant.PROGRESS_STATUS, Constant.allow_ads_ON, "");
                    }
                 *//*   Intent intent = new Intent();
                    intent.setClass(SettingScreen.this.getActivity(), LockScreenService.class);
                    SettingScreen.this.getActivity().stopService(intent);
                    Utils.saveLockScreenStatus(SettingScreen.this.getActivity(), false);*//*
                }*/
            }
        }
    }

    /**
     * firstIntance
     */
    private void firstIntance() {
        isShowProfile = true;
        isShowGuide = false;
        isShowTerm = false;
        isShowFunction = false;
        isShowInfo = false;
        if (mProfileLn != null) {
            mProfileLn.setVisibility(View.VISIBLE);
        }

        //TODO will apply
              /*  mInterestLn.setVisibility(View.VISIBLE);
                mQuestionLn.setVisibility(View.VISIBLE);*/
        ///
        if (mIsVisibleQuestion) {
            mQuestionLn.setVisibility(View.VISIBLE);
        }
        mWvTerm.setVisibility(View.GONE);
        mMainFunctionRl.setVisibility(View.GONE);
        mMainFunctionRl_below.setVisibility(View.GONE);
        seek.setVisibility(View.GONE);
        mWvGuide.setVisibility(View.GONE);

        mWvTerm.setVisibility(View.GONE);
        mWvInfo.setVisibility(View.GONE);
        mMainFunctionRl.setVisibility(View.GONE);
        mMainFunctionRl_below.setVisibility(View.GONE);
        mWvGuide.setVisibility(View.GONE);
        seek.setVisibility(View.GONE);
        mTitleBar.setText(getResources().getString(R.string.profile_Screen));
    }

    public void cleanStack() {
        Fragment fragment;
        fragment = mActivity.getSupportFragmentManager().findFragmentByTag("coupon_detail");
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
        fragment = mActivity.getSupportFragmentManager().findFragmentByTag("coupon_sub_list");
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            try {
              /*  if (mToken_user != null && idAnswerSubmit != null) {
                    mProfilePresenter.submitIdQuestion(mToken_user, idAnswerSubmit);
                    listIdQuestion = ",-1,";
                }*/
                mProfilePresenter.requestQuestion(mToken_user);
                if (CouponsDetailScreen.mMediaPlayer != null) {
                    CouponsDetailScreen.mMediaPlayer.pause();
                    CouponsDetailScreen.mMediaPlayer.setVolume(0f, 0f);
                }

                if(mProfilePresenter.hasUserInfo()) {
                    mTvMobile.setText(mProfilePresenter.getUserInfo().getPhone());
                }
                firstIntance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setUserVisibleHint(final boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible) {
            try {
                if (mToken_user != null && idAnswerSubmit != null) {
                    mProfilePresenter.submitIdQuestion(mToken_user, idAnswerSubmit);
                    listIdQuestion = ",-1,";
                }
                firstIntance();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //cleanStack();
        }
    }

    @Override
    public void networkUnavailable() {

    }

    @Override
    public void networkAvailable() {
        if(mNetworkDisconnected) {
            mNetworkDisconnected = false;
            loadData();
        }
    }

}