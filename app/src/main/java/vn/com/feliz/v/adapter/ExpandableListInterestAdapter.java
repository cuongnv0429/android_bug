package vn.com.feliz.v.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.p.eventbus.LockEvent;
import vn.com.feliz.p.eventbus.idInterest;
import vn.com.feliz.v.fragment.SettingScreen;
import vn.com.feliz.R;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListInterestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int INTEREST = 1;
    private String list = null;
    private List<Item> data;
    private ArrayList<String> ar = new ArrayList<String>();
    private boolean onBind;
    public String listCheck = ",-1,";
    private Handler handler;

    public ExpandableListInterestAdapter(List<Item> data) {
        this.data = data;
        handler = new Handler();
        listCheck = SettingScreen.listId;
    }
/* public ExpandableListInterestAdapter(List<Item> data) {
        this.data = data;
    }*/

    private boolean isSend = false;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        Context context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (18 * dp);
        int subItemPaddingTopAndBottom = (int) (5 * dp);
        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_header_interest, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case INTEREST:
                LayoutInflater inflater_item = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater_item.inflate(R.layout.item_interest, parent, false);
                ListItemInteresstViewHolder item_interest = new ListItemInteresstViewHolder(view);
                return item_interest;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Item item = data.get(position);
        switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.text);
                if (item.invisibleChildren == null) {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.arrow_up_ic);
                } else {
                    itemController.btn_expand_toggle.setImageResource(R.drawable.arrow_down_ic);
                }
                itemController.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // EventBus.getDefault().post(new LockEvent());
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == INTEREST) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.arrow_down_ic);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            itemController.btn_expand_toggle.setImageResource(R.drawable.arrow_up);
                            item.invisibleChildren = null;

                        }
                    }
                });
                break;
            case INTEREST:
                final ListItemInteresstViewHolder item_control = (ListItemInteresstViewHolder) holder;
                TextView itemInterest = (TextView) item_control.mContent;
                itemInterest.setText(data.get(position).text);
                CheckBox itemChoose = (CheckBox) item_control.checkBoxInterest;
                onBind = true;
                if (data.get(position).isCheck == 1) {
                    ar.add(data.get(position).id);
                    if (!listCheck.contains("," + data.get(position).id + ","))
                        listCheck = listCheck + "," + data.get(position).id + ",";

                    itemChoose.setChecked(true);
                } else if (data.get(position).isCheck == 0) {
                    itemChoose.setChecked(false);

                    if (listCheck.contains("," + data.get(position).id + ",")) {
                        listCheck = listCheck.replace("," + data.get(position).id + ",", "");
                    }
                }
                onBind = false;
                itemChoose.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                        if (!onBind) {
                            //handle multi touch
                            EventBus.getDefault().post(new LockEvent());
                            if (!isChecked) {
                                try {
                                    //if (Arrays.asList(ar).contains(data.get(position).id)) {
                                    ar.remove(data.get(position).id);
                                    for (int pos = 0; pos <= data.size() - 1; pos++) {
                                        if (data.get(position).id.equals(data.get(pos).id)) {
                                            data.get(pos).isCheck = 0;
                                        }
                                    }
                             /*   notifyDataSetChanged();*/

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                notifyDataSetChanged();
                                //  }
                            } else if (isChecked) {
                                try {
                                    if (!Collections.singletonList(ar).contains(data.get(position).id)) {
                                        ar.add(data.get(position).id);
                                        for (int posAdd = 0; posAdd <= data.size() - 1; posAdd++) {
                                            if (data.get(position).id.equals(data.get(posAdd).id)) {
                                                data.get(posAdd).isCheck = 1;
                                            }
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                notifyDataSetChanged();
                            }


                        }
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new idInterest(listCheck));

                            }
                        };
                        try {
                            handler.removeCallbacks(r);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        handler.postDelayed(r, Constant.SPLASH_TIME_OUT_SUBMIT);
                    }


                });
                View mLine = (View) item_control.view_line;
                View mLine_top = (View) item_control.view_line_top;
                if (data.get(position).isInvisibleViewLine) {
                    mLine.setVisibility(View.VISIBLE);
                    mLine_top.setVisibility(View.GONE);
                } else {
                    mLine.setVisibility(View.GONE);
                    mLine_top.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public Item refferalItem;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.item_header_name_interest);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.item_arrow_interest);

        }
    }

    private static class ListItemInteresstViewHolder extends RecyclerView.ViewHolder {
        public TextView mContent;
        public CheckBox checkBoxInterest;
        public View view_line, view_line_top;


        public ListItemInteresstViewHolder(View itemView) {
            super(itemView);
            mContent = (TextView) itemView.findViewById(R.id.item_content);
            checkBoxInterest = (CheckBox) itemView.findViewById(R.id.item_check);
            view_line = (View) itemView.findViewById(R.id.interest_view_line);
            view_line_top = (View) itemView.findViewById(R.id.view_item_interest_top);
        }
    }

    public static class Item {
        public int type;
        public String text;
        public int isCheck;
        public boolean isInvisibleViewLine;
        public String id;
        public List<Item> invisibleChildren;


        public Item() {
        }

        public Item(int type, String text, int isCheck, String id, boolean isInvisibleViewLine) {
            this.type = type;
            this.text = text;
            this.isCheck = isCheck;
            this.id = id;
            this.isInvisibleViewLine = isInvisibleViewLine;
        }

        public Item(int type, String text, int isCheck) {
            this.type = type;
            this.text = text;
            this.isCheck = isCheck;
        }
    }
}

