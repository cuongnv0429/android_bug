package vn.com.feliz.v.interfaces.listviewcustom;

/**
 * Created by tuanhai on 11/3/15.
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
