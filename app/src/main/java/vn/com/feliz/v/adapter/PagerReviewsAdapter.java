package vn.com.feliz.v.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import vn.com.feliz.v.fragment.ReviewsAllScreen;
import vn.com.feliz.v.fragment.ReviewsFriendsScreen;
import vn.com.feliz.v.fragment.ReviewsMeScreen;

/**
 * Created by Nguyen Thai Son on 2016-12-16.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class PagerReviewsAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerReviewsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ReviewsAllScreen tabAll = ReviewsAllScreen.newInstance(true);
                return tabAll;
            case 1:
                ReviewsFriendsScreen tabFriend = ReviewsFriendsScreen.newInstance(true);
                return tabFriend;
            case 2:
                ReviewsMeScreen tabYou = ReviewsMeScreen.newInstance(true);
                return tabYou;
            default:

                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            Log.e("PagerReviewsAdapter: ", "Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}