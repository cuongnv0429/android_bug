package vn.com.feliz.v.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.p.RegisterPresenter;

/**
 * Created by ITV01 on 12/23/16.
 */

public class LuckyGibiScreen extends BaseFragment implements OnResponseListener {
    View view;
    RegisterPresenter mRegisterPresenter;
    ImageView imgLucky;
    TextView txtLucky;
    int widthScreen = 0;
    int heightScreen = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_lucky_gibi, container, false);
            initView();
        }
        return view;
    }

    private void initView() {
        mRegisterPresenter = new RegisterPresenter(mActivity, this);
        imgLucky = (ImageView) view.findViewById(R.id.img_lucky);
        txtLucky = (TextView) view.findViewById(R.id.txt_lucky);


        Point size = new Point();
        WindowManager w = getActivity().getWindowManager();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
            w.getDefaultDisplay().getSize(size);
            widthScreen = size.x;
            heightScreen = size.y;
        }else{
            Display d = w.getDefaultDisplay();
            widthScreen = d.getWidth();
            heightScreen = d.getHeight();
        }
        Log.e("heightScreen: ", heightScreen + "");
        imgLucky.getLayoutParams().width = (int)(heightScreen * 0.4);
        imgLucky.getLayoutParams().height = (int) (heightScreen * 0.4);
        imgLucky.requestLayout();
        try {
            if (mRegisterPresenter.hasMetaInfo().getGame_image_url() != null) {
                Glide
                        .with(mActivity)
                        .load(mRegisterPresenter.hasMetaInfo().getGame_image_url())
                        .centerCrop()
                        .placeholder(R.drawable.goldbeetle_ic)
                        .error(R.drawable.goldbeetle_ic)
                        .into(imgLucky);
            } else {
                imgLucky.setBackgroundResource(R.drawable.goldbeetle_ic);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            imgLucky.setBackgroundResource(R.drawable.goldbeetle_ic);
        }

        if (mRegisterPresenter.hasMetaInfo() != null) {
            if (mRegisterPresenter.hasMetaInfo().getGame_message().equalsIgnoreCase("")) {
                txtLucky.setText(getResources().getString(R.string.title_lucky_gibi));
            } else {
                txtLucky.setText(mRegisterPresenter.hasMetaInfo().getGame_message());
            }
        }

        imgLucky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegisterPresenter.hasMetaInfo() != null) {
                    openWebUrl(mRegisterPresenter.hasMetaInfo().getGame_link());
                }
            }
        });
    }

    private void openWebUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        PackageManager packageManager = mActivity.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);

        String default_package;
        default_package = null;

        if (list.size() > 0) {
            for (ResolveInfo resolveInfo : list) {
                if(resolveInfo.isDefault) {
                    default_package = resolveInfo.activityInfo.packageName;
                }
            }

            if(default_package == null) {
                default_package = list.get(0).activityInfo.packageName;
            }

        }

        intent.setPackage(default_package);
        try {
            startActivity(intent);
        } catch (Exception e) {
            intent.setPackage(null);
            startActivity(intent);
        }
    }
}