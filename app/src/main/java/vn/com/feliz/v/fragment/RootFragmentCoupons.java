package vn.com.feliz.v.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import vn.com.feliz.R;
import vn.com.feliz.v.activity.MainActivity;

/**
 * Created by Nguyen Thai Son on 2016-12-24.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RootFragmentCoupons extends BaseFragment {
    RelativeLayout container_list;
    RelativeLayout container_detail;
    private boolean isLoadListVisible;
    public static boolean isLoadCouponCreate;
    public RootFragmentCoupons() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.root_fragment, container, false);
        /* Inflate the layout for this fragment */
        mActivity.mBottom().setVisibility(View.VISIBLE);
        container_list= (RelativeLayout) view.findViewById(R.id.container_list);
        container_detail= (RelativeLayout) view.findViewById(R.id.container_detail);
        if (MainActivity.isFromPushMap) {

            container_detail.setVisibility(View.VISIBLE);
            container_list.setVisibility(View.GONE);
            CouponsBackDetailScreen fr = CouponsBackDetailScreen.newInstance(MainActivity.fromPushMap_coupon_id, MainActivity.fromPushMap_sub_coupon_id);
            FragmentTransaction transaction2 = getChildFragmentManager()
                    .beginTransaction();
            transaction2.add(R.id.container_detail, fr,"back_detail");
            transaction2.commit();
            MainActivity.isFromPushMap = false;
        } else
        if (MainActivity.isFirstRun) {

            container_detail.setVisibility(View.VISIBLE);
            container_list.setVisibility(View.GONE);
            CouponsDetailScreen fr = CouponsDetailScreen.newInstance(MainActivity.couponsIdFirst, "", true);
            FragmentTransaction transaction2 = getChildFragmentManager()
                    .beginTransaction();
            transaction2.add(R.id.container_detail, fr,"detail");
            transaction2.commit();
            MainActivity.isFirstRun = false;

        } else if(!isLoadListVisible){
//            Fragment fragment = mActivity.getSupportFragmentManager().findFragmentByTag("detail");
//            if(fragment != null) {
//                mActivity.getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//            }
           // Toast.makeText(mActivity,"list1",Toast.LENGTH_SHORT).show();
            container_detail.setVisibility(View.GONE);
            container_list.setVisibility(View.VISIBLE);
       /*     getChildFragmentManager().executePendingTransactions();
            FragmentTransaction transaction = getChildFragmentManager()
                    .beginTransaction();
            transaction.remove(null);
            transaction.replace(R.id.container_list,  CouponsScreen.newInstance(null));
            transaction.addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .commitAllowingStateLoss();
            getChildFragmentManager().executePendingTransactions();*/
            FragmentTransaction transaction = getChildFragmentManager()
                    .beginTransaction();
            transaction.addToBackStack(null);
            transaction.add(R.id.container_list, CouponsScreen.newInstance(null));
            transaction.commit();
            isLoadCouponCreate= true;

        }
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        //container_detail.setVisibility(View.GONE);
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && !MainActivity.isFirstRun&& !isLoadListVisible) {
            try {
                //hide bottom ovelay
                container_detail.setVisibility(View.GONE);
                container_list.setVisibility(View.VISIBLE);
                FragmentTransaction transaction = getChildFragmentManager()
                        .beginTransaction();
                transaction.add(R.id.container_list,  CouponsScreen.newInstance(null));
                transaction.addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commitAllowingStateLoss();
//                getChildFragmentManager().executePendingTransactions();
                isLoadListVisible=true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
