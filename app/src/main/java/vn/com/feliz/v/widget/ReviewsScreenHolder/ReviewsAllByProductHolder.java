package vn.com.feliz.v.widget.ReviewsScreenHolder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import vn.com.feliz.R;
import vn.com.feliz.v.interfaces.ItemTouchHelperViewHolder;


/**
 * Created by Mr Son on 2016-08-01.
 */
public class ReviewsAllByProductHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {
    public TextView fullname, tvShowMore, product_review,dateUserProduct;
    public TextView contentReviewbyProduct, isReview_1, isReview_2;
    public ImageView avatar;
    private boolean expand = false;
    public RatingBar rbUserProduct;

    // public LinearLayout mLinearLayoutMain;

    public ReviewsAllByProductHolder(View view) {
        super(view);
        fullname = (TextView) view.findViewById(R.id.tvNameUserProduct);
        tvShowMore = (TextView) view.findViewById(R.id.tvShowMore);
        // tvViewless = (TextView) view.findViewById(R.id.tvViewless);
        contentReviewbyProduct = (TextView) view.findViewById(R.id.contentReviewbyProduct);
        product_review = (TextView) view.findViewById(R.id.product_review);
        isReview_1 = (TextView) view.findViewById(R.id.isReview_1);
        isReview_2 = (TextView) view.findViewById(R.id.isReview_2);
        avatar = (ImageView) view.findViewById(R.id.AvatarUserByProduct);
        rbUserProduct = (RatingBar) view.findViewById(R.id.rbUserProduct);
        dateUserProduct = (TextView) view.findViewById(R.id.dateUserProduct);
        tvShowMore.setOnClickListener(this);

    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == tvShowMore.getId()) {
//            if (!expand) {
//                expand = true;
//                ObjectAnimator animation = ObjectAnimator.ofInt(contentReviewbyProduct, "maxLines", 100);
//                animation.setDuration(100).start();
//                tvShowMore.setText(R.string.view_less);
//                tvShowMore.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_less_xx, 0, 0, 0);
//
//            } else {
//                expand = false;
//                ObjectAnimator animation = ObjectAnimator.ofInt(contentReviewbyProduct, "maxLines", 2);
//
//                animation.setDuration(100).start();
//                tvShowMore.setText(R.string.view_more);
//                tvShowMore.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_expand_xx, 0, 0, 0);
//            }

    }
}
}
