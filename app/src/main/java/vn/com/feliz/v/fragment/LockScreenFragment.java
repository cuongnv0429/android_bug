package vn.com.feliz.v.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.databinding.FragmentLockScreenBinding;
import vn.com.feliz.gbservice1.api.GetAdsDetailService;
import vn.com.feliz.gbservice1.api.GetCouponDetailService;
import vn.com.feliz.gbservice1.api.SubmitAdsService;
import vn.com.feliz.gbservice1.api.SubmitConfirmProductService;
import vn.com.feliz.gbservice1.api.SubmitPushStatusService;
import vn.com.feliz.m.AdsModel;
import vn.com.feliz.m.BaseModel;
import vn.com.feliz.m.CouponAvailableModel;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.NotificationModel;
import vn.com.feliz.m.response.PushResponse;
import vn.com.feliz.v.activity.LockScreenActivity;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.activity.SplashScreen;
import vn.com.feliz.v.adapter.NotificationAdapter;
import vn.com.feliz.v.fragment.dialog.LockScreenRatingFragment;

public class LockScreenFragment extends Fragment implements View.OnClickListener,
        NotificationAdapter.OnItemRecyclerListener, LockScreenAdsFragment.OnAdsItemClick,
        LockScreenRatingFragment.OnRatingItemClicked, ResultCallback<Status> {

    private static final String TAG = LockScreenFragment.class.getSimpleName();
    private FragmentLockScreenBinding mBinding;
    private NotificationReceiver mNotificationReceiver;
    private AdsModel mAdsModel;
    private boolean mIsCloseAds = false;

    public LockScreenFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_lock_screen, container, false);
        mBinding.rvNotification.addItemDecoration(new RecyclerItemDivider(getResources().getDrawable(R.drawable.rv_item_decoration_notification)));
        mBinding.rvNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBinding.rvNotification.setAdapter(new NotificationAdapter(getActivity(), null, this));
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated....");
        updateTime();
//
        mNotificationReceiver = new NotificationReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(getActivity().getPackageName());
        getActivity().registerReceiver(mNotificationReceiver, filter);

        mBinding.logoGibi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContext().startActivity(new Intent(getContext(), SplashScreen.class));
            }
        });

        Log.e(">>>> GET ADS: ", "onActivityCreated");
        getAds();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart....");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHandlerTime != null) mHandlerTime.sendEmptyMessage(1);
        if (getActivity().getIntent() == null) {//normal
            Log.d(TAG, " onResume.... intent null");

        } else {//sent from broadcast SCREEN_ON
            Log.d(TAG, " onResume.... : " + getActivity().getIntent().getAction());
            Fragment fAds = getFragmentManager().findFragmentByTag("fragment_ads");
            if (fAds == null) {//check "fragment_ads" exist or not
                Log.e(">>>> GET ADS: ", "OnResume FAds null");
                //getAds();
            }
        }
        if(mIsCloseAds) {
            mIsCloseAds = false;
            closeAds();
            Log.e(">>>> GET ADS: ", "onResume mIsCloseAds");
            //getAds();//if exits
        }
    }

    private void getAds() {

        Log.e("Lockscreen", ": GET ADS <><><>");

        if (Utils.isNetworkAvailable(getActivity())) {
            PushResponse pushResponse = Utils.getAds(getActivity());
            LockScreenActivity.loading(View.GONE);
            if(pushResponse != null && pushResponse.getPush_type() != null && pushResponse.getAds_id() != null) {
                if (pushResponse.getPush_type().equals("10")) {
                    LockScreenActivity.loading(View.VISIBLE);
                    Intent i2 = new Intent(getActivity(), GetAdsDetailService.class);
                    i2.putExtra("ads_id", pushResponse.getAds_id());
                    i2.putExtra("ads_description", pushResponse.getDescription());
                    getActivity().startService(i2);
                } else {
                    try {
                        List<NotificationModel> notificationModels = new ArrayList<>();
                        NotificationModel nm = new NotificationModel();
                        nm.setDescription(pushResponse.getDescription());
                        nm.setPushType(Integer.parseInt(pushResponse.getPush_type()));
                        nm.setAds_id(pushResponse.getAds_id());
                        nm.setCouponId(pushResponse.getCoupon_id());
                        nm.setCouponSubId(pushResponse.getCoupon_sub_id());
                        if (pushResponse.getPost_time() != null && !pushResponse.getPost_time().equals("")) {
                            try {
                                nm.setPostTime(Long.parseLong(pushResponse.getPost_time()));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        notificationModels.add(nm);
                        if (notificationModels != null) {
                            mBinding.rvNotification.setVisibility(View.VISIBLE);
                            NotificationAdapter adapter = ((NotificationAdapter) mBinding.rvNotification.getAdapter());
                            adapter.setData(notificationModels);
                            adapter.notifyItemChanged(0);
                        }

                        if(pushResponse.getPush_type().equals("6")
                                || pushResponse.getPush_type().equals("7")
                                || pushResponse.getPush_type().equals("8")
                                ) {
                            Intent coupon = new Intent(getContext(), GetCouponDetailService.class);
                            coupon.putExtra("coupon_id", pushResponse.getCoupon_id());
                            getActivity().startService(coupon);
                        }

                    } catch (Exception e) {
                        Utils.saveAds(getContext(), "");
                        e.printStackTrace();
                    }
                }

            } else {
                Utils.saveAds(getContext(), "");
                getActivity().finish();
            }
        } else {
            getActivity().finish();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause....");
        if (mHandlerTime != null) mHandlerTime.removeMessages(1);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop....");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(mNotificationReceiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.vv_Ads:

                break;
            case R.id.iv_ads_more:
                if (Utils.isNetworkAvailable(getActivity())) {
                    updateAds("1", "0", "");
                    if (mAdsModel != null && !TextUtils.isEmpty(mAdsModel.getAds().getLink())) {
                        if (mAdsModel.getAds().getIsBrowser().equalsIgnoreCase("1")) {
                            // open in browser
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mAdsModel.getAds().getLink()));
                            startActivity(intent);
                        } else {
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.fl_container, LockScreenMore.newInstance(mAdsModel.getAds().getLink()), "coupon_detail")
                                    .addToBackStack(null)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                        }

                    }
                }
                break;
            case R.id.iv_ads_close:
                closeAds();
                if (Utils.isNetworkAvailable(getActivity())) {
                    closeNotification(mAdsModel.getAds().getAdsId());
                    updateAds("5", "0", "");
                }
                break;
            case R.id.iv_ads_repeat:
                closeAds();
                if (Utils.isNetworkAvailable(getActivity())) {
                    dismissNotification(mAdsModel.getAds().getAdsId());
                } else {
                    //Utils.showNoInternetMessage(getActivity(), null);
                }
                break;
            case R.id.iv_ads_send:
                if(mAdsModel.getAds() != null) {
                    if (Utils.isNetworkAvailable(getActivity())) {
                        updateAds("3", "0", "");
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, mAdsModel.getAds().getShareTitle());
                        sendIntent.putExtra(Intent.EXTRA_TEXT, mAdsModel.getAds().getShareLink());
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, "Gửi"));
                    }
                }
                break;
            case R.id.ll_ads://only close ads, do not perform anything else
                try {
                    getFragmentManager().beginTransaction().remove(getFragmentManager().findFragmentByTag("fragment_ads")).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.v_dot:
                updateAds("2", "0", "");
                break;
        }
    }

    @Override
    public void onItemClicked(View v, int pos) {
        NotificationAdapter adapter = (NotificationAdapter) mBinding.rvNotification.getAdapter();
        int type = 10001;
        try {
            type = adapter.getData().get(pos).getPushType();
        } catch (Exception e) {
            e.printStackTrace();
        }
        switch (type) {
            case 1:
                dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 0);
                break;
            case 2:
                /*dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 2);*/
                switch (v.getId()) {
                    case R.id.btn_ok:
                        openApplication(getActivity().getPackageName(), 2);
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
            case 3:
                switch (v.getId()) {
                    case R.id.btn_ok:
                        openApplication(getActivity().getPackageName(), 3);
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
            case 4:
                dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 0);
                break;
            case 5://map
                switch (v.getId()) {
                    case R.id.btn_ok:
                        openApplication(getActivity().getPackageName(), 5, adapter.getData().get(pos));
                        submitPushStatus(adapter.getData().get(pos).getCouponId(), 2);
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        //remind in next 30m
                        //checkShowNotificationAgain(adapter.getData().get(pos), 1800);
                        submitPushStatus(adapter.getData().get(pos).getCouponId(), 1);
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
            case 6:
                switch (v.getId()) {
                    case R.id.btn_ok:
                        openApplication(getActivity().getPackageName(), 6, adapter.getData().get(pos));
                        closeNotification(adapter.getData().get(pos), 1);
                        //dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
            case 7:
                switch (v.getId()) {
                    case R.id.btn_ok:
                        openApplication(getActivity().getPackageName(), 7, adapter.getData().get(pos));
                        closeNotification(adapter.getData().get(pos), 1);
                        //dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
                //Toast.makeText(getContext(), "Open screen", Toast.LENGTH_LONG).show();
            case 8:
                switch (v.getId()) {
                    case R.id.btn_ok:
//                        Intent coupon = new Intent(getContext(), GetCouponDetailService.class);
//                        coupon.putExtra("coupon_id", adapter.getData().get(pos).getCouponId());
//                        getActivity().startService(coupon);
                        if(adapter.getData().get(pos) != null) {
                            if(adapter.getData().get(pos).getCoupon() != null) {
                                updateAds("3", "0", "");
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_SUBJECT, adapter.getData().get(pos).getCoupon().getShareTitle());
                                sendIntent.putExtra(Intent.EXTRA_TEXT, adapter.getData().get(pos).getCoupon().getShareLink());
                                sendIntent.setType("text/plain");
                                startActivity(Intent.createChooser(sendIntent, "Gửi"));
                            }
                        }

                        //openApplication(getActivity().getPackageName(), 8, adapter.getData().get(pos));

                        //dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.btn_cancel:
                        dismissNotification(adapter.getData().get(pos));
                        break;
                    case R.id.lockscreen_close:
                        closeNotification(adapter.getData().get(pos));
                        break;
                }
                break;
            case 9:
                dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 0);
                //Toast.makeText(getContext(), "Open screen", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Open screen");
                break;
            case 12:
                dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 12);
                break;
            case R.id.rl_notification_root:
                dismissNotification(adapter.getData().get(pos));
                openApplication(adapter.getData().get(pos).getPackageName(), 0);
                //Toast.makeText(getContext(), "Notification of others", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Notification of others");
                break;
            default:
                break;
        }
    }

    @Override
    public void onAdsItemClicked(View v) {
        onClick(v);
    }

    @Override
    public void onLookedAtAds(String adsId) {
        updateAds(adsId, "2", "0", "");
    }

    @Override
    public void onRatingItemClick(View v, String couponId, ArrayList<String> selectedProductsId) {
        switch (v.getId()) {
            case R.id.btn_confirm:
                submitProduct(couponId, selectedProductsId);
                break;
        }
    }

    @Override
    public void onResult(@NonNull Status status) {
        if (status.isSuccess()) {
            Log.d(TAG, "geofence adding or removing is ok");
        } else {
            Log.d(TAG, "geofence adding or removing is failed");
        }
    }

    private void submitProduct(String couponId, ArrayList<String> selectedProductsId){
        Intent confirmCoupon = new Intent(getContext(), SubmitConfirmProductService.class);
        confirmCoupon.putExtra("coupon_id", couponId);
        confirmCoupon.putStringArrayListExtra("list_product_id", selectedProductsId);
        getActivity().startService(confirmCoupon);
    }

    private void submitPushStatus(String couponId, int status){
        Intent submitPushStatus = new Intent(getContext(), SubmitPushStatusService.class);
        submitPushStatus.putExtra("coupon_id", couponId);
        submitPushStatus.putExtra("status", status);
        getActivity().startService(submitPushStatus);
    }

    private void dismissNotification(NotificationModel notification){

        Log.i(TAG,"onNotificationRemoved...");
        Intent i = new  Intent(getContext().getPackageName());
        i.putExtra(Constant.ACTION, Constant.ACTION_REMOVE_SYSTEM_NOTIFICATION);
        i.putExtra("notification_event", notification);
        getActivity().sendBroadcast(i);
    }

    private void dismissNotification(String ads_id){

        Log.i(TAG,"onNotificationRemoved...");
        Intent i = new  Intent(getContext().getPackageName());
        i.putExtra(Constant.ACTION, Constant.ACTION_REMOVE_SYSTEM_NOTIFICATION);
        i.putExtra("ads_event_id", ads_id);
        getActivity().sendBroadcast(i);
    }

    private void closeNotification(NotificationModel notification) { closeNotification(notification, 0); }

    private void closeNotification(NotificationModel notification, int no_open_ads) {
        Log.i(TAG,"onNotificationRemoved...");
        Intent i = new  Intent(getContext().getPackageName());
        i.putExtra(Constant.ACTION, Constant.ACTION_CLOSE_SYSTEM_NOTIFICATION);
        i.putExtra("notification_event", notification);
        i.putExtra("ads_event_no_open_ads", no_open_ads);
        getActivity().sendBroadcast(i);
    }

    private void closeNotification(String ads_id) {
        closeNotification(ads_id, 0);
    }

    private void closeNotification(String ads_id, int no_open_ads) {
        Log.i(TAG,"onNotificationRemoved...");
        Intent i = new  Intent(getContext().getPackageName());
        i.putExtra(Constant.ACTION, Constant.ACTION_CLOSE_SYSTEM_NOTIFICATION);
        i.putExtra("ads_event_id", ads_id);
        i.putExtra("ads_event_no_open_ads", no_open_ads);
        getActivity().sendBroadcast(i);
    }

    private void openApplication(String packageName, int code){
        openApplication(packageName, code, null);
    }

    private void openApplication(String packageName, int code, NotificationModel notification){//code 1 open setting if this is our package
        if (getActivity().getPackageName().equals(packageName)){//our app
            Intent i = new Intent(getActivity(), MainActivity.class);
            if (notification != null) {
                i.putExtra("coupon_id", notification.getCouponId());
                i.putExtra("coupon_sub_id", notification.getCouponSubId());
            }
            i.putExtra("push_type", code);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }else {//other app
            Intent launchIntent = getActivity().getPackageManager().getLaunchIntentForPackage(packageName);
            if (launchIntent != null) {
                launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(launchIntent);
            }
        }
        getActivity().finish();
    }

    private void closeAds(){
        try {
            Fragment fragment;
            fragment = getActivity().getSupportFragmentManager().findFragmentByTag("fragment_ads");
            if (fragment != null) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            }
            //Utils.saveAds(getContext(), "");//remove ads
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Handler mHandlerTime;
    private Runnable r = null;
    /**
     * update current time every second
     */
    private void updateTime() {
        final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        final SimpleDateFormat dateFormat = new SimpleDateFormat("EE, dd MMM");
        mHandlerTime = new Handler(getActivity().getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what==1) {
                    mBinding.tvTime.setText(timeFormat.format(Calendar.getInstance().getTime()));
                    mBinding.tvDate.setText(dateFormat.format(Calendar.getInstance().getTime()));
                    mHandlerTime.sendEmptyMessageDelayed(1, 1000);
                }
            }
        };
    }

    private void updateAds(String status, String repeatTime, String shareLink) {
        updateAds(null, status, repeatTime, shareLink);
    }

    private void updateAds(String adsId, String status, String repeatTime, String shareLink) {
        if (getActivity() != null) {
            Intent i = new Intent(getActivity(), SubmitAdsService.class);
//        Log.e(">>>>>> Update ads", "Get Ads REPEAT: "+adsId+" - "+status);
            if (adsId == null) {
                try {
                    i.putExtra("ads_id", Utils.getAds(getActivity()).getAds_id());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else {
                i.putExtra("ads_id", adsId);
            }
            i.putExtra("status", status);
            i.putExtra("repeat_time", repeatTime);
            i.putExtra("share_link", shareLink);
            getActivity().startService(i);
        }

    }

    class NotificationReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            LockScreenActivity.loading(View.GONE);
            final int action = intent.getIntExtra(Constant.ACTION, 0);
            NotificationModel notification;
            String ads_id = "";
            switch (action) {
                case Constant.ACTION_SUBMIT_ADS:
                    AdsModel ads = (AdsModel) intent.getSerializableExtra("ads");
                    if (ads != null && ads.getCode().equals(Constant.CODE_SUCCESS)) {
                        Log.e("Lockscreen request", "SUBMIT ADS OK");
                        //Toast.makeText(getActivity(), "Đã cập nhật", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constant.ACTION_GET_ADS:
                    mAdsModel = (AdsModel) intent.getSerializableExtra("ads");
                    String description = intent.getStringExtra("ads_description");
                    if (mAdsModel != null) {
                        if (mAdsModel.getCode().equals(Constant.CODE_SUCCESS)) {
                            if (getFragmentManager().findFragmentByTag("fragment_ads") == null) {

                                if(description != null) {
                                    AdsModel.Ads mAds = mAdsModel.getAds();
                                    mAds.setDescription(description);
                                    mAdsModel.setAds(mAds);
                                }

                                LockScreenAdsFragment adss = LockScreenAdsFragment.newInstance(mAdsModel);
                                adss.setListener(LockScreenFragment.this);
                                FragmentTransaction ts = getFragmentManager().beginTransaction();
                                ts.add(R.id.fl_container, adss, "fragment_ads");
                                ts.commitAllowingStateLoss();
                                updateAds("0", "0", "");
                            }
                        } else {
                            String adsId = intent.getStringExtra("ads_id");
                            if(adsId != null) {
                                //closeNotification(adsId);
                            }
                            Utils.saveAds(getActivity(), "");
                            closeAds();
//                            Log.e(">>>>> Get Ads", "load ads failure, "+mAdsModel.getCode()+" - "+mAdsModel.getMessage());
                            getAds();
                            //Utils.showMessageNoTitle(getContext(), mAdsModel.getMessage(), null);
                            if(mAdsModel.getMessage() != null) {
                                Log.e(TAG, mAdsModel.getMessage());
                            } else {
                                Log.e(TAG, "mAdsModel.getMessage() is NULL");
                            }
                        }
                    }
                    break;
//                case Constant.ACTION_GET_SYSTEM_NOTIFICATION_LIST:
//                    try {
//                        List<NotificationModel> notificationModels = (List<NotificationModel>) intent.getSerializableExtra("notification_event");
//                        if (notificationModels != null) {
//                            ((NotificationAdapter) mBinding.rvNotification.getAdapter()).addData(notificationModels);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    break;
//                case Constant.ACTION_GET_SYSTEM_NOTIFICATION_POSTED:
//                    try {
//                        notification = (NotificationModel) intent.getSerializableExtra("notification_event");
//                        if (notification != null) {
//                            ((NotificationAdapter) mBinding.rvNotification.getAdapter()).addData(notification);
//                        }
//                        mBinding.rvNotification.setVisibility(View.GONE);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    break;
                case Constant.ACTION_REMOVE_SYSTEM_NOTIFICATION:
                    ads_id = intent.getStringExtra("ads_event_id");
                    Utils.deleteAds(getContext(), ads_id);
                    Log.e(">>>> GET ADS: ", "ACTION REMOVE");
                    updateAds(ads_id, "4", "1", "");
                    getAds();
                    break;
                case Constant.ACTION_CLOSE_SYSTEM_NOTIFICATION:
                    ads_id = intent.getStringExtra("ads_event_id");
                    int ads_event_no_open = intent.getIntExtra("ads_event_no_open_ads", 0);
                    Utils.deleteAds(getContext(), ads_id);
                    Log.e(">>>> GET ADS: ", "ACTION CLOSE");
                    updateAds(ads_id, "5", "0", "");
                    if(ads_event_no_open == 0) {
                        getAds();
                    } else {
                        if(getActivity() != null) {
                            getActivity().finish();
                        }
                    }
                    break;
                case Constant.ACTION_GET_COUPON_DETAIL:
                    final CouponModel couponModel = (CouponModel)intent.getSerializableExtra("coupon");
                    if (couponModel != null && Constant.CODE_SUCCESS.equals(couponModel.getCode())) {
                        try {
                            if (couponModel.getCoupon().getListProduct().size() > 0) {//have many products

                                ((NotificationAdapter) mBinding.rvNotification.getAdapter()).setCoupon(couponModel.getCoupon());
//                                LockScreenRatingFragment dialog = LockScreenRatingFragment.newInstance(couponModel);
//                                dialog.show(getFragmentManager(), "dialog");
//                                dialog.setListener(LockScreenFragment.this);
                            }else {//single product
                                submitProduct(couponModel.getCoupon().getCouponId(), new ArrayList<String>(){{add(couponModel.getCoupon().getListProduct().get(0).getId());}});
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        closeAds();
                    }
                    break;
                case Constant.ACTION_SUBMIT_CONFIRM_PRODUCT:
                    BaseModel model = (BaseModel) intent.getSerializableExtra("base");
                    if (model != null && Constant.CODE_SUCCESS.equals(model.getCode())){
                        Log.d("Lockscreen request", "ACTION_SUBMIT_CONFIRM_PRODUCT OK");
                        //Toast.makeText(getActivity(), "Đã cập nhật", Toast.LENGTH_SHORT).show();
                    }
                    break;
                case Constant.ACTION_HAVE_ADS:
                    Fragment fAds = getFragmentManager().findFragmentByTag("fragment_ads");
                    if (fAds == null) {//check "fragment_ads" exist or not
                        Log.e(">>>> GET ADS: ", "ACTION HAVE ADS");
                        getAds();
                    }
                    break;
                case Constant.ACTION_GET_COUPON_AVAILABLE_LIST:
                    CouponAvailableModel couponAvailables = (CouponAvailableModel) intent.getSerializableExtra("coupon_available");
                    if (couponAvailables == null) return;
                    Utils.saveCouponAvailable(getContext(), couponAvailables.getCouponAvailable());
                    break;
                default:
                    Log.e(TAG, "NotificationReceiver receive action: " + action);
                    break;
            }
        }
    }

    public class RecyclerItemDivider extends RecyclerView.ItemDecoration {

        private Drawable mDivider;
        public RecyclerItemDivider(Drawable line) {
            mDivider = line;
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();
            final int childCount = parent.getChildCount();
            final int dividerHeight = mDivider.getIntrinsicHeight();

            for (int i = 1; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
                final int ty = (int)(child.getTranslationY() + 0.5f);
                final int top = child.getTop() - params.topMargin + ty;
                final int bottom = top + dividerHeight;
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
}