package vn.com.feliz.v.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.databinding.FragmentCouponDetailScreenBinding;
import vn.com.feliz.gbservice1.api.GetCouponDetailService;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.ProductUsedRating;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.p.eventbus.refreshData;
import vn.com.feliz.v.activity.BaseActivity;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.adapter.AdsPagerAdapter;


public class CouponsDetailScreen extends BaseFragment implements View.OnClickListener,
        View.OnTouchListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, SeekBar.OnSeekBarChangeListener,
        OnResponseListener, OnPostResponseListener{
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    protected FragmentCouponDetailScreenBinding mBinding;
    private CouponDetailReceiver mReceiver;
    private String mCouponId;
    private String mCouponSubId;
    private Drawable mDotNormal;
    private Drawable mDotActive;
    public static MediaPlayer mMediaPlayer;
    private boolean isMute;
    private boolean mFromRegister;
    private int mPagerPosition;
    private int mColorGray;
    private int mCurrentUnitNumber = 0;
    private int MAX_UNIT_NUMBER = 0;
    private boolean mEndVideo;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private Location mLastLocation;
    private int mDeviceWidth;
    private Handler handler = new Handler();
    private boolean mStopIntroduction = false;
    private String mCurrentCouponSubId = "";
    private ProgressDialog mProgressDialog;
    private TSnackbar snackbar;
    SharedPreferences settings;
    private CouponsPresenter couponsPresenter;
    private static final int REQUEST_APP_SETTINGS = 168;
    private boolean updateChangeGift = false;
    private boolean updateStatusProductUsed = false;
    private String message = "";
    private String statusDoNotOrAlready = "1";
    private List<ProductUsedRating> productUsedRatings = new ArrayList<>();
    private boolean loadCouponDetail = false;
	private Dialog mDialog;
    private ProductUsedRating updateProductUsedRating;
    int position = 0;
    ///no call services when getCouponDetil from CouponsBackDetailScreen
    public static boolean updateLocationDeatail = false;
    String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    // private boolean isEnableChangeGuide;
    //public static final String MYPREFSFILE_GUIDE_CHANGE = Constant.MYPREFSFILE_GUIDE_CHANGE;
    BaseApplication mBaseApplication;
    Tracker mTracker;
    private Runnable runnable = new Runnable() {
        public void run() {
            if (mBinding.vpAds.getAdapter() != null) {
                if (mPagerPosition >= mBinding.vpAds.getAdapter().getCount()) {
                    mPagerPosition = 0;
                } else {
                    mPagerPosition++;
                }
                try {
                    mBinding.vpAds.setCurrentItem(mPagerPosition, true);
                    handler.postDelayed(runnable, 5000);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    };
    private Handler mHandlerVideo = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (mBinding.vvAds.isPlaying()) {
                    position = mBinding.vvAds.getCurrentPosition();
                    int duration = mBinding.vvAds.getDuration();
                    int progress = position * 100 / (duration == 0 ? 1 : duration);
                    Log.e("position", "" + Utils.convertLongTimeToString(position));
                    mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(position));
                    mBinding.tvEndProgress.setText(Utils.convertLongTimeToString(duration));
                    mBinding.sbVideoProgress.setProgress(progress);
                    mBinding.sbVideoProgress.setSecondaryProgress(mBinding.vvAds.getBufferPercentage());
                    Log.d("HoangNM", "pos-dur: "  + mBinding.vvAds.getBufferPercentage());
                    if (position >= (duration - 1000)) {
                        Log.d("HoangNM", "pause here.....");
                        if (mMediaPlayer != null) {
                            mMediaPlayer.pause();
                            mHandlerVideo.removeMessages(1);
                            mEndVideo = true;
                            mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                            mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(duration));
                            mBinding.tvEndProgress.setText(Utils.convertLongTimeToString(duration));
                            mBinding.sbVideoProgress.setProgress(100);
                        }
                    } else {
                        mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                    }
                }
            }
        }
    };
    private CouponModel.Coupon mCoupon;

    public CouponsDetailScreen() {
        // Required empty public constructor
    }

    public static CouponsDetailScreen newInstance(CouponModel.Coupon coupon) {
//        Log.e("TAG", coupon.getEsComericalText());
        CouponsDetailScreen fragment = new CouponsDetailScreen();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM3, coupon);
        fragment.setArguments(args);
        return fragment;
    }

    public static CouponsDetailScreen newInstance(String couponId, String couponSubId, boolean fromRegister) {
        CouponsDetailScreen fragment = new CouponsDetailScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, couponId);
        args.putBoolean(ARG_PARAM2, fromRegister);
        args.putString(ARG_PARAM4, couponSubId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().post(this);
        //  EventBus.getDefault().post(new SetTitleMessage(getString(R.string.coupons_Screen)));
       /* settings = mActivity.getSharedPreferences(MYPREFSFILE_GUIDE_CHANGE, 0);
        isEnableChangeGuide = settings.getBoolean(Constant.CHANGE_COUPON, true)*/
        ;

        Log.i("Back to coupon detail", "aa");

        //hideSystemUI();

        CouponsScreen.onBackFromDetail = true;
//        updateLocationDeatail = true;
        if (getArguments() != null) {
            mCouponId = getArguments().getString(ARG_PARAM1);
            mCouponSubId = getArguments().getString(ARG_PARAM4, "");
            mFromRegister = getArguments().getBoolean(ARG_PARAM2, false);
            mCoupon = (CouponModel.Coupon) getArguments().getSerializable(ARG_PARAM3);
        }

        MainActivity.not_from_list = true;

    }

    // This snippet hides the system bars.
    private void hideSystemUI() {
        // Set the IMMERSIVE flag.
        // Set the content to appear under the system bars so that the content
        // doesn't resize when the system bars hide and show.
//        View decorView = getActivity().getWindow().getDecorView();
//
//        decorView.setSystemUiVisibility(
//                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupon_detail_screen, container, false);
      /*  if(isEnableChangeGuide){
            mBinding.lnGuide.setVisibility(View.VISIBLE);
           mActivity.mBottom().setVisibility(View.GONE);
        }*/

            Log.i("Back to coupon detail", "xx");
            Log.i("Check temp stop >> ", "" + mStopIntroduction);
            if (isAdded()) {
                initView();
            }

        }
        return mBinding.getRoot();
    }
    private void initView() {
        mBaseApplication = BaseApplication.getInstance();
        mTracker = mBaseApplication.getDefaultTracker();

        mActivity.mBottom().setVisibility(View.VISIBLE);
        mBinding.vvAds.setOnTouchListener(this);
        mBinding.ivLocation.setOnClickListener(this);
        mBinding.ivRating.setOnClickListener(this);
        mBinding.btnGift.setOnClickListener(this);
        mBinding.ivPlayVideo.setOnClickListener(this);
        mBinding.sbVideoProgress.setOnSeekBarChangeListener(this);
        mBinding.ivSpeaker.setOnClickListener(this);
        //cuongnv
        mBinding.layoutProduct.imgCircleCheck1.setOnClickListener(this);
        mBinding.layoutProduct.imgCircleCheck2.setOnClickListener(this);
        mBinding.layoutProduct.imgCircleCheck3.setOnClickListener(this);
        mBinding.btnDoNot.setOnClickListener(this);
        mBinding.btnAlready.setOnClickListener(this);
        //----
        mBinding.llIntroductionOption.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mBinding.vIntroductionDot.setBackgroundResource(R.drawable.check_org_ck);
                        break;
                    case MotionEvent.ACTION_UP:
                        mBinding.vIntroductionDot.setBackgroundResource(R.drawable.check_org);
                        break;
                }
                return false;
            }});

        mBinding.rlIntroductionBackground.setOnClickListener(this);
        mBinding.llIntroductionOption.setOnClickListener(this);

        if (mActivity != null && mActivity instanceof MainActivity) {
            ((MainActivity) mActivity).getmViewOverlayBottomBar().setOnClickListener(this);
        }

        mColorGray = ContextCompat.getColor(getContext(), R.color.couponscreen_text_gray);
        mDotNormal = ContextCompat.getDrawable(getContext(), R.drawable.lockscreen_ads_dot_normal);
        mDotActive = ContextCompat.getDrawable(getContext(), R.drawable.lockscreen_ads_dot_active);
        mBinding.mainTopbar.mainTopBarTitle.setText(R.string.coupons_Screen);
        mBinding.mainTopbar.mainTopbarBackLn.setOnClickListener(this);
        mBinding.mainTopbar.tvInvite.setOnClickListener(this);


        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        mDeviceWidth = displayMetrics.widthPixels;
        ViewGroup.LayoutParams lp = mBinding.flImage.getLayoutParams();
        lp.height = mDeviceWidth;
        mBinding.layoutProduct.linearCheckUsedProduct.getLayoutParams().height = mDeviceWidth;
        mBinding.flImage.setLayoutParams(lp);
        ViewGroup.LayoutParams lp1 = mBinding.ivIntroductionCenter.getLayoutParams();
        lp1.height = mDeviceWidth;
        mBinding.ivIntroductionCenter.setLayoutParams(lp1);

        if (mCoupon == null) {
            //getCouponDetail(mCouponId);
        } else {

            mCurrentCouponSubId = mCoupon.getCouponSubId();
            if (MainActivity.reload_detail) {
                MainActivity.reload_detail = false;
                updateLocationDeatail = false;
                getCouponDetail(mCoupon.getCouponId(), "");
            } else {
                showCouponDetail(mCoupon);
            }
        }
        couponsPresenter = new CouponsPresenter(mActivity, this);
//            Log.e("token: ", couponsPresenter.getUserInfo().getToken());
        createGoogleApiClient();
        //cuongnv
        //listener event review add
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof ProductUsedRating) {
                    MainActivity.not_from_list = true;
                    loadCouponDetail = true;
                    updateProductUsedRating = (ProductUsedRating) o;
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mReceiver = new CouponDetailReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(getActivity().getPackageName());
        getActivity().registerReceiver(mReceiver, filter);
        mActivity.mBottom().setVisibility(View.VISIBLE);


        if(MainActivity.fromPushType == 6) {
            MainActivity.fromPushType = 0;
            if(mCoupon != null) {
                if(couponsPresenter.getUserInfo() != null) {
                    String token = couponsPresenter.getUserInfo().getToken();
                    if(token != null) {
                        submitStatusCoupon(token, mCoupon.getCouponId(), mCoupon.getCouponSubId()!=null?mCoupon.getCouponSubId():"", "2");
                    }
                }
            }
        }
    }

  /*  @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COUPONS;
    }*/

    @Override
    public void onPause() {
        super.onPause();
        //if (mMediaPlayer != null) mMediaPlayer.start();
        //Toast.makeText(mActivity,"OK",Toast.LENGTH_SHORT).show();
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.pause();
                mMediaPlayer.setVolume(0f, 0f);
                mBinding.ivSpeaker.setImageResource(R.drawable.l2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        String token = "";
        switch (v.getId()) {
            case R.id.main_topbar_back_ln:
                // mActivity.getSupportFragmentManager().popBackStack();
//                mActivity.getSupportFragmentManager().popBackStack();

                if (mFromRegister) {
                    Log.i("popBackStack", "A");
                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                } else {
                    Log.i("popBackStack", "B");
                    if (!MainActivity.reload_list) {
                        if (!MainActivity.from_sub_list) {
                            MainActivity.not_from_list = false;
                            MainActivity.from_sub_list = false;
                        }
                        mActivity.getSupportFragmentManager().popBackStack();
                    } else {
                        if (mCoupon.getList() != null && mCoupon.getList().size() > 1) {
                            mActivity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.root_frame, CouponsSubScreen.newInstance(mCoupon, true))
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                        } else {
                            mActivity.getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.root_frame, CouponsScreen.newInstance(null, false))
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                        }
                    }
                }

                break;
            case R.id.iv_rating:
                //Toast.makeText(getContext(), "Tính năng này đang được phát triển", Toast.LENGTH_SHORT).show();
                viewDetailReview(mCoupon);
                break;
            case R.id.iv_location:
                if (mCoupon != null) {
                    if (!BaseActivity.hasPermissions(mActivity, PERMISSIONS_LOCATION)) {
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                            checkAndRequestPermissionsLoaction();
                        }
                    } else {
                        if (Utils.isLocationEnabled(mActivity)) {
//                            if (mCoupon.getShops() == null) {
//                                Toast.makeText(getContext(), "Không tìm thấy cửa hàng nào cho coupon này.", Toast.LENGTH_LONG).show();
//                                break;
//                            }
                            boolean isPlaying = false;
                            if (mMediaPlayer != null && mBinding.vvAds.isPlaying()) {
                                mBinding.vvAds.pause();
                                mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                                mHandlerVideo.removeMessages(1);
                                isPlaying = true;
                            }
                            Bundle arguments = new Bundle();
//                            Log.e("getCouponSubId", mCoupon.getCouponSubId());
                            arguments.putString(ARG_PARAM2, mCoupon.getCouponId());
                            arguments.putString(ARG_PARAM4, mCoupon.getCouponSubId());
                            if (mCoupon.getShops() == null) {
                                arguments.putString("list_shop_message", mCoupon.getListShopMessage());
                            }
                            CouponsBackDetailScreen fragment = CouponsBackDetailScreen.newInstance(mCoupon.getShops(), isPlaying);
                            fragment.setArguments(arguments);

                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.root_frame, fragment)
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
                        } else {
                            Utils.displayPromptForEnablingGPS(mActivity);
                        }
                    }

                } else {
                    Toast.makeText(getContext(), "Đang lấy dữ liệu...", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_gift:
                if (mCoupon != null && mCoupon.getStatus() != null) {
                    if (mCoupon.getIsReview() != null) {
                        if (mCoupon.getIsReview().equalsIgnoreCase("3") && mBinding.btnGift.getText().toString()
                                .equalsIgnoreCase(getResources().getString(R.string.text_rated_the_product_to_receive_coins))) {
                            if (mCoupon.getListProduct() != null) {
                                for (int i = 0 ; i < mCoupon.getListProduct().size(); i++) {
                                    if (!mCoupon.getListProduct().get(i).getIs_review().equalsIgnoreCase("1")) {
                                        mBinding.btnGift.setText(getResources().getString(R.string.text_confirm_already_product));
                                        mBinding.layoutProduct.linearCheckUsedProduct.setVisibility(View.VISIBLE);
                                        mBinding.flImage.setVisibility(View.GONE);
                                        displayCheckProduct(mCoupon);
                                        break;
                                    }
                                }

                            }
                            break;
                        } else if (mCoupon.getIsReview().equalsIgnoreCase("3") && mBinding.btnGift.getText().toString()
                                .equalsIgnoreCase(getResources().getString(R.string.text_confirm_already_product))) {
                            String parseProduct = new Gson().toJson(productUsedRatings).toString();
                            getActivity().getSupportFragmentManager()
                                    .beginTransaction()
                                    .replace(R.id.root_frame, FragmentCouponReview.newInstance(parseProduct))
                                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                    .addToBackStack(null)
                                    .commitAllowingStateLoss();
                            getChildFragmentManager().executePendingTransactions();
//                            if (isRating != 0) {
//
//                            } else {
//                                snackbar = TSnackbar
//                                        .make(getView(), getResources().getText(R.string.alert_validate_all_product_reviewed), TSnackbar.LENGTH_SHORT);
//                                Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_validate_all_product_reviewed), mActivity);
//                                Utils.vibarte(mActivity);
//                            }
                            break;
                        }
                    }

                    String coupon_status = mCoupon.getStatus();
                    if(coupon_status.equals(Constant.COUPON_STATUS_NEW)) {
                        coupon_status = Constant.COUPON_STATUS_NOT_EXCHANGE_YET;
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.custom_dialog, null);
                    builder.setView(dialogView);
                    LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
                    TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                    tvYes.setText(getString(R.string.dialog_retry_yes));
                    LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
                    TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                    tvNo.setText(getString(R.string.dialog_retry_no));
                    TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
                    contentDialog.setText(getString(R.string.alert_confirm_buy_counpon));
                    final Dialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCancelable(false);

                    switch (coupon_status) {

                        case Constant.COUPON_STATUS_EXCHANGED:
                            break;
                        case Constant.COUPON_STATUS_EXPIRED:
                            break;
                        case Constant.COUPON_STATUS_CAN_BUY:
                            if (mCoupon.getEsComericalPopupText().equalsIgnoreCase("")) {
                                contentDialog.setText(getString(R.string.alert_confirm_buy_counpon_ec));
                            } else {
                                contentDialog.setText(mCoupon.getEsComericalPopupText());
                            }
                            dialog.show();
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    mProgressDialog = Utils.showLoading(mActivity);

                                    String token = couponsPresenter.getUserInfo().getToken();

                                    if (!token.equalsIgnoreCase("")) {
//                                        Log.e("couponId", mCoupon.getCouponId());
                                        if(mCoupon.getCouponSubId() != null) {
                                            buyCouponEC(token, mCoupon.getCouponId(), mCoupon.getCouponSubId());
                                        } else {
                                            buyCouponEC(token, mCoupon.getCouponId(), "");
                                        }
                                    } else {
                                        Utils.hideLoading(mProgressDialog);
                                    }
                                }
                            });
                            btnNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            break;

                        case Constant.COUPON_STATUS_NOT_EXCHANGE_YET:
                            if (mCurrentUnitNumber == 0 && mCoupon.getQuantity() > 1) {//have more one
                                showUnitNumber();
                            } else {
                                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                                    if (!BaseActivity.hasPermissions(mActivity, Constant.PERMISSIONSCAMERA)) {
                                        checkAndRequestPermissionsCamera();
                                    } else {
                                        if (mCurrentUnitNumber == 0)
                                            mCurrentUnitNumber = 1;//fix for coupon have only 1 unit
                                        ScanbarcodeScreen fragment = new ScanbarcodeScreen();
                                        Bundle arguments = new Bundle();
                                        arguments.putString(Constant.F_ARG_COUPON_ID, mCoupon.getCouponId());
                                        arguments.putString(Constant.F_ARG_SUB_COUPON_ID, mCoupon.getCouponSubId());
                                        arguments.putString(Constant.F_ARG_COUPON_BARCODE, mCoupon.getCouponBarCode());
                                        arguments.putBoolean("fromRegister", mFromRegister);
                                        arguments.putInt(Constant.F_ARG_COUPON_TOTAL, mCurrentUnitNumber);
                                        fragment.setArguments(arguments);

                                        mActivity.getSupportFragmentManager()
                                                .beginTransaction()
                                                .replace(R.id.root_frame, fragment, "fragment_scan")
                                                .addToBackStack(null)
                                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                                .commit();
                                        mCurrentUnitNumber = 0;
                                    }
                                } else {
                                    if (mCurrentUnitNumber == 0)
                                        mCurrentUnitNumber = 1;//fix for coupon have only 1 unit
                                    ScanbarcodeScreen fragment = new ScanbarcodeScreen();
                                    Bundle arguments = new Bundle();
                                    arguments.putString(Constant.F_ARG_COUPON_ID, mCoupon.getCouponId());
                                    arguments.putString(Constant.F_ARG_SUB_COUPON_ID, mCoupon.getCouponSubId());
                                    arguments.putString(Constant.F_ARG_COUPON_BARCODE, mCoupon.getCouponBarCode());
                                    arguments.putBoolean("fromRegister", mFromRegister);
                                    arguments.putInt(Constant.F_ARG_COUPON_TOTAL, mCurrentUnitNumber);
                                    fragment.setArguments(arguments);

                                    mActivity.getSupportFragmentManager()
                                            .beginTransaction()
                                            .replace(R.id.root_frame, fragment, "fragment_scan")
                                            .addToBackStack(null)
                                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                            .commit();
                                    mCurrentUnitNumber = 0;
                                }

                                //getChildFragmentManager().executePendingTransactions();

                            }
                            break;
                        default:

                            if(couponsPresenter.getUserInfo().getIs_verify().equals("0")){
                                mActivity.showVerifyPhone(getContext(), "buycoupon");
                                break;
                            }

                            contentDialog.setText(getString(R.string.alert_confirm_buy_counpon));
                            dialog.show();
                            btnYes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    String token = couponsPresenter.getUserInfo().getToken();
                                    if (!token.equalsIgnoreCase("")) {
//                                        Log.e("couponId", mCoupon.getCouponId());
                                        if(mCoupon.getCouponSubId() != null) {
                                            buyCoupon(token, mCoupon.getCouponId(), mCoupon.getCouponSubId());
                                        } else {
                                            buyCoupon(token, mCoupon.getCouponId(), "");
                                        }
                                    }
                                }
                            });
                            btnNo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            break;
                    }
                } else {
                    Toast.makeText(getContext(), "Đang lấy dữ liệu...", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.tv_invite:
                mActivity.lockEventIn(200);
                Log.e("coupondetai", "tv_invite");
                switch (mCoupon.getStatus()) {
                    case Constant.COUPON_STATUS_CAN_BUY://moi mua
                        //Toast.makeText(getContext(), "Mời mua sẽ được cài đặt sau!", Toast.LENGTH_LONG).show();
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, mCoupon.getShareBuyLink());
                        sendIntent.putExtra(Intent.EXTRA_SUBJECT, mCoupon.getShareBuyTitle());
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, "Mời mua"));
                        break;
                    default://gui tang
                        //Toast.makeText(getContext(), "Gửi tặng sẽ được cài đặt sau!", Toast.LENGTH_LONG).show();
                        Intent i = new Intent();
                        i.setAction(Intent.ACTION_SEND);
                        i.putExtra(Intent.EXTRA_TEXT, mCoupon.getShareLink());
                        i.putExtra(Intent.EXTRA_SUBJECT, mCoupon.getShareTitle());
                        i.setType("text/plain");
                        startActivity(Intent.createChooser(i, "Gửi tặng"));
                        break;
                }
            case R.id.iv_pre:
                mCurrentUnitNumber--;
                if (mCurrentUnitNumber <= 1) {
                    mCurrentUnitNumber = 1;
                    mBinding.ivPre.setImageResource(R.drawable.btn_pre_gray);
                }
                mBinding.ivNext.setImageResource(R.drawable.btn_next_org);
                mBinding.tvNumber.setText("" + mCurrentUnitNumber);
                break;
            case R.id.iv_next:
                mCurrentUnitNumber = MAX_UNIT_NUMBER;
                mBinding.ivNext.setImageResource(R.drawable.btn_next_gray);
                mBinding.ivPre.setImageResource(R.drawable.btn_pre_org);
                mBinding.tvNumber.setText("" + mCurrentUnitNumber);
                break;
            case R.id.iv_play_video:
                if (mBinding.vvAds.isPlaying()) {
                    mBinding.vvAds.pause();
                    mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                    mHandlerVideo.removeMessages(1);
                } else {
                    if (mEndVideo) {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.seekTo(mBinding.sbVideoProgress.getProgress());
                            mMediaPlayer.setOnSeekCompleteListener(new MediaPlayer.OnSeekCompleteListener() {
                                @Override
                                public void onSeekComplete(MediaPlayer mediaPlayer) {
                                    if (mediaPlayer != null) {
                                        mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(position));
//                                        Log.e("onSeekComplete", mBinding.sbVideoProgress.getProgress() + "");
                                        mBinding.sbVideoProgress.setProgress(mBinding.sbVideoProgress.getProgress());
                                        mediaPlayer.start();
                                        mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                                    }
                                }
                            });
                        }
                    } else {
                        mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                        mBinding.vvAds.start();
                    }
                    mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                    mEndVideo = false;
                }
                break;
            case R.id.iv_speaker:
                toogleVolumn();
                break;
            case R.id.v_overlay://click on overlay on bottom bar
                RxBus.getInstance().post("bottom_bar");
                mBinding.rlIntroductionBackground.setVisibility(View.GONE);
                Log.i("Check temp stop", "dd");
                mStopIntroduction = true;
                if (mActivity != null) {
                    ((MainActivity) mActivity).getmViewOverlayBottomBar().setVisibility(View.GONE);
                }
                break;
            case R.id.rl_introduction_background://click on anywhere else on this screen
                mBinding.rlIntroductionBackground.setVisibility(View.GONE);
                Log.i("Check temp stop", "dd");
                mStopIntroduction = true;
                if (mActivity != null) {
                    ((MainActivity) mActivity).getmViewOverlayBottomBar().setVisibility(View.GONE);
                }
                break;
            case R.id.ll_introduction_option:
                //mStopIntroduction = !mStopIntroduction;
                /*if (mStopIntroduction) {
                    mBinding.vIntroductionDot.setBackgroundResource(R.drawable.circle_coupon_introduction_active);
                }else {
                    mBinding.vIntroductionDot.setBackgroundResource(R.drawable.circle_coupon_introduction_normal);
                }*/
                mBinding.vIntroductionDot.setBackgroundResource(R.drawable.check_org_ck);
                Utils.saveTimesEnterCouponDetail(getContext(), true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBinding.rlIntroductionBackground.setVisibility(View.GONE);
                        if (mActivity != null && mActivity instanceof MainActivity) {
                            ((MainActivity) mActivity).getmViewOverlayBottomBar().setVisibility(View.GONE);
                        }
                    }
                }, 500);
                break;
            //cuongnv
            case R.id.btn_already:
                updateStatusProductUsed = true;
                mBinding.btnDoNot.setBackgroundColor(getResources().getColor(R.color.couponscreen_btn_gift_expired));
                mBinding.btnAlready.setBackgroundColor(getResources().getColor(R.color.cl_text_box));
                mProgressDialog = Utils.showLoading(mActivity);
                token = couponsPresenter.getUserInfo().getToken();
//                Log.e("token : coupon", token + "::" + mCoupon.getCouponId() + "::" + mCoupon.getCouponSubId() + "::" + statusDoNotOrAlready);
                if (!token.equalsIgnoreCase("")) {
                    statusDoNotOrAlready = Constant.COUPON_IS_REVIEW_ALREADY;
                    submitStatusCoupon(token, mCoupon.getCouponId(), mCoupon.getCouponSubId(), statusDoNotOrAlready);
                }
                break;
            case R.id.btn_do_not:
                updateStatusProductUsed = true;
                mBinding.btnAlready.setBackgroundColor(getResources().getColor(R.color.couponscreen_btn_gift_expired));
                mBinding.btnDoNot.setBackgroundColor(getResources().getColor(R.color.cl_text_box));
                mProgressDialog = Utils.showLoading(mActivity);
                statusDoNotOrAlready = Constant.COUPON_IS_REVIEW;
                token = couponsPresenter.getUserInfo().getToken();
//                Log.e("token : coupon", token + "::" + mCoupon.getCouponId() + "::" + mCoupon.getCouponSubId() + "::" + statusDoNotOrAlready);
                if (!token.equalsIgnoreCase("")) {
                    submitStatusCoupon(token, mCoupon.getCouponId(), mCoupon.getCouponSubId(), statusDoNotOrAlready);
                }
                break;
            case R.id.img_circle_check_1:
                updateCheckCircle(1);
                break;
            case R.id.img_circle_check_2:
                updateCheckCircle(2);
                break;
            case R.id.img_circle_check_3:
                updateCheckCircle(3);
                break;
        }
    }
    //cuongnv
    private void displayCheckProduct(CouponModel.Coupon coupon) {
        if (productUsedRatings.size() < 1) {
            if (coupon.getListProduct() != null) {
                productUsedRatings.clear();
                productUsedRatings = new ArrayList<>();
                for (int i = 0; i < coupon.getListProduct().size(); i++) {
                    CouponModel.Coupon.Product product = coupon.getListProduct().get(i);
                    ProductUsedRating productUsedRating = new ProductUsedRating();
                    productUsedRating.setId(product.getId());
                    productUsedRating.setImage(product.getImage());
                    productUsedRating.setName(product.getName());
                    if (product.getIs_review().equalsIgnoreCase("1")) {
                        productUsedRating.setStatusCheck(true);
                    } else {
                        productUsedRating.setStatusCheck(false);
                    }
                    productUsedRating.setIs_review(product.getIs_review());
                    productUsedRating.setBarcode(product.getBarcode());
                    productUsedRatings.add(productUsedRating);
                }
            }
        }
        Log.e("displayCheckProduct", productUsedRatings.size() + "");
        switch (productUsedRatings.size()) {
            case 1:
                mBinding.layoutProduct.linearProduct2.setVisibility(View.GONE);
                mBinding.layoutProduct.linearProduct3.setVisibility(View.GONE);
                loadImageProductRating(this.productUsedRatings.get(0).getImage(), mBinding.layoutProduct.imgProduct1);
                mBinding.layoutProduct.linearProduct1.setVisibility(View.VISIBLE);
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                }
                break;
            case 2:
                mBinding.layoutProduct.linearProduct1.setVisibility(View.VISIBLE);
                loadImageProductRating(this.productUsedRatings.get(0).getImage(), mBinding.layoutProduct.imgProduct1);
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                }
                mBinding.layoutProduct.linearProduct2.setVisibility(View.VISIBLE);
                loadImageProductRating(this.productUsedRatings.get(1).getImage(), mBinding.layoutProduct.imgProduct2);
                if (this.productUsedRatings.get(1).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                }
                mBinding.layoutProduct.linearProduct3.setVisibility(View.GONE);
                break;
            case 3:
                mBinding.layoutProduct.linearProduct1.setVisibility(View.VISIBLE);
                loadImageProductRating(this.productUsedRatings.get(0).getImage(), mBinding.layoutProduct.imgProduct1);
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                }
                mBinding.layoutProduct.linearProduct2.setVisibility(View.VISIBLE);
                loadImageProductRating(this.productUsedRatings.get(1).getImage(), mBinding.layoutProduct.imgProduct2);
                if (this.productUsedRatings.get(1).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                }
                mBinding.layoutProduct.linearProduct3.setVisibility(View.VISIBLE);
                loadImageProductRating(this.productUsedRatings.get(2).getImage(), mBinding.layoutProduct.imgProduct3);
                if (this.productUsedRatings.get(2).getIs_review().equalsIgnoreCase("0")) {
                } else {
                    mBinding.layoutProduct.imgCircleCheck3.setBackgroundResource(R.drawable.cir1);
                }
                break;
        }
    }
    //cuongnv
    private void updateCheckCircle(int position) {
        if (productUsedRatings == null) return;
        if (productUsedRatings.size() < position) return;
        switch (position) {
            case 1:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                if (productUsedRatings.size() > 1) {
                    mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(1).setStatusCheck(false);
                }
                if (productUsedRatings.size() > 2) {
                    mBinding.layoutProduct.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(2).setStatusCheck(false);
                }
                break;
            case 2:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(0).setStatusCheck(false);

                if (productUsedRatings.size() > 2) {
                    mBinding.layoutProduct.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(2).setStatusCheck(false);
                }
                break;
            case 3:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    mBinding.layoutProduct.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    mBinding.layoutProduct.imgCircleCheck3.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                mBinding.layoutProduct.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(0).setStatusCheck(false);

                mBinding.layoutProduct.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(1).setStatusCheck(false);
                break;
        }
    }
    //cuongnv
    private void loadImageProductRating(String linkImage, final ImageView imageView) {
        Picasso
                .with(mActivity)
                .load(linkImage)
                .resize(mDeviceWidth / 3, mDeviceWidth * 2 / 3)
                .into(imageView);
    }
    //cuongnv
    private void submitStatusCoupon(String token, String couponId, String couponSubId, String value) {
        couponsPresenter.submitStatus(token, couponId, couponSubId, value);
    }

    /**
     * check permission camera
     *
     * @return
     */
    private boolean checkAndRequestPermissionsCamera() {
        int writepermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), Constant.REQUEST_PERMISSIONS_CAMERA);
            }
            return false;
        }
        return true;
    }

    public void playVideoIfCan() {
        if (!mEndVideo) {
            if (mMediaPlayer != null) {
                mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                mBinding.vvAds.start();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.vv_Ads:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    toogleVolumn();
                }
                break;
            case R.id.vp_ads:
                if (handler != null) {
                    handler.removeCallbacks(runnable);
                }
                break;
        }
        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        getLocation();


        if (mLastLocation == null) {
            mLastLocation = new Location("dummyprovider");
            mLastLocation.setLatitude(0);
            mLastLocation.setLongitude(0);
        }

        if (mCoupon == null) {
            updateLocationDeatail = false;
            getCouponDetail(mCouponId, mCouponSubId);
        }

    }

    public void getLocation() {
        if (ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            //get coupon available update
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandlerVideo.removeMessages(1);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandlerVideo.sendEmptyMessage(1);
        mBinding.vvAds.seekTo((int) (seekBar.getProgress() / 100f * mBinding.vvAds.getDuration()));
        mBinding.sbVideoProgress.setProgress(seekBar.getProgress());
        Log.e("progress",seekBar.getProgress() + "");
    }

    private void buyCoupon(String token, String idCoupon, String subIdCoupon) {
        if(mActivity.checkAndAlertNetWork(getView())) {
            mProgressDialog = Utils.showLoading(mActivity);
            couponsPresenter.buyCounpon(token, idCoupon, subIdCoupon);
        }
    }

    private void buyCouponEC(String token, String idCoupon, String subIdCoupon) {
        if(mActivity.checkAndAlertNetWork(getView())) {
            couponsPresenter.buyCounponEC(token, idCoupon, subIdCoupon);
        }
    }

    private void createGoogleApiClient() {
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void getCouponDetail(String couponId, String couponSubId) {
        if (Utils.isNetworkAvailable(mActivity)) {
///            mBinding.icLoadding.setVisibility(View.VISIBLE);
            if (updateChangeGift) {
                updateChangeGift = false;
                if (message.equalsIgnoreCase("")) {
                    message = getResources().getString(R.string.alert_coupon_buy_success);
                }
                snackbar = TSnackbar
                        .make(getView(), message, TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, message, mActivity);
                Utils.vibarte(mActivity);
            } else {
                if (!updateStatusProductUsed) {
                    mProgressDialog = Utils.showLoading(mActivity);
                }
                updateStatusProductUsed = false;
            }
            Intent i = new Intent(mActivity, GetCouponDetailService.class);
            i.putExtra("coupon_id", couponId);
            i.putExtra("coupon_sub_id", couponSubId);
            i.putExtra("location", mLastLocation);
            mActivity.startService(i);
        } else {
            if (mProgressDialog != null) {
                Utils.hideLoading(mProgressDialog);
            }
            //Utils.showNoInternetMessage(getContext(), null);
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {

        if (mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }

        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mActivity.showNetworkAlert(getView());
        } else {

            BaseResponse baseResponse;
            switch (task.getType()) {
                case BUY_COUNPON:
                    baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse != null) {
                        if (status == 200) {
                            if (baseResponse.getData() != null) {
                                MainActivity.reload_list = true;
                                mActivity.getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.root_frame, CouponsScreen.newInstance(true, getResources().getString(R.string.alert_coupon_buy_success)))
                                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                        .addToBackStack(null)
                                        .commitAllowingStateLoss();
                                getChildFragmentManager().executePendingTransactions();
                            /*
                            message = baseResponse.getMessage();
                            updateChangeGift = true;
                            List<BuyCouponRespone> buyCouponRespones = (List<BuyCouponRespone>) baseResponse.getData();
                            Log.e("onPostResponse : Co", buyCouponRespones.toString());
                            getCouponDetail(buyCouponRespones.get(0).getCoupon_id(), buyCouponRespones.get(0).getCoupon_sub_id());
                            */
                            }
                        } else {
                            snackbar = TSnackbar
                                    .make(getView(), baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                            Utils.vibarte(mActivity);
                        }
                    }
//                getCouponDetail();
                    break;
                case SUBMIT_STATUS_COUPON:
                    baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse != null) {
                        if (baseResponse.getCode() == Constant.STATUS_SUCCESS) {
                            MainActivity.not_from_list = true;
                            //  mCoupon.setIsReview(statusDoNotOrAlready);
                            updateLocationDeatail = false;
                            getCouponDetail(mCoupon.getCouponId(), mCoupon.getCouponSubId());
                        }
                    }
                    break;
                case GET_LIST_REVIEW_BY_PRODUCT_ONLY:
                    baseResponse = (BaseResponse) task.getResponse().body();
                    if (status == ApiResponseCode.SUCCESS) {
                        ReviewAllItem mReviewAllItem = (ReviewAllItem) baseResponse.getData();

                        mActivity.gotoReviewDetail(mReviewAllItem);

                    } else {
                        TSnackbar snackbar = TSnackbar
                                .make(getView(), baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                        Utils.vibarte(mActivity);
                    }
                    break;
                case BUY_COUNPON_EC:
                    baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse != null) {
                        if (status == 200) {
                            MainActivity.reload_list = true;
                            updateLocationDeatail = false;
                            getCouponDetail(mCoupon.getCouponId(), "");
                            snackbar = TSnackbar
                                    .make(getView(), getString(R.string.alert_coupon_buy_success), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, getString(R.string.alert_coupon_buy_success), mActivity);
                            if (mCoupon.getEscommerceLink() != null && mCoupon.getEscommerceLink() != "") {
                                openWebUrl(mCoupon.getEscommerceLink());
                            } else {
                                snackbar = TSnackbar
                                        .make(getView(), getString(R.string.escommerce_link_not_found), TSnackbar.LENGTH_SHORT);
                                Utils.showMessageTopbar(snackbar, getString(R.string.escommerce_link_not_found), mActivity);
                                Utils.vibarte(mActivity);
                            }
                        } else {
                            snackbar = TSnackbar
                                    .make(getView(), baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                            Utils.vibarte(mActivity);
                        }
                    }
                    break;
            }
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("onResume", loadCouponDetail + "");
        if (loadCouponDetail) {
            loadCouponDetail = false;
            if (mCoupon != null) {
                updateLocationDeatail = false;
                getCouponDetail(mCoupon.getCouponId(), mCoupon.getCouponSubId());
            }
        }
    }

    public class CouponDetailReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.hideLoading(mProgressDialog);
            if (!updateLocationDeatail) {
                final int connect_status = intent.getIntExtra(Constant.CONNECT_STATUS, 200);
                if (connect_status == Constant.CANNOT_CONNECT_SERVER) {
                    mActivity.showNetworkAlert(getView());

                } else {
                    final int action = intent.getIntExtra(Constant.ACTION, 0);
                    //final String coupon_sub_id = intent.getStringExtra("arg_coupon_sub_id");
                    CouponModel.Coupon mDetailCoupon = null;
                    switch (action) {
                        case Constant.ACTION_GET_COUPON_DETAIL:
                            //mBinding.icLoadding.setVisibility(View.GONE);
                            CouponModel couponModel = (CouponModel) intent.getSerializableExtra("coupon");
                            if (couponModel != null) {
                                if (couponModel.getCode().equals(Constant.CODE_SUCCESS)) {
                                    mCoupon = couponModel.getCoupon();
                                    if (mCoupon != null) {

                                        if (MainActivity.couponList != null) {
                                            for (int i = 0; i < MainActivity.couponList.size(); i++) {
                                                if (MainActivity.couponList.get(i).getCouponId().equals(mCoupon.getCouponId())) {
                                                    MainActivity.couponList.set(i, mCoupon);
                                                }
                                            }
                                        }

                                        if (mCoupon.getList() != null && mCoupon.getList().size() > 0 && (mCurrentCouponSubId != null && !mCurrentCouponSubId.equals(""))) {
                                            for (int i = 0; i < mCoupon.getList().size(); i++) {
                                                if (mCoupon.getList().get(i).getCouponSubId().equals(mCurrentCouponSubId)) {
                                                    mDetailCoupon = mCoupon.getList().get(i);
                                                }
                                            }
                                        } else {
                                            mDetailCoupon = mCoupon;
                                        }

                                        //update data, when review coupon ---cuongnv
                                        if (updateProductUsedRating != null) {
                                            if (mDetailCoupon.getListProduct() != null) {
                                                for (int i = 0; i < mDetailCoupon.getListProduct().size(); i++) {
                                                    if (mDetailCoupon.getListProduct().get(i).getId()
                                                            .equalsIgnoreCase(updateProductUsedRating.getId())) {
                                                        mDetailCoupon.getListProduct().remove(i);
                                                        break;
                                                    }
                                                }
                                            }
                                            updateProductUsedRating = null;
                                        }

                                        try {
                                            showCouponDetail(mDetailCoupon);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                    }

                                } else {
                                    Utils.showMessageNoTitle(getContext(), couponModel.getMessage(), null);
                                }
                            }
                            break;
                    }
                }
            }
        }
    }

    private void toogleVolumn() {
        try {
            if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
                if (isMute) {
                    mMediaPlayer.setVolume(1f, 1f);
                    mBinding.ivSpeaker.setImageResource(R.drawable.l1);
                } else {
                    mMediaPlayer.setVolume(0f, 0f);
                    mBinding.ivSpeaker.setImageResource(R.drawable.l2);
                }
                isMute = !isMute;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUnitNumber() {
        mBinding.llCanBuy.setVisibility(View.GONE);
        mBinding.llTheRest.setVisibility(View.GONE);
        mBinding.tvGiftExchange.setVisibility(View.GONE);
        mBinding.llCanBuy.setVisibility(View.GONE);
        mBinding.llSelectUnitNumber.setVisibility(View.VISIBLE);

        /*for (CouponModel.Coupon coupon : mCoupon.getList()){
            if (coupon.getStatus().equals(Constant.COUPON_STATUS_NOT_EXCHANGE_YET) || coupon.getStatus().equals(Constant.COUPON_STATUS_NEW)){
                mCurrentUnitNumber ++;
            }
        }*/
        mCurrentUnitNumber = mCoupon.getQuantity();
        MAX_UNIT_NUMBER = mCoupon.getQuantity();
        //mCurrentUnitNumber = mCoupon.getCoupon().getList().size();
        mBinding.tvNumber.setText("" + mCurrentUnitNumber);
        mBinding.ivNext.setOnClickListener(this);
        mBinding.ivPre.setOnClickListener(this);
    }

    public void showCouponDetail(CouponModel.Coupon coupon) {
        mTracker.enableAdvertisingIdCollection(true);
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory(Constant.CATEGORY_ANALYTISC_COUPONS)
                .setAction(Constant.EVENT_ANALYTISC_COUPON_DETAIl + "_" + coupon.getCouponId() + "_" + coupon.getCouponName())
                .build());
//        Log.e("showCouponDetail", coupon.getCouponId() + ":" + coupon.getCouponSubId());
        if (coupon == null) return;
        switch (coupon.getMediaType()) {
            case "1"://static image
                mBinding.ivStaticAds.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(Uri.parse(coupon.getMediaUrl())).resize(mDeviceWidth, mDeviceWidth).into(mBinding.ivStaticAds);
                break;
            case "2":
                mBinding.flImagesAds.setVisibility(View.VISIBLE);
                AdsPagerAdapter adapter = new AdsPagerAdapter(getChildFragmentManager());
                List<AdsImageFragment> fragments = new ArrayList<>();
                switch (coupon.getMediaUrlList().size()) {
                    case 3:
                        mBinding.vDot2.setVisibility(View.VISIBLE);
                    case 2:
                        mBinding.vDot1.setVisibility(View.VISIBLE);
                    case 1:
                        mBinding.vDot0.setVisibility(View.VISIBLE);
                        break;
                }
                for(int i = 0; i < coupon.getMediaUrlList().size(); i++) {
                    fragments.add(AdsImageFragment.newInstance(coupon.getMediaUrlList().get(i), mDeviceWidth, false));
                }
                adapter.setData(fragments);
                mBinding.vpAds.addOnPageChangeListener(new PagerListener());
                mBinding.vpAds.setAdapter(adapter);
                handler.postDelayed(runnable, 2000);
                break;
            case "3":
                if (coupon.getVideoUrl() == null) {
                    Toast.makeText(getContext(), "Không có video để hiển thị.", Toast.LENGTH_LONG).show();
                    break;
                }
                mBinding.rlVideoAds.setVisibility(View.VISIBLE);
                mBinding.ivVideoThumb.setVisibility(View.VISIBLE);
                mBinding.rlProgress.setVisibility(View.GONE);
                Picasso.with(getContext()).load(Uri.parse(coupon.getMediaUrl())).resize(mDeviceWidth, mDeviceWidth).into(mBinding.ivVideoThumb);
                mBinding.vvAds.setVideoURI(Uri.parse(coupon.getVideoUrl()));
                mBinding.vvAds.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mBinding.pbAdsWaiting.setVisibility(View.GONE);
                        mBinding.ivVideoThumb.setVisibility(View.GONE);
                        mBinding.rlProgress.setVisibility(View.VISIBLE);
                        mediaPlayer.start();
                        mMediaPlayer = mediaPlayer;
                        mMediaPlayer.setVolume(0f, 0f);
                        isMute = true;
                        mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                    }
                });
                mBinding.vvAds.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mediaPlayer.seekTo(mediaPlayer.getDuration());
                        //mMediaPlayer = null;
                    }
                });
                break;

        }
        //left
        mBinding.tvLabel.setText(coupon.getCouponName());
        mBinding.tvName.setText(coupon.getCouponSizeDes());
        mBinding.tvExpired.setText(coupon.getCouponTypeDes());
        mBinding.tvValue.setText(coupon.getCouponCoinDes());
        //check status
        if (coupon.getStatus() != null) {
            switch (coupon.getIsInvite()) {
                case "1":
                    mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText(getString(R.string.invite_buy));
                    break;
                case "2":
                    mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText("Gửi tặng");
                    break;
                default:
                    mBinding.mainTopbar.tvInvite.setVisibility(View.GONE);
                    break;
            }

            switch (coupon.getStatus()) {
                case Constant.COUPON_STATUS_CAN_BUY: // can buy EC
                    mBinding.llTheRest.setVisibility(View.VISIBLE);
                    mBinding.tvDayNumber.setText(coupon.getCouponExpiredDay() + " ngày");
                    //SimpleDateFormat sdf = new SimpleDateFormat(Constant.DATE_FORMAT);
                    mBinding.llCanBuy.setVisibility(View.GONE);
                    mBinding.tvExpiredDate.setText(coupon.getCouponExpired());
                    if (coupon.getEsComericalText().equalsIgnoreCase("")) {
                        mBinding.btnGift.setText("CLICK ĐỂ MUA TRỰC TUYẾN");
                    } else {
                        mBinding.btnGift.setText(coupon.getEsComericalText().toUpperCase());
                    }
                    if (!Utils.getTimesEnterCouponDetail(getContext()) && !mStopIntroduction) {
                        mBinding.ivIntroductionCenter.setImageResource(R.drawable.file_huong_dan_mua);
                        mBinding.rlIntroductionBackground.setVisibility(View.VISIBLE);
                        if (mActivity != null) {
                            mActivity.getmViewOverlayBottomBar().setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                case Constant.COUPON_STATUS_EXCHANGED:
                    //cuongnv
                    updateStatusIsReview(coupon);
                    break;
                case Constant.COUPON_STATUS_EXPIRED:
                    //text in left is gray color
                    mBinding.tvLabel.setTextColor(mColorGray);
                    mBinding.tvName.setTextColor(mColorGray);
                    mBinding.tvExpired.setTextColor(mColorGray);
                    mBinding.tvValue.setTextColor(mColorGray);
                    //menu on top-right
                    mBinding.btnGift.setText("ĐÃ HẾT HẠN");
                    mBinding.btnGift.setEnabled(false);
                    mBinding.btnGift.setTextColor(Color.BLACK);
                    mBinding.btnGift.setBackgroundResource(R.color.couponscreen_btn_gift_expired);
                    mBinding.btnGift.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            return false;
                        }
                    });
                    mBinding.vLine.setBackgroundResource(android.R.color.black);
                    //
                    mBinding.tvGiftExchange.setVisibility(View.VISIBLE);
                    mBinding.tvGiftExchange.setText(coupon.getCouponExpired());
                    //
                    /*mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText("Gửi tặng");*/
                    //
                    mBinding.ivLocation.setVisibility(View.GONE);
                    break;
                case Constant.COUPON_STATUS_NEW:
                case Constant.COUPON_STATUS_NOT_EXCHANGE_YET:
                    mBinding.llTheRest.setVisibility(View.VISIBLE);
                    mBinding.tvDayNumber.setText(coupon.getCouponExpiredDay() + " ngày");
                    //SimpleDateFormat sdf = new SimpleDateFormat(Constant.DATE_FORMAT);
                    mBinding.llCanBuy.setVisibility(View.GONE);
                    mBinding.tvExpiredDate.setText(coupon.getCouponExpired());
                    mBinding.btnGift.setText("CLICK ĐỂ ĐỔI QUÀ");
                    //
                    /*mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText("Gửi tặng");*/

                    Log.i("Check temp stop 2 >> ", "" + mStopIntroduction);

                    if (!Utils.getTimesEnterCouponDetail(getContext()) && !mStopIntroduction) {
                        mBinding.ivIntroductionCenter.setImageResource(R.drawable.file_huong_dan_doi);
                        mBinding.rlIntroductionBackground.setVisibility(View.VISIBLE);
                        if (mActivity != null) {
                            mActivity.getmViewOverlayBottomBar().setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                default: // can buy EC
                    //mBinding.mainTopbar.mainTopBarCoupons.setText("Mời mua");
                    mBinding.llCanBuy.setVisibility(View.VISIBLE);
                    mBinding.btnGift.setText("CLICK ĐỂ MUA COUPON");
                    mBinding.tvPrice.setText(coupon.getCouponCoin());
                    /*mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText(getString(R.string.invite_buy));*/
                    if (!Utils.getTimesEnterCouponDetail(getContext()) && !mStopIntroduction) {
                        mBinding.ivIntroductionCenter.setImageResource(R.drawable.file_huong_dan_mua);
                        mBinding.rlIntroductionBackground.setVisibility(View.VISIBLE);
                        if (mActivity != null) {
                            mActivity.getmViewOverlayBottomBar().setVisibility(View.VISIBLE);
                        }
                    }
                    break;
            }
        }
    }

    private void updateStatusIsReview(CouponModel.Coupon coupon) {
        //text in left is gray color
        mBinding.tvLabel.setTextColor(mColorGray);
        mBinding.tvName.setTextColor(mColorGray);
        mBinding.tvExpired.setTextColor(mColorGray);
        mBinding.tvValue.setTextColor(mColorGray);
        //menu on top-right
        if (coupon.getIsEscommerce().equalsIgnoreCase("1")) {
            mBinding.btnGift.setText(getResources().getString(R.string.text_used_coupon));
        } else {
            mBinding.btnGift.setText("ĐÃ ĐỔI QUÀ");
        }
        mBinding.btnGift.setEnabled(false);
        mBinding.btnGift.setTextColor(Color.BLACK);
        mBinding.btnGift.setBackgroundResource(R.color.couponscreen_btn_gift_expired);
        mBinding.vLine.setBackgroundResource(android.R.color.black);
        //
        mBinding.tvGiftExchange.setVisibility(View.VISIBLE);
        mBinding.tvGiftExchange.setText("Đã đổi quà ngày " + coupon.getCouponExchangeDate());
        mBinding.linearNotYetOrAlready.setVisibility(View.GONE);
        mBinding.linearRatingCouponDetail.setVisibility(View.GONE);
        mBinding.llCanBuy.setVisibility(View.GONE);
        mBinding.tvDayNumber.setVisibility(View.GONE);
        mBinding.tvExpiredDate.setVisibility(View.GONE);
        //
                    /*mBinding.mainTopbar.tvInvite.setVisibility(View.VISIBLE);
                    mBinding.mainTopbar.tvInvite.setText("Gửi tặng");*/
        //
        mBinding.ivLocation.setVisibility(View.GONE);
        mBinding.layoutProduct.linearCheckUsedProduct.setVisibility(View.GONE);
        mBinding.flImage.setVisibility(View.VISIBLE);
        if (coupon.getIsReview() != null) {
            switch (coupon.getIsReview()) {
                case Constant.COUPON_IS_REVIEW_DO_NOT_OR_ALREADY:
                    mBinding.tvLabel.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvName.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvExpired.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvValue.setTextColor(getResources().getColor(R.color.cl_enter_coin));
                    statusDoNotOrAlready = Constant.COUPON_IS_REVIEW_DO_NOT_OR_ALREADY;
                    mBinding.vLine.setBackgroundResource(R.color.main_title_color);
                    mBinding.linearNotYetOrAlready.setVisibility(View.VISIBLE);
                    mBinding.tvGiftExchange.setVisibility(View.GONE);
                    mBinding.btnGift.setText(getResources().getString(R.string.text_you_have_used_the_product_yet));
                    mBinding.btnGift.setEnabled(false);
                    mBinding.btnGift.setAllCaps(false);
                    mBinding.btnGift.setBackgroundColor(getResources().getColor(R.color.cl_coupon_you_have_used_product_yet));
                    mBinding.btnDoNot.setBackgroundColor(getResources().getColor(R.color.couponscreen_btn_gift_expired));
                    mBinding.btnAlready.setBackgroundColor(getResources().getColor(R.color.couponscreen_btn_gift_expired));
                    break;
                case Constant.COUPON_IS_REVIEW_ALREADY:
                    mBinding.tvLabel.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvName.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvExpired.setTextColor(getResources().getColor(R.color.black));
                    mBinding.tvValue.setTextColor(getResources().getColor(R.color.cl_enter_coin));
                    statusDoNotOrAlready = Constant.COUPON_IS_REVIEW_ALREADY;
                    mBinding.vLine.setBackgroundResource(R.color.main_title_color);
                    mBinding.linearNotYetOrAlready.setVisibility(View.GONE);
                    mBinding.linearRatingCouponDetail.setVisibility(View.VISIBLE);
                    mBinding.tvGiftExchange.setVisibility(View.GONE);
                    mBinding.btnGift.setText(getResources().getString(R.string.text_rated_the_product_to_receive_coins));
                    mBinding.btnGift.setEnabled(true);
                    mBinding.btnGift.setAllCaps(false);
                    mBinding.btnGift.setClickable(true);
                    mBinding.btnGift.setBackgroundColor(getResources().getColor(R.color.cl_coupon_you_have_used_product_yet));
//                    Log.e("rating coupon", coupon.getCouponRating() + "");
                    mBinding.rbRatingReview.setRating(coupon.getCouponRating());
                    mBinding.tvRatingNumberReview.setText("(" + coupon.getCouponReview() + ")");
                    break;
            }
        }
    }

    private class PagerListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotActive);
                        mBinding.vDot1.setBackground(mDotNormal);
                        mBinding.vDot2.setBackground(mDotNormal);
                    } else {
                        mBinding.vDot0.setBackgroundDrawable(mDotActive);
                        mBinding.vDot1.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot2.setBackgroundDrawable(mDotNormal);
                    }
                    break;
                case 1:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotNormal);
                        mBinding.vDot1.setBackground(mDotActive);
                        mBinding.vDot2.setBackground(mDotNormal);
                    } else {
                        mBinding.vDot0.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot1.setBackgroundDrawable(mDotActive);
                        mBinding.vDot2.setBackgroundDrawable(mDotNormal);
                    }
                    break;
                case 2:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotNormal);
                        mBinding.vDot1.setBackground(mDotNormal);
                        mBinding.vDot2.setBackground(mDotActive);
                    } else {
                        mBinding.vDot0.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot1.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot2.setBackgroundDrawable(mDotActive);
                    }
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    @Subscribe
    public void feFreshData(refreshData refreshData) {
        if (refreshData.getNameFragment().toLowerCase().equals("detailcoupon")) {
// noi reload data
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("eonActivityResult", ""+requestCode+" - "+resultCode);
        if (requestCode == REQUEST_APP_SETTINGS) {
            if (BaseActivity.hasPermissions(mActivity,Manifest.permission.CAMERA)) {
                Toast.makeText(mActivity, "All permissions granted!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "Permissions not granted.", Toast.LENGTH_LONG).show();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    private boolean checkAndRequestPermissionsLoaction() {
        int contactpermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.GET_ACCOUNTS);
        int locationpermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (contactpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), Constant.REQUEST_PERMISSIONS_LOCATION);
            }
            return false;
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0) {
            switch (requestCode) {
                case Constant.REQUEST_PERMISSIONS_CAMERA:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (mCurrentUnitNumber == 0)
                            mCurrentUnitNumber = 1;//fix for coupon have only 1 unit
                        ScanbarcodeScreen fragment = new ScanbarcodeScreen();
                        Bundle arguments = new Bundle();
                        arguments.putString(Constant.F_ARG_COUPON_ID, mCoupon.getCouponId());
                        arguments.putString(Constant.F_ARG_SUB_COUPON_ID, mCoupon.getCouponSubId());
                        arguments.putString(Constant.F_ARG_COUPON_BARCODE, mCoupon.getCouponBarCode());
                        arguments.putBoolean("fromRegister", mFromRegister);
                                /*if(mCoupon.getCoupon().getQuatity()!=null) {
                                    arguments.putInt(Constant.F_ARG_COUPON_TOTAL, Integer.parseInt(mCoupon.getCoupon().getQuatity()));
                                }*/
                        arguments.putInt(Constant.F_ARG_COUPON_TOTAL, mCurrentUnitNumber);
                        fragment.setArguments(arguments);

                        mActivity.getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.root_frame, fragment, "fragment_scan")
                                .addToBackStack(null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .commitAllowingStateLoss();

                    } else {
                        // if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        snackbar = TSnackbar
                                .make(getView(), " " + getString(R.string.not_alow_permission), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbarNextWork(snackbar, getString(R.string.not_alow_permission), mActivity);
                      /*  // user rejected the permission
                        boolean showRationale = shouldShowRequestPermissionRationale(Manifest.permission.CAMERA);
                        if (!showRationale) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, REQUEST_APP_SETTINGS);*/
                        // user also CHECKED "never ask again"
                        // you can either enable some fall back,
                        // disable features of your app
                        // or open another dialog explaining
                        // again the permission and directing to
                        // the app setting
                        //  }
                        // }

                    }
                    break;
                case Constant.REQUEST_PERMISSIONS_LOCATION:
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (mCoupon.getShops() == null) {
                            Toast.makeText(getContext(), "Không tìm thấy cửa hàng nào cho coupon này.", Toast.LENGTH_LONG).show();
                            break;
                        }
                        boolean isPlaying = false;
                        if (mMediaPlayer != null && mBinding.vvAds.isPlaying()) {
                            mBinding.vvAds.pause();
                            mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                            mHandlerVideo.removeMessages(1);
                            isPlaying = true;
                        }

                        Bundle arguments = new Bundle();
                        arguments.putString(ARG_PARAM2, mCoupon.getCouponId());
                        arguments.putString(ARG_PARAM4, mCoupon.getCouponSubId());
                        CouponsBackDetailScreen fragment = CouponsBackDetailScreen.newInstance(mCoupon.getShops(), isPlaying);
                        fragment.setArguments(arguments);
                        getActivity().getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.root_frame, fragment, null)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                .addToBackStack(null)
                                .commitAllowingStateLoss();
                        getChildFragmentManager().executePendingTransactions();
                    } else {
                        Utils.displayPromptForEnablingGPS(mActivity);
                    }
                default:
                    super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    private void viewDetailReview(final CouponModel.Coupon coupon) {
        if(coupon == null) {
            return;
        }

        List<CouponModel.Coupon.Product> products = coupon.getListProduct();

        int mNumberProduct = 0;
        if(products != null && products.size() > 0) {
            mNumberProduct = products.size();
        }

        if(mNumberProduct == 0) {
            snackbar = TSnackbar
                    .make(getView(),  getString(R.string.coupon_not_have_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.coupon_not_have_product), mActivity);
            return;
        }

        if(mNumberProduct == 1) {
            gotoProductReview(Integer.parseInt(products.get(0).getId()));
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int mDeviceWidth = displayMetrics.widthPixels - 50;

        int partWidth = (mDeviceWidth / 3) - 10;

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater li = LayoutInflater.from(mActivity);
        final View myView = li.inflate(R.layout.dialog_select_product_view_review, null);

        View product1 = myView.findViewById(R.id.dialog_select_product_1);
        View product2 = myView.findViewById(R.id.dialog_select_product_2);
        View product3 = myView.findViewById(R.id.dialog_select_product_3);

        // Reset all view to invisible
        product1.setVisibility(View.GONE);
        product2.setVisibility(View.GONE);
        product3.setVisibility(View.GONE);

        builder.setView(myView);
        mDialog = builder.create();
        mDialog.show();

        int imageWidth = partWidth;
        int imageHeight = partWidth * 2;

        ViewGroup.LayoutParams p = product1.getLayoutParams();
        p.width = imageWidth;
        p.height = imageHeight;

        product1.setLayoutParams(p);
        product2.setLayoutParams(p);
        product3.setLayoutParams(p);

        if(mNumberProduct >= 1) {
            product1.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_1);
            Picasso.with(getContext()).load(products.get(0).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product1.setId(Integer.parseInt(products.get(0).getId()));

            product1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });

            product1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 2) {
            product2.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_2);
            Picasso.with(getContext()).load(coupon.getListProduct().get(1).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product2.setId(Integer.parseInt(products.get(1).getId()));

            product2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 3) {
            product3.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_3);
            Picasso.with(getContext()).load(coupon.getListProduct().get(2).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product3.setId(Integer.parseInt(products.get(2).getId()));

            product3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product3.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private void gotoProductReview(int product_id) {

        if(mDialog != null) {
            mDialog.dismiss();
        }

        if(mActivity.checkAndAlertNetWork(getView())) {
            mProgressDialog = Utils.showLoading(getContext());
            String token = couponsPresenter.getUserInfo().getToken();
            couponsPresenter.getListAllReviewByProductPresenta(token, "" + product_id, "", "", "product");
        }

    }

    private void openWebUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;

        PackageManager packageManager = mActivity.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);

        String default_package;
        default_package = null;

        if (list.size() > 0) {
            for (ResolveInfo resolveInfo : list) {
                if(resolveInfo.isDefault) {
                    default_package = resolveInfo.activityInfo.packageName;
                }
            }

            if(default_package == null) {
                default_package = list.get(0).activityInfo.packageName;
            }

        }

        intent.setPackage(default_package);
        try {
            startActivity(intent);
        } catch (Exception e) {
            intent.setPackage(null);
            startActivity(intent);
        }
    }
}
