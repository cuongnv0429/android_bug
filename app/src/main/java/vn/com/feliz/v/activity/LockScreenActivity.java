package vn.com.feliz.v.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.databinding.ActivityLockScreenBinding;
import vn.com.feliz.services.LCService;
import vn.com.feliz.v.adapter.LockScreenPagerAdapter;

public class LockScreenActivity extends AppCompatActivity{
    private static String TAG = LockScreenActivity.class.getSimpleName();
    public static boolean isLocked = false;
    private PagerAdapter mPagerAdapter;
    private ActivityLockScreenBinding mBinding;
    private static final String FIRE_BASE_URL = Constant.FIREBASE_URL;
    public static DatabaseReference mFirebaseDatabase;
    public static FirebaseDatabase mFirebaseInstance;
    protected static ValueEventListener mFireBaseRef;
    public static FrameLayout loading;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        makeFullScreen();

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_lock_screen);
        loading = mBinding.progressContainer;
        isLocked = true;
        mPagerAdapter = new LockScreenPagerAdapter(getSupportFragmentManager());
        mBinding.pager.setPagingEnabled(false);
        mBinding.pager.setAdapter(mPagerAdapter);
        mBinding.pager.setCurrentItem(1);

        startService(new Intent(this,LCService.class));

        Log.d(TAG, "onCreate...");
    }

    /**
     * A simple method that sets the screen to fullscreen.  It removes the Notifications bar,
     *   the Actionbar and the virtual keys (if they are on the phone)
     */
    public void makeFullScreen() {
        this.getWindow().addFlags(
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        );

        if(Build.VERSION.SDK_INT < 19) { //View.SYSTEM_UI_FLAG_IMMERSIVE is only on API 19+
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        } else {
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume...");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause...");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy...");
        /*if(mNetworkStateReceiver!=null){
            mNetworkStateReceiver.removeListener(this);
        }*/
    }

    @Override
    public void onBackPressed() {
        return;
        //nothing
    }

    @Override
    public boolean onKeyDown(int keyCode, android.view.KeyEvent event) {
        // Key code constant: Home key. This key is handled by the framework and is never delivered to applications.
        return (keyCode == KeyEvent.KEYCODE_HOME) || super.onKeyDown(keyCode, event);
    }

    public static void loading(int status) {
        loading.setVisibility(status);
    }

}
