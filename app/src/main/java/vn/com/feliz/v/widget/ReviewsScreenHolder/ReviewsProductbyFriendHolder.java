package vn.com.feliz.v.widget.ReviewsScreenHolder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import vn.com.feliz.R;
import vn.com.feliz.v.interfaces.ItemTouchHelperViewHolder;


/**
 * Created by Mr Son on 2016-08-01.
 */
public class ReviewsProductbyFriendHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {
    public TextView item_reviews_name,product_rating,created_at,product_review,tvShowMore_me;
    public TextView content;
    public ImageView img_coupon_review_all;
    public RatingBar rb_rating_all;
    public LinearLayout main_content_me;
    private boolean expand = false;
   // public LinearLayout mLinearLayoutMain;

    public ReviewsProductbyFriendHolder(View view) {
        super(view);
        item_reviews_name = (TextView) view.findViewById(R.id.item_reviews_name_me);

        product_rating = (TextView) view.findViewById(R.id.product_rating_me);
        content = (TextView) view.findViewById(R.id.content_me);
        created_at = (TextView) view.findViewById(R.id.created_at_me);
        product_review = (TextView) view.findViewById(R.id.product_review);
        img_coupon_review_all = (ImageView) view.findViewById(R.id.img_coupon_review_me);
        rb_rating_all = (RatingBar) view.findViewById(R.id.rb_rating_me);
        tvShowMore_me = (TextView) view.findViewById(R.id.tvShowMore_me);
        main_content_me =(LinearLayout) view.findViewById(R.id.main_content_me);
     //   mLinearLayoutMain = (LinearLayout) view.findViewById(R.id.ln_main_message_item);
         tvShowMore_me.setOnClickListener(this);
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }

    @Override
    public void onClick(View view) {
//        if (view.getId() == tvShowMore_me.getId()) {
//            if (!expand) {
//                expand = true;
//                ObjectAnimator animation = ObjectAnimator.ofInt(content, "maxLines", 100);
//                animation.setDuration(100).start();
//                tvShowMore_me.setText(R.string.view_less);
//                tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_less_xx, 0, 0, 0);
//
//            } else {
//                expand = false;
//                ObjectAnimator animation = ObjectAnimator.ofInt(content, "maxLines", 2);
//
//                animation.setDuration(100).start();
//                tvShowMore_me.setText(R.string.view_more);
//                tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_expand_xx, 0, 0, 0);
//            }
//
//        }
    }
}
