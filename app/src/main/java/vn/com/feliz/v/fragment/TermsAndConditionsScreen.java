package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;

import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.p.eventbus.SetTitleMessage;
import vn.com.feliz.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsAndConditionsScreen extends Fragment implements View.OnClickListener {
    WebView mTextContent;
    Button mSubmit;

    private String mCouponId;
    public TermsAndConditionsScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        // Set Title
        EventBus.getDefault().post(new SetTitleMessage(getString(R.string.Terms_and_conditions)));
        EventBus.getDefault().post(this);

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_terms_conditions_screen, container, false);
        // Bind Resources
        //mUnbinder = ButterKnife.bind(this, rootView);
        mTextContent = (WebView) rootView.findViewById(R.id.textContent) ;
        mSubmit= (Button) rootView.findViewById(R.id.btn_confirm_term_condition);
        String textContent;
        textContent = "<html><body><p align=\"justify\">";
        textContent += getString(R.string.test);
        textContent += "</p></body></html>";
        mTextContent.loadData(textContent, "text/html", "utf-8");
        BaseApplication application = BaseApplication.getInstance();
//        mSubmit.setTypeface(application.getFontHelvetical());
        mSubmit.setVisibility(View.VISIBLE);
        mSubmit.setOnClickListener(this);


        return rootView;
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm_term_condition:
              getFragmentManager().popBackStack();
                break;
        }

    }
}
