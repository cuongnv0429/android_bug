package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import vn.com.feliz.common.Utils;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.eventbus.HideAdress;
import vn.com.feliz.v.adapter.PlaceArrayAdapter;
import vn.com.feliz.R;
import vn.com.feliz.databinding.FragmentSearchScreenBinding;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.p.eventbus.PlaceChange;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchPlaceScreen extends BaseFragment implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, OnPostResponseListener, OnMenuItemSelected, View.OnClickListener{
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(
            new LatLng(14.058324, 108.277199), new LatLng(14.058324, 108.277199));
    private String mAddress;
    private String mAddressFromSetting;
    private FragmentSearchScreenBinding fragmentSearchScreenBinding;
    public SearchPlaceScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set Title
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Bundle bundle = getArguments();
        mAddressFromSetting = bundle.getString("address");
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentSearchScreenBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_screen, container, false);
        // Bind Resources
        fragmentSearchScreenBinding.atComplete.requestFocus();
        // show soft keyboard

        fragmentSearchScreenBinding.tvAdressFromSetting.setText(mAddressFromSetting);
     /*   mActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        try {

            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addApi(Places.GEO_DATA_API)
                    .enableAutoManage(mActivity, GOOGLE_API_CLIENT_ID, this)
                    .addConnectionCallbacks(this)
                    .build();

        } catch (Exception e) {
            e.printStackTrace();
        }
        fragmentSearchScreenBinding.atComplete.performClick();
        fragmentSearchScreenBinding.lnMain.setOnClickListener(this);
        fragmentSearchScreenBinding.imgDelete.setOnClickListener(this);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (Utils.isNetworkAvailable(mActivity)) {
            // mECompleteTextView.setText(mAddressFromSetting);
            fragmentSearchScreenBinding.atComplete.setFocusableInTouchMode(true);
            fragmentSearchScreenBinding.atComplete.clearFocus();
            fragmentSearchScreenBinding.atComplete.requestFocus();
            fragmentSearchScreenBinding.atComplete.setFocusable(true);
            fragmentSearchScreenBinding.atComplete.setThreshold(3);
            fragmentSearchScreenBinding.atComplete.setOnItemClickListener(mAutocompleteClickListener);

            mPlaceArrayAdapter = new PlaceArrayAdapter(mActivity, android.R.layout.simple_list_item_1,
                    BOUNDS_MOUNTAIN_VIEW, null);
            fragmentSearchScreenBinding.atComplete.setAdapter(mPlaceArrayAdapter);
        }
        return fragmentSearchScreenBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().post(this);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            assert item != null;
            final String placeId = String.valueOf(item.placeId);
            if (item.description != null) {
                mAddress = item.description + "";
                fragmentSearchScreenBinding.atComplete.getText().clear();
                PlaceChange placeChange = new PlaceChange();
                placeChange.setPlace(mAddress);

                placeChange.setPlaceId(placeId);
                EventBus.getDefault().post(placeChange);
                Utils.hideSoftKeyboard(mActivity);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mActivity.getSupportFragmentManager().popBackStack();
                    }
                }, 100);
            }
        }
    };


    /**
     * hide view when have a address search action
     *
     * @param mHideAdress
     */
    @Subscribe
    public void HideView(HideAdress mHideAdress) {
        fragmentSearchScreenBinding.tvAdressFromSetting.setText("");
        fragmentSearchScreenBinding.tvAdressFromSetting.setBackgroundResource(R.color.white);
    }

/*
    @OnClick(R.id.atComplete)
    public void showKeyboard() {
        mECompleteTextView.setFocusableInTouchMode(true);
        mECompleteTextView.requestFocus();
        Utils.showSoftKeyboard(mActivity);
    }*/

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if(mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(mActivity);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mGoogleApiClient != null) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null) {
            if(mGoogleApiClient.isConnected()) {
                mPlaceArrayAdapter.setGoogleApiClient(null);
                mGoogleApiClient.disconnect();
            }
        }

        super.onStop();
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mPlaceArrayAdapter != null)
            mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e("sssDEBUG", "Google Places API connection suspended.");
    }

    @Override
    public void onResume() {
        RxBus.getInstance().post("title");
        fragmentSearchScreenBinding.atComplete.setFocusableInTouchMode(true);
        fragmentSearchScreenBinding.atComplete.requestFocus();
        super.onResume();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("sssDEBUG", "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(mActivity,
                "Mất kết nối internet, xin vui lòng thử lại sau !"
                ,
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("SearchPlace", "onDestroy");
        // send broadcast , back to setting
        RxBus.getInstance().post("setting");
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_SETTING;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgDelete:
                fragmentSearchScreenBinding.atComplete.setText("");
                break;
        }
    }
}
