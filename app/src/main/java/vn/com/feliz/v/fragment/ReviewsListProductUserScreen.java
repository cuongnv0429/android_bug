package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.CreateReview;
import vn.com.feliz.p.eventbus.positionProductByFriend;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_Product_byFriend_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsListProductUserScreen extends BaseFragment implements OnPostResponseListener, OnItemClickListener, OnLoadMoreListener {
    private Reviews_Product_byFriend_Adapter mReviewsMeAdapter;
    @BindView(R.id.container_review_friend_list)
    FrameLayout mContainer_from_user_list;
    @BindView(R.id.recycleReviewsFriendList)
    RecyclerView mRecyclerView;
    @BindView(R.id.tv_sum_list_by_user)
    TextView mCountByUser;
    @BindView(R.id.rl_top_bar_by_user)
    RelativeLayout mTopbarListUser;
    @BindView(R.id.edt_search_bar_top_by_user)
    EditText mEdtSearchListPrduct;
    @BindView(R.id.title_bar_review_list)
    TextView mTitleBar;
    @BindView(R.id.swipe_review_friends_list)
    SwipeRefreshLayout mSwipeReviewFriendsList;
    @BindView(R.id.btn_retry_friends_list)
    Button mBtnRetryFriendsList;
    @BindView(R.id.txt_no_data_friend_list)
    TextView txtNoDataFriendList;

    private List<ReviewAllItem> mReviewMeItemList = new ArrayList<>();
    private List<ReviewAllItem> mReviewMeItemListSum = new ArrayList<>();
    private OnLoadMoreListener mOnLoadMoreListener;
    private ReviewsPrensenter mPrensenter;
    private String mTokenUser;
    //TransactionInfo
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ListAllReviewsResponse mReviewAllResponse;
    public static int totalListOfUser;
    private ProgressDialog mProgressDialog;
    private String keyWordSearch;
    private boolean isSearch;
    private String mUserId;
    private String mUserName;
    private CountDownTimer waitTimer;
    View.OnKeyListener onKeyListener;

    public ReviewsListProductUserScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrensenter = new ReviewsPrensenter(mActivity, this);

        Bundle bundle = getArguments();
        if (bundle != null) {
            mUserId = bundle.getString(Constant.F_ARG_USER_ID);
            mUserName = bundle.getString(Constant.F_ARG_USER_NAME);
        }

        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reviews_fiend_list, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        mEdtSearchListPrduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(200, 200) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }

                    public void onFinish() {
                        //After 60000 milliseconds (60 sec) finish current
                        keyWordSearch = s.toString().trim();
                        getEventSearch();

                    }
                }.start();
            }
        });
        //set font size
        mCountByUser.setTextSize(6 * mActivity.getResources().getDisplayMetrics().density);
        //setup recycleView
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        setOnLoadMoreListener(this);

        //set Data first
        mPageOffset = 0;
        setData(mPageOffset, mPageLimit);
        inItRecycleViewsLoadmore();
        onKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                        if (fragment instanceof ReviewsListProductUserScreen) {
                            mActivity.getSupportFragmentManager().beginTransaction()
                                    .remove(fragment).commit();
                            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            mActivity.getSupportFragmentManager().executePendingTransactions();
                            ReviewsScreen.mRlNewReview.removeAllViewsInLayout();
                            ReviewsScreen.mRlNewReview.setVisibility(View.GONE);
                            Utils.hideSoftKeyboard(mActivity);
                            mActivity.lockEventIn(600);
                        }
                        return true;
                    }
                }
                return false;

            }
        };
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //refresh event listener back press
                    if (((String) o).equalsIgnoreCase("refresh_listener_back_press")) {
                        if (getView() != null) {
                            getView().setFocusableInTouchMode(true);
                            getView().requestFocus();
                            if (onKeyListener != null) {
                                getView().setOnKeyListener(onKeyListener);
                            }
                        }
                    }
                }
            }
        });
        mSwipeReviewFriendsList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!isSearch) {
                    mPageOffset = 0;
                    setData(mPageOffset, mPageLimit);
                } else {
                    getEventSearch();
                }
            }
        });
        mBtnRetryFriendsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isSearch) {
                    mPageOffset = 0;
                    if (mPrensenter.hasUserInfo()) {
                        mTokenUser = mPrensenter.getUserInfo().getToken();
                        //API get ListReviews
                        try {
                            mPrensenter.getListByUser(mTokenUser, String.valueOf(mPageOffset),
                                    String.valueOf(mPageLimit), mUserId);
                        } catch (Exception e) {
                            Utils.hideLoading(mProgressDialog);
                            e.printStackTrace();
                        }
                    }
                    if (mReviewsMeAdapter != null) {
                        setUpRecycleView();
                    }
                } else {
                    getEventSearch();
                }
            }
        });
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(onKeyListener);
    }


    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }

    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerView != null) {
            mReviewsMeAdapter = new Reviews_Product_byFriend_Adapter(mActivity, mReviewMeItemListSum);
            mRecyclerView.setAdapter(mReviewsMeAdapter);
        }
    }

    /**
     * set Data first load
     */
    private void setData(int mOffset, int mLimit) {
        if (!mSwipeReviewFriendsList.isRefreshing()) {
            mProgressDialog = Utils.showLoading(mActivity);
        }
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            //API get ListReviews
            try {
                mPrensenter.getListByUser(mTokenUser, String.valueOf(mOffset),
                        String.valueOf(mLimit), mUserId);
            } catch (Exception e) {
                Utils.hideLoading(mProgressDialog);
                e.printStackTrace();
            }
        }
        if (mReviewsMeAdapter != null) {
            setUpRecycleView();
        }
    }

    /**
     * set enable/disable loadMore
     */


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        switch (task.getType()) {
            case GET_LIST_PRODUCT_BY_USER:
                txtNoDataFriendList.setVisibility(View.GONE);
                mBtnRetryFriendsList.setVisibility(View.GONE);
                mSwipeReviewFriendsList.setVisibility(View.VISIBLE);
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();
                    if (mReviewAllResponse != null) {
                        try {
                            if (mReviewAllResponse.getTotal() != null) {
                                totalListOfUser = Integer.parseInt(mReviewAllResponse.getTotal());
                            }
                            mReviewMeItemList = mReviewAllResponse.getListReviews();
                            if (mSwipeReviewFriendsList.isRefreshing()) {
                                mReviewMeItemListSum = new ArrayList<>();
                                if (mReviewMeItemList != null) {
                                    mReviewMeItemListSum = mReviewMeItemList;
                                } else {
                                    if (isSearch) {
                                        mCountByUser.setText("0" + " " + getString(R.string.review_list_by_user) + " " + (mUserName));
                                        mReviewMeItemListSum.clear();
                                        mReviewsMeAdapter.notifyDataSetChanged();
                                        txtNoDataFriendList.setVisibility(View.VISIBLE);
                                    }
                                }
                                setUpRecycleView();
                                loading = true;
                                mSwipeReviewFriendsList.setRefreshing(false);
                            } else if (mReviewMeItemList != null) {
                                if (loading) {
                                    if (mCountByUser != null) {
                                        mCountByUser.setText(String.valueOf(totalListOfUser) + " " + getString(R.string.review_list_by_user) + " " + (mUserName));
                                    }
                                    mReviewMeItemListSum = mReviewMeItemList;
                                    setUpRecycleView();
                                } else if (!loading) {
                                    loading = true;
                                    mReviewMeItemListSum.addAll(mReviewMeItemList);
                                    mReviewsMeAdapter.notifyDataSetChanged();
                                }
                            } else if (mReviewMeItemList == null && isSearch) {
                                mCountByUser.setText("0" + " " + getString(R.string.review_list_by_user) + " " + (mUserName));
                                mReviewMeItemListSum.clear();
                                mReviewsMeAdapter.notifyDataSetChanged();
                                txtNoDataFriendList.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

        }
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        if (task.getThrowable() != null) {
            if (task.getType() == ApiTaskType.GET_LIST_PRODUCT_BY_USER) {
                Utils.hideLoading(mProgressDialog);
                mSwipeReviewFriendsList.setRefreshing(false);
                mReviewMeItemListSum.clear();
                mReviewMeItemList.clear();
                if (mReviewsMeAdapter != null) {
                    mReviewsMeAdapter.notifyDataSetChanged();
                }
                mSwipeReviewFriendsList.setVisibility(View.GONE);
                txtNoDataFriendList.setVisibility(View.VISIBLE);
                mBtnRetryFriendsList.setVisibility(View.VISIBLE);
            }
        }
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onItemClick(View view, int position) {
    }

    @Subscribe
    public void clickItem(positionProductByFriend positionProductByFriend) {
        int position = positionProductByFriend.getPosition();
        ReviewsDetailProductByUserScreen reviewsDetailProductScreen = new ReviewsDetailProductByUserScreen();
        Bundle bundle = new Bundle();
        bundle.putString(Constant.F_ARG_PRODUCT_IMG, mReviewMeItemListSum.get(position).getProduct_image());
        bundle.putString(Constant.F_ARG_PRODUCT_NAME, mReviewMeItemListSum.get(position).getProduct_name());
        bundle.putString(Constant.F_ARG_PRODUCT_CONTENT, mReviewMeItemListSum.get(position).getContent());
        bundle.putString(Constant.F_ARG_PRODUCT_RATING, mReviewMeItemListSum.get(position).getProduct_rating());
        if (mReviewMeItemListSum.get(position).getListStart() != null) {
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_5, mReviewMeItemListSum.get(position).getListStart().getFive());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_4, mReviewMeItemListSum.get(position).getListStart().getFour());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_3, mReviewMeItemListSum.get(position).getListStart().getThree());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_2, mReviewMeItemListSum.get(position).getListStart().getTwo());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_1, mReviewMeItemListSum.get(position).getListStart().getOne());
        }
        bundle.putString(Constant.F_ARG_PRODUCT_REVIEWS, mReviewMeItemListSum.get(position).getProduct_review());
        bundle.putString(Constant.F_ARG_PRODUCT_ID, mReviewMeItemListSum.get(position).getProduct_id());

        reviewsDetailProductScreen.setArguments(bundle);
        android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.add(R.id.rlNewReview, reviewsDetailProductScreen, "ReviewsDetailProductByUserScreen");
        fragmentTransaction.commit();
        Utils.hideLoading(mProgressDialog);
    }

    /**
     *
     */
    @OnClick(R.id.btnCreateReviewByUser)
    public void clickCreateView() {
        EventBus.getDefault().post(new CreateReview());
    }

    @OnClick(R.id.imgBackReviewListByUser)
    public void backFromListProductUser() {
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
        if (fragment instanceof ReviewsListProductUserScreen) {
            mActivity.getSupportFragmentManager().beginTransaction()
                    .remove(fragment).commit();
            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mActivity.getSupportFragmentManager().executePendingTransactions();
            ReviewsScreen.mRlNewReview.removeAllViewsInLayout();
            ReviewsScreen.mRlNewReview.setVisibility(View.GONE);
            Utils.hideSoftKeyboard(mActivity);
        }
        mActivity.lockEventIn(500);
    }

    @Subscribe
    public void feFreshData(refreshDataReview refreshData) {
        if (refreshData.getTypeScreen().equals("ReviewsListProductUserScreen")) {
            try {

                mPageOffset = 0;
                loading = true;

                mPrensenter.getListByUser(mTokenUser, String.valueOf(mPageOffset),
                        String.valueOf(mPageLimit), mUserId);
                // setData();
                inItRecycleViewsLoadmore();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLoadMore() {
        mReviewMeItemListSum.add(null);
        mReviewsMeAdapter.notifyItemInserted(mReviewMeItemListSum.size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Remove loading item
                mReviewMeItemListSum.remove(mReviewMeItemListSum.size() - 1);
                mReviewsMeAdapter.notifyItemRemoved(mReviewMeItemListSum.size());
                if (isSearch) {
                    isSearch = false;
                    requestGetListAllSearchReviewNextPage();
                } else {
                    requestGetListAllReviewNextPage();
                }

            }
        }, 1000);
    }

    /**
     * request list
     */
    private void requestGetListAllSearchReviewNextPage() {
        mPrensenter.getListMeReviewsSearch(mTokenUser, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), mUserId, keyWordSearch);
    }

    private void searchEvent() {
        //search event tab all
        mEdtSearchListPrduct.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (waitTimer != null) {
                    waitTimer.cancel();
                    waitTimer = null;
                }
                waitTimer = new CountDownTimer(200, 200) {

                    public void onTick(long millisUntilFinished) {
                        //called every 300 milliseconds, which could be used to
                        //send messages or some other action
                    }

                    public void onFinish() {
                        //After 60000 milliseconds (60 sec) finish current
                        keyWordSearch = s.toString().trim();
                        getEventSearch();

                    }
                }.start();
            }
        });
    }

    /**
     * search event
     */
    private void getEventSearch() {
        try {
            isSearch = true;
            loading = true;
            mPageOffset = 0;
            mPrensenter.getListByUserSearch(mTokenUser, String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), mUserId, keyWordSearch);
            inItRecycleViewsLoadmore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {

        mPrensenter.getListByUser(mTokenUser, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), mUserId);
    }

    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

}
