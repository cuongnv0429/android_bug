package vn.com.feliz.v.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

import vn.com.feliz.common.Utils;
import vn.com.feliz.R;

public class LauncherLockScreenActivity extends AppCompatActivity {
    private String mPackageName;
    private String mClassName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("HoangNM","onCreate......LauncherHomeActivity");
        setContentView(R.layout.activity_launcher_lock_screen);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("HoangNM","onResume......LauncherHomeActivity");
        // BYME
        // Intent intent = new Intent(this, LockScreenService.class);
        //startService(intent);

        getLauncherPackageName(this);

        if (!LockScreenActivity.isLocked) {
            if (!TextUtils.isEmpty(mPackageName) && !TextUtils.isEmpty(mClassName) && Utils.getLockScreenStatus(this)) {
                Intent systemIntent = new Intent();
                systemIntent.setComponent(new ComponentName(mPackageName, mClassName));
                startActivity(systemIntent);
            }
        }

        moveTaskToBack(false);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /**
     * get the system launcher package name and class name,we use system launcher as default<br/>
     * also we can let user to choose a launcher from system laucher and our custom launchers,"BaiDu" lock screen app and "Go" lock screen app just do this.<br/>
     *
     * @param context
     */
    private void getLauncherPackageName(Context context) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, 0); // old using PackageManager.GET_ACTIVITIES
        if (resInfoList != null) {
            ResolveInfo resInfo;
            for (int i = 0; i < resInfoList.size(); i++) {
                resInfo = resInfoList.get(i);
                if ((resInfo.activityInfo.applicationInfo.flags &
                        ApplicationInfo.FLAG_SYSTEM) > 0) {
                    mPackageName = resInfo.activityInfo.packageName;
                    mClassName = resInfo.activityInfo.name;

                    break;
                }
            }
        }

        Intent intent1 = new Intent(Intent.ACTION_MAIN);
        intent1.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent1, PackageManager.MATCH_DEFAULT_ONLY);
        String currentHomePackage = resolveInfo.activityInfo.packageName;
        Log.d("HoangNM", "currentHomePackage: " + currentHomePackage);
    }

}
