package vn.com.feliz.v.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.ProductUsedRating;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.R;
import vn.com.feliz.databinding.FragmentCouponReviewBinding;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.v.activity.MainActivity;

/**
 * Created by ITV01 on 1/10/17.
 */

public class FragmentCouponReview extends BaseFragment implements View.OnClickListener, OnPostResponseListener{
    private String TAG = "FragmentCouponReview";
    private static final String PRODUCT_USED_RATING = "productusedratings";
    FragmentCouponReviewBinding fragmentCouponReviewBinding;
    List<ProductUsedRating> productUsedRatings;
    private int mDeviceWidth;
    private CouponsPresenter couponsPresenter;
    private ProgressDialog mProgressDialog;
    private TSnackbar snackbar;
    private Boolean checkFB = false;
    private ProductUsedRating currentProduct;
    private boolean isShowKeyboard = false;
    private String idNewCouponReview;
    ShareDialog mShareDialog;
    private int mIsProductChoose = 0;
    private Bitmap mBitmaoImagesPostFackebook;

    public static FragmentCouponReview newInstance(String str) {
        FragmentCouponReview fragment = new FragmentCouponReview();
        Bundle args = new Bundle();
        args.putString(PRODUCT_USED_RATING, str);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().post(this);
        productUsedRatings = new ArrayList<>();
        if (getArguments() != null) {
            String str = getArguments().getString(PRODUCT_USED_RATING);
            try {
                JSONArray jsonArray = new JSONArray(str);
                for (int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductUsedRating pro = new ProductUsedRating();
                    if (jsonObject.has("id")) {
                        pro.setId(jsonObject.getString("id"));
                    }
                    if (jsonObject.has("image")) {
                        pro.setImage(jsonObject.getString("image"));
                    }
                    if (jsonObject.has("name")) {
                        pro.setName(jsonObject.getString("name"));
                    }
                    if (jsonObject.has("statusCheck")) {
                        pro.setStatusCheck(jsonObject.getBoolean("statusCheck"));
                    }
                    if (jsonObject.has("is_review")) {
                        pro.setIs_review(jsonObject.getString("is_review"));
                    }
                    if (jsonObject.has("barcode")) {
                        pro.setBarcode(jsonObject.getString("barcode"));
                    }
                    if (!pro.getIs_review().equalsIgnoreCase("1")) {
                        productUsedRatings.add(pro);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        MainActivity.callbackManager = CallbackManager.Factory.create();
        mShareDialog = new ShareDialog(mActivity);
        mShareDialog.registerCallback(MainActivity.callbackManager, callback);
        if (fragmentCouponReviewBinding == null) {
            fragmentCouponReviewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupon_review, container, false);
            intiView();
            if (productUsedRatings != null) {
                displayCheckProduct(productUsedRatings);
            }
        }
        return fragmentCouponReviewBinding.getRoot();
    }

    private void intiView() {
        fragmentCouponReviewBinding.mainTopbar.mainTopBarTitle.setText(R.string.coupons_Screen);
        fragmentCouponReviewBinding.mainTopbar.mainTopbarBackLn.setOnClickListener(this);
        couponsPresenter = new CouponsPresenter(mActivity, this);
        fragmentCouponReviewBinding.imgCircleCheck1.setOnClickListener(this);
        fragmentCouponReviewBinding.imgCircleCheck2.setOnClickListener(this);
        fragmentCouponReviewBinding.imgCircleCheck3.setOnClickListener(this);
        fragmentCouponReviewBinding.linearCheckUpFacebook.setOnClickListener(this);
        fragmentCouponReviewBinding.btnUp.setOnClickListener(this);
        fragmentCouponReviewBinding.linearCouponReview.btnSuccessReview.setOnClickListener(this);

        if(couponsPresenter.hasMetaInfo()!=null) {
            String ads_review = couponsPresenter.hasMetaInfo().getAds_review();
            if (ads_review != null) {
                fragmentCouponReviewBinding.txtUpToFacebook.setText("Đăng trên facebook để nhận gấp " + ads_review + " gb$");
            }
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        mDeviceWidth = displayMetrics.widthPixels;
        fragmentCouponReviewBinding.linearCouponReviewRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboardDefaultOfDevices();
            }
        });
        fragmentCouponReviewBinding.edtContentReviewCoupon.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View view, MotionEvent event) {
                // TODO Auto-generated method stub
                if (view.getId() ==R.id.edt_content_review_coupon) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction()&MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });


        fragmentCouponReviewBinding.scrollCouponReview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                if (getView() != null) {
                    getView().getWindowVisibleDisplayFrame(rect);
                    int screenHeight = getView().getRootView().getHeight();
                    int heightDifference = screenHeight - rect.bottom + rect.top;
                    if (heightDifference > screenHeight * 0.15) {
                        if (!isShowKeyboard) {
                            isShowKeyboard = true;
                        }
                    } else {
                        if (isShowKeyboard) {
                            isShowKeyboard = false;
                        }
                    }
                }

            }
        });
    }

    //cuongnv
    private void displayCheckProduct(List<ProductUsedRating> productUsedRatings) {
        this.productUsedRatings = productUsedRatings;
        switch (productUsedRatings.size()) {
            case 1:
                fragmentCouponReviewBinding.linearProduct2.setVisibility(View.GONE);
                fragmentCouponReviewBinding.linearProduct3.setVisibility(View.GONE);
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                    loadImageProductRating(this.productUsedRatings.get(0).getImage(), fragmentCouponReviewBinding.imgProduct1);
                    fragmentCouponReviewBinding.linearProduct1.setVisibility(View.VISIBLE);
                }

                if (this.productUsedRatings.get(0).getStatusCheck()) {
                    fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                } else {
                    fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                }
                break;
            case 2:
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearProduct1.setVisibility(View.VISIBLE);
                    loadImageProductRating(this.productUsedRatings.get(0).getImage(),
                            fragmentCouponReviewBinding.imgProduct1);
                    if (this.productUsedRatings.get(0).getStatusCheck()) {
                        fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                    } else {
                        fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                    }
                }
                if (this.productUsedRatings.get(1).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearProduct2.setVisibility(View.VISIBLE);
                    loadImageProductRating(this.productUsedRatings.get(1).getImage(),
                            fragmentCouponReviewBinding.imgProduct2);

                    if (this.productUsedRatings.get(1).getStatusCheck()) {
                        fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                    } else {
                        fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    }
                }
                fragmentCouponReviewBinding.linearProduct3.setVisibility(View.GONE);
                break;
            case 3:
                if (this.productUsedRatings.get(0).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearProduct1.setVisibility(View.VISIBLE);
                    loadImageProductRating(this.productUsedRatings.get(0).getImage(),
                            fragmentCouponReviewBinding.imgProduct1);

                    if (this.productUsedRatings.get(0).getStatusCheck()) {
                        fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                    } else {
                        fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                    }
                }
                if (this.productUsedRatings.get(1).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearProduct2.setVisibility(View.VISIBLE);
                    loadImageProductRating(this.productUsedRatings.get(1).getImage(),
                            fragmentCouponReviewBinding.imgProduct2);

                    if (this.productUsedRatings.get(1).getStatusCheck()) {
                        fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                    } else {
                        fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    }
                }
                if (this.productUsedRatings.get(2).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearProduct3.setVisibility(View.VISIBLE);
                    loadImageProductRating(this.productUsedRatings.get(2).getImage(),
                            fragmentCouponReviewBinding.imgProduct3);

                    if (this.productUsedRatings.get(2).getStatusCheck()) {
                        fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir1);
                    } else {
                        fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    }
                }
                break;
        }
    }
    //cuongnv
    private void loadImageProductRating(String linkImage, final ImageView imageView) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        mDeviceWidth = displayMetrics.widthPixels;
        Picasso
                .with(mActivity)
                .load(linkImage)
                .resize(mDeviceWidth / 3, (int) (mDeviceWidth * 0.5))
                .onlyScaleDown()
                .into(imageView);
    }

    /**
     * FacebookCallback
     */
    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Utils.hideLoading(mProgressDialog);
            Toast.makeText(mActivity, "Đã chia sẻ sản phẩm lên trang cá nhân của bạn !", Toast.LENGTH_LONG).show();
            String token = couponsPresenter.getUserInfo().getToken();
            if (!token.equalsIgnoreCase("")) {
                addReview(token);
            }
            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            Utils.hideLoading(mProgressDialog);
            fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
            checkFB = false;
            String token = couponsPresenter.getUserInfo().getToken();
            if (!token.equalsIgnoreCase("")) {
                addReview(token);
            }
            //  Toast.makeText(mActivity, "callbackManager cancle posted", Toast.LENGTH_SHORT).show();
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            Utils.hideLoading(mProgressDialog);
            fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
            checkFB = false;
            String token = couponsPresenter.getUserInfo().getToken();
            if (!token.equalsIgnoreCase("")) {
                addReview(token);
            }
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };

    //cuongnv
    private void updateCheckCircle(int position) {
        if (productUsedRatings == null) return;
        if (productUsedRatings.size() < position) return;
        switch (position) {
            case 1:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                if (productUsedRatings.size() > 1) {
                    fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(1).setStatusCheck(false);
                }
                if (productUsedRatings.size() > 2) {
                    fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(2).setStatusCheck(false);
                }
                break;
            case 2:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(0).setStatusCheck(false);

                if (productUsedRatings.size() > 2) {
                    fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(2).setStatusCheck(false);
                }
                break;
            case 3:
                if (productUsedRatings.get(position - 1).getStatusCheck()) {
                    fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir2);
                    productUsedRatings.get(position - 1).setStatusCheck(false);
                } else {
                    fragmentCouponReviewBinding.imgCircleCheck3.setBackgroundResource(R.drawable.cir1);
                    productUsedRatings.get(position - 1).setStatusCheck(true);
                }

                fragmentCouponReviewBinding.imgCircleCheck1.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(0).setStatusCheck(false);

                fragmentCouponReviewBinding.imgCircleCheck2.setBackgroundResource(R.drawable.cir2);
                productUsedRatings.get(1).setStatusCheck(false);
                break;
        }
    }

    private void addReview(String token) {
        String isShare = "";
        String barCode = currentProduct.getBarcode();
        String point = ((int) fragmentCouponReviewBinding.rtCouponReview.getRating()) + "";
        String content = fragmentCouponReviewBinding.edtContentReviewCoupon.getText().toString().trim();
        if (checkFB) {
            isShare = "1";
        } else {
            isShare = "0";
        }
        mProgressDialog = Utils.showLoading(mActivity);
        couponsPresenter.addReviewCoupon(token, barCode, point, content, isShare);
    }

    private int validate(String point, String content, String barCode) {
        if (barCode.equalsIgnoreCase("")) {
            return 2;
        }
        if (point.equalsIgnoreCase("0")) {
            return 0;
        }

        if (content.equalsIgnoreCase("")) {
            return 1;
        }

        return -1;
    }

    private void successReview(ProductUsedRating mPro) {
        if (mPro != null) {
            loadImageProductRating(mPro.getImage(), fragmentCouponReviewBinding.linearCouponReview.imgSuccessReview);
            fragmentCouponReviewBinding.linearCouponReview.txtSuccessReview.setText(mPro.getName());
            fragmentCouponReviewBinding.linearCouponReview.rtCouponReview.setRating(fragmentCouponReviewBinding.rtCouponReview.getRating());
            for (int i = 0; i < productUsedRatings.size(); i++) {
                if (productUsedRatings.get(i).getIs_review().equalsIgnoreCase("0")) {
                    fragmentCouponReviewBinding.linearCouponReview.btnSuccessReview.setText(getResources().getString(R.string.text_continute_review));
                    return;
                }
            }
            fragmentCouponReviewBinding.linearCouponReview.btnSuccessReview.setText(getResources().getString(R.string.text_finish_review));
        }
    }

    private void refreshViewAfterSuccessReview(ProductUsedRating pro) {
        for (int i = 0; i < productUsedRatings.size(); i++) {
            if (productUsedRatings.get(i).getId().equalsIgnoreCase(pro.getId())) {
                productUsedRatings.remove(i);
                break;
            }
        }
        displayCheckProduct(productUsedRatings);
        fragmentCouponReviewBinding.edtContentReviewCoupon.setText("");
        fragmentCouponReviewBinding.rtCouponReview.setRating(0);
        fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
        checkFB = false;
    }

    private void hideKeyboardDefaultOfDevices() {
        InputMethodManager inputMethod = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethod.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public void onStop() {
        super.onStop();
        RxBus.getInstance().post(currentProduct);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.main_topbar_back_ln:
                mActivity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.img_circle_check_1:
                mIsProductChoose = 0;
                fragmentCouponReviewBinding.imgProduct1.setDrawingCacheEnabled(true);
                fragmentCouponReviewBinding.imgProduct1.buildDrawingCache();
                mBitmaoImagesPostFackebook = fragmentCouponReviewBinding.imgProduct1.getDrawingCache();
                updateCheckCircle(1);
                break;
            case R.id.img_circle_check_2:
                mIsProductChoose = 1;
                fragmentCouponReviewBinding.imgProduct2.setDrawingCacheEnabled(true);
                fragmentCouponReviewBinding.imgProduct2.buildDrawingCache();
                mBitmaoImagesPostFackebook = fragmentCouponReviewBinding.imgProduct2.getDrawingCache();
                updateCheckCircle(2);
                break;
            case R.id.img_circle_check_3:
                mIsProductChoose = 2;
                fragmentCouponReviewBinding.imgProduct3.setDrawingCacheEnabled(true);
                fragmentCouponReviewBinding.imgProduct3.buildDrawingCache();
                mBitmaoImagesPostFackebook = fragmentCouponReviewBinding.imgProduct1.getDrawingCache();
                updateCheckCircle(3);
                break;
            case R.id.linear_check_up_facebook:
                if (!checkFB) {
                    fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir1);
                    checkFB = true;
                } else {
                    fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
                    checkFB = false;
                }
                break;
            case R.id.btn_up:
                for (int i = 0; i < productUsedRatings.size(); i++) {
                    if (productUsedRatings.get(i).getStatusCheck()) {
                        currentProduct = productUsedRatings.get(i);
                        break;
                    }
                }
                String barCode = currentProduct.getBarcode();
                String point = ((int) fragmentCouponReviewBinding.rtCouponReview.getRating()) + "";
                String content = fragmentCouponReviewBinding.edtContentReviewCoupon.getText().toString().trim();
                int mVarlidate = validate(point, content, barCode);
                if ( mVarlidate == 0) {
                    snackbar = TSnackbar
                            .make(getView(), getResources().getString(R.string.alert_validate_rating), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_validate_rating), mActivity);
                    Utils.vibarte(mActivity);
                    return;
                }
                if ( mVarlidate == 1) {
                    snackbar = TSnackbar
                            .make(getView(), getResources().getString(R.string.alert_validate_enter_rating), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_validate_enter_rating), mActivity);
                    Utils.vibarte(mActivity);
                    return;
                }

                if ( mVarlidate == 2) {
                    snackbar = TSnackbar
                            .make(getView(), getResources().getString(R.string.alert_validate_product_choose), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_validate_product_choose), mActivity);
                    Utils.vibarte(mActivity);
                    return;
                }
                if (checkFB) {
                    mProgressDialog = Utils.showLoading(mActivity);
                    checkPermission();
                } else {
                    String token = couponsPresenter.getUserInfo().getToken();
                    if (!token.equalsIgnoreCase("")) {
                        addReview(token);
                    }
                }
                break;
            case R.id.btn_success_review:
                if (fragmentCouponReviewBinding.linearCouponReview.btnSuccessReview.getText()
                        .equals(getResources().getString(R.string.text_continute_review))) {
                    fragmentCouponReviewBinding.scrollCouponReview.setVisibility(View.VISIBLE);
                    fragmentCouponReviewBinding.linearCouponReview.linearSuccessReview.setVisibility(View.GONE);
                    refreshViewAfterSuccessReview(currentProduct);
                } else if (fragmentCouponReviewBinding.linearCouponReview.btnSuccessReview.getText()
                        .equals(getResources().getString(R.string.text_finish_review))) {
                    mActivity.getSupportFragmentManager().popBackStack();
                }
                break;
        }
    }

    /**
     * checkPermission
     */
    private void checkPermission() {
        final Bundle permBundle = new Bundle();
        permBundle.putCharSequence("permission", "publish_actions");
        GraphRequest request = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions", permBundle, HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {

                        try {
                            JSONArray permList = (JSONArray) graphResponse.getJSONObject().get("data");
                            if (permList.length() == 0) {
                                postPhotoDialog();
                            } else {
                                JSONObject permData = (JSONObject) permList.get(0);
                                String permVal = (String) permData.get("status");
                                if (permVal.equals("granted")) {
                                    postFacebook();
                                } else {
                                    askForFBPublishPerm();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            snackbar = TSnackbar
                                    .make(getView(), getResources().getString(R.string.alert_facebook_post_error), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_facebook_post_error), mActivity);
                            Utils.vibarte(mActivity);
                            fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
                            checkFB = false;
                            String token = couponsPresenter.getUserInfo().getToken();
                            if (!token.equalsIgnoreCase("")) {
                                addReview(token);
                            }
                        }

                    }
                }
        );
        request.executeAsync();
    }

    //request permission
    void askForFBPublishPerm() {
        MainActivity.callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithPublishPermissions(
                mActivity,
                Arrays.asList("publish_actions"));

        LoginManager.getInstance().registerCallback(MainActivity.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Set<String> getRecentlyGrantedPermissions = loginResult.getRecentlyGrantedPermissions();
                if (getRecentlyGrantedPermissions.contains("publish_actions")) {
                    postFacebook();
                } else {
                 /*   shareImg();*/
                    postPhotoDialog();
                }

            }

            @Override
            public void onCancel() {
                snackbar = TSnackbar
                        .make(getView(), getResources().getString(R.string.alert_facebook_post_error), TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_facebook_post_error), mActivity);
                Utils.vibarte(mActivity);
                fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
                checkFB = false;
                String token = couponsPresenter.getUserInfo().getToken();
                if (!token.equalsIgnoreCase("")) {
                    addReview(token);
                }
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
                snackbar = TSnackbar
                        .make(getView(), getResources().getString(R.string.alert_facebook_post_error), TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, getResources().getString(R.string.alert_facebook_post_error), mActivity);
                Utils.vibarte(mActivity);
                fragmentCouponReviewBinding.imgCheck.setBackgroundResource(R.drawable.cir2);
                checkFB = false;
                String token = couponsPresenter.getUserInfo().getToken();
                if (!token.equalsIgnoreCase("")) {
                    addReview(token);
                }
            }

        });


    }

    private void postFacebook() {
        //convert Bitmap* into ByteArray
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        mBitmaoImagesPostFackebook.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Bundle params = new Bundle();
        params.putByteArray("picture", byteArray);
        params.putString("message", fragmentCouponReviewBinding.edtContentReviewCoupon.getText().toString());

        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                "/me/photos", params, HttpMethod.POST,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {
                        Utils.hideLoading(mProgressDialog);
                        Toast.makeText(mActivity, "Đã chia sẻ sản phẩm lên trang cá nhân của bạn !", Toast.LENGTH_SHORT).show();
                        String token = couponsPresenter.getUserInfo().getToken();
                        if (!token.equalsIgnoreCase("")) {
                            addReview(token);
                        }
                    }
                });
        request.executeAsync();
    }

    private void postPhotoDialog() {
        if(mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }
        //  Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        SharePhoto.Builder photoBuilder = new SharePhoto.Builder();
        photoBuilder.setBitmap(mBitmaoImagesPostFackebook);
        // photoBuilder.setCaption("HBD Caption");
        final SharePhoto sharePhoto = photoBuilder.build();
        SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(sharePhoto).build();
        mShareDialog.show(content);
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        BaseResponse baseResponse;
        switch (task.getType()) {
            case ADD_REVIEW_COUPON:
                if (mProgressDialog != null) {
                    Utils.hideLoading(mProgressDialog);
                }
                baseResponse = (BaseResponse) task.getResponse().body();
                if (baseResponse != null) {
                    if (status == 200) {
                        Log.e(TAG, "succsess");
                        for (int i = 0; i < productUsedRatings.size(); i++) {
                            if (productUsedRatings.get(i).getId().equalsIgnoreCase(currentProduct.getId())) {
                                productUsedRatings.get(i).setStatusCheck(true);
                                productUsedRatings.get(i).setIs_review("1");
                            }
                        }
                        fragmentCouponReviewBinding.scrollCouponReview.setVisibility(View.GONE);
                        fragmentCouponReviewBinding.linearCouponReview.linearSuccessReview.setVisibility(View.VISIBLE);
                        RxBus.getInstance().post(currentProduct);
                        successReview(currentProduct);
                    } else {
                        snackbar = TSnackbar
                                .make(getView(), baseResponse.getMessage() , TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                        Utils.vibarte(mActivity);
                    }
                }
                break;
        }
        return false;
    }
}
