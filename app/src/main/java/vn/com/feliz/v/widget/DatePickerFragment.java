package vn.com.feliz.v.widget;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.DatePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import vn.com.feliz.v.interfaces.OnBirthdaySetListener;


/**
 * Copyright Innotech Vietnam
 *
 */
public class DatePickerFragment
           extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    /** Listener Instance */
    private OnBirthdaySetListener mListener;
    /** Current Date */
    private String mCurrentDate;

    /**
     * Set Listener
     * @param listener IViewRegister
     */
    public void setListener(OnBirthdaySetListener listener) {
        mListener = listener;
    }

    /**
     * Set Current Date
     * @param date String
     */
    public void setCurrentDate(String date) {
        mCurrentDate = date;
    }

    /**
     * Override to build your own custom Dialog container.  This is typically
     * used to show an AlertDialog instead of a generic Dialog; when doing so,
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)} does not need
     * to be implemented since the AlertDialog takes care of its own content.
     * <p>
     * <p>This method will be called after {@link #onCreate(Bundle)} and
     * before {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.  The
     * default implementation simply instantiates and returns a {@link Dialog}
     * class.
     * <p>
     * <p><em>Note: DialogFragment own the {@link Dialog#setOnCancelListener
     * Dialog.setOnCancelListener} and {@link Dialog#setOnDismissListener
     * Dialog.setOnDismissListener} callbacks.  You must not set them yourself.</em>
     * To find out about these events, override {@link #onCancel(DialogInterface)}
     * and {@link #onDismiss(DialogInterface)}.</p>
     *
     * @param savedInstanceState The last saved instance state of the Fragment,
     *                           or null if this is a freshly created Fragment.
     * @return Return a new Dialog instance to be displayed by the Fragment.
     */
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();

        if (!TextUtils.isEmpty(mCurrentDate)) {
            try {
                SimpleDateFormat format = new
                        SimpleDateFormat("dd/MM/yyyy", Locale.US);
                c.setTime(format.parse(mCurrentDate));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog;
        //if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        //} else {
        //    dialog = new DatePickerDialog(getActivity(),
        //            R.style.AppTheme_DatePickerDialog,
        //            this, year, month, day);
        //}
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        return dialog;
    }

    /**
     * @param view        The view associated with this listener.
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if (mListener != null) {
            mListener.onBirthdaySet(year, monthOfYear, dayOfMonth);
        }
    }
}
