package vn.com.feliz.v.interfaces;

/**
 * Copyright Innotech Vietnam
 */
public interface OnMenuItemSelected {
    int SELECT_COUPONS = 1;
    int SELECT_COINS = 2;
    int SELECT_REVIEWS= 3;
    int SELECT_SHOPPING = 4;
    int SELECT_SETTING = 5;
    int SELECT_SETTING_CONFIRM_REGISTER = 6;
    int TERMS_AND_CONDITIONS = 7;
    int INPUT_STORE_CODE = 8;

    /**
     * @return Type
     */
    int getType();
}
