package vn.com.feliz.v.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashSet;
import java.util.List;

import vn.com.feliz.m.response.ShopModel;
import vn.com.feliz.R;
import vn.com.feliz.databinding.RvItemCouponBackBinding;

public class CouponsBackAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<ShopModel.Shop> mData;
    protected HashSet<MapView> mMapViews = new HashSet<>();
    private int mSelectedPos = -1;
    private OnItemRecyclerListener onItemListener;
    private int mExpandedPosition;
    private ViewGroup mParent;
    private Location mLocation;

    public static class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener,OnMapReadyCallback {

        /**
         * callback when click on button of each item
         */
        protected GoogleMap mGoogleMap;
        private IViewHolderMovie mCallback;
        private RvItemCouponBackBinding mBinding;
        private Context mContext;
        private ShopModel.Shop mShop;

        public ViewHolderItem(Context context, RvItemCouponBackBinding binding, IViewHolderMovie callback) {
            super(binding.getRoot());
            mContext = context;
            mCallback = callback;
            mBinding = binding;
            //binding.rlCouponItemRoot.setOnClickListener(this);
            mBinding.map.onCreate(null);
            mBinding.map.getMapAsync(this);
        }

        public RvItemCouponBackBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {
            mCallback.OnClicked(v, ViewHolderItem.this, getAdapterPosition());
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            mGoogleMap = googleMap;

            MapsInitializer.initialize(mContext);
            googleMap.getUiSettings().setMapToolbarEnabled(false);

            // If we have map data, update the map content.
            if (mShop != null) {
                updateMapContents();
            }
        }

        public void setMapLocation(ShopModel.Shop shop) {
            mShop = shop;
            // If the map is ready, update its content.
            if (mGoogleMap != null) {
                updateMapContents();
            }
        }

        private void updateMapContents() {
            // Since the mapView is re-used, need to remove pre-existing mapView features.
            mGoogleMap.clear();
            LatLng center = new LatLng(mShop.getLatitude(), mShop.getLongitude());
            // Update the mapView feature data and camera position.
            mGoogleMap.addMarker(new MarkerOptions().position(center));

            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(center, 15f);
            mGoogleMap.moveCamera(cameraUpdate);
        }

        public interface IViewHolderMovie {
            void OnClicked(View v, ViewHolderItem viewHolder, int position);
        }
    }

    public CouponsBackAdapter(Context context, List<ShopModel.Shop> data, OnItemRecyclerListener listener) {
        mContext = context;
        mData = data;
        onItemListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        mParent = parent;
        RvItemCouponBackBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_item_coupon_back, parent, false);
        final ViewHolderItem viewHolder = new ViewHolderItem(mContext, binding, new ViewHolderItem.IViewHolderMovie() {
            @Override
            public void OnClicked(View v, final ViewHolderItem vHolder, int position) {
                onItemListener.onRecyclerViewItemClicked(v, position);
            }
        });
        mMapViews.add(binding.map);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        final ViewHolderItem holder = (ViewHolderItem) viewHolder;
        holder.setMapLocation(mData.get(position));

        holder.getBinding().tvDistance.setText(mData.get(position).getDistance());
        holder.getBinding().tvStatus.setText(mData.get(position).getStatusDes());
        holder.getBinding().tvShopName.setText((position+1) + ". " + mData.get(position).getName());

        final boolean isExpanded = position==mExpandedPosition;
        holder.getBinding().flMap.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        holder.itemView.setActivated(isExpanded);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1:position;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    TransitionManager.beginDelayedTransition(mParent);
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData==null?0:mData.size();
    }

    public List<ShopModel.Shop> getData() {
        return mData;
    }

    public HashSet<MapView> getMapViews() {
        return mMapViews;
    }
    /**
     * interface to fire event of item
     */
    public interface OnItemRecyclerListener {
        void onRecyclerViewItemClicked(View v, int pos);
    }

    public void setData(List<ShopModel.Shop> data) {
        mData = data;
    }
}
