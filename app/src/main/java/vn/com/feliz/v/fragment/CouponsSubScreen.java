package vn.com.feliz.v.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.v.adapter.CouponsAdapter;
import vn.com.feliz.R;
import vn.com.feliz.databinding.FragmentCouponsSubScreenBinding;
import vn.com.feliz.network.OnPostResponseListener;


public class CouponsSubScreen extends BaseFragment implements OnMenuItemSelected,
        CouponsAdapter.OnItemRecyclerListener, View.OnClickListener, OnPostResponseListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private FragmentCouponsSubScreenBinding mBinding;
    private CouponModel.Coupon mParentCoupon;
    private boolean mCanBack;
    private ProgressDialog mProgressDialog;
    private Dialog mDialog;
    private static boolean is_updated_sub_viewed = false;

public static boolean isLoad;

    public CouponsSubScreen() {
        // Required empty public constructor
    }

    public static CouponsSubScreen newInstance(CouponModel.Coupon parentCoupon) {
        return CouponsSubScreen.newInstance(parentCoupon, false);
    }

    public static CouponsSubScreen newInstance(CouponModel.Coupon parentCoupon, boolean canBack) {
        CouponsSubScreen fragment = new CouponsSubScreen();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, parentCoupon);
        args.putBoolean(ARG_PARAM2, canBack);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("mCouponSubScreen:SonDB1", ">>");
        isLoad= true;

        if (getArguments() != null) {
            mParentCoupon = (CouponModel.Coupon) getArguments().getSerializable(ARG_PARAM1);
            mCanBack = getArguments().getBoolean(ARG_PARAM2, false);
        }

        MainActivity.not_from_list = true;
        MainActivity.from_sub_list = true;
        //Picasso.with(getActivity()).setIndicatorsEnabled(true);
    }

    public void onFragmentResume() {
        Log.i("COUPON LIST", "IS RESUME 1x");

        if(mBinding.rvCoupons != null) {
            CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
            if(adapter != null) {
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupons_sub_screen, container, false);
        /*View decorView = getActivity().getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(uiOptions);*/
        mBinding.rvCoupons.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.rvCoupons.addOnScrollListener(new EndlessScrollListener());
        mBinding.mainTopbar.mainTopBarTitle.setText(R.string.coupons_Screen);
        //mBinding.icLoaddingCoupons.setImageResource(R.drawable.loadding_ic_gif);
        Log.d("HoangNM", "onCreateView......");
        if(mParentCoupon!=null) {
            Log.i("mCouponSubScreen:SonDB2", mParentCoupon.toString());
        } else {
            Log.i("mCouponSubScreen:SonDB2", "null");
        }
        if (mCanBack) {
            mBinding.mainTopbar.mainTopbarBackLn.setOnClickListener(this);
        } else {
            mBinding.mainTopbar.mainTopbarBackLn.setVisibility(View.INVISIBLE);
        }

        if(mParentCoupon!=null) {
            if (MainActivity.reload_list) {
                for (int i = 0; i < MainActivity.couponList.size(); i++) {
                    if (MainActivity.couponList.get(i).getCouponId().equals(mParentCoupon.getCouponId())) {
                        mParentCoupon = MainActivity.couponList.get(i);
                    }
                }
            }
            Collections.sort(mParentCoupon.getList(), new CustomComparator());
            mBinding.rvCoupons.setAdapter(new CouponsAdapter(getContext(), mParentCoupon.getList(), CouponsSubScreen.this));
        } else {
            mActivity.getSupportFragmentManager().popBackStack();
        }

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COUPONS;
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("COUPON LIST", "IS RESUME 2");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v == null) return;
        switch (v.getId()) {
            case R.id.main_topbar_back_ln:
                MainActivity.not_from_list = false;
                MainActivity.from_sub_list = false;

                if(!MainActivity.reload_list) {
                    setSubViewed();
                    mActivity.getSupportFragmentManager().popBackStack();
                } else {
                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                }
                break;
        }
    }

    private static boolean mIsRecyclerItemClicked;

    @Override
    public void onRecyclerViewItemClicked(View v, int pos) {

        CouponModel.Coupon coupon;

        switch (v.getId()) {
            case R.id.rl_coupon_item_root:
                //fix click twice rapidly, we should stop and wait 700ms
                if (mIsRecyclerItemClicked) return;
                mIsRecyclerItemClicked = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIsRecyclerItemClicked = false;
                    }
                }, 700);

                MainActivity.reload_detail = false;
                MainActivity.reload_list = false;

                setSubViewed();

                coupon = ((CouponsAdapter) mBinding.rvCoupons.getAdapter()).getData().get(pos);
                //Utils.saveTimesEnterCouponDetail(getContext(), Utils.getTimesEnterCouponDetail(getContext()) + 1);
                mActivity.getSupportFragmentManager()
                        .beginTransaction()
//                        .setCustomAnimations(R.anim.fragment_move_in_from_right_to_left, R.anim.fragment_move_out_from_right_to_left,
//                                R.anim.fragment_move_in_from_left_to_right, R.anim.fragment_move_out_from_left_to_right)
                        .replace(R.id.root_frame, CouponsDetailScreen.newInstance(coupon), "coupon_detail")
                        .addToBackStack(null)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commitAllowingStateLoss();
                getChildFragmentManager().executePendingTransactions();

                break;
            case R.id.iv_rating:
                coupon = ((CouponsAdapter) mBinding.rvCoupons.getAdapter()).getData().get(pos);
                viewDetailReview(coupon);
                //Toast.makeText(getContext(), "Tính năng này đang được phát triển.", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void setSubViewed() {
        // Set viewed for sub items
        if(!is_updated_sub_viewed) {
            if(mParentCoupon != null) {
                CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
                adapter.updateViewedSub(mParentCoupon.getCouponId());
                adapter.notifyDataSetChanged();
            }
        } else {
            is_updated_sub_viewed = true;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    class EndlessScrollListener extends RecyclerView.OnScrollListener {
        public EndlessScrollListener() {
            super();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }
    }

    private class CustomComparator implements Comparator<CouponModel.Coupon> {
        @Override
        public int compare(CouponModel.Coupon obj1, CouponModel.Coupon obj2) {
            if (obj1.getStatus().compareTo(obj2.getStatus()) == 0) {//equal
                return obj1.getCouponExpiredDate().compareTo(obj2.getCouponExpiredDate());
            }
            return obj1.getStatus().compareTo(obj2.getStatus());
        }
    }

    private void viewDetailReview(final CouponModel.Coupon coupon) {
        if(coupon == null) {
            return;
        }

        List<CouponModel.Coupon.Product> products = coupon.getListProduct();

        int mNumberProduct = 0;
        if(products != null && products.size() > 0) {
            mNumberProduct = products.size();
        }

        if(mNumberProduct == 0) {
            TSnackbar snackbar = TSnackbar
                    .make(getView(),  getString(R.string.coupon_not_have_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.coupon_not_have_product), mActivity);
            return;
        }

        if(mNumberProduct == 1) {
            gotoProductReview(Integer.parseInt(products.get(0).getId()));
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int mDeviceWidth = displayMetrics.widthPixels - 50;

        int partWidth = (mDeviceWidth / 3) - 10;

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater li = LayoutInflater.from(mActivity);
        final View myView = li.inflate(R.layout.dialog_select_product_view_review, null);

        View product1 = myView.findViewById(R.id.dialog_select_product_1);
        View product2 = myView.findViewById(R.id.dialog_select_product_2);
        View product3 = myView.findViewById(R.id.dialog_select_product_3);

        // Reset all view to invisible
        product1.setVisibility(View.GONE);
        product2.setVisibility(View.GONE);
        product3.setVisibility(View.GONE);

        builder.setView(myView);
        mDialog = builder.create();
        mDialog.show();

        int imageWidth = partWidth;
        int imageHeight = partWidth * 2;

        ViewGroup.LayoutParams p = product1.getLayoutParams();
        p.width = imageWidth;
        p.height = imageHeight;

        product1.setLayoutParams(p);
        product2.setLayoutParams(p);
        product3.setLayoutParams(p);

        if(mNumberProduct >= 1) {
            product1.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_1);
            Picasso.with(getContext()).load(products.get(0).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product1.setId(Integer.parseInt(products.get(0).getId()));

            product1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });

            product1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 2) {
            product2.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_2);
            Picasso.with(getContext()).load(coupon.getListProduct().get(1).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product2.setId(Integer.parseInt(products.get(1).getId()));

            product2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 3) {
            product3.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_3);
            Picasso.with(getContext()).load(coupon.getListProduct().get(2).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product3.setId(Integer.parseInt(products.get(2).getId()));

            product3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product3.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private void gotoProductReview(int product_id) {

        if(mDialog != null) {
            mDialog.dismiss();
        }

        if(mActivity.checkAndAlertNetWork(getView())) {
            CouponsPresenter mCouponsPresenter = new CouponsPresenter(mActivity, this);
            if (mCouponsPresenter.hasUserInfo()) {
                mProgressDialog = Utils.showLoading(getContext());
                String mTokenUser = mCouponsPresenter.getUserInfo().getToken();
                mCouponsPresenter.getListAllReviewByProductPresenta(mTokenUser, "" + product_id, "", "", "product");
            }
        }

    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mActivity.showNetworkAlert(getView());
        } else {
            switch (task.getType()) {
                case GET_LIST_REVIEW_BY_PRODUCT_ONLY:
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    if (status == ApiResponseCode.SUCCESS) {
                        ReviewAllItem mReviewAllItem = (ReviewAllItem) baseResponse.getData();

                        mActivity.gotoReviewDetail(mReviewAllItem);

                    } else {
                        TSnackbar snackbar = TSnackbar
                                .make(getView(), baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                        Utils.vibarte(mActivity);
                    }
                    break;
            }
        }
        return false;
    }

}
