package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.RequestFocus;
import vn.com.feliz.p.eventbus.RequestFocusInNewReview;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewResultScanbarCodeScreen extends BaseFragment implements OnPostResponseListener, OnMenuItemSelected, SurfaceHolder.Callback {
    private String mNameProduct;
    private String mRatingvalue;
    private String mComment;
    private String mMediaUrl;
    private String mBarcode;
    @BindView(R.id.imgResult)
    ImageView mImageProduct;
    @BindView(R.id.tvNameProduct)
    TextView mTvNameProduct;
    @BindView(R.id.rb_rating_result)
    RatingBar mRatingBarResult;
    @BindView(R.id.tvCommentResult)
    TextView mTvCommentResult;
    @BindView(R.id.item_submit_facebook_check)
    CheckBox mCheckBox;
    private ReviewsPrensenter mPrensenter;
    private String mToken_user;
    private String mIsCheck;
    private String mIsHasImageScanBarcode;

    private Camera camera;
    @BindView(R.id.cameraView)
    SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera.PictureCallback captureImageCallback;
    // private boolean frontCam;
    @BindView(R.id.lnMainResult)
    LinearLayout mMainResult;
    @BindView(R.id.lnCamera)
    LinearLayout mMainCamera;
    @BindView(R.id.btnCapture)
    ImageView mBtnCapture;
    private String mPath;
    private TSnackbar snackbar;
    File file;
    @BindView(R.id.rl_top_bar_reviews)
    RelativeLayout mTopbar;
    @BindView(R.id.main_top_bar_title_review)
    TextView mTitleTopbar;
    @BindView(R.id.btnSubmitCalback)
    TextView mBtnCallback;
    @BindView(R.id.btnSubmitReview)
    TextView mBtnSubmit;
    @BindView(R.id.tv_item_submit_facebook)
    TextView TvFacebook;
    @BindView(R.id.imgBackReviewRs)
    ImageView mBackImg;
    private boolean isCameraOpen;
   private ProgressDialog mProgressDialog;
    Bitmap bmp;
    LoginManager mLoginManager;
    private boolean isHasImageCapture;
    ShareDialog mShareDialog;

    public ReviewResultScanbarCodeScreen() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    //  Log.i(tag, "keyCode: " + keyCode);
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        onBack();
                        return true;
                    }
                }
                return false;

            }


        });


    }

    @Subscribe
    public void requestFocusKeyback(RequestFocus requestFocus) {
        try {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        //  Log.i(tag, "keyCode: " + keyCode);
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            onBack();
                            return true;
                        }
                    }
                    return false;

                }


            });
        } catch (Exception e) {

        }
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mPrensenter = new ReviewsPrensenter(mActivity, this);
        // Initialize facebook SDK.


        EventBus.getDefault().post(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mNameProduct = bundle.getString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE);
            mRatingvalue = bundle.getString(Constant.F_ARG__REVIEWS_RATING_VALUE_CREATE);
            mComment = bundle.getString(Constant.F_ARG_COMMENT_CREATE);
            mMediaUrl = bundle.getString(Constant.F_ARG_MEDIA_URL);
            mBarcode = bundle.getString(Constant.F_ARG_BARCODE_REVIEW_CREATE);
            mIsHasImageScanBarcode = bundle.getString(Constant.F_ARG_IS_HAS_IMAGE);
        }
        if (mPrensenter.hasUserInfo()) {
            mToken_user = mPrensenter.getUserInfo().getToken();
        }
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


    }


    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        MainActivity.callbackManager = CallbackManager.Factory.create();
        mShareDialog = new ShareDialog(mActivity);
        mShareDialog.registerCallback(MainActivity.callbackManager, callback);
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_result_review, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        //set size for text


        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        mImageProduct.setClickable(true);
        captureImageCallback = new Camera.PictureCallback() {
            public void onPictureTaken(byte[] data, Camera camera) {
                BitmapFactory.Options scalingOptions = new BitmapFactory.Options();
                scalingOptions.inSampleSize = camera.getParameters().getPictureSize().width / mImageProduct.getMeasuredWidth();
                bmp = BitmapFactory.decodeByteArray(data, 0, data.length, scalingOptions);
                // SaveImage(bmp);
                //   mImageProduct.setImageBitmap(bmp);

                mPath = prepareNeckPicture( fixOrientation(bmp));
                Picasso.with(mActivity).load(new File(mPath)).into(mImageProduct);
                mBackImg.setImageDrawable(getResources().getDrawable(R.drawable.btn_back_2x_ograne));
                mMainResult.setVisibility(View.VISIBLE);
                mMainCamera.setVisibility(View.GONE);
                releaseCamera();
                //refresh layout
                mTopbar.setBackgroundResource(R.color.activity_main_bar);
                mTitleTopbar.setTextColor(getResources().getColor(R.color.main_title_color));
                mTitleTopbar.setText(R.string.me_review);
                mTitleTopbar.setTextSize(26);
                //refreshCamera();
                //set type for image capture
                mIsHasImageScanBarcode = Constant.TYPE_IMAGE_CAPTURE;
                mImageProduct.setClickable(false);
                isHasImageCapture = true;
            }
        };

        //frontCam = false;

        inItData();

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            onBack();
                            return true;
                        }
                    }
                    return false;
                }


            });
        } catch (Exception e) {

        }
    }

    /**
     *
     * @param mBitmap
     */
    public Bitmap fixOrientation(Bitmap mBitmap) {
        if (mBitmap.getWidth() > mBitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            mBitmap = Bitmap.createBitmap(mBitmap , 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
        }
        return mBitmap;
    }
    /**
     * FacebookCallback
     */
    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            Utils.hideLoading(mProgressDialog);
            mCheckBox.setChecked(true);
            mCheckBox.setClickable(false);
            mIsCheck = "1";
            Toast.makeText(mActivity, "Đã chia sẻ sản phẩm lên trang cá nhân của bạn !", Toast.LENGTH_LONG).show();
            submitData();
            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            Utils.hideLoading(mProgressDialog);
            if (mCheckBox != null) {
                mCheckBox.setChecked(false);
            }

            //  Toast.makeText(mActivity, "callbackManager cancle posted", Toast.LENGTH_SHORT).show();
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            Utils.hideLoading(mProgressDialog);
            if (mCheckBox != null) {
                mCheckBox.setChecked(false);
            }
            error.printStackTrace();
//            Toast.makeText(mActivity, "callbackManager fail posted", Toast.LENGTH_SHORT).show();
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };


    /**
     * write bitmap image
     *
     * @param data
     * @return
     */
    public String prepareNeckPicture(Bitmap data) {
        String path = getRandomPath("jpeg");

        try {
            Utils.writeBitmapIntoFile(data, path);
            mPrensenter.addTempImage(path);
        } catch (IOException e) {
            e.printStackTrace();
            Crashlytics.logException(e);
            path = null;
        }

        data.recycle();

        return path;
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    /**
     * Create File Path
     *
     * @param ext String
     * @return String
     */
    private String getRandomPath(String ext) {
        File file;
        long time = System.currentTimeMillis();
        int index = 0;

        do {
            file = new File(mActivity.getExternalFilesDir(null), time + "_" + index + "." + ext);
        } while (file.exists());

        return file.getPath();
    }

    /**
     * checkPermission
     */
    private void checkPermission() {
        final Bundle permBundle = new Bundle();
        permBundle.putCharSequence("permission", "publish_actions");
        GraphRequest request = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/permissions", permBundle, HttpMethod.GET,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {

                        try {
                            JSONArray permList = (JSONArray) graphResponse.getJSONObject().get("data");
                            if (permList.length() == 0) {
                                postPhotoDialog();
                            } else {
                                JSONObject permData = (JSONObject) permList.get(0);
                                String permVal = (String) permData.get("status");
                                if (permVal.equals("granted")) {
                                    postFacebook();
                                } else {
                                    askForFBPublishPerm();
                                }
                            }
                        } catch (JSONException e) {

                        }

                    }
                }
        );
        request.executeAsync();
    }

    //request permission
    void askForFBPublishPerm() {
        Log.d("sss", "asking for the permissions");
        MainActivity.callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithPublishPermissions(
                mActivity,
                Arrays.asList("publish_actions"));

        LoginManager.getInstance().registerCallback(MainActivity.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Set<String> getRecentlyGrantedPermissions = loginResult.getRecentlyGrantedPermissions();
                if (getRecentlyGrantedPermissions.contains("publish_actions")) {
                    postFacebook();

                } else {
                 /*   shareImg();*/
                    postPhotoDialog();
                }

            }

            @Override
            public void onCancel() {
                mCheckBox.setClickable(false);
            }

            @Override
            public void onError(FacebookException error) {
                mCheckBox.setClickable(false);
            }

        });


    }

    private void postPhotoDialog() {
        if(mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }
        //  Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        mImageProduct.setDrawingCacheEnabled(true);
        mImageProduct.buildDrawingCache();
        Bitmap bm = mImageProduct.getDrawingCache();
        SharePhoto.Builder photoBuilder = new SharePhoto.Builder();
        photoBuilder.setBitmap(bm);
        // photoBuilder.setCaption("HBD Caption");
        final SharePhoto sharePhoto = photoBuilder.build();
        SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(sharePhoto).build();
        mShareDialog.show(content);


    }


    /**
     *
     */
    private void postFacebook() {
        //How to convert View into Bitmap
        mImageProduct.setDrawingCacheEnabled(true);
        mImageProduct.buildDrawingCache();
        Bitmap bm = mImageProduct.getDrawingCache();
        //convert Bitmap* into ByteArray
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Bundle params = new Bundle();
        params.putByteArray("picture", byteArray);
        params.putString("message", mComment);

        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                "/me/photos", params, HttpMethod.POST,
                new GraphRequest.Callback() {
                    @Override
                    public void onCompleted(GraphResponse graphResponse) {
                        Utils.hideLoading(mProgressDialog);
                        mCheckBox.setChecked(true);
                        mCheckBox.setClickable(false);
                        mIsCheck = "1";
                        Toast.makeText(mActivity, "Đã chia sẻ sản phẩm lên trang cá nhân của bạn !", Toast.LENGTH_SHORT).show();
                        submitData();


                    }
                });
        request.executeAsync();
    }

    /**
     * inItData
     */
    private void inItData() {

        if(mPrensenter.hasMetaInfo() != null) {
            String ads_review = mPrensenter.hasMetaInfo().getAds_review();
            if (ads_review != null) {
                TvFacebook.setText("Đăng trên facebook để nhận gấp " + ads_review + " gb$");
            }
        }

        if (mIsHasImageScanBarcode.equals("1")) {
            mTvNameProduct.setText(mNameProduct);
            try {
                mRatingBarResult.setRating(Float.parseFloat(mRatingvalue));
            } catch (NumberFormatException ex) {
                mRatingBarResult.setRating(0);
                ex.printStackTrace();
            }
            Picasso.with(mActivity).load(Uri.parse(mMediaUrl)).
                    resizeDimen(R.dimen.dp_0, R.dimen.dp150).placeholder(R.drawable.loading_new).
                    into(mImageProduct);
            mTvCommentResult.setText(mComment);
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mCheckBox.setChecked(true);
                    } else {
                        mIsCheck = "0";
                    }
                }
            });
        } else {
            mImageProduct.setImageDrawable(getResources().getDrawable(R.drawable.camera_ic));
            mImageProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMainResult.setVisibility(View.GONE);
                    mMainCamera.setVisibility(View.VISIBLE);
                    mTopbar.setBackgroundResource(R.color.topbar_scan_bar);
                    mTitleTopbar.setTextColor(getResources().getColor(R.color.white));
                    mTitleTopbar.setText(R.string.title_review_scan);
                    mBackImg.setImageDrawable(getResources().getDrawable(R.drawable.btn_back_white_2x));
                    mTitleTopbar.setTextSize(15);
                    if (camera != null) {
                        camera.startPreview();

                    } else {
                        //  camera = android.hardware.Camera.open();
                    }


                    mBtnCapture.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            camera.takePicture(null, null, captureImageCallback);
                        }
                    });

                }
            });
            mTvNameProduct.setText(mNameProduct);
            mRatingBarResult.setRating(Float.parseFloat(mRatingvalue));
            mTvCommentResult.setText(mComment);
           /* mCheckBox.setClickable(false);*/

            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked && mImageProduct.getDrawable() != null && isHasImageCapture) {
                        mCheckBox.setChecked(true);
                    } else if (!isChecked) {
                        mIsCheck = "0";
                    } else {
                        mCheckBox.setChecked(false);
                        Toast.makeText(mActivity, "Vui lòng chụp hình ảnh sản phẩm trước khi chia sẻ", Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }
    }

 /*   // convert from bitmap to byte array
    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }*/

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        refreshCamera();

    }

    @OnClick(R.id.imgBackReviewRs)
    public void onBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);

        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);

        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);

        contentDialog.setText(getString(R.string.dialog_review_new));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (mActivity.getSupportFragmentManager().findFragmentByTag("ReviewResultScanbarCodeScreen") != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(mActivity.getSupportFragmentManager().findFragmentByTag("ReviewResultScanbarCodeScreen")).commit();
                }
                mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mActivity.getSupportFragmentManager().executePendingTransactions();
                Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                //create review from ReviewsListProductUserScreen
                if (fragment == null) {
                    return;
                }
                if (fragment instanceof ReviewsListProductUserScreen) {
                    // send event back from CreateReviewScreen to ReviewsListProductUserScreen
                    RxBus.getInstance().post("refresh_listener_back_press");
                } else if ( fragment instanceof ReviewsDetailProductScreen) {
                    // send event back from CreateReviewScreen to ReviewsDetailProductScreen
                    RxBus.getInstance().post("refresh_listener_back_press_detail");
                }else if (fragment instanceof ReviewsDetailProductByUserScreen) {
                    RxBus.getInstance().post("refresh_listener_back_press_detail_product");
                } else if (fragment instanceof ScanbarcodeReviewScreen) {

                }else {
                    ReviewsScreen.mRlNewReview.removeAllViewsInLayout();
                    ReviewsScreen.mRlNewReview.setVisibility(View.GONE);
                }
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if (task.getType() == ApiTaskType.ADD_REVIEW) {

            if (status == ApiResponseCode.SUCCESS) {
                mCheckBox.setVisibility(View.GONE);
                TvFacebook.setVisibility(View.GONE);
                mTvCommentResult.setText(getString(R.string.submit_facebook));
                mBtnSubmit.setVisibility(View.GONE);
                mBtnCallback.setVisibility(View.VISIBLE);
                mBtnCallback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoListAndResetData();
                    }
                });

            } else {
                try {
                    mCheckBox.setClickable(true);
                    snackbar = TSnackbar
                            .make(getView(), " " + mPrensenter.messageErr(), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, mPrensenter.messageErr(), mActivity);
                    Utils.vibarte(mActivity);
                    try {
                        getView().setFocusableInTouchMode(true);
                        getView().requestFocus();
                        getView().setOnKeyListener(new View.OnKeyListener() {
                            @Override
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                                    //  Log.i(tag, "keyCode: " + keyCode);
                                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                                        onBack();
                                        return true;
                                    }
                                }
                                return false;

                            }


                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (task.getType() == ApiTaskType.ADD_REVIEW_UPLOAD) {
            if (status == ApiResponseCode.SUCCESS) {
                mCheckBox.setVisibility(View.GONE);
                TvFacebook.setVisibility(View.GONE);
                mTvCommentResult.setText(getString(R.string.submit_facebook));
                mBtnSubmit.setVisibility(View.GONE);
                mBtnCallback.setVisibility(View.VISIBLE);
                mBtnCallback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        gotoListAndResetData();
                    }
                });
            } else {
                try {

                    mCheckBox.setClickable(true);
                String err = mPrensenter.messageErr();
                //setup snackBar
                snackbar = TSnackbar
                        .make(getView(), " " + err, TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, err, mActivity);
                Utils.vibarte(mActivity);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }
        return true;
    }

    /**
     * gotoListAndResetData
     */
    private void gotoListAndResetData() {
        EventBus.getDefault().post(new refreshDataReview("review_all"));
        EventBus.getDefault().post(new refreshDataReview("review_friend"));
        EventBus.getDefault().post(new refreshDataReview("review_me"));
        getFragmentManager().popBackStack();
        ReviewsScreen.mRlViewpager.setVisibility(View.VISIBLE);
        ReviewsScreen.mRlNewReview.setVisibility(View.GONE);
      //  EventBus.getDefault().post(new resetRequestSult());
        EventBus.getDefault().post(new RequestFocusInNewReview());

    }

    /**
     * submit review
     */
    @OnClick(R.id.btnSubmitReview)
    public void submit() {
        mActivity.lockEventIn(3000);
       // mProgressDialog = Utils.showLoading(mActivity);
        if (mCheckBox.isChecked()) {
            mProgressDialog = Utils.showLoading(mActivity);
            checkPermission();
        } else {

            submitData();
        }

    }

    /**
     * submitData
     */
    private void submitData() {
        if(mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }
        mProgressDialog = Utils.showLoading(mActivity);
        if (mIsHasImageScanBarcode.equals("1")) {
            if (mIsCheck != null) {
                mPrensenter.addReviewProduct(mToken_user, mBarcode, mRatingvalue, mComment, mIsCheck);
            } else {
                mPrensenter.addReviewProduct(mToken_user, mBarcode, mRatingvalue, mComment, "0");
            }
            Log.e("submitData", "mToken_user: " + mToken_user + " mBarcode:" + mBarcode + "mRatingvalue: " + mRatingvalue + "mComment: " + mComment);
        } else if (mIsHasImageScanBarcode.equals("2")) {

            if (mIsCheck != null) {
                mPrensenter.addReviewUploadProduct(mToken_user, mBarcode, mRatingvalue, mNameProduct, mComment, mIsCheck, mPath);
            } else {
                mPrensenter.addReviewUploadProduct(mToken_user, mBarcode, mRatingvalue, mNameProduct, mComment, "0", mPath);
            }
            Log.e("submitData", "mToken_user: " + mToken_user + " mBarcode:" + mBarcode + " mRatingvalue: " + mRatingvalue + " mComment: " + mComment
            +" mNameProduct: " + mNameProduct);
        } else {
            if(mProgressDialog != null) {
                Utils.hideLoading(mProgressDialog);
            }
            Toast.makeText(mActivity, "Vui lòng chụp hình ảnh sản phẩm trước khi gửi đánh giá", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_REVIEWS;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!isCameraOpen) {
            try {
                camera = Camera.open();
                isCameraOpen = true;

                changeOrientation();
            } catch (RuntimeException e) {
                System.err.println(e);
            }
            Camera.Parameters param;
            param = camera.getParameters();
            param.set("jpeg-quality", 70);
            param.setPictureFormat(PixelFormat.JPEG);
            param.setPreviewSize(640, 480);
            camera.setParameters(param);
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
            } catch (Exception e) {
                System.err.println(e);
                return;
            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        refreshCamera();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);

            camera.release();
            camera = null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
        changeOrientation();
    }

    public void changeOrientation() {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(Camera.CameraInfo.CAMERA_FACING_BACK, info);
        int rotation = mActivity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break; //Natural orientation
            case Surface.ROTATION_90:
                degrees = 90;
                break; //Landscape left
            case Surface.ROTATION_180:
                degrees = 180;
                break;//Upside down
            case Surface.ROTATION_270:
                degrees = 270;
                break;//Landscape right
        }
        int rotate = (info.orientation - degrees + 360) % 360;
        Camera.Parameters params = camera.getParameters();
        params.setRotation(rotate);
        camera.setParameters(params);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            camera.setDisplayOrientation(0);
        else
            camera.setDisplayOrientation(90);
    }

    public int isFrontCameraExisted() {
        int cameraId = -1;

        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                cameraId = i;
                break;
            }
        }
        return cameraId;
    }

    /*
    releaseCamera
     */
    private void releaseCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    public void refreshCamera() {
        if (surfaceHolder.getSurface() == null) return;

        try {
            camera.stopPreview();
        } catch (Exception e) {
        }

        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (Exception e) {
        }
    }


}
