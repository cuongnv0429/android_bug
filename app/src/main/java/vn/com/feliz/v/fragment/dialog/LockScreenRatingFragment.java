package vn.com.feliz.v.fragment.dialog;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import vn.com.feliz.R;
import vn.com.feliz.databinding.FragmentLockScreenRatingBinding;
import vn.com.feliz.m.CouponModel;

public class LockScreenRatingFragment extends DialogFragment implements View.OnClickListener{
    private static final String ARG_PARAM1 = "param1";

    private FragmentLockScreenRatingBinding mBinding;
    private OnRatingItemClicked mListener;
    private CouponModel mCoupon;

    public LockScreenRatingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param coupon Parameter 1.
     * @return A new instance of fragment LockScreenMoreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LockScreenRatingFragment newInstance(CouponModel coupon) {
        LockScreenRatingFragment fragment = new LockScreenRatingFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, coupon);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCoupon = (CouponModel) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mBinding =  DataBindingUtil.inflate(inflater, R.layout.fragment_lock_screen_rating, container, false);
        mBinding.ivCloseDialog.setOnClickListener(this);
        mBinding.btnConfirm.setOnClickListener(this);

        switch (mCoupon.getCoupon().getListProduct().size()) {
            case 1:
                break;
            case 2:
                mBinding.llProduct2.setVisibility(View.GONE);
                Picasso.with(getContext()).load(mCoupon.getCoupon().getListProduct().get(0).getImage()).into(mBinding.ivProduct0);
                Picasso.with(getContext()).load(mCoupon.getCoupon().getListProduct().get(1).getImage()).into(mBinding.ivProduct1);
                break;
            case 3:
            default:
                Picasso.with(getContext()).load(mCoupon.getCoupon().getListProduct().get(0).getImage()).into(mBinding.ivProduct0);
                Picasso.with(getContext()).load(mCoupon.getCoupon().getListProduct().get(1).getImage()).into(mBinding.ivProduct1);
                Picasso.with(getContext()).load(mCoupon.getCoupon().getListProduct().get(2).getImage()).into(mBinding.ivProduct2);
                break;
        }
        return mBinding.getRoot();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close_dialog:
                dismiss();
                break;
            case R.id.btn_confirm:
                if (mListener != null){
                    ArrayList<String> result = new ArrayList<>();
                    if (mBinding.cbProduct0.isChecked()){
                        result.add(mCoupon.getCoupon().getListProduct().get(0).getId());
                    }
                    if (mBinding.cbProduct1.isChecked()){
                        result.add(mCoupon.getCoupon().getListProduct().get(1).getId());
                    }
                    if (mBinding.cbProduct2.isChecked()){
                        result.add(mCoupon.getCoupon().getListProduct().get(2).getId());
                    }
                    if (result.size() == 0){
                        Toast.makeText(getContext(), "Vui lòng chọn sản phẩm trước khi xác nhận", Toast.LENGTH_LONG).show();
                    }else {
                        mListener.onRatingItemClick(view, mCoupon.getCoupon().getCouponId(), result);
                        dismiss();
                    }
                }
                break;
        }
    }

    public OnRatingItemClicked getListener() {
        return mListener;
    }

    public void setListener(OnRatingItemClicked listener) {
        this.mListener = listener;
    }

    public interface OnRatingItemClicked {
        void onRatingItemClick(View v, String couponId, ArrayList<String> selectedProducsId);
    }
}
