package vn.com.feliz.v.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.m.response.NotificationRespone;
import vn.com.feliz.v.widget.ReviewsScreenHolder.ReviewsAllHolder;

import static vn.com.feliz.R.id.txt_content_noti;

/**
 * Created by ITV01 on 2/24/17.
 */

public class NotificationsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<NotificationRespone> mList;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ViewHolder mHolder;

    public NotificationsListAdapter(Context context, List<NotificationRespone> mList) {
        this.mList = mList;
        this.context = context;
    }

    public void setList(List<NotificationRespone> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_list_notifications, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            NotificationRespone mNoti = mList.get(position);
            mHolder = (ViewHolder) holder;
            mHolder.txtContent.setText(mNoti.getPush_content());
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtContent;

        public ViewHolder(View view) {
            super(view);
            txtContent = (TextView) view.findViewById(txt_content_noti);
        }
    }
}
