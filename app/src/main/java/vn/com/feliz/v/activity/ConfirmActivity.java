package vn.com.feliz.v.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import org.greenrobot.eventbus.EventBus;

import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.R;


public class ConfirmActivity extends BaseActivity implements View.OnClickListener{

    WebView mTextContent;
    Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_terms_conditions_screen);
        mTextContent = (WebView) findViewById(R.id.textContent) ;
        mSubmit= (Button) findViewById(R.id.btn_confirm_term_condition);
        String textContent;
        textContent = "<html><body><p align=\"justify\">";
        textContent += getString(R.string.test);
        textContent += "</p></body></html>";
        mTextContent.loadData(textContent, "text/html", "utf-8");
        BaseApplication application = BaseApplication.getInstance();
//        mSubmit.setTypeface(application.getFontHelvetical());
        mSubmit.setVisibility(View.VISIBLE);
        mSubmit.setOnClickListener(this);

    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm_term_condition:
                Intent i = new Intent(this, RegisterActivity.class);
                startActivity(i);
                break;
        }

    }
    /**
     * Destroy all fragments and loaders.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
