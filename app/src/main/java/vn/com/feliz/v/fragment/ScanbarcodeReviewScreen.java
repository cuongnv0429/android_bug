package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ScanbarProductReviewResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.v.widget.CameraPreview;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("deprecation")
public class ScanbarcodeReviewScreen extends BaseFragment implements OnPostResponseListener {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private ImageScanner scanner;
    @BindView(R.id.cameraPreviewRv)
    FrameLayout mCameraPreview;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private String mBarCode;
    private String mToken_user;
    private ReviewsPrensenter mPresenter;
    private TSnackbar snackbar;
    @BindView(R.id.scan_barcode_rootRv)
    RelativeLayout mRootView;
    private ProgressDialog mProgressDialog;
    @BindView(R.id.imgBackReviewSc)
    ImageView mBack;
    private String mNameProduct;
    private String mNameProductNew;
    private String mRatingvalue;
    private String mReview;
    @BindView(R.id.tvNoteBarcode)
    TextView tvNoteBarcode;

    static {
        try {

            System.loadLibrary("iconv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ScanbarcodeReviewScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new ReviewsPrensenter(mActivity, this);
        // Set Title
        //EventBus.getDefault().post(new SetTitleMessage(getString(R.string.reviews_Screen)));
        EventBus.getDefault().post(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mNameProduct = bundle.getString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE);
            mRatingvalue = bundle.getString(Constant.F_ARG__REVIEWS_RATING_VALUE_CREATE);
            mReview = bundle.getString(Constant.F_ARG_COMMENT_CREATE);
        }

        if (mPresenter.hasUserInfo()) {
            mToken_user = mPresenter.getUserInfo().getToken();
        }
        // Register Events
        /*if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = null;
        try {
            rootView = inflater.inflate(R.layout.fragment_scan_barcode_review, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);

        mActivity.mBottom().setVisibility(View.GONE);
        initControls();


        return rootView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        back();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        back();
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        back();
    }

    private void back() {
        if (getView() != null) {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
//                            releaseCamera();
                            try {
                                Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                                if (fragment != null) {
//                                    //send event to ScanbarcodeScreen
//                                    mActivity.getSupportFragmentManager().beginTransaction()
//                                            .remove(fragment).commit();
                                    mActivity.getSupportFragmentManager().popBackStack();
                                    mActivity.getSupportFragmentManager().executePendingTransactions();
                                    RxBus.getInstance().post("back_scan_barcode_review");
                                    mActivity.mBottom().setVisibility(View.VISIBLE);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            return true;
                        }
                    }
                    return false;

                }

            });
        }
    }

    /**
     * setup Widget
     */
    private void initControls() {
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        try {

            autoFocusHandler = new Handler();
            mCamera = getCameraInstance();

            // Instance barcode scanner
            scanner = new ImageScanner();
            scanner.setConfig(0, Config.X_DENSITY, 3);
            scanner.setConfig(0, Config.Y_DENSITY, 3);
            mPreview = new CameraPreview(mActivity, mCamera, previewCb,
                    autoFocusCB);
            mCameraPreview.addView(mPreview);
            mCamera.setPreviewCallback(previewCb);
            mCamera.startPreview();
            previewing = true;
            mCamera.autoFocus(autoFocusCB);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * previewCb
     */
    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            try {

                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = parameters.getPreviewSize();

                Image barcode = new Image(size.width, size.height, "Y800");
                barcode.setData(data);

                int result = scanner.scanImage(barcode);

                if (result != 0) {
                    previewing = false;
                    mCamera.setPreviewCallback(null);
                    mCamera.stopPreview();

                    SymbolSet syms = scanner.getResults();
                    for (Symbol sym : syms) {
                        String scanResult = sym.getData().trim();
                        mBarCode = scanResult;
                        Log.d("sssDEBUG", mBarCode);
                        if (!mBarCode.isEmpty()) {
                            mProgressDialog = Utils.showLoading(mActivity);
                            mPresenter.requestScanbarReview(mToken_user, mBarCode);
                            barcodeScanned = true;
                        } else {
                            barcodeScanned = false;
                            mCamera.setPreviewCallback(previewCb);
                            mCamera.startPreview();
                            previewing = true;
                            mCamera.autoFocus(autoFocusCB);
                        }


                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 3000);
        }
    };

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Utils.hideLoading(mProgressDialog);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        back();
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = mActivity.getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.rlNewReview, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    @OnClick(R.id.imgBackReviewSc)
    public void backEventScanbar() {
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
        if (fragment instanceof ScanbarcodeReviewScreen) {
            //send event to ScanbarcodeScreen
            RxBus.getInstance().post("back_scan_barcode_review");
            mActivity.getSupportFragmentManager().beginTransaction()
                    .remove(fragment).commit();
            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mActivity.getSupportFragmentManager().executePendingTransactions();
        }
        mActivity.mBottom().setVisibility(View.VISIBLE);
    }


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (task.getType() == ApiTaskType.SCAN_BARCODE_REVIEW) {
            Utils.hideLoading(mProgressDialog);
            if (status == ApiResponseCode.SUCCESS) {
                // nextStep(task, Constant.F_ARG_IS_HAS_IMAGE_VALUE);
                BaseResponse mBaseResponse = (BaseResponse) task.getResponse().body();
                ScanbarProductReviewResponse mReviewResponse = (ScanbarProductReviewResponse) mBaseResponse.getData();
                ReviewResultScanbarCodeScreen fragment = new ReviewResultScanbarCodeScreen();
                Bundle bundle = new Bundle();
                try {
                    mNameProductNew = mReviewResponse.getProduct_name();
                } catch (Exception e) {

                }
                if (mNameProductNew != null && mNameProductNew != "") {
                    mNameProduct = mNameProductNew;
                }
                bundle.putString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE, mNameProduct);
                bundle.putString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE, mNameProduct);
                bundle.putString(Constant.F_ARG__REVIEWS_RATING_VALUE_CREATE, mRatingvalue);
                bundle.putString(Constant.F_ARG_COMMENT_CREATE, mReview);
                bundle.putString(Constant.F_ARG_BARCODE_REVIEW_CREATE, mBarCode);
                bundle.putString(Constant.F_ARG_IS_HAS_IMAGE, Constant.F_ARG_IS_HAS_IMAGE_VALUE);
                try {
                    if (mReviewResponse.getMedia_url() != null) {
                        bundle.putString(Constant.F_ARG_MEDIA_URL, mReviewResponse.getMedia_url());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.addToBackStack(null);
                transaction.add(R.id.rlNewReview, fragment, "ReviewResultScanbarCodeScreen");
                transaction.commit();
                mBarCode = "";
                Fragment fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("ScanbarcodeReviewScreen");
                if (fragmentremove != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(fragmentremove).commit();
                    mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    mActivity.getSupportFragmentManager().executePendingTransactions();
                }

                fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsCreateScreen");
                if (fragmentremove != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(fragmentremove).commit();
                    mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    mActivity.getSupportFragmentManager().executePendingTransactions();
                }
            } else {

                BaseResponse mBaseResponse = (BaseResponse) task.getResponse().body();
                ScanbarProductReviewResponse mReviewResponse = (ScanbarProductReviewResponse) mBaseResponse.getData();
                ReviewResultScanbarCodeScreen fragment = new ReviewResultScanbarCodeScreen();
                Bundle bundle = new Bundle();
                bundle.putString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE, mNameProduct);
                bundle.putString(Constant.F_ARG__REVIEWS_RATING_VALUE_CREATE, mRatingvalue);
                bundle.putString(Constant.F_ARG_COMMENT_CREATE, mReview);
                bundle.putString(Constant.F_ARG_BARCODE_REVIEW_CREATE, mBarCode);
                bundle.putString(Constant.F_ARG_IS_HAS_IMAGE, Constant.F_ARG_IS_HAS_IMAGE_NO_VALUE);
                try {
                    if (mReviewResponse.getMedia_url() != null) {
                        bundle.putString(Constant.F_ARG_MEDIA_URL, mReviewResponse.getMedia_url());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                fragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.addToBackStack(null);
                transaction.add(R.id.rlNewReview, fragment, "ReviewResultScanbarCodeScreen");
                transaction.commit();
                mBarCode = "";
                if (barcodeScanned) {
                    try {

                        barcodeScanned = false;
                        mCamera.setPreviewCallback(previewCb);
                        mCamera.startPreview();
                        previewing = true;
                        mCamera.autoFocus(autoFocusCB);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                Fragment fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("ScanbarcodeReviewScreen");
                if (fragmentremove != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(fragmentremove).commit();
                    mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    mActivity.getSupportFragmentManager().executePendingTransactions();
                }
                fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsCreateScreen");
                if (fragmentremove != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(fragmentremove).commit();
                    mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    mActivity.getSupportFragmentManager().executePendingTransactions();
                }

            }
        }

        return true;

    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        releaseCamera();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
