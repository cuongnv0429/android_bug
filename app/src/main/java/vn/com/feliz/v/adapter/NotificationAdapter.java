package vn.com.feliz.v.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.NotificationModel;
import vn.com.feliz.R;
import vn.com.feliz.databinding.RvItemNotificationBinding;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<NotificationModel> mData;
    private int mSelectedPos = -1;
    private OnItemRecyclerListener onItemListener;
    private String mTextYear;
    private String mTextMonth;
    private String mTextDay;
    private String mTextHour;
    private String mTextMinute;
    private String mTextSecond;
    private String mTextYears;
    private String mTextMonths;
    private String mTextDays;
    private String mTextHours;
    private String mTextMinutes;
    private String mTextSeconds;
    private String mTextAgo;
    private int partWidth;

    public static class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * callback when click on button of each item
         */
        private IViewHolderMovie mCallback;
        private RvItemNotificationBinding mBinding;

        public ViewHolderItem(RvItemNotificationBinding binding, IViewHolderMovie callback) {
            super(binding.getRoot());
            mCallback = callback;
            mBinding = binding;
            binding.btnOk.setOnClickListener(this);
            binding.btnCancel.setOnClickListener(this);
            binding.lockscreenClose.setOnClickListener(this);
            binding.rlNotificationRoot.setOnClickListener(this);
        }

        public RvItemNotificationBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {
            mCallback.OnClicked(v, ViewHolderItem.this, getAdapterPosition());
        }

        public interface IViewHolderMovie {
            void OnClicked(View v, ViewHolderItem viewHolder, int position);
        }
    }

    public NotificationAdapter(Context context, List<NotificationModel> data, OnItemRecyclerListener listener) {
        mContext = context;
        mData = data;
        if (mData == null) {
            mData = new ArrayList<>();
        }
        onItemListener = listener;
        mTextYear = context.getResources().getString(R.string.year);
        mTextMonth = context.getResources().getString(R.string.month);
        mTextDay = context.getResources().getString(R.string.day);
        mTextHour = context.getResources().getString(R.string.hour);
        mTextMinute = context.getResources().getString(R.string.minute);
        mTextSecond = context.getResources().getString(R.string.second);
        mTextYears = context.getResources().getString(R.string.years);
        mTextMonths = context.getResources().getString(R.string.months);
        mTextDays = context.getResources().getString(R.string.days);
        mTextHours = context.getResources().getString(R.string.hours);
        mTextMinutes = context.getResources().getString(R.string.minutes);
        mTextSeconds = context.getResources().getString(R.string.seconds);
        mTextAgo = context.getResources().getString(R.string.ago);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int mDeviceWidth = displayMetrics.widthPixels - 50;

        partWidth = (mDeviceWidth / 3) - 10;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        RvItemNotificationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_item_notification, parent, false);
        final ViewHolderItem viewHolder = new ViewHolderItem(binding, new ViewHolderItem.IViewHolderMovie() {
            @Override
            public void OnClicked(View v, final ViewHolderItem vHolder, int position) {
                /*if (mSelectedPos >= 0 && mSelectedPos != position)
                    notifyItemChanged(mSelectedPos);
                mSelectedPos = position;*/
                onItemListener.onItemClicked(v, position);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        final ViewHolderItem holder = (ViewHolderItem) viewHolder;
        holder.getBinding().productImages.setVisibility(View.GONE);
        holder.getBinding().lockscreenGroupButtons.setVisibility(View.VISIBLE);
        if (mData.get(position).getPushType() == 2){
            holder.getBinding().btnOk.setVisibility(View.VISIBLE);
            holder.getBinding().btnOk.setText("Cập nhật");
            holder.getBinding().btnCancel.setVisibility(View.VISIBLE);
            holder.getBinding().btnCancel.setText("Để sau");
        } else if (mData.get(position).getPushType() == 3){
            holder.getBinding().btnOk.setVisibility(View.VISIBLE);
            holder.getBinding().btnOk.setText("Cập nhật");
            holder.getBinding().btnCancel.setVisibility(View.VISIBLE);
            holder.getBinding().btnCancel.setText("Để sau");
        }else if (mData.get(position).getPushType() == 5){
            holder.getBinding().btnOk.setVisibility(View.VISIBLE);
            holder.getBinding().btnOk.setText("Bản đồ");
            holder.getBinding().btnCancel.setVisibility(View.VISIBLE);
            holder.getBinding().btnCancel.setText("Để sau");
        }else if (mData.get(position).getPushType() == 6){
            holder.getBinding().btnOk.setVisibility(View.VISIBLE);
            holder.getBinding().btnOk.setText("Đã dùng");
            holder.getBinding().btnCancel.setVisibility(View.VISIBLE);
            holder.getBinding().btnCancel.setText("Để sau");
            holder.getBinding().productImages.setVisibility(View.VISIBLE);

            if(mData.get(position).getProductList() != null) {
                holder.getBinding().lockscreenProductLoading.setVisibility(View.GONE);
            } else {
                holder.getBinding().lockscreenProductLoading.setVisibility(View.VISIBLE);
            }

            List<CouponModel.Coupon.Product> products = new ArrayList<>();
            int total = 0;
            if(mData.get(position) != null) {
                if (mData.get(position).getCoupon() != null) {
                    products = mData.get(position).getCoupon().getListProduct();
                    if(products != null) {
                        total = products.size();
                    }
                }
            }

            holder.getBinding().dialogSelectProduct1.setVisibility(View.GONE);
            holder.getBinding().dialogSelectProduct2.setVisibility(View.GONE);
            holder.getBinding().dialogSelectProduct3.setVisibility(View.GONE);
            if(total >= 1) {
                holder.getBinding().dialogSelectProduct1.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(products.get(0).getImage())
                        .resize(partWidth, partWidth*2).onlyScaleDown()
                        .into(holder.getBinding().productPicture1);
            }
            if(total >= 2) {
                holder.getBinding().dialogSelectProduct2.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(products.get(1).getImage())
                        .resize(partWidth, partWidth*2).onlyScaleDown()
                        .into(holder.getBinding().productPicture2);
            }
            if(total >= 3) {
                holder.getBinding().dialogSelectProduct3.setVisibility(View.VISIBLE);
                Picasso.with(mContext).load(products.get(2).getImage())
                        .resize(partWidth, partWidth*2).onlyScaleDown()
                        .into(holder.getBinding().productPicture3);
            }
            holder.getBinding().dialogSelectProduct1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.getBinding().vIntroductionDot1.setBackgroundResource(R.drawable.check_org_ck);
                    holder.getBinding().vIntroductionDot2.setBackgroundResource(R.drawable.check_org);
                    holder.getBinding().vIntroductionDot3.setBackgroundResource(R.drawable.check_org);
                }
            });
            holder.getBinding().dialogSelectProduct2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.getBinding().vIntroductionDot2.setBackgroundResource(R.drawable.check_org_ck);
                    holder.getBinding().vIntroductionDot1.setBackgroundResource(R.drawable.check_org);
                    holder.getBinding().vIntroductionDot3.setBackgroundResource(R.drawable.check_org);
                }
            });
            holder.getBinding().dialogSelectProduct3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.getBinding().vIntroductionDot3.setBackgroundResource(R.drawable.check_org_ck);
                    holder.getBinding().vIntroductionDot1.setBackgroundResource(R.drawable.check_org);
                    holder.getBinding().vIntroductionDot2.setBackgroundResource(R.drawable.check_org);
                }
            });

        }else if (mData.get(position).getPushType() == 8){
            holder.getBinding().btnOk.setVisibility(View.VISIBLE);
            holder.getBinding().btnOk.setText("Tặng quà");
            holder.getBinding().btnCancel.setVisibility(View.VISIBLE);
            holder.getBinding().btnCancel.setText("Để sau");
        }else {
            holder.getBinding().lockscreenGroupButtons.setVisibility(View.GONE);
            holder.getBinding().btnOk.setVisibility(View.GONE);
            holder.getBinding().btnCancel.setVisibility(View.GONE);
        }
        //holder.getBinding().tvTitle.setText(mData.get(position).getTitle());
        holder.getBinding().tvDescription.setText(mData.get(position).getDescription());
//        holder.getBinding().tvTime.setText(convertDate(mData.get(position).getPostTime()));
//        holder.getBinding().ivIcon.setVisibility(View.VISIBLE);
//        try {
//            holder.getBinding().ivIcon.setVisibility(View.VISIBLE);
//            holder.getBinding().ivIcon.setImageDrawable(getIcon(mData.get(position)));
//        }catch (Exception e) {
//            holder.getBinding().ivIcon.setVisibility(View.INVISIBLE);
//            e.printStackTrace();
//        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mData != null) {
            return mData.get(position).getPushType();
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<NotificationModel> items) {
        if (items == null){
            mData.clear();
        }else {
            mData = items;
        }
        notifyDataSetChanged();
    }

    public void setProducts(List<CouponModel.Coupon.Product> products) {
        if(products != null) {
            if(products.size() > 0) {
                mData.get(0).setProductList(products);
                notifyDataSetChanged();
            }
        }
    }

    public void setCoupon(CouponModel.Coupon coupon) {
        if(coupon != null) {
            mData.get(0).setCoupon(coupon);
            notifyDataSetChanged();
        }
    }

    public void addData(List<NotificationModel> items) {
        if (items == null)
            return;
        mData.addAll(items);
        notifyItemRangeInserted(mData.size()-items.size(), items.size());
    }

    public void addData(NotificationModel items) {
        if (items == null)
            return;
        for (int i = 0; i < mData.size(); i++) {
            if ("com.android.providers.downloads".equals(items.getPackageName())) {//download process
                if (items.getTitle().equals(mData.get(i).getTitle())) {
                    mData.set(i, items);
                    notifyItemChanged(i);
                    return;
                }
            }else {
                if (items.equals(mData.get(i))) {//others exist and replace
                    mData.set(i, items);
                    notifyItemChanged(i);
                    return;
                }
            }
        }
        mData.add(0, items);
        notifyItemInserted(0);
    }

    public boolean removeNotification(NotificationModel notification){
        if (notification != null) {
            for (int i = 0; i < mData.size(); i ++) {
                if (notification.equals(mData.get(i))){
                    mData.remove(i);
                    notifyItemRemoved(i);
                    return true;
                }
            }
        }
        return false;
    }

    public boolean removeNotification(int pos) {
        if (mData == null || pos < 0 || pos >= mData.size()) return false;
        mData.remove(pos);
        notifyItemRemoved(pos);
        return true;
    }

    public List<NotificationModel> getData() {
        return mData;
    }

    public int getSelectedPos() {
        return mSelectedPos;
    }

    private Drawable getIcon(NotificationModel notification) throws PackageManager.NameNotFoundException{
        Context remotePackageContext = mContext.createPackageContext(notification.getPackageName(), 0);
        return remotePackageContext.getResources().getDrawable(notification.getIcon());
    }

    /**
     * convert created date of movie to '3 days ago' format
     *
     * @param created
     * @return
     */
    private String convertDate(long created) {
        if(created == 0) {
            return "a " + mTextSecond + " " + mTextAgo;
        }
        long now = Calendar.getInstance().getTime().getTime()/1000;
        long oneSecond = 1;
        long oneMinute = oneSecond * 60;
        long oneHour = oneMinute * 60;
        long oneDay = oneHour * 24;
        long oneMonth = oneDay * 30;
        long oneYear = oneMonth * 12;

        long delta = (now - created) / oneYear;
        if (delta > 0) return (delta + " " + (delta > 1 ? mTextYears : mTextYear) + " " + mTextAgo);
        delta = (now - created) / oneMonth;
        if (delta > 0)
            return (delta + " " + (delta > 1 ? mTextMonths : mTextMonth) + " " + mTextAgo);
        delta = (now - created) / oneDay;
        if (delta > 0) return (delta + " " + (delta > 1 ? mTextDays : mTextDay) + " " + mTextAgo);
        delta = (now - created) / oneHour;
        if (delta > 0) return (delta + " " + (delta > 1 ? mTextHours : mTextHour) + " " + mTextAgo);
        delta = (now - created) / oneMinute;
        if (delta > 0)
            return (delta + " " + (delta > 1 ? mTextMinutes : mTextMinute) + " " + mTextAgo);
        delta = (now - created) / oneSecond;
        delta = (delta < 0) ? 0 : delta;
        return (delta + " " + (delta > 1 ? mTextSeconds : mTextSecond) + " " + mTextAgo);

    }

    /**
     * interface to fire event of item
     */
    public interface OnItemRecyclerListener {
        void onItemClicked(View v, int pos);
    }
}
