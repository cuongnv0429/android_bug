package vn.com.feliz.v.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.p.eventbus.HelpfulModle_AllTab;
import vn.com.feliz.v.interfaces.ItemTouchHelperAdapter;
import vn.com.feliz.v.widget.ReviewsScreenHolder.LoadingViewHolder;
import vn.com.feliz.v.widget.ReviewsScreenHolder.ReviewsAllByProductHolder;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class Reviews_All_By_Product_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        ItemTouchHelperAdapter {

    private List<ReviewAllItem> mReviewAllItemList;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public Reviews_All_By_Product_Adapter(Context context, List<ReviewAllItem> mReviewAllItemList) {
        this.mReviewAllItemList = mReviewAllItemList;
        this.context = context;
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //    Collections.swap(mMessageList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        //   mMessageList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        // Collections.swap(mMessageList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    @Override
    public int getItemViewType(int position) {
        return mReviewAllItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_frag_user_review_of_product, parent, false);
            return new ReviewsAllByProductHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ReviewsAllByProductHolder) {
            final ReviewAllItem mUser = mReviewAllItemList.get(position);
            final ReviewsAllByProductHolder item_control = (ReviewsAllByProductHolder) holder;
            final TextView imgShowMore = item_control.tvShowMore;
            final TextView tvContent = item_control.contentReviewbyProduct;
            final TextView isRv1 = item_control.isReview_1;
            final TextView isRv2 = item_control.isReview_2;

            if (mUser.getFullname() != null) {
                item_control.fullname.setText(mUser.getFullname());
            }
            if (mUser.getCreated_at() != null) {
                item_control.dateUserProduct.setText(mUser.getCreated_at());
            }
            if (mUser.getContent().length() >= Constant.MAX_LENGTH_CONTENT_REVIEW) {
                imgShowMore.setVisibility(View.VISIBLE);
                if (!mUser.isExpand()) {
                    item_control.contentReviewbyProduct.setText(mUser.getContent().substring(0, Constant.MAX_LENGTH_CONTENT_REVIEW) + "...");
                    item_control.tvShowMore.setText(R.string.view_more);
                } else {
                    item_control.contentReviewbyProduct.setText(mUser.getContent());
                    item_control.tvShowMore.setText(R.string.view_less);
                }
            } else {
                item_control.contentReviewbyProduct.setText(mUser.getContent());
                imgShowMore.setVisibility(View.GONE);
            }
            item_control.rbUserProduct.setRating(Float.parseFloat(mUser.getProduct_rating()));
            int coutRating;
            try {
                coutRating = Integer.parseInt(mUser.getUsefull());
            } catch (Exception e) {
                coutRating = 0;
            }
            //so nguoi danh gia huu ich
            item_control.product_review.setText(String.valueOf(coutRating) + " " + context.getString(R.string.user_ful_count_zero));
            Picasso.with(context).load(Uri.parse(mUser.getAvatar())).
                    resizeDimen(R.dimen.dp_0, R.dimen.dp96).onlyScaleDown().placeholder(R.drawable.holder_img).
                    into(item_control.avatar);


            if (mUser.getIs_review() != null) {

                if (mUser.getIs_review().equals("1")) {
                    isRv1.setClickable(true);
                    isRv2.setClickable(true);
                    isRv1.setBackgroundResource(R.color.lockscreen_ads_button_bg_active);
                    isRv2.setBackgroundResource(R.color.view_line_color);
                } else if (mUser.getIs_review().equals("2")) {
                    isRv1.setClickable(true);
                    isRv2.setClickable(true);
                    isRv2.setBackgroundResource(R.color.lockscreen_ads_button_bg_active);
                    isRv1.setBackgroundResource(R.color.view_line_color);
                } else if (mUser.getIs_review().equals("0")) {
                    isRv1.setClickable(true);
                    isRv2.setClickable(true);
                    isRv1.setBackgroundResource(R.color.view_line_color);
                    isRv2.setBackgroundResource(R.color.view_line_color);
                }

            }

            isRv1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReviewAllItem student = mReviewAllItemList.get(position);
                    int count;
                    try {
                        count = Integer.parseInt(student.getUsefull());
                    } catch (Exception e) {
                        count = 0;
                    }
                    if (!student.getIs_review().equals("1")) {
                        count++;
                        isRv1.setBackgroundResource(R.color.lockscreen_ads_button_bg_active);
                        isRv2.setBackgroundResource(R.color.view_line_color);
                        item_control.product_review.setText
                                (String.valueOf(count) + " " + context.getString(R.string.user_ful_count_zero));
                        student.setIs_review("1");
                        student.setUsefull(String.valueOf(count));
                        mReviewAllItemList.get(position).setUsefull(String.valueOf(count));
                        isRv1.setClickable(true);
                        isRv2.setClickable(true);
                        EventBus.getDefault().post(new HelpfulModle_AllTab(mUser.getReview_id(), "1"));
                        //
                    }

                }
            });
            isRv2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ReviewAllItem student1 = mReviewAllItemList.get(position);
                    if (!student1.getIs_review().equals("2")) {
                        int count;
                        try {
                            count = Integer.parseInt(student1.getUsefull());
                        } catch (Exception e) {
                            count = 0;
                        }
                        if(count > 0 && student1.getIs_review().equals("1") ) {
                            count--;
                            item_control.product_review.setText
                                    (String.valueOf(count) + " " + context.getString(R.string.user_ful_count_zero));
                            student1.setUsefull(String.valueOf(count));
                            mReviewAllItemList.get(position).setUsefull(String.valueOf(count));
                        }

                        isRv2.setBackgroundResource(R.color.lockscreen_ads_button_bg_active);
                        isRv1.setBackgroundResource(R.color.view_line_color);
                        student1.setIs_review("2");
                        isRv1.setClickable(true);
                        isRv2.setClickable(true);
                        EventBus.getDefault().post(new HelpfulModle_AllTab(mUser.getReview_id(), "2"));
                    }

                }
            });
            item_control.tvShowMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mUser.isExpand()) {
                        item_control.contentReviewbyProduct.setText(mUser.getContent());
                        item_control.tvShowMore.setText(R.string.view_less);
                        item_control.tvShowMore.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_less_xx, 0, 0, 0);
                        mUser.setExpand(true);
                    } else {
                        item_control.contentReviewbyProduct.setText(mUser.getContent().substring(0, Constant.MAX_LENGTH_CONTENT_REVIEW) + "...");
                        item_control.tvShowMore.setText(R.string.view_more);
                        item_control.tvShowMore.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_expand_xx, 0, 0, 0);
                        mUser.setExpand(false);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mReviewAllItemList == null ? 0 : mReviewAllItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void remove(int position) {
        mReviewAllItemList.remove(position);
        notifyItemRemoved(position);
    }


}