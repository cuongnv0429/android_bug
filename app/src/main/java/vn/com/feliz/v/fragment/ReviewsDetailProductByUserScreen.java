package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.CreateReview;
import vn.com.feliz.p.eventbus.HelpfulModle;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_All_Product_byUser_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;
import vn.com.feliz.v.widget.LineProgressBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsDetailProductByUserScreen extends BaseFragment implements OnPostResponseListener, OnLoadMoreListener, OnItemClickListener {
    private String mImageUrl;
    private String mNameProduct;
    private String mIdProduct;
    private String mContent;
    private double one = 0;
    private double two = 0;
    private double three = 0;
    private double four = 0;
    private double five = 0;
    private String rating;
    private String mReviews;
    @BindView(R.id.imgProductDetail__by_user)
    ImageView mImgProduct;
    @BindView(R.id.nameProductDetail__by_user)
    TextView tvNameProduct;
    @BindView(R.id.rb_rating_product__by_user)
    RatingBar mRbProduct;
    @BindView(R.id.progressBar5__by_user)
    LineProgressBar mP5;
    @BindView(R.id.progressBar4__by_user)
    LineProgressBar mP4;
    @BindView(R.id.progressBar3__by_user)
    LineProgressBar mP3;
    @BindView(R.id.progressBar2__by_user)
    LineProgressBar mP2;
    @BindView(R.id.progressBar1__by_user)
    LineProgressBar mP1;
    @BindView(R.id.reviewsProductDetail__by_user)
    TextView mTvReviewsProduct;
    //@BindView(R.id.recycleReviewsByProduct__by_user)
    private RecyclerView mRecyclerView;
    @BindView(R.id.percent5__by_user)
    TextView mTvPercent5;
    @BindView(R.id.percent4__by_user)
    TextView mTvPercent4;
    @BindView(R.id.percent3__by_user)
    TextView mTvPercent3;
    @BindView(R.id.percent2__by_user)
    TextView mTvPercent2;
    @BindView(R.id.percent1__by_user)
    TextView mTvPercent1;
    @BindView(R.id.textView4__by_user)
    TextView mT4;
    @BindView(R.id.textView3__by_user)
    TextView mT3;
    @BindView(R.id.textView2__by_user)
    TextView mT2;
    @BindView(R.id.textView1__by_user)
    TextView mT1;
    @BindView(R.id.textView0__by_user)
    TextView mT0;
    @BindView(R.id.txt_no_review_detail_by_user)
    TextView txtNoReviewDetailByUser;
    private ReviewsPrensenter mPrensenter;
    private String mTokenUser;
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private OnLoadMoreListener mOnLoadMoreListener;
    private List<ReviewAllItem> mReviewAllItemListByproduct = new ArrayList<>();
    private List<ReviewAllItem> mReviewAllItemListByproductSum = new ArrayList<>();
    private Reviews_All_Product_byUser_Adapter mReviewsProductByUserAdapter;
    private ListAllReviewsResponse mReviewAllResponse;
    private double sumRating = 0;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    // public static boolean isSuccessSubmit;
    private ProgressDialog mProgressDialog;
    public ReviewsDetailProductByUserScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrensenter = new ReviewsPrensenter(mActivity, this);

        // Set Title
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }


        Bundle bundle = getArguments();

        mImageUrl = bundle.getString(Constant.F_ARG_PRODUCT_IMG);
        mNameProduct = bundle.getString(Constant.F_ARG_PRODUCT_NAME);
        mContent = bundle.getString(Constant.F_ARG_PRODUCT_CONTENT);
        rating = bundle.getString(Constant.F_ARG_PRODUCT_RATING);

        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_1) != null) {
            try {
                one = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_1).replace(",", ""));
            } catch (NumberFormatException e) {
                one = 0; // your default value
            }
        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_2) != null) {
            try {
                two = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_2).replace(",", ""));
            } catch (NumberFormatException e) {
                two = 0; // your default value
            }
        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_3) != null) {
            try {
                three = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_3).replace(",", ""));
            } catch (NumberFormatException e) {
                three = 0; // your default value
            }

        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_4) != null) {
            try {
                four = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_4).replace(",", ""));
            } catch (NumberFormatException e) {
                four = 0; // your default value
            }

        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_5) != null) {
            try {
                five = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_5).replace(",", ""));
            } catch (NumberFormatException e) {
                five = 0; // your default value
            }
        }
        mReviews = bundle.getString(Constant.F_ARG_PRODUCT_REVIEWS);
        mIdProduct = bundle.getString(Constant.F_ARG_PRODUCT_ID);
        sumRating = one + two + three + four + five;
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail_review_all_by_user, container, false);

        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
//        mTopBarReview.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycleReviewsByProduct__by_user);
        //init progressBar

        tvNameProduct.setText(mNameProduct);
        Picasso.with(mActivity).load(Uri.parse(mImageUrl)).
                resizeDimen(R.dimen.dp_0, R.dimen.review_image_main_product).onlyScaleDown().placeholder(R.drawable.holder_img).
                into(mImgProduct);
        mRbProduct.setRating(Float.parseFloat(rating));


        mTvReviewsProduct.setText("(" + mReviews + ")");

        setOnLoadMoreListener(this);
        setData();

        inItRecycleViewsLoadmore();

        float p1 = (float) Math.round(((one * 100) / sumRating) * 10) / 10;
        float p2 = (float) Math.round(((two * 100) / sumRating) * 10) / 10;
        float p3 = (float) Math.round(((three * 100) / sumRating) * 10) / 10;
        float p4 = (float) Math.round(((four * 100) / sumRating) * 10) / 10;
        float p5 = 0;
        if (sumRating > 0) {
            p5  = 100 - (p1 + p2 + p3 + p4);
        }
        p5 = (float) Math.round(p5 * 10) / 10;

        mP1.setProgress(20);
        mTvPercent1.setText(p1 + "%");

        mP2.setProgress(40);
        mTvPercent2.setText(p2 + "%");

        mP3.setProgress(60);
        mTvPercent3.setText(p3 + "%");

        mP4.setProgress(80);
        mTvPercent4.setText(p4 + "%");

        mP5.setProgress(100);
        mTvPercent5.setText(p5 + "%");

        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("refresh_listener_back_press_detail_product")) {
                        backEventRequestFocus();
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        backEventRequestFocus();
    }

    private void backEventRequestFocus() {
        try {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                            if (fragment instanceof ReviewsDetailProductByUserScreen) {
                                RxBus.getInstance().post("refresh_listener_back_press");
                                mActivity.getSupportFragmentManager().beginTransaction()
                                        .remove(fragment).commit();
                                mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                mActivity.getSupportFragmentManager().executePendingTransactions();
                                mActivity.lockEventIn(500);
                            }
                            return true;
                        }
                    }
                    return false;
                }
            });
        } catch (Exception e) {

        }
    }

    private void setData() {
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            mPrensenter.getListAllReviewByProductPresent(mTokenUser, mIdProduct, String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), "review");
        }

    }
    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }


    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerView != null) {
            mReviewsProductByUserAdapter = new Reviews_All_Product_byUser_Adapter(mActivity, mReviewAllItemListByproductSum);
            mRecyclerView.setAdapter(mReviewsProductByUserAdapter);
        }
    }


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if (txtNoReviewDetailByUser != null) {
            txtNoReviewDetailByUser.setVisibility(View.GONE);
        }
        switch (task.getType()) {
            case GET_LIST_REVIEW_BY_PRODUCT:
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();
                    if (mReviewAllResponse != null) {
                        try {
                            mReviewAllItemListByproduct = mReviewAllResponse.getListReviews();
                            if (mReviewAllItemListByproduct != null) {
                                if (loading) {
                                    mReviewAllItemListByproductSum = mReviewAllItemListByproduct;
                                    setUpRecycleView();
                                } else if (!loading) {
                                    //Remove loading item
                                    mReviewAllItemListByproductSum.remove(mReviewAllItemListByproductSum.size() - 1);
                                    mReviewsProductByUserAdapter.notifyItemRemoved(mReviewAllItemListByproductSum.size());
                                    loading = true;
                                    mReviewAllItemListByproductSum.addAll(mReviewAllItemListByproduct);
                                    mReviewsProductByUserAdapter.notifyDataSetChanged();
                                }
                            } else {
                                if (!loading) {
                                    mReviewAllItemListByproductSum.remove(mReviewAllItemListByproductSum.size() - 1);
                                    mReviewsProductByUserAdapter.notifyItemRemoved(mReviewAllItemListByproductSum.size());
                                    txtNoReviewDetailByUser.setVisibility(View.GONE);
                                } else {
                                    txtNoReviewDetailByUser.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case SUBMIT_HELP_FULL:
                if (status == ApiResponseCode.SUCCESS) {
                    try {
                        EventBus.getDefault().post(new refreshDataReview("ReviewsListProductUserScreen"));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    //  Toast.makeText(mActivity, "err" + mPrensenter.messageErr(), Toast.LENGTH_LONG).show();
                    // isSuccessSubmit = false;
                }
                break;
        }
        return true;

    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Subscribe
    public void eventSubmitHelpful(HelpfulModle mHelpfulModle) {
        if (mPrensenter.hasUserInfo()) {
            try {
                mTokenUser = mPrensenter.getUserInfo().getToken();
                mPrensenter.submitHelpful(mTokenUser, mHelpfulModle.getReview_id(), mHelpfulModle.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onLoadMore() {
        mReviewAllItemListByproductSum.add(null);
        mReviewsProductByUserAdapter.notifyItemInserted(mReviewAllItemListByproductSum.size() - 1);
        requestGetListAllReviewNextPage();

    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {
        mPrensenter.getListAllReviewByProductPresent(mTokenUser, mIdProduct, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), "review");
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    @OnClick(R.id.btnCreateReviewByUser)
    public void clickCreateView() {
        EventBus.getDefault().post(new CreateReview());
    }

    @OnClick(R.id.imgBackReviewListByUser)
    public void backFromListProductUser() {
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
        if (fragment instanceof ReviewsDetailProductByUserScreen) {
            RxBus.getInstance().post("refresh_listener_back_press");
            RxBus.getInstance().post("back_review_all");
            mActivity.getSupportFragmentManager().beginTransaction()
                    .remove(fragment).commit();
            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mActivity.getSupportFragmentManager().executePendingTransactions();
            mActivity.lockEventIn(500);
        }
    }
}
