package vn.com.feliz.v.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.p.eventbus.idAnswer;
import vn.com.feliz.v.fragment.SettingScreen;
import vn.com.feliz.R;

/**
 * Created by anandbose on 09/06/15.
 */
public class ExpandableListQuestionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    public static final int HEADER = 0;
    public static final int QUESTION = 1;
    private List<Item> data;
    private String listChooseId=",-1,";
    Handler handler;
    private boolean onBind;
    public ExpandableListQuestionAdapter(List<Item> data) {
        this.data = data;
        handler = new Handler();
        listChooseId = SettingScreen.listIdQuestion;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        Context context = parent.getContext();
        float dp = context.getResources().getDisplayMetrics().density;
        int subItemPaddingLeft = (int) (18 * dp);
        int subItemPaddingTopAndBottom = (int) (5 * dp);
        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.item_header_question, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case QUESTION:
                LayoutInflater inflater_item = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater_item.inflate(R.layout.item_today_question, parent, false);
                ListItemInteresstViewHolder item_interest = new ListItemInteresstViewHolder(view);
                return item_interest;
        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Item item = data.get(position);
        switch (item.type) {
            case HEADER:
                final ListHeaderViewHolder itemController = (ListHeaderViewHolder) holder;
                itemController.refferalItem = item;
                itemController.header_title.setText(item.text);
                if (item.invisibleChildren == null) {
                    // itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                } else {
                    // itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                }
                itemController.btn_expand_toggle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (item.invisibleChildren == null) {
                            item.invisibleChildren = new ArrayList<Item>();
                            int count = 0;
                            int pos = data.indexOf(itemController.refferalItem);
                            while (data.size() > pos + 1 && data.get(pos + 1).type == QUESTION) {
                                item.invisibleChildren.add(data.remove(pos + 1));
                                count++;
                            }
                            notifyItemRangeRemoved(pos + 1, count);
                            //  itemController.btn_expand_toggle.setImageResource(R.drawable.circle_plus);
                        } else {
                            int pos = data.indexOf(itemController.refferalItem);
                            int index = pos + 1;
                            for (Item i : item.invisibleChildren) {
                                data.add(index, i);
                                index++;
                            }
                            notifyItemRangeInserted(pos + 1, index - pos - 1);
                            //  itemController.btn_expand_toggle.setImageResource(R.drawable.circle_minus);
                            item.invisibleChildren = null;
                        }
                    }
                });
                break;
            case QUESTION:

                final ListItemInteresstViewHolder item_control = (ListItemInteresstViewHolder) holder;
                TextView itemTextView = (TextView) item_control.mAnswer;
                itemTextView.setText(data.get(position).text);
                final CheckBox itemCheckBox = (CheckBox) item_control.checkBoxQuestion;

                onBind = true;
                itemCheckBox.setChecked(data.get(position).isCheck);
                onBind = false;
                itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if(!onBind) {
                            if (isChecked) {
                                listChooseId = listChooseId + "," + data.get(position).idAnswer + ",";
                                //   EventBus.getDefault().post(new idQuestion(listChooseId,data.get(position).));
                            } else if (!isChecked) {
                                listChooseId = listChooseId.replace("," + data.get(position).idAnswer + ",", "");

                            }
                        }
                        Runnable r = new Runnable() {
                            @Override
                            public void run() {
                                EventBus.getDefault().post(new idAnswer(listChooseId));
                            }
                        };
                        try {
                            handler.removeCallbacks(r);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        handler.postDelayed(r, Constant.SPLASH_TIME_OUT_SUBMIT);
                    }


                });

            /*    itemCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(selected != null)
                        {
                            selected.setChecked(false);
                        }
                        itemCheckBox.setChecked(true);
                        selected = itemCheckBox;
                        EventBus.getDefault().post(new idAnswer(String.valueOf(position)));

                    }
                });*/
                //for default check in first item
                break;

        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).type;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    class ListHeaderViewHolder extends RecyclerView.ViewHolder {
        public TextView header_title;
        public ImageView btn_expand_toggle;
        public Item refferalItem;

        public ListHeaderViewHolder(View itemView) {
            super(itemView);
            header_title = (TextView) itemView.findViewById(R.id.item_header_name);
            btn_expand_toggle = (ImageView) itemView.findViewById(R.id.item_arrow);
        }
    }

    class ListItemInteresstViewHolder extends RecyclerView.ViewHolder {
        public TextView mAnswer;
        public CheckBox checkBoxQuestion;

        public ListItemInteresstViewHolder(View itemView) {
            super(itemView);
            mAnswer = (TextView) itemView.findViewById(R.id.item_answer);
            checkBoxQuestion = (CheckBox) itemView.findViewById(R.id.item_question_check);
        }
    }

    public static class Item {
        public int type;
        public String text;
        public boolean isCheck;
        public String idAnswer;
        public String idQuestion;
        public List<Item> invisibleChildren;

        public Item() {
        }

        public Item(int type, String text, String idAnswer, String idQuestion) {
            this.type = type;
            this.text = text;
            this.idAnswer = idAnswer;
            this.idQuestion = idQuestion;
        }
        public Item(int type, String text) {
            this.type = type;
            this.text = text;
        }
        public Item(int type, String text, boolean isCheck) {
            this.type = type;
            this.text = text;
            this.isCheck = isCheck;

        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public boolean isCheck() {
            return isCheck;
        }

        public void setCheck(boolean check) {
            isCheck = check;
        }

        public List<Item> getInvisibleChildren() {
            return invisibleChildren;
        }

        public void setInvisibleChildren(List<Item> invisibleChildren) {
            this.invisibleChildren = invisibleChildren;
        }
    }
}
