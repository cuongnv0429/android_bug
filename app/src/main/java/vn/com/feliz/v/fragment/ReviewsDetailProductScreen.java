package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.CreateReview;
import vn.com.feliz.p.eventbus.HelpfulModle_AllTab;
import vn.com.feliz.p.eventbus.RequestFocus;
import vn.com.feliz.p.eventbus.RequestFocusInNewReview;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_All_By_Product_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;
import vn.com.feliz.v.widget.LineProgressBar;

import static vn.com.feliz.v.fragment.ReviewsScreen.imgBackReview;
import static vn.com.feliz.v.fragment.ReviewsScreen.mRlNewReview;
import static vn.com.feliz.v.fragment.ReviewsScreen.mTopBarReview;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsDetailProductScreen extends BaseFragment implements OnPostResponseListener, OnLoadMoreListener, OnItemClickListener {
    private String mImageUrl;
    private String mNameProduct;
    private String mIdProduct;
    private String mContent;
    private double one = 0;
    private double two = 0;
    private double three = 0;
    private double four = 0;
    private double five = 0;
    private String rating;
    private String mReviews;
    @BindView(R.id.imgProductDetail)
    ImageView mImgProduct;
    @BindView(R.id.nameProductDetail)
    TextView tvNameProduct;
    @BindView(R.id.rb_rating_product)
    RatingBar mRbProduct;
    @BindView(R.id.progressBar5)
    LineProgressBar mP5;
    @BindView(R.id.progressBar4)
    LineProgressBar mP4;
    @BindView(R.id.progressBar3)
    LineProgressBar mP3;
    @BindView(R.id.progressBar2)
    LineProgressBar mP2;
    @BindView(R.id.progressBar1)
    LineProgressBar mP1;
    @BindView(R.id.reviewsProductDetail)
    TextView mTvReviewsProduct;
    @BindView(R.id.recycleReviewsByProduct)
    RecyclerView mRecyclerView;
    @BindView(R.id.percent5)
    TextView mTvPercent5;
    @BindView(R.id.percent4)
    TextView mTvPercent4;
    @BindView(R.id.percent3)
    TextView mTvPercent3;
    @BindView(R.id.percent2)
    TextView mTvPercent2;
    @BindView(R.id.percent1)
    TextView mTvPercent1;
    @BindView(R.id.textView4)
    TextView mT4;
    @BindView(R.id.textView3)
    TextView mT3;
    @BindView(R.id.textView2)
    TextView mT2;
    @BindView(R.id.textView1)
    TextView mT1;
    @BindView(R.id.textView0)
    TextView mT0;
    @BindView(R.id.imgBackDetailReview)
    ImageButton imgBackDetailReview;
    @BindView(R.id.btnCreateReviewDetail)
    TextView btnCreateReviewDetail;
    @BindView(R.id.txt_no_review_detail)
    TextView txtNoReviewDetail;
    private ReviewsPrensenter mPrensenter;
    private String mTokenUser;
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private OnLoadMoreListener mOnLoadMoreListener;
    private List<ReviewAllItem> mReviewAllItemListByproduct = new ArrayList<>();
    private List<ReviewAllItem> mReviewAllItemListByproductSum = new ArrayList<>();
    private Reviews_All_By_Product_Adapter mReviewsAllAdapter;
    private ListAllReviewsResponse mReviewAllResponse;
    private double sumRating = 0;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private boolean referFromCoupon = false;
    private String mProductId = "";
    private ProgressDialog mProgressDialog;
    private View.OnKeyListener onKeyListener;
    private TSnackbar snackbar;
    // public static boolean isSuccessSubmit;

    public ReviewsDetailProductScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrensenter = new ReviewsPrensenter(mActivity, this);

        // Set Title
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Bundle bundle = getArguments();

        mImageUrl = bundle.getString(Constant.F_ARG_PRODUCT_IMG);
        mNameProduct = bundle.getString(Constant.F_ARG_PRODUCT_NAME);
        mContent = bundle.getString(Constant.F_ARG_PRODUCT_CONTENT);
        rating = bundle.getString(Constant.F_ARG_PRODUCT_RATING);

        if (bundle.getString(Constant.F_ARG_REFER_FROM_COUPON) != null) {
            referFromCoupon = bundle.getString(Constant.F_ARG_REFER_FROM_COUPON).equals("1");
        }

        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_1) != null) {
            try {
                one = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_1).replace(",", ""));
            } catch (NumberFormatException e) {
                one = 0; // your default value
            }
        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_2) != null) {
            try {
                two = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_2).replace(",", ""));
            } catch (NumberFormatException e) {
                two = 0; // your default value
            }
        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_3) != null) {
            try {
                three = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_3).replace(",", ""));
            } catch (NumberFormatException e) {
                three = 0; // your default value
            }

        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_4) != null) {
            try {
                four = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_4).replace(",", ""));
            } catch (NumberFormatException e) {
                four = 0; // your default value
            }

        }
        if (bundle.getString(Constant.F_ARG_PRODUCT_RATING_5) != null) {
            try {
                five = Double.parseDouble(bundle.getString(Constant.F_ARG_PRODUCT_RATING_5).replace(",", ""));
            } catch (NumberFormatException e) {
                five = 0; // your default value
            }
        }
        mReviews = bundle.getString(Constant.F_ARG_PRODUCT_REVIEWS);
        mIdProduct = bundle.getString(Constant.F_ARG_PRODUCT_ID);
        sumRating = one + two + three + four + five;
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_detail_review_all, container, false);

        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);

        tvNameProduct.setText(mNameProduct);
        Picasso.with(mActivity).load(Uri.parse(mImageUrl)).
                resizeDimen(R.dimen.dp_0, R.dimen.review_image_main_product).onlyScaleDown().placeholder(R.drawable.holder_img).
                into(mImgProduct);
        mRbProduct.setRating(Float.parseFloat(rating));
        mTvReviewsProduct.setText("(" + mReviews + ")");
        setOnLoadMoreListener(this);
        setData();
        inItRecycleViewsLoadmore();

        float p1 = (float) Math.round(((one * 100) / sumRating) * 10) / 10;
        float p2 = (float) Math.round(((two * 100) / sumRating) * 10) / 10;
        float p3 = (float) Math.round(((three * 100) / sumRating) * 10) / 10;
        float p4 = (float) Math.round(((four * 100) / sumRating) * 10) / 10;
        float p5 = 0;
        if (sumRating > 0) {
           p5  = 100 - (p1 + p2 + p3 + p4);
        }
        p5 = (float) Math.round(p5 * 10) / 10;

        mP1.setProgress(20);
        mTvPercent1.setText(p1 + "%");

        mP2.setProgress(40);
        mTvPercent2.setText(p2 + "%");

        mP3.setProgress(60);
        mTvPercent3.setText(p3 + "%");

        mP4.setProgress(80);
        mTvPercent4.setText(p4 + "%");

        mP5.setProgress(100);
        mTvPercent5.setText(p5 + "%");

        imgBackReview.setEnabled(false);
        imgBackReview.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgBackReview.setEnabled(true);
            }
        }, 1000);
        onKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        RxBus.getInstance().post("back_review_all");
                        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                        if (fragment instanceof ReviewsDetailProductScreen) {
                            mActivity.getSupportFragmentManager().beginTransaction()
                                    .remove(fragment).commit();
                            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            mActivity.getSupportFragmentManager().executePendingTransactions();
                            mRlNewReview.removeAllViewsInLayout();
                            mRlNewReview.setVisibility(View.GONE);
                        }
                        return true;
                    }
                }
                return false;
            }
        };
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("refresh_listener_back_press_detail")) {
                        mTopBarReview.setVisibility(View.VISIBLE);
                        if (getView() != null) {
                            backEventRequestFocus();
                        }
                    }
                }
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (referFromCoupon) {
            referFromCoupon = false;
            mActivity.setCurrentTab(2);
        }
        backEventRequestFocus();
    }

    private void setData() {
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            mPrensenter.getListAllReviewByProductPresent(mTokenUser, mIdProduct, String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), "review");
        }
    }

    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }


    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerView != null) {
            mReviewsAllAdapter = new Reviews_All_By_Product_Adapter(mActivity, mReviewAllItemListByproductSum);
            mRecyclerView.setAdapter(mReviewsAllAdapter);
        }
    }


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        switch (task.getType()) {
            case GET_LIST_REVIEW_BY_PRODUCT:
                if (txtNoReviewDetail != null) {
                    txtNoReviewDetail.setVisibility(View.GONE);
                }
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse;
                    try {
                        baseResponse = (BaseResponse) task.getResponse().body();
                        if (baseResponse.getData() != null) {
                            mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();
                            try {
                                mReviewAllItemListByproduct = mReviewAllResponse.getListReviews();
                                if (mReviewAllItemListByproduct != null) {
                                    if (loading) {
                                        mReviewAllItemListByproductSum = mReviewAllItemListByproduct;
                                        setUpRecycleView();
                                    } else if (!loading) {
                                        mReviewAllItemListByproductSum.remove(mReviewAllItemListByproductSum.size() - 1);
                                        mReviewsAllAdapter.notifyItemRemoved(mReviewAllItemListByproductSum.size());
                                        loading = true;
                                        mReviewAllItemListByproductSum.addAll(mReviewAllItemListByproduct);
                                        mReviewsAllAdapter.notifyDataSetChanged();
                                    }
                                } else {
                                    if (!loading) {
                                        mReviewAllItemListByproductSum.remove(mReviewAllItemListByproductSum.size() - 1);
                                        mReviewsAllAdapter.notifyItemRemoved(mReviewAllItemListByproductSum.size());
                                        txtNoReviewDetail.setVisibility(View.GONE);
                                    } else {
                                        txtNoReviewDetail.setVisibility(View.VISIBLE);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            snackbar = TSnackbar
                                    .make(getView(), getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                            Utils.vibarte(mActivity);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        snackbar = TSnackbar
                                .make(getView(), getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                        Utils.vibarte(mActivity);
                    }
                }
                break;

            case SUBMIT_HELP_FULL:
                if (status == ApiResponseCode.SUCCESS) {
                    EventBus.getDefault().post(new refreshDataReview("review_all"));
                    EventBus.getDefault().post(new refreshDataReview("review_friend"));
                    EventBus.getDefault().post(new refreshDataReview("review_me"));
                } else {

                }
                break;
        }
        return true;

    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe
    public void eventSubmitHelpfulAll(HelpfulModle_AllTab mHelpfulModle) {
        if (mPrensenter.hasUserInfo()) {
            try {
                mTokenUser = mPrensenter.getUserInfo().getToken();
                mPrensenter.submitHelpful(mTokenUser, mHelpfulModle.getReview_id(), mHelpfulModle.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onLoadMore() {
        mReviewAllItemListByproductSum.add(null);
        mReviewsAllAdapter.notifyItemInserted(mReviewAllItemListByproductSum.size() - 1);
        requestGetListAllReviewNextPage();

    }

    @Subscribe
    public void requestFocusKeyback(RequestFocus requestFocus) {
        backEventRequestFocus();
    }

    @Subscribe
    public void requestFocusKeybackFromNewCreate(RequestFocusInNewReview requestFocus) {
        backEventRequestFocus();
    }

    private void backEventRequestFocus() {
        try {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(onKeyListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {
        mPrensenter.getListAllReviewByProductPresent(mTokenUser, mIdProduct, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), "review");
    }

    @Override
    public void onItemClick(View view, int position) {
    }

    /**
     * btnCreateReview
     */
    @OnClick(R.id.imgBackDetailReview)
    public void back() {
        //send event to ReviewAll
        RxBus.getInstance().post("back_review_all");
        Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
        if (fragment instanceof ReviewsDetailProductScreen) {
            mActivity.getSupportFragmentManager().beginTransaction()
                    .remove(fragment).commit();
            mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            mActivity.getSupportFragmentManager().executePendingTransactions();
            mRlNewReview.removeAllViewsInLayout();
            mRlNewReview.setVisibility(View.GONE);
            mActivity.lockEventIn(500);
        }
    }

    /**
     * btnCreateReview
     */
    @OnClick(R.id.btnCreateReviewDetail)
    public void createReview() {
        EventBus.getDefault().post(new CreateReview());
    }
}
