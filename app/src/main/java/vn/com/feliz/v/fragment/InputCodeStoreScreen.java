package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.adapter.CouponsAdapter;
import vn.com.feliz.v.widget.KeyBoardFragmentCoupons;

/**
 * A simple {@link Fragment} subclass.
 */
public class InputCodeStoreScreen extends BaseFragment implements OnPostResponseListener, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener,View.OnClickListener, View.OnTouchListener {
    private KeyBoardFragmentCoupons mKeyboard_fragment;
    @BindView(R.id.code1)
    TextInputEditText mCode1;
    @BindView(R.id.code2)
    TextInputEditText mCode2;
    @BindView(R.id.code3)
    TextInputEditText mCode3;
    @BindView(R.id.code4)
    TextInputEditText mCode4;
    private StringBuilder sb = new StringBuilder();

    @BindView(R.id.fragment_input_ln_confirm)
    LinearLayout mViewConfirm;
    @BindView(R.id.root_view)
    LinearLayout mRootview;
    private String mCouponId;
    private String mBarcodeId;
    private int mCouponsTotal;
    private String mCouponsSubId;
    private String mShopId;
    private String mToken_user;
    private CouponsPresenter mPresenter;
    private TSnackbar snackbar;
    private ProgressDialog mProgressDialog;
    @BindView(R.id.tv_topbar_left)
    TextView mTitleBarLeft;
    @BindView(R.id.main_topbar)
    RelativeLayout mTopbar;
    @BindView(R.id.main_topbar_back)
    ImageView mBack;
    @BindView(R.id.main_top_bar_coupons)
    TextView mTitleCouponsDetail;
    private CouponsAdapter adapter;
    private boolean isFirst;
    private boolean mFromRegister;

    public InputCodeStoreScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CouponsPresenter(mActivity, this);
        isFirst = true;
        // Set Title
        // EventBus.getDefault().post(new SetTitleMessage(getString(R.string.reviews_Screen)));
        EventBus.getDefault().post(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mCouponId = bundle.getString(Constant.F_ARG_COUPON_ID);
            mBarcodeId = bundle.getString(Constant.F_ARG_COUPON_BARCODE);
            mCouponsTotal = bundle.getInt(Constant.F_ARG_COUPON_TOTAL);
            mCouponsSubId = bundle.getString(Constant.F_ARG_SUB_COUPON_ID);
            mFromRegister = bundle.getBoolean("fromRegister");

        }
        if (mPresenter.hasUserInfo()) {
            mToken_user = mPresenter.getUserInfo().getToken();
        }
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);


    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        Fragment fragment1 = mActivity.getSupportFragmentManager().findFragmentByTag("fragment_scan");
//        if (fragment1 != null) {
//            Log.e("Remove fragment_scan", "ok");
//            mActivity.getSupportFragmentManager().beginTransaction().remove(fragment1).commit();
//        } else {
//            Log.e("Remove fragment_scan", "null");
//        }
//    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_input_code_store, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        mActivity.mBottom().setVisibility(View.GONE);
        // closeKeyboard(getActivity(), mCode1.getWindowToken());
        mTopbar.setBackgroundResource(R.color.topbar_scan_bar);
        mBack.setImageDrawable(getResources().getDrawable(R.drawable.btn_back_white_2x));
        mTitleBarLeft.setText(R.string.topbar_input_code);
        mTitleCouponsDetail.setVisibility(View.VISIBLE);
        if (mCouponsTotal < 10 && mCouponsTotal >= 0) {
            mTitleCouponsDetail.setText("0" + mCouponsTotal);
        }/*else if(mCouponsTotal==0){
                mActivity.getTitleCouponsDetail().setText("01");
            }*/ else {
            mTitleCouponsDetail.setText("" + mCouponsTotal);
        }
        setEvent();
        return rootView;
    }

    /*
    set Onclick
     */
    private void setEvent() {
        mCode1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mCode1.getText().clear();
                mCode1.setFocusableInTouchMode(true);
                mCode1.requestFocus();
                return false;
            }
        });
        mCode2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mCode2.getText().clear();
                mCode2.setFocusableInTouchMode(true);
                mCode2.requestFocus();
                return false;
            }
        });
        mCode3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mCode3.getText().clear();
                mCode3.setFocusableInTouchMode(true);
                mCode3.requestFocus();
                return false;
            }
        });
        mCode4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mCode4.getText().clear();
                mCode4.setFocusableInTouchMode(true);
                mCode4.requestFocus();
                return false;
            }
        });
       /* mCode1.setOnTouchListener(this);
        mCode2.setOnTouchListener(this);
        mCode3.setOnTouchListener(this);
        mCode4.setOnTouchListener(this);
        mCode1.setOnClickListener(this);
        mCode2.setOnClickListener(this);
        mCode3.setOnClickListener(this);
        mCode4.setOnClickListener(this);*/
        mCode1.setOnKeyListener(this);
        mCode2.setOnKeyListener(this);
        mCode3.setOnKeyListener(this);
        mCode4.setOnKeyListener(this);
        mCode4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCode4.setCursorVisible(true);
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        MainActivity.isScreenEnterCodeOfShop = false;
        Utils.hideSoftKeyboard(mActivity);
    }

    /**
     * first instance layout
     */
    private void initLayout() {
        mViewConfirm.setVisibility(View.INVISIBLE);
        mRootview.getViewTreeObserver().addOnGlobalLayoutListener(this);
        mCode1.setFocusableInTouchMode(true);
        mCode1.requestFocus();
        InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imgr.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @Override
    public void onResume() {
        //alway hide keyboard
        try {
            mActivity.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            initLayout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onResume();
        MainActivity.isScreenEnterCodeOfShop = true;

    }


    /**
     * submitExchange
     */
    @OnClick(R.id.btn_confirm_term)
    public void submitExchange() {
        mShopId = mCode1.getText().toString() + mCode2.getText().toString() + mCode3.getText().toString()
                + mCode4.getText().toString();
        if(mActivity.isConnected(getContext())) {
            mProgressDialog = Utils.showLoading(mActivity);
            mPresenter.requestSubmitCoupons(mToken_user, mCouponId, mBarcodeId, mShopId, String.valueOf(mCouponsTotal), mCouponsSubId);
        } else {
            mActivity.showNetworkAlert(getView());
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mActivity.showNetworkAlert(getView());
        } else {
            if (task.getType() == ApiTaskType.SUBMIT_COUPONS) {
                if (status == ApiResponseCode.SUCCESS) {
                    MainActivity.reload_detail = true;
                    MainActivity.reload_list = true;

                    CouponsDetailScreen fr = CouponsDetailScreen.newInstance(mCouponId, mCouponsSubId, true);
                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, fr)
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                    try {
                        Fragment fragmentremove = mActivity.getSupportFragmentManager().findFragmentByTag("fragment_input_code");
                        if (fragmentremove != null) {
                            mActivity.getSupportFragmentManager().beginTransaction().remove(fragmentremove).commit();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                } else {
                    mViewConfirm.setVisibility(View.INVISIBLE);
                    //show message success
                    String err = mPresenter.messageErr();
                    //setup snackBar
                    snackbar = TSnackbar
                            .make(mRootview, " " + err, TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, err, mActivity);
                    Utils.vibarte(mActivity);
                    mCode1.setBackgroundResource(R.drawable.edt_background_error_input_code);
                    mCode2.setBackgroundResource(R.drawable.edt_background_error_input_code);
                    mCode3.setBackgroundResource(R.drawable.edt_background_error_input_code);
                    mCode4.setBackgroundResource(R.drawable.edt_background_error_input_code);
                    //clear input
                    mCode1.getText().clear();
                    mCode2.getText().clear();
                    mCode3.getText().clear();
                    mCode4.getText().clear();


                }
            }
        }
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    /**
     * @param et EditText
     */
    /*private void setUpKeyboard(EditText et) {
        try {
            if (Utils.hasNavBar(mActivity)) {
                // Do whatever you need to do, this device has a navigation bar
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mKeyboard.getLayoutParams();
                // params.setMargins(0, 0, 0, 36);
                mKeyboard.setLayoutParams(params);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mKeyboard_fragment == null) {
            //hide view confirm when keyboard display
            mViewConfirm.setVisibility(View.GONE);
            mKeyboard_fragment = KeyBoardFragmentCoupons.newInstance(et.getText().toString());
            et.setFocusableInTouchMode(true);
            et.requestFocus();
            mKeyboard.setVisibility(View.VISIBLE);
            mActivity.getSupportFragmentManager().beginTransaction().add(R.id.key_board_coupons, mKeyboard_fragment).commit();
        } else {
            //hide view confirm when keyboard display
            mViewConfirm.setVisibility(View.GONE);
            mKeyboard_fragment = KeyBoardFragmentCoupons.newInstance(et.getText().toString());
            et.setFocusableInTouchMode(true);
            et.requestFocus();
            mKeyboard.setVisibility(View.VISIBLE);
            mActivity.getSupportFragmentManager().beginTransaction().add(R.id.key_board_coupons, mKeyboard_fragment).commit();
        }

    }*/

    /**
     * Verify Code
     *
     * @param s      CharSequence
     * @param start  int
     * @param before int
     * @param count  int
     */
    @OnTextChanged(value = {
            R.id.code1, R.id.code2,
            R.id.code3, R.id.code4},
            callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void verifyCode(CharSequence s, int start, int before, int count) {
        String text = s.toString();
        int position = s.length();

        boolean isChanged = false;
        if (text.length() > 1) {
            for (int i = (start + count) - 1; i >= 0; --i) {
                if (Character.isDigit(text.charAt(i))) {
                    text = text.substring(i, i + 1);
                    isChanged = true;
                }
            }
        }

        if (mCode1.isFocused()) {
            if (isChanged) {
                mCode1.setText(text);
            }
            if (s.length() == 1) {
                Editable etext = mCode1.getText();
                Selection.setSelection(etext, position);
                mCode2.setFocusableInTouchMode(true);
                mCode2.requestFocus();
                mCode2.getText().clear();

            }
        } else if (mCode2.isFocused()) {
            if (isChanged) {

                mCode2.setText(text);

            }
            if (s.length() == 1) {
                Editable etext = mCode2.getText();
                Selection.setSelection(etext, position);
                mCode3.setFocusableInTouchMode(true);
                mCode3.requestFocus();
                mCode3.getText().clear();
            }
        } else if (mCode3.isFocused()) {
            if (isChanged) {

                mCode3.setText(text);

            }
            if (s.length() == 1) {
                Editable etext = mCode3.getText();
                Selection.setSelection(etext, position);
                mCode4.setFocusableInTouchMode(true);
                mCode4.requestFocus();
                mCode4.getText().clear();
                mCode4.setCursorVisible(true);
            }
        } else {
            if (isChanged) {

                mCode4.setText(text);
            }
            if (s.length() == 1) {
                Editable etext = mCode4.getText();
                Selection.setSelection(etext, position);
                Utils.hideSoftKeyboard(mActivity);
                mCode4.setCursorVisible(false);
            }
        }
    }

    @OnClick(R.id.main_topbar_back)
    public void backEvent() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(getString(R.string.dialog_change));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                mActivity.getSupportFragmentManager().popBackStack();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });
      /*  Utils.showRetryDialog(mActivity,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //  CouponsDetailScreen fr = CouponsDetailScreen.newInstance(mIdCoupons, mIdCouponsSubid, true);

                        mActivity.getSupportFragmentManager().popBackStack();
                        mActivity.getSupportFragmentManager().popBackStack();

                    }
                },
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Todo cancel all pending task
                        dialogInterface.dismiss();
                    }
                });*/

    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DEL) {
            //this is for backspace
            if (mCode1.isFocused()) {
            } else if (mCode2.isFocused()) {
                mCode1.setFocusableInTouchMode(true);
                mCode1.setFocusable(true);
                mCode1.requestFocus();
            } else if (mCode3.isFocused()) {
                mCode2.setFocusableInTouchMode(true);
                mCode2.setFocusable(true);
                mCode2.requestFocus();
            } else if (mCode4.isFocused()) {
                mCode3.setFocusableInTouchMode(true);
                mCode3.setFocusable(true);
                mCode3.requestFocus();
            }
        } else if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (mCode1.getText().length() == 1 && mCode2.getText().length() == 1 && mCode3.getText().length() == 1 && mCode4.getText().length() == 1) {
                Utils.hideSoftKeyboard(mActivity);
            }
        }
        return false;
    }

    @Override
    public void onGlobalLayout() {
        //check keyboard show/hide
        Rect r = new Rect();
        if (mRootview != null) {
            mRootview.getWindowVisibleDisplayFrame(r);
            int screenHeight = mRootview.getRootView().getHeight();
            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                // has one show keyboard
                isFirst = false;
                // keyboard is opened
                mViewConfirm.setVisibility(View.INVISIBLE);

            } else {
                if (!isFirst && mCode1.getText().length() >= 1 && mCode2.getText().length() >= 1
                        && mCode3.getText().length() >= 1 && mCode4.getText().length() >= 1) {
                    mViewConfirm.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void updateCoupon(CouponModel coupons) {
        for (int i = 0; i < MainActivity.couponList.size(); i++) {
            if (MainActivity.couponList.get(i).getCouponId().equals(coupons.getCoupon().getCouponId())) {
                MainActivity.couponList.set(i, coupons.getCoupon());
            }
        }
    }


    @Override
    public void onClick(View v) {
       /* switch (v.getId()) {
            case R.id.code1:
                Utils.showSoftKeyboard(mActivity);
                mCode1.setFocusable(true);
                mCode1.requestFocus();
                mCode1.setText("");
                break;
            case R.id.code2:
                Utils.showSoftKeyboard(mActivity);
                mCode2.setFocusable(true);
                mCode2.requestFocus();
                mCode2.setText("");
                break;
            case R.id.code3:
                Utils.showSoftKeyboard(mActivity);
                mCode3.setFocusable(true);
                mCode3.requestFocus();
                mCode3.setText("");
                break;
            case R.id.code4:
                Utils.showSoftKeyboard(mActivity);
                mCode4.setFocusable(true);
                mCode4.requestFocus();
                mCode4.setText("");
                break;
        }*/
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.code1:
                Utils.showSoftKeyboard(mActivity);
                mCode1.setFocusable(true);
                mCode1.requestFocus();
                mCode1.setText("");
                break;
            case R.id.code2:
                Utils.showSoftKeyboard(mActivity);
                mCode2.setFocusable(true);
                mCode2.requestFocus();
                mCode2.setText("");
                break;
            case R.id.code3:
                Utils.showSoftKeyboard(mActivity);
                mCode3.setFocusable(true);
                mCode3.requestFocus();
                mCode3.setText("");
                break;
            case R.id.code4:
                Utils.showSoftKeyboard(mActivity);
                mCode4.setFocusable(true);
                mCode4.requestFocus();
                mCode4.setText("");
                break;
        }
        return false;
    }

}
