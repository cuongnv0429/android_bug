package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.RefreshListMe;
import vn.com.feliz.p.eventbus.SearchReviewMe;
import vn.com.feliz.p.eventbus.positionMe;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_Me_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;

import static vn.com.feliz.v.fragment.ReviewsScreen.imgBackMain;
import static vn.com.feliz.v.fragment.ReviewsScreen.mTopBarReview;
import static vn.com.feliz.v.fragment.ReviewsScreen.tvTotalMe;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsMeScreen extends BaseFragment implements OnPostResponseListener, OnItemClickListener, OnLoadMoreListener {
    private Reviews_Me_Adapter mReviewsMeAdapter;
    @BindView(R.id.recycleReviewsMe)
    RecyclerView mRecyclerView;
    private List<ReviewAllItem> mReviewMeItemList = new ArrayList<>();
    private List<ReviewAllItem> mReviewMeItemListSum = new ArrayList<>();
    private OnLoadMoreListener mOnLoadMoreListener;
    private ReviewsPrensenter mPrensenter;
    private String mTokenUser;
    //TransactionInfo
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ListAllReviewsResponse mReviewAllResponse;
    public static int totalMe;
    private ProgressDialog mProgressDialog;
    private String keyWordSearch;
    private boolean isSearch;
    @BindView(R.id.tvNodataMe)
    TextView mtvNodataMe;
    @BindView(R.id.swipe_review_me)
    SwipeRefreshLayout mSwipeReviewMe;
    @BindView(R.id.btn_retry_me)
    Button mBtnRetryMe;

    static final String ARG_PARAM1 = "param1";
    private boolean mFistLoad = false;
    private boolean isLoad = false;

    public ReviewsMeScreen() {
        // Required empty public constructor
    }

    public static ReviewsMeScreen newInstance(boolean first_load) {
        ReviewsMeScreen fragment = new ReviewsMeScreen();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, first_load);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        mPrensenter = new ReviewsPrensenter(mActivity, this);

        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
        }

        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (getArguments() != null) {
            mFistLoad = getArguments().getBoolean(ARG_PARAM1);
        }

        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reviews_me, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        //setup recycleView
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        setOnLoadMoreListener(this);

        //set Data first
        if(!mFistLoad) {
            isLoad = true;
            mPageOffset = 0;
            setData(mPageOffset, mPageLimit);
        }
        inItRecycleViewsLoadmore();
        mSwipeReviewMe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isLoad = true;
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset, mPageLimit);
            }
        });
        mBtnRetryMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoad = true;
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset, mPageLimit);
            }
        });
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //Load data when no data
                    if (((String) o).equalsIgnoreCase("reload_data_review_detail")) {
                        if (!isLoad) {
                            isSearch = false;
                            isLoad = true;
                            loading = true;
                            mPageOffset = 0;
                            mPrensenter.getListMeReviews(mTokenUser, "me", String.valueOf(mPageOffset),
                                    String.valueOf(mPageLimit));
                        }
                    }

                    if (((String) o).equalsIgnoreCase("back_review_me")) {
                        back();
                    }
                }
            }
        });
        return rootView;
    }

    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        back();
        if (visible) {
            try {
                if(mReviewMeItemList == null || (mReviewMeItemList != null && mReviewMeItemList.size() == 0)) {
                    isLoad = true;
                    mPageOffset = 0;
                    setData(mPageOffset, mPageLimit);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerView != null) {
            mReviewsMeAdapter = new Reviews_Me_Adapter(mActivity, mReviewMeItemListSum);
            mRecyclerView.setAdapter(mReviewsMeAdapter);
        }
    }

    /**
     * set Data first load
     */
    private void setData(int mOffset, int mLimit) {
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            mPrensenter.getListMeReviews(mTokenUser, "me", String.valueOf(mOffset),
                    String.valueOf(mLimit));
        }
        if (mReviewsMeAdapter != null) {
            setUpRecycleView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        back();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        back();
    }

    private void back() {
        if (getView() != null) {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();

            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        //  Log.i(tag, "keyCode: " + keyCode);
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            //send event to ReviewScreen
                            RxBus.getInstance().post("back");
                            return true;
                        }
                        mActivity.lockEventIn(500);
                    }
                    return false;

                }


            });
        }
    }

    /**
     * set enable/disable loadMore
     */


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        // Utils.hideLoading(mProgressDialog);
        switch (task.getType()) {
            case GET_LIST_ME:
                mBtnRetryMe.setVisibility(View.GONE);
                mSwipeReviewMe.setVisibility(View.VISIBLE);
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();
                    if (mReviewAllResponse != null) {
                        try {
                            mReviewMeItemList = mReviewAllResponse.getListReviews();
                            totalMe = 0;
                            if (mReviewAllResponse.getTotal() != null) {
                                totalMe = Integer.parseInt(mReviewAllResponse.getTotal());

                            }
                            tvTotalMe.setText("(" + totalMe + ")");
                            if (mSwipeReviewMe.isRefreshing()) {
                                mReviewMeItemListSum = new ArrayList<>();
                                if (mReviewMeItemList != null) {
                                    mReviewMeItemListSum = mReviewMeItemList;
                                }
                                setUpRecycleView();
                                loading = true;
                                mSwipeReviewMe.setRefreshing(false);
                            } else {
                                if (mReviewMeItemList != null) {
                                    if (loading) {
                                        mReviewMeItemListSum = mReviewMeItemList;
                                        setUpRecycleView();
                                        mRecyclerView.smoothScrollBy(0, 2);
                                    } else if (!loading) {
                                        loading = true;
                                        mReviewMeItemListSum.addAll(mReviewMeItemList);
                                        mReviewsMeAdapter.notifyDataSetChanged();
                                    }
                                } else if (mReviewMeItemList == null && isSearch) {
                                    mReviewMeItemListSum.clear();
                                    mReviewsMeAdapter.notifyDataSetChanged();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (mReviewMeItemListSum.size() > 0) {
                        if (mtvNodataMe != null) {
                            mtvNodataMe.setVisibility(View.GONE);
                            mSwipeReviewMe.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (mtvNodataMe != null) {
                            mtvNodataMe.setVisibility(View.VISIBLE);
                            mSwipeReviewMe.setVisibility(View.GONE);
                        }
                    }
                }
                break;

        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(mActivity);
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        if (task.getThrowable() != null) {
            if (task.getType() == ApiTaskType.GET_LIST_ME) {
                Utils.hideLoading(mProgressDialog);
                mSwipeReviewMe.setRefreshing(false);
                mReviewMeItemListSum = new ArrayList<>();
                mReviewMeItemList = new ArrayList<>();
                if (mReviewsMeAdapter != null) {
                    mReviewsMeAdapter.notifyDataSetChanged();
                }
                mSwipeReviewMe.setVisibility(View.GONE);
                mtvNodataMe.setVisibility(View.VISIBLE);
                mBtnRetryMe.setVisibility(View.VISIBLE);
            }
        }
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onItemClick(View view, int position) {
    }

    @Subscribe
    public void listenerReloadListMe(RefreshListMe refreshListMe) {
        mRecyclerView.setVisibility(View.VISIBLE);
        mTopBarReview.setVisibility(View.VISIBLE);
        imgBackMain.setVisibility(View.VISIBLE);
        mRecyclerView.smoothScrollBy(0, 2);

    }

    @Override
    public void onLoadMore() {
        mReviewMeItemListSum.add(null);
        mReviewsMeAdapter.notifyItemInserted(mReviewMeItemListSum.size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Remove loading item
                if (mReviewMeItemListSum.size() > 0) {
                    mReviewMeItemListSum.remove(mReviewMeItemListSum.size() - 1);
                    mReviewsMeAdapter.notifyItemRemoved(mReviewMeItemListSum.size());
                }
                if (isSearch) {
                    isSearch = false;
                    requestGetListAllSearchReviewNextPage();
                } else {
                    requestGetListAllReviewNextPage();
                }

            }
        }, 1000);
    }

    /**
     * request list
     */
    private void requestGetListAllSearchReviewNextPage() {
        mPrensenter.getListMeReviewsSearch(mTokenUser, "me", String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), keyWordSearch);
    }

    @Subscribe
    public void getEventSearch(SearchReviewMe searchReviewMe) {
        if (getView() != null) {
            if (isFragmentUIActive()) {
                getView().setFocusableInTouchMode(true);
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                mActivity.showDialogExitApp(mActivity);
                                return true;
                            }
                            mActivity.lockEventIn(500);
                        }
                        return false;
                    }


                });
            }
        }
        try {
            isLoad = true;
            isSearch = true;
            loading = true;
            mPageOffset = 0;
            keyWordSearch = searchReviewMe.getKeyWord().trim();
            mPrensenter.getListMeReviewsSearch(mTokenUser, "me", String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), keyWordSearch);
            inItRecycleViewsLoadmore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void getClickPosFromAdapter(positionMe positionMe) {
        int position = positionMe.getPos();
        ReviewsDetailProductScreen reviewsDetailProductScreen = new ReviewsDetailProductScreen();
        Bundle bundle = new Bundle();
        try {

            bundle.putString(Constant.F_ARG_PRODUCT_IMG, mReviewMeItemListSum.get(position).getProduct_image());
            bundle.putString(Constant.F_ARG_PRODUCT_NAME, mReviewMeItemListSum.get(position).getProduct_name());
            bundle.putString(Constant.F_ARG_PRODUCT_CONTENT, mReviewMeItemListSum.get(position).getContent());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING, mReviewMeItemListSum.get(position).getProduct_rating());
            if (mReviewMeItemListSum.get(position).getListStart() != null) {
                bundle.putString(Constant.F_ARG_PRODUCT_RATING_5, mReviewMeItemListSum.get(position).getListStart().getFive());
                bundle.putString(Constant.F_ARG_PRODUCT_RATING_4, mReviewMeItemListSum.get(position).getListStart().getFour());
                bundle.putString(Constant.F_ARG_PRODUCT_RATING_3, mReviewMeItemListSum.get(position).getListStart().getThree());
                bundle.putString(Constant.F_ARG_PRODUCT_RATING_2, mReviewMeItemListSum.get(position).getListStart().getTwo());
                bundle.putString(Constant.F_ARG_PRODUCT_RATING_1, mReviewMeItemListSum.get(position).getListStart().getOne());
            }
            if (mReviewMeItemListSum.get(position).getProduct_review() != null) {
                bundle.putString(Constant.F_ARG_PRODUCT_REVIEWS, mReviewMeItemListSum.get(position).getProduct_review());
            }
            bundle.putString(Constant.F_ARG_PRODUCT_ID, mReviewMeItemListSum.get(position).getProduct_id());
        } catch (Exception e) {
            e.printStackTrace();
        }
        reviewsDetailProductScreen.setArguments(bundle);
        android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.add(R.id.rlNewReview, reviewsDetailProductScreen, "ReviewsDetailProductScreen");
        fragmentTransaction.commit();
        ReviewsScreen.mRlNewReview.setVisibility(View.VISIBLE);
    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {

        mPrensenter.getListMeReviews(mTokenUser, "me", String.valueOf(mPageOffset),
                String.valueOf(mPageLimit));
    }

    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }


    @Subscribe
    public void feFreshData(refreshDataReview refreshData) {
        if (refreshData.getTypeScreen().toLowerCase().equals("review_me") && !isLoad && isFragmentUIActive()) {
            isLoad = true;
            loading = true;
            mPageOffset = 0;
            mPrensenter.getListMeReviews(mTokenUser, "me", String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit));

        }
    }

    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }

}
