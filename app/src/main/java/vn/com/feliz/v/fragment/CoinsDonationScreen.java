package vn.com.feliz.v.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.androidadvance.topsnackbar.TSnackbar;

import org.greenrobot.eventbus.EventBus;

import rx.functions.Action1;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.v.interfaces.IEnterBoxCoinsDonation;
import vn.com.feliz.v.widget.CustomEdittext;
import vn.com.feliz.R;

/**
 * Created by ITV01 on 12/23/16.
 */

@SuppressLint("ValidFragment")
public class CoinsDonationScreen extends BaseFragment implements View.OnClickListener{
    private static final String TAG = "CoinsDonationScreen";
    View view;
    private CustomEdittext edtCoinsEnter;
    private Button btnGift;

    private boolean showKeyBoard;
    private IEnterBoxCoinsDonation iEnterBoxCoinsDonation;
    private StringBuilder stringBuilder;
    private TSnackbar snackbar;
    private TextWatcher textWatcher;

    public CoinsDonationScreen(IEnterBoxCoinsDonation iEnterBoxCoinsDonation) {
        this.iEnterBoxCoinsDonation = iEnterBoxCoinsDonation;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_coins_donation_screen, container, false);
            initView();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void initView() {
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("200")) {
                        btnGift.setVisibility(View.GONE);
                        edtCoinsEnter.setText("");
                    }
                }
            }
        });
        edtCoinsEnter = (CustomEdittext) view.findViewById(R.id.edt_coins_enter);
        btnGift = (Button) view.findViewById(R.id.btn_gift);
        edtCoinsEnter.setOnClickListener(this);
        showKeyBoard = false;
        stringBuilder = new StringBuilder();
        edtCoinsEnter.setCursorVisible(false);

        btnGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.lockEventIn(500);
                if (!edtCoinsEnter.getText().toString().trim().equalsIgnoreCase("")) {
                    hideKeyboardDefaultOfDevices();
                    edtCoinsEnter.setCursorVisible(false);
                    showKeyBoard = false;
                    EventBus.getDefault().post(edtCoinsEnter.getText().toString().trim());
                }
            }
        });


        edtCoinsEnter.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                edtCoinsEnter.setCursorVisible(true);
                showKeyBoard = true;
                return true;
            }
        });
        textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    int coins = Integer.parseInt(s.toString().trim());
                    if (validateCoinNumber(coins)) {
                        edtCoinsEnter.removeTextChangedListener(textWatcher);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(s.toString());
                        edtCoinsEnter.setText(stringBuilder.deleteCharAt(start).toString());
                        edtCoinsEnter.setSelection(edtCoinsEnter.getText().length());
                        edtCoinsEnter.addTextChangedListener(textWatcher);
                        return;
                    }
                    edtCoinsEnter.setBackgroundResource(R.color.cl_box);
                    if (coins == 0 && s.length() > 1) {
                        edtCoinsEnter.removeTextChangedListener(textWatcher);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(s.toString());
                        edtCoinsEnter.setText(stringBuilder.deleteCharAt(start).toString());
                        edtCoinsEnter.setSelection(edtCoinsEnter.getText().length());
                        edtCoinsEnter.addTextChangedListener(textWatcher);
                        return;
                    }
                    if (coins == 0) {
                        btnGift.setVisibility(View.GONE);
                    } else {
                        btnGift.setVisibility(View.VISIBLE);
                    }
                } catch (NumberFormatException numBerFormat) {
                    btnGift.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        edtCoinsEnter.addTextChangedListener(textWatcher);

    }

    private boolean validateCoinNumber(int number) {
        if (number > CoinsScreen.currentCoin) {
            iEnterBoxCoinsDonation.validateCoin(getActivity().getResources().getString(R.string.alert_error_coin_number_rather_coin_current));
            edtCoinsEnter.setBackgroundResource(R.drawable.bg_error_enter_coin);
            return true;
        }
        return false;
    }

    public void showInputMethod(View view) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    private void hideKeyboardDefaultOfDevices() {
        InputMethodManager inputMethod = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethod.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

//    @Subscribe
//    public void addNumberFromKeyboard(DigitCoins digitCoins) {
//        String number = digitCoins.getDigit();
//        int positionCursor = edtCoinsEnter.getSelectionStart();
//        if (number.equalsIgnoreCase("*")) {
//            if (0 < positionCursor &&
//                    positionCursor - 1 < stringBuilder.length()) {
//                stringBuilder.deleteCharAt(edtCoinsEnter.getSelectionStart() - 1);
//                edtCoinsEnter.setText(stringBuilder.toString());
//                edtCoinsEnter.setSelection(positionCursor - 1);
//            }
//
//        } else if (number.equalsIgnoreCase("done")) {
//            if (!edtCoinsEnter.getText().toString().trim().equalsIgnoreCase("")) {
//                edtCoinsEnter.setCursorVisible(false);
//                showKeyBoard = false;
//                EventBus.getDefault().post(edtCoinsEnter.getText().toString().trim());
//            }
//        } else {
//            stringBuilder.insert(positionCursor, number);
//            edtCoinsEnter.setText(stringBuilder.toString());
//            edtCoinsEnter.setSelection(positionCursor + 1);
//        }
//    }

//    @Override
//    public void onKeyIme(int keyCode, KeyEvent event) {
//        Log.e(TAG, "onKeyIme");
//        if (KeyEvent.KEYCODE_BACK == event.getKeyCode() && showKeyBoard) {
//            edtCoinsEnter.setCursorVisible(false);
//            showKeyBoard = false;
//            iEnterBoxCoinsDonation.enterBoxCoinsDonation(edtCoinsEnter, showKeyBoard);
//        }
//        Log.e(TAG, event.getCharacters() + "__" + event.getKeyCode());
//        if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//            Log.e(TAG, event.getCharacters() );
//        }
//    }

    @Override
    public void onClick(View v) {
        Log.e(TAG, "edt_coins_onclick: " + showKeyBoard);
        switch (v.getId()) {
            case R.id.edt_coins_enter:
                showInputMethod(edtCoinsEnter);
                if (!showKeyBoard) {
                    edtCoinsEnter.setCursorVisible(true);
                    showKeyBoard = true;
//                    this.iEnterBoxCoinsDonation.enterBoxCoinsDonation(edtCoinsEnter, showKeyBoard);
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        Utils.hideKeyBoardWhenFocus(edtCoinsEnter, getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        EventBus.getDefault().unregister(this);
    }
}