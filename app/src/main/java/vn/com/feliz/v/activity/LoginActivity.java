package vn.com.feliz.v.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidadvance.topsnackbar.TSnackbar;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.LoginAuth;
import vn.com.feliz.common.NetworkStateReceiver;
import vn.com.feliz.common.ResizeAnimation.ResizeAnimation;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.LoginModel;
import vn.com.feliz.m.ProfileConfirmInfo;
import vn.com.feliz.m.response.RegisterResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.LoginPresenter;
import vn.com.feliz.v.widget.PhoneEditText;


public class LoginActivity extends BaseActivity implements NetworkStateReceiver.NetworkStateReceiverListener, View.OnClickListener,
        OnPostResponseListener, ViewTreeObserver.OnGlobalLayoutListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    //facebook
    private LoginButton mLoginButtonFacebook;
    private static LoginManager mLoginManagerFb;
    private AccessTokenTracker accessTokenTracker;
    private AccessToken accessToken;
    private ProfileTracker profileTracker;
    private static CallbackManager callbackManager;
    private String mFBAccessToken;

    private FirebaseAuth mFireBaseAuth;

    private LoginPresenter mLoginPresenter;

    private RelativeLayout mLoginView;

    private ProgressDialog mProgressDialog;
    private TSnackbar snackbar;
    private NetworkStateReceiver mNetworkStateReceiver;

    private LinearLayout loginGBLogoWP;
    private ImageView loginLogoImg;
    private PhoneEditText mPhoneNumber;
    private Button mButtonPhoneConfirm;
    private String mPhoneNumberText;
    private boolean isSideTop = false;
    private LoginModel mLoginModel;

    private boolean isRegister = true;

    private LoginAuth mLoginAuth;

    private LinearLayout mMainLogin;

    private boolean keyboardOpen = false;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        /*
        * Initial firebase instance
         */
        mFireBaseAuth = FirebaseAuth.getInstance();

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        mLoginView = (RelativeLayout) findViewById(R.id.login_activity_container);
        loginGBLogoWP = (LinearLayout) findViewById(R.id.login_logo_wp);
        loginLogoImg = (ImageView) findViewById(R.id.login_log_img);
        mPhoneNumber = (PhoneEditText) findViewById(R.id.edt_phone);
        mButtonPhoneConfirm = (Button) findViewById(R.id.btn_confirm);
        mMainLogin = (LinearLayout) findViewById(R.id.login_main);

        mPhoneNumber.setOnClickListener(this);
        mButtonPhoneConfirm.setOnClickListener(this);
        findViewById(R.id.activity_term).setOnClickListener(this);

        // Initial LoginModel, LoginPresenter
        mLoginModel = new LoginModel();
        mLoginPresenter = new LoginPresenter(this, this);
        mLoginPresenter.clearUser();
        LoginManager.getInstance().logOut();

        //check nextwork connect create
        try {
            mNetworkStateReceiver = new NetworkStateReceiver();
            mNetworkStateReceiver.addListener(this);
            this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }

        mLoginButtonFacebook = (LoginButton) findViewById(R.id.login_activity_login_button);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        int maxLength = 11;
        mPhoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

        mPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
            @Override
            public void afterTextChanged(Editable s) {
                mPhoneNumberText = s.toString().trim();
                if(s.length() > 9) {
                    mButtonPhoneConfirm.setBackgroundColor(getResources().getColor(R.color.btn_confirm_background));
                }
            }
        });

        mLoginAuth = new LoginAuth(this);

        mMainLogin.getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (mLoginPresenter.hasMetaInfo() != null) {
            int version = 0;
            try {
                PackageInfo mPackageInfor = getPackageManager().getPackageInfo(getPackageName(), 0);
                version = mPackageInfor.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            String message = "";
            if (mLoginPresenter.hasMetaInfo().getForce_update().equalsIgnoreCase("1")) {
                if (mLoginPresenter.hasMetaInfo().getForce_message() != null) {
                    if (mLoginPresenter.hasMetaInfo().getForce_message().equalsIgnoreCase("")) {
                        message = getResources().getString(R.string.alert_force_update_app);
                    } else {
                        message = mLoginPresenter.hasMetaInfo().getForce_message();
                    }
                } else {
                    message = mLoginPresenter.hasMetaInfo().getForce_message();
                }
                dialogForceUpdate(message);
                return;
            } else {
                if (mLoginPresenter.hasMetaInfo().getForce_update().equalsIgnoreCase("2")) {
                    if (mLoginPresenter.hasMetaInfo().getForce_message() != null) {
                        if (mLoginPresenter.hasMetaInfo().getForce_message().equalsIgnoreCase("")) {
                            message = getResources().getString(R.string.alert_lock_account);
                        } else {
                            message = mLoginPresenter.hasMetaInfo().getForce_message();
                        }
                    }
                    dialogLockAccount(message);
                    return;
                }
            }
        }

        //check notification settings
        Set<String> allowedApps = NotificationManagerCompat.getEnabledListenerPackages(this);
        if (allowedApps != null && allowedApps.contains(getPackageName())) {
            Log.d("HoangNM", "Granted the permission");
        } else {
            Utils.showMessageNoTitle(this, "Bạn cần cho phép bật tính năng truy cập thông báo.", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        Intent ii = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                        startActivity(ii);
                        dialogInterface.dismiss();
                    } else {
                        try {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                            startActivity(intent);
                            dialogInterface.dismiss();
                        } catch (ActivityNotFoundException ex) {
                            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                            startActivity(intent);
                            dialogInterface.dismiss();
                        }
                    }
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndRequestPermissionsLoaction();
        }
        if (checkPlayServices()) {
            createGoogleApiClient();
        }
    }

    private void createGoogleApiClient() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private boolean checkAndRequestPermissionsLoaction() {
        int coraespermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int locationpermission = ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (coraespermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), Constant.REQUEST_PERMISSIONS_LOCATION);
            }
            return false;
        }
        return true;
    }

    private void dialogLockAccount(String message) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = (LayoutInflater) LoginActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout linear = (LinearLayout) dialogView.findViewById(R.id.linear);
        linear.setVisibility(View.GONE);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(message);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void dialogForceUpdate(String message) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = (LayoutInflater) LoginActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.text_update));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        btnNo.setVisibility(View.GONE);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(message);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    finish();
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    finish();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (LoginAuth.callbackManager == null) {
                LoginAuth.callbackManager = CallbackManager.Factory.create();
            }
            LoginAuth.callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    private LoginCallback mLoginCallback = new LoginCallback() {
        @Override
        public void processFB(LoginModel loginModel) {
            Log.e(">>", mLoginModel.toString());
            mLoginModel = loginModel;
            processAccount();
        }

        @Override
        public void fbLoginOther() {
            Utils.hideLoading(mProgressDialog);
            fbLoginOtherAction();
        }

        @Override
        public void cancelFB() {
            Utils.hideLoading(mProgressDialog);
            if ( mLoginPresenter != null) {
                mLoginPresenter.submitErrorLoginFB("Cancle Facebook");
            }
        }


        public void errorFB(String error) {

            Utils.hideLoading(mProgressDialog);
            if ( mLoginPresenter != null) {
                mLoginPresenter.submitErrorLoginFB(error);
            }
            try {
                snackbar = TSnackbar
                        .make(mLoginView, " " + getString(R.string.error_login_fb), TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbarNextWork(snackbar, getString(R.string.error_login_fb), LoginActivity.this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.edt_phone:

                break;
            case R.id.btn_confirm:
                if(mPhoneNumber.getText().toString().trim().length() >= 10) {
                    mLoginModel.setPhone(mPhoneNumber.getText().toString().trim());
                    mProgressDialog = Utils.showLoading(this);
                    mLoginAuth.nextStepLoginFB(mLoginCallback);
                }
                break;
            case R.id.activity_term:
                try {
                    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.setContentView(R.layout.fragment_terms_conditions_screen);
                    dialog.setTitle(getString(R.string.term_title));
                    WebView mTextContent = (WebView) dialog.findViewById(R.id.textContent);

                    String textContent;
                    textContent = "<html><body><p align=\"justify\">";
                    textContent += mLoginPresenter.hasMetaInfo().getTerm();
                    textContent += "</p></body></html>";
                    // mTextContent.loadData(textContent, "text/html", "utf-8");
                    mTextContent.getSettings().setJavaScriptEnabled(true);
                    mTextContent.loadDataWithBaseURL("", textContent, "text/html", "UTF-8", "");
                    Button btnCancel = (Button) dialog.findViewById(R.id.btn_confirm_term_condition);
                    BaseApplication application = BaseApplication.getInstance();
//                    btnCancel.setTypeface(application.getFontHelvetical());
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /**
     * Destroy all fragments and loaders.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNetworkStateReceiver != null) {
            mNetworkStateReceiver.removeListener(this);
            this.unregisterReceiver(mNetworkStateReceiver);
        }

        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        //accessTokenTracker.stopTracking();
    }

    @Override
    public void networkAvailable() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    @Override
    public void networkUnavailable() {
        if (mLoginPresenter != null) {
            mLoginPresenter.submitErrorLoginFB("không kết nối được internet");
        }
        //setup snackBar
        snackbar = TSnackbar
                .make(mLoginView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
        Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
    }

    @Override
    public void onGlobalLayout() {
        //check keyboard show/hide
        Rect r = new Rect();
        mMainLogin.getWindowVisibleDisplayFrame(r);
        int screenHeight = mMainLogin.getRootView().getHeight();
        // r.bottom is the position above soft keypad or device button.
        // if keypad is shown, the r.bottom is smaller than that before.
        int keypadHeight = screenHeight - r.bottom;

        if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
            // keyboard is open
            if(!keyboardOpen) {
                keyboardOpen = true;
                ResizeAnimation resizeAnimation = new ResizeAnimation(loginGBLogoWP, 0.1f, 0.5f, true);
                resizeAnimation.setDuration(300);
                loginGBLogoWP.startAnimation(resizeAnimation);
                loginGBLogoWP.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {}
                    @Override
                    public void onAnimationEnd(Animation animation) {
                        loginLogoImg.setVisibility(View.GONE);
                    }
                    @Override
                    public void onAnimationRepeat(Animation animation) {}
                });
            }
        } else {
            // keyboard is close
            if(keyboardOpen) {
                keyboardOpen = false;
                loginLogoImg.setVisibility(View.VISIBLE);
                ResizeAnimation resizeAnimation = new ResizeAnimation(loginGBLogoWP, 0.5f, 0.1f, false);
                resizeAnimation.setDuration(300);
                loginGBLogoWP.startAnimation(resizeAnimation);
            }
        }
    }

    private void loginUser(String token, String accessToken) {
        mProgressDialog = Utils.showLoading(this);
        mLoginPresenter.loginRequest(token, accessToken);
    }


    private void processAccount() {
        mLoginModel.setPhone(mPhoneNumber.getText().toString().trim());
        mLoginModel.setFirebaseUID(mLoginModel.getFbID() + Constant.FIREBASE_EMAIL_PREFIX);
        mLoginModel.setFirebasePassword(Utils.md5(mLoginModel.getFbID()));
        // Start SingIn and check account exist
        Log.e(">>>>>>>1", mLoginModel.toString());
        fireBaseSignIn();
    }

    private void fireBaseSignIn() {
        //authenticate user
        try {
            Log.e(">>>>>>2", mLoginModel.toString());
            mFireBaseAuth.createUserWithEmailAndPassword(mLoginModel.getFirebaseUID(), mLoginModel.getFirebasePassword())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                // SignIn failure
                                Log.e("fbx", "2");
                                fireBaseLogin();
                            } else {
                                Log.e("fbx", "3");
                                // SignIn OK
                                FirebaseUser firebaseUser = task.getResult().getUser();
                                mLoginModel.setFirebaseUID(firebaseUser.getUid());
                                registerUser();
                            }
                        }
                    });
        } catch(Exception e) {
            Log.e("fbx", "Error");
            if (mLoginPresenter != null) {
                mLoginPresenter.submitErrorLoginFB(e.getMessage());
            }
            fireBaseLogin();
            e.printStackTrace();
        }
    }

    private void fireBaseLogin() {
        try {
            mFireBaseAuth.signInWithEmailAndPassword(mLoginModel.getFirebaseUID(), mLoginModel.getFirebasePassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.getException() != null) {
                        task.getException().printStackTrace();
                        task.getException();
                        if ( mLoginPresenter != null) {
                            mLoginPresenter.submitErrorLoginFB(task.getException().getMessage());
                        }
                        Utils.hideLoading(mProgressDialog);
                    }
                    if (task.isSuccessful()) {
                        Log.e("fbx", "4");
                        FirebaseUser firebaseUser = task.getResult().getUser();
                        mLoginModel.setFirebaseUID(firebaseUser.getUid());
                        registerUser();
                    }
                }
            });
        } catch (Exception e) {
            if (mLoginPresenter != null) {
                mLoginPresenter.submitErrorLoginFB(e.getMessage());
            }
            snackbar = TSnackbar
                    .make(mLoginView, getString(R.string.firebase_error), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.firebase_error), this);
            e.printStackTrace();
        }
    }

    private void registerUser() {
        Log.e("fbx", "5");
        mLoginPresenter.registerAccount2(
                mLoginModel.getAccessToken(),
                mLoginModel.getFbID(),
                mLoginModel.getName(),
                mLoginModel.getEmail(),
                mLoginModel.getPhone(),
                mLoginModel.getGender(),
                mLoginModel.getAddress(),
                mLoginModel.getAvatar(),
                mLoginModel.getFirebasePassword(),
                mLoginModel.getFirebaseUID(),
                mLoginModel.getBirthday()
        );
    }

    public void gotoNewRegister() {
        mPhoneNumber.setText("");
        mPhoneNumberText = "";
        mLoginPresenter.clearUser();
    }

    public void dialogConfirm(String message, String textYes, String textNo, final DialogCallback callback) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(textYes);
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(textNo);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);

        contentDialog.setText(message);

        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.goit(dialog);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.cancel(dialog);
            }
        });
    }

    public void fbLoginOtherAction() {
        dialogConfirm(
                getString(R.string.login_facebook_login_other_account),
                getString(R.string.login_facebook_retry),
                getString(R.string.login_facebook_register_new),
                new DialogCallback() {
                    @Override
                    public void goit(Dialog dialog) {
                        dialog.dismiss();
                        mLoginAuth.nextStepLoginFB(mLoginCallback);
                    }
                    @Override
                    public void cancel(Dialog dialog) {
                        dialog.dismiss();
                        LoginManager.getInstance().logOut();
                        gotoNewRegister();
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLastLocation == null) {
            mLastLocation = location;
            mLoginPresenter.initDevice(LoginActivity.this, mLastLocation);
        }
    }

    public interface LoginCallback {
        void processFB(LoginModel mLoginModel);
        void fbLoginOther();
        void cancelFB();
        void errorFB(String error);
    }

    public interface DialogCallback {
        void goit(Dialog dialog);
        void cancel(Dialog dialog);
    }

    // check if google play services is installed on the device
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(),
                        "This device is supported. Please download google play services", Toast.LENGTH_LONG)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void getLocation() {
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constant.REQUEST_PERMISSIONS_LOCATION);
        } else {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
            mLoginPresenter.initDevice(LoginActivity.this, mLastLocation);
        }
    }

    //


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if (task.getType() == ApiTaskType.REGISTER) {
            if(task.getResponse() == null) {
//                showNetworkAlert(mLoginView);
                showErrorLogin(mLoginView, "respone null");
            } else {
                BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                if (baseResponse != null) {
                    RegisterResponse data = (RegisterResponse) baseResponse.getData();
                    if (status == ApiResponseCode.SUCCESS && data != null) {
                        try {
                            Log.d("sssDEBUG", "mRegisterRespone" + data.getToken());

                            Utils.saveToken(this, data.getToken());

                            Intent i = new Intent(LoginActivity.this, MainActivity.class);
                            ProfileConfirmInfo mP = new ProfileConfirmInfo();
                            mP.setAdress(data.getAddress());
                            mP.setBirthday(data.getBirthday());
                            mP.setGender(data.getGender());
                            mP.setUserName(data.getFullname());
                            mP.setMobile(data.getPhone());
                            mP.setCoupons_id(data.getCoupon_id());
                            mP.setIsVerify(data.getIsVerify());
                            //put data to bundle
                            i.putExtra(Constant.FACEBOOK_OBJECT, mP);
                            startActivity(i);

                            //animation fast start activity
                            overridePendingTransition(0, R.anim.pull_in_left);
                            finish();

                        } catch (Exception e) {
                            e.printStackTrace();
                            if (mLoginPresenter != null) {
                                mLoginPresenter.submitErrorLoginFB(e.getMessage());
                            }
                        }
                    } else {
                        Utils.hideLoading(mProgressDialog);
                        if (mLoginPresenter != null) {
                            mLoginPresenter.submitErrorLoginFB("Không có data, data null");
                        }
                        // mRlLoading.setVisibility(View.GONE);
                        //setup snackBar
                        snackbar = TSnackbar
                                .make(mLoginView, " ", TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), this);
                        Utils.vibarte(this);
                    }
                } else {
//                    showNetworkAlert(mLoginView);
                    showErrorLogin(mLoginView, "body null");
                }
            }
        } else if (task.getType() == ApiTaskType.LOGIN) {
            if (status == ApiResponseCode.SUCCESS) {
                //mLoginAuth.nextStepLoginFB();
            } else //if login not success
            {
                gotoNewRegister();
            }
        }
        return true;
    }

    public void showErrorLogin(View view, String error) {
        if (view != null) {
            if (mLoginPresenter != null) {
                mLoginPresenter.submitErrorLoginFB(error);
            }
            TSnackbar snackbar = TSnackbar
                    .make(view, " " + getString(R.string.error_login_fail), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbarNextWork(snackbar, getString(R.string.error_login_fail), this);
        }

    }

    private void gotoMainActivity() {
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        overridePendingTransition(0, R.anim.pull_in_left);
        // close this activity
        finish();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQUEST_PERMISSIONS_LOCATION:
                if (permissions.length > 1) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        getLocation();
                    }else {
//                    Utils.displayPromptForEnablingGPS(LoginActivity.this);
                    }
                }
                break;
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
