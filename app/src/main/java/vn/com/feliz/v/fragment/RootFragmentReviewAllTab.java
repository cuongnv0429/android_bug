package vn.com.feliz.v.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.com.feliz.R;

/**
 * Created by Nguyen Thai Son on 2016-12-24.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RootFragmentReviewAllTab extends BaseFragment {

    static final String ARG_PARAM1 = "param1";
    private boolean mFistLoad = false;

    public RootFragmentReviewAllTab() {
        // Required empty public constructor
    }

    public static RootFragmentReviewAllTab newInstance(boolean first_load) {
        Log.e("REVIEW LOAD", "RootFragmentReviewAllTab");
        RootFragmentReviewAllTab fragment = new RootFragmentReviewAllTab();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, first_load);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.root_fragment_review, container, false);

        if (getArguments() != null) {
            mFistLoad = getArguments().getBoolean(ARG_PARAM1);
        }

        /* Inflate the layout for this fragment */
        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.addToBackStack(null);
        transaction.add(R.id.root_review, ReviewsAllScreen.newInstance(mFistLoad));
        transaction.commit();
       // getChildFragmentManager().beginTransaction().replace(R.id.root_review, new ReviewsAllScreen()).addToBackStack(null).commit();

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);

    }


}
