package vn.com.feliz.v.fragment;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import rx.Observable;
import rx.Subscriber;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.Profile;
import vn.com.feliz.m.response.DeviceInfo;
import vn.com.feliz.m.response.MessageGiftCoin;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.data.DataManager;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.CoinsPrensenter;
import vn.com.feliz.p.RegisterPresenter;
import vn.com.feliz.v.interfaces.IEnterBoxCoinsDonation;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;

/**
 * A simple {@link Fragment} subclass.
 */
public class CoinsScreen extends BaseFragment implements OnPostResponseListener, OnMenuItemSelected,
        IEnterBoxCoinsDonation, OnResponseListener , View.OnClickListener{

    private static final String TAG = "CoinsScreen";
    @BindView(R.id.txt_coins_donation)
    TextView txtCoinsDonation;
    @BindView(R.id.txt_lucky_gibi)
    TextView txtLuckyGibi;
    @BindView(R.id.txt_app_donation)
    TextView txtAppDonation;
    @BindView(R.id.linear_data)
    LinearLayout linearData;
    @BindView(R.id.scroll_coin_screen)
    ScrollView scrollCoinScreen;
    @BindView(R.id.main_topbar_back)
    ImageButton imgButtonTopbarBack;
    @BindView(R.id.main_top_bar_title)
    TextView txtTopbarTitle;
    @BindView(R.id.txt_current_coin)
    TextView txtCurrentCoin;
    @BindView(R.id.txt_save_coin)
    TextView txtSaveCoin;
    @BindView(R.id.txt_gift_coin)
    TextView txtGiftCoin;
    @BindView(R.id.txt_used_coin)
    TextView txtUsedCoin;
    @BindView(R.id.main_top)
    View mainTop;
    @BindView(R.id.frame_item_coins)
    FrameLayout frame_item_coins;
    @BindView(R.id.linear_tab)
    LinearLayout linearTab;
    @BindView(R.id.linear_translate)
    LinearLayout linearTranslate;
    @BindView(R.id.linear_root)
    LinearLayout linearRoot;
    private int itemSelected = 2;
    IEnterBoxCoinsDonation iEnterBoxCoinsDonation = this;
    private View rootView;
    private CompositeSubscription mComposite;
    private RegisterPresenter registerPresenter;
    public static int currentCoin = 0;
    private CoinsPrensenter coinsPrensenter;
    private MessageGiftCoin messageGiftCoin;
    private TSnackbar snackbar;
    private Boolean isShowKeyBoard = false;
    private ProgressDialog mProgressDialog;
    private int screenHeight;
    Rect rect = new Rect();
    private float positionTopRoot = 0;
    DeviceInfo mDeviceInfo;
    private Realm mRealm;
    private String devcieId;
    private int heightResize;
    private int frameHeight;

    public CoinsScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        // Set Title
//        EventBus.getDefault().post(new SetTitleMessage(getString(R.string.coin_Screen)));
//        EventBus.getDefault().post(this);
        // Register Events

        mRealm = Realm.getDefaultInstance();

        if (mDeviceInfo == null) {
            mDeviceInfo = mRealm.where(DeviceInfo.class).findFirst();
            if(mDeviceInfo == null) {
                devcieId = "no-device-id";
            } else {
                devcieId = mDeviceInfo.getmDeviceId();
            }
        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (rootView == null) {
            // Inflate the layout for this fragment
            rootView = inflater.inflate(R.layout.fragment_coin_screen, container, false);
            // Bind Resources
            mUnbinder = ButterKnife.bind(this, rootView);
            mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            initView();
        }
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideKeyboardDefaultOfDevices();
    }

    private void hideKeyboardDefaultOfDevices() {
        InputMethodManager inputMethod = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethod.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void initView() {
        registerPresenter = new RegisterPresenter(mActivity, this);
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
        coinsPrensenter = new CoinsPrensenter(mActivity, this);
        frame_item_coins.setOnClickListener(this);
        imgButtonTopbarBack.setOnClickListener(this);
        txtLuckyGibi.setTextColor(getResources().getColor(R.color.white));
        txtLuckyGibi.setBackgroundColor(getResources().getColor(R.color.coins_total));
        imgButtonTopbarBack.setVisibility(View.GONE);
        txtTopbarTitle.setText(getResources().getString(R.string.coin_Screen));
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_item_coins, new LuckyGibiScreen())
                .commitAllowingStateLoss();

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if(registerPresenter.getUserInfo() != null) {
            String uid = registerPresenter.getUserInfo().getUid();
            getData(uid);
        }
        scrollCoinScreen.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootView.getWindowVisibleDisplayFrame(rect);
                screenHeight = rootView.getRootView().getHeight();
                int heightDifference = screenHeight - rect.bottom + rect.top;
                Rect rectFrameHeight = new Rect();
                linearTranslate.getLocalVisibleRect(rectFrameHeight);
                if (positionTopRoot == 0) {
                    positionTopRoot = linearTranslate.getY();
                }
                if (frameHeight == 0) {
                    frameHeight = frame_item_coins.getHeight();
                }
                if (heightDifference > screenHeight * 0.15) {
                    if (!isShowKeyBoard) {
                        isShowKeyBoard = true;
                        heightResize = screenHeight - (screenHeight - rect.bottom)
                                - linearTab.getHeight() - mainTop.getHeight();
                        if (mActivity.getCurrentTab() == 1) {
                            imgButtonTopbarBack.setVisibility(View.VISIBLE);
                            translateView(linearTranslate, linearTab.getY(), linearRoot.getY());
                        }
                    }
                } else {
                    if (isShowKeyBoard) {
                        isShowKeyBoard = false;
                        if (mActivity.getCurrentTab() == 1) {
                            imgButtonTopbarBack.setVisibility(View.GONE);
                            translateView(linearTranslate, rootView.getY(), positionTopRoot);
                        }
                    }
                }

            }
        });
    }

    private void updateTabItem(int position) {
        switch (position) {
            case 1:
                txtCoinsDonation.setTextColor(getResources().getColor(R.color.white));
                txtCoinsDonation.setBackgroundColor(getResources().getColor(R.color.coins_total));
                txtLuckyGibi.setTextColor(getResources().getColor(R.color.black));
                txtLuckyGibi.setBackgroundColor(getResources().getColor(R.color.activity_main_action_bar));
                txtAppDonation.setTextColor(getResources().getColor(R.color.black));
                txtAppDonation.setBackgroundColor(getResources().getColor(R.color.activity_main_action_bar));
                break;
            case 2:
                txtCoinsDonation.setTextColor(getResources().getColor(R.color.black));
                txtCoinsDonation.setBackgroundColor(getResources().getColor(R.color.activity_main_action_bar));
                txtLuckyGibi.setTextColor(getResources().getColor(R.color.white));
                txtLuckyGibi.setBackgroundColor(getResources().getColor(R.color.coins_total));
                txtAppDonation.setTextColor(getResources().getColor(R.color.black));
                txtAppDonation.setBackgroundColor(getResources().getColor(R.color.activity_main_action_bar));
                break;
        }

    }

    private void translateView(final View root, float from, float to) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(root, "y", from, to);
        objectAnimator.setDuration(100);
        objectAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (isShowKeyBoard) {
                    frame_item_coins.setLayoutParams(new LinearLayout.LayoutParams(frame_item_coins.getWidth(),
                            ViewGroup.LayoutParams.MATCH_PARENT));
                } else {
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(frame_item_coins.getWidth(),
                            ViewGroup.LayoutParams.MATCH_PARENT);
                    frame_item_coins.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        objectAnimator.start();
    }

    private void setCoins(Profile profile) {
        currentCoin = profile.getCurrent_coin();
        if (txtCurrentCoin != null && txtSaveCoin != null && txtUsedCoin != null && txtGiftCoin != null) {
            txtCurrentCoin.setText(profile.getCurrent_coin() + "");
            txtSaveCoin.setText(profile.getSave_coin() + "");
            txtGiftCoin.setText(profile.getGift_coin() + "");
            txtUsedCoin.setText(profile.getUsed_coin() + "");
        }
    }

    @Subscribe
    public void listenerShareCoins(String valueGiftCoin) {
        int valueCoins  = 0;
        try {
            valueCoins = Integer.parseInt(valueGiftCoin);
        } catch (NumberFormatException numberFormat) {
            validateCoins(getResources().getString(R.string.snack_over_current_coins));
            return;
        }
        if (valueCoins > currentCoin) {
            validateCoins(getResources().getString(R.string.snack_over_current_coins));
            return;
        }

        if (valueCoins <= 0) {
            validateCoins(getResources().getString(R.string.snack_coin_number_rather_0));
            return;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();

        String messageCoins = "Sau 7 ngày, nếu người nhận không tải app nhận gb$, bạn sẽ được hoàn trả số gb$ đã bị trừ. " +
                "\n\nBạn có muốn tiếp tục?";
        confirmGiftCoins(messageCoins, valueCoins + "");
    }

    private void confirmGiftCoins(String smsConfirm, final String valueCoins) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(smsConfirm);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String token = coinsPrensenter.getUserInfo().getToken();
                getMessage(token, "1", valueCoins + "");
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void confirmGiftApp(String smsConfirm) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(smsConfirm);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                String token = coinsPrensenter.getUserInfo().getToken();
                getMessage(token, "2", "gibi");
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void validateCoins(String errEnterCoins) {
        //setup snackBar
        snackbar = TSnackbar
                .make(rootView, errEnterCoins, TSnackbar.LENGTH_SHORT);
        Utils.showMessageTopbar(snackbar, errEnterCoins, mActivity);
        Utils.vibarte(mActivity);
    }

    private void share(MessageGiftCoin messageGiftCoin) {
        Intent i = new Intent();
        i.setAction(Intent.ACTION_SEND);
        i.putExtra(Intent.EXTRA_TEXT, messageGiftCoin.getMessage());
        i.putExtra(Intent.EXTRA_SUBJECT, messageGiftCoin.getTitle());
        i.setType("text/plain");
        startActivity(Intent.createChooser(i, "Gửi tặng"));
    }

   /* private void updateCoins(final String uId, final int value) {
        if (mComposite == null) {
            mComposite = new CompositeSubscription();
        }
        mComposite.add(AppObservable.bindActivity(mActivity,
                Observable.create(new Observable.OnSubscribe<Boolean>() {
                    @Override
                    public void call(final Subscriber<? super Boolean> subscriber) {
                        DatabaseReference ref = FirebaseDatabase.getInstance()
                                .getReferenceFromUrl("https://gibi-fcdb4.firebaseio.com/");
                        ref.child("user")
                                .child(uId).child("profile").child("current_coin").setValue(value)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {

                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        Log.e(TAG, task.isSuccessful() + "");
                                    }
                                });
                    }
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DataManager.getInstance(mActivity).getmScheduler())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onStart() {
                        super.onStart();
                        Log.e(TAG, "onStart");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Boolean bool) {
                    }
                }));
    }*/

    private void getData(final String uId) {
        mComposite = new CompositeSubscription();
        mComposite.add(AppObservable.bindActivity(mActivity,
                Observable.create(new Observable.OnSubscribe<Profile>() {
                    @Override
                    public void call(final Subscriber<? super Profile> subscriber) {
                        DatabaseReference ref = FirebaseDatabase.getInstance()
                                .getReferenceFromUrl(Constant.FIREBASE_URL);
                        ref.child("user").child(uId).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String json = new Gson().toJson(dataSnapshot.getValue());
                                Profile profile = new Profile();
                                try {
                                    JSONObject jsonObject = new JSONObject(json);
                                    JSONObject jsonProfile = jsonObject.getJSONObject("profile");
                                    if (jsonProfile.has("uid")) {
                                        profile.setUid(jsonProfile.getString("uid"));
                                    }
                                    if (jsonProfile.has("save_coin")) {
                                        profile.setSave_coin(jsonProfile.getInt("save_coin"));
                                    }
                                    if (jsonProfile.has("used_coin")) {
                                        profile.setUsed_coin(jsonProfile.getInt("used_coin"));
                                    }
                                    if (jsonProfile.has("gift_coin")) {
                                        profile.setGift_coin(jsonProfile.getInt("gift_coin"));
                                    }
                                    if (jsonProfile.has("current_coin")) {
                                        profile.setCurrent_coin(jsonProfile.getInt("current_coin"));
                                    }
                                    if (jsonProfile.has("fullname")) {
                                        profile.setFullname(jsonProfile.getString("fullname"));
                                    }
                                    if (jsonProfile.has("phone")) {
                                        profile.setPhone(jsonProfile.getString("phone"));
                                    }
                                    setCoins(profile);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DataManager.getInstance(mActivity).getmScheduler())
                .subscribe(new Subscriber<Profile>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Profile profileRespone) {
                        setCoins(profileRespone);
                    }
                }));
    }

    public void getMessage(final String token, final String type, final String value) {
        mProgressDialog = Utils.showLoading(mActivity);
        coinsPrensenter.getMessageGiftCoin(token, type, value);
    }

    @OnClick(R.id.txt_coins_donation)
    public void onClickCoinsDonation() {

        if(coinsPrensenter.getUserInfo().getIs_verify().equals("0")) {
            mActivity.showVerifyPhone(getContext(), "coin");
            return;
        }

        if (itemSelected != 1) {
            itemSelected = 1;
            updateTabItem(1);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_item_coins, new CoinsDonationScreen(iEnterBoxCoinsDonation))
                    .commit();
        }
    }

    @OnClick(R.id.txt_lucky_gibi)
    public void onClickLuckyGibi() {
        Utils.hideSoftKeyboard(mActivity);
        if (itemSelected != 2) {
            itemSelected = 2;
            updateTabItem(2);
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_item_coins, new LuckyGibiScreen())
                    .commit();
        }
    }

    @OnClick(R.id.txt_app_donation)
    public void onClickAppDonation() {
        mActivity.lockEventIn(1500);
        Utils.hideSoftKeyboard(mActivity);
        if (Utils.isNetworkAvailable(mActivity)) {
            itemSelected = 3;
            confirmGiftApp(getResources().getString(R.string.alert_share_app));
        } else {
            validateCoins(getResources().getString(R.string.wifi_speed));
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        switch (task.getType()) {
            case GET_MESSAGE_GIFT_COINS:
                BaseResponse baseResponse = null;
                try {
                    baseResponse = (BaseResponse) task.getResponse().body();
                    if (baseResponse.getData() != null) {
                        if (status == 200) {
                            if (mProgressDialog != null) {
                                Utils.hideLoading(mProgressDialog);
                            }
                            messageGiftCoin = (MessageGiftCoin) baseResponse.getData();
                            if (messageGiftCoin == null) {
                                messageGiftCoin = new MessageGiftCoin();
                            }
                            RxBus.getInstance().post("200");

                            share(messageGiftCoin);
                        } else {
                            if (mProgressDialog != null) {
                                Utils.hideLoading(mProgressDialog);
                            }
                            snackbar = TSnackbar
                                    .make(rootView, baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                            Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                            Utils.vibarte(mActivity);

                        }
                    } else {
                        snackbar = TSnackbar
                                .make(rootView, getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                        Utils.vibarte(mActivity);
                    }
                } catch (Exception ex) {
                    snackbar = TSnackbar
                            .make(rootView, getResources().getString(R.string.error_connect_server), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, getResources().getString(R.string.error_connect_server), mActivity);
                    Utils.vibarte(mActivity);
                    return false;
                }
                break;
        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COINS;
    }

    @Override
    public void enterBoxCoinsDonation(View view, Boolean statusKeyboard) {
    }

    @Override
    public void validateCoin(String error) {
        snackbar = TSnackbar
                .make(rootView, error, TSnackbar.LENGTH_SHORT);
        Utils.showMessageTopbar(snackbar, error, mActivity);
        Utils.vibarte(mActivity);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frame_item_coins:
                hideKeyboardDefaultOfDevices();
                break;
            case R.id.main_topbar_back:
                hideKeyboardDefaultOfDevices();
                break;
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            try {
                if (CouponsDetailScreen.mMediaPlayer != null) {
                    CouponsDetailScreen.mMediaPlayer.pause();
                    CouponsDetailScreen.mMediaPlayer.setVolume(0f, 0f);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}