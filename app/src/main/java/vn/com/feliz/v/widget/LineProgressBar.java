package vn.com.feliz.v.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.util.AttributeSet;
import android.view.View;

import vn.com.feliz.R;


/**
 * Copyright Innotech Vietnam
 * Created by Huynh Thanh Long on 6/16/2016.
 */
public class LineProgressBar extends View {

    private Drawable mBackgroundDrawable;
    private Drawable mSourceDrawable;
    private float mProgress;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can
     *                access the current theme, resources, etc.
     */
    public LineProgressBar(Context context) {
        super(context);
        sharingConstructor(context, null, 0, 0);
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file. This version uses a default style of
     * 0, so the only attribute values applied are those in the Context's Theme
     * and the given AttributeSet.
     * <p/>
     * <p/>
     * The method onFinishInflate() will be called after all children have been
     * added.
     *
     * @param context The Context the view is running in, through which it can
     *                access the current theme, resources, etc.
     * @param attrs   The attributes of the XML tag that is inflating the view.
     * @see View#View(Context, AttributeSet, int)
     */
    public LineProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        sharingConstructor(context, attrs, 0, 0);
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating. For example, a Button class's
     * constructor would call this version of the super class constructor and
     * supply <code>R.attr.buttonStyle</code> for <var>defStyleAttr</var>; this
     * allows the theme's button style to modify all of the base view attributes
     * (in particular its background) as well as the Button class's attributes.
     *
     * @param context      The Context the view is running in, through which it can
     *                     access the current theme, resources, etc.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     * @see View#View(Context, AttributeSet)
     */
    public LineProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        sharingConstructor(context, attrs, defStyleAttr, 0);
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute or style resource. This constructor of View allows
     * subclasses to use their own base style when they are inflating.
     * <p/>
     * When determining the final value of a particular attribute, there are
     * four inputs that come into play:
     * <ol>
     * <li>Any attribute values in the given AttributeSet.
     * <li>The style resource specified in the AttributeSet (named "style").
     * <li>The default style specified by <var>defStyleAttr</var>.
     * <li>The default style specified by <var>defStyleRes</var>.
     * <li>The base values in this theme.
     * </ol>
     * <p/>
     * Each of these inputs is considered in-order, with the first listed taking
     * precedence over the following ones. In other words, if in the
     * AttributeSet you have supplied <code>&lt;Button * textColor="#ff000000"&gt;</code>
     * , then the button's text will <em>always</em> be black, regardless of
     * what is specified in any of the styles.
     *
     * @param context      The Context the view is running in, through which it can
     *                     access the current theme, resources, etc.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     * @param defStyleRes  A resource identifier of a style resource that
     *                     supplies default values for the view, used only if
     *                     defStyleAttr is 0 or can not be found in the theme. Can be 0
     *                     to not look for defaults.
     * @see View#View(Context, AttributeSet, int)
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LineProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        sharingConstructor(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Sharing Constructor
     */
    private void sharingConstructor(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.LineProgressBar);

            int value = typedArray.getInt(R.styleable.LineProgressBar_screenType, 0);
            switch (value) {
                case 1: // OTP Screen
                    loadValues(context, R.drawable.time_line_white,R.drawable.time_line_color);
                    break;

                default:
                    loadValues(context, R.drawable.time_line_white, R.drawable.time_line_color);
                    break;
            }

            typedArray.recycle();
        }

        mProgress = 0.f;
    }

    /**
     * Load Default Values
     * @param context Context
     */
    private void loadValues(Context context, @DrawableRes int bg, @DrawableRes int pg) {
        if (mBackgroundDrawable == null) {
            mBackgroundDrawable = ActivityCompat.getDrawable(context, bg);
            mBackgroundDrawable.setBounds(0, 0,
                    mBackgroundDrawable.getIntrinsicWidth(),
                    mBackgroundDrawable.getIntrinsicHeight());
        }

        if (mSourceDrawable == null) {
            mSourceDrawable = ActivityCompat.getDrawable(context, pg);
            mSourceDrawable.setBounds(0, 0,
                    mSourceDrawable.getIntrinsicWidth(),
                    mSourceDrawable.getIntrinsicHeight());
        }
    }

    /**
     * Set Current Progress
     * @param progress float
     */
    public void setProgress(float progress) {
        mProgress = progress / 100;
        invalidate();
    }

    /**
     * Draw Background
     * @param canvas Canvas
     */
    private void drawBackground(Canvas canvas) {
        int saveCount = canvas.getSaveCount();
        canvas.save();

        float sx = canvas.getWidth() / (float) mBackgroundDrawable.getIntrinsicWidth();
        float sy = canvas.getHeight() / (float) mBackgroundDrawable.getIntrinsicHeight();
        canvas.scale(sx, sy);

        mBackgroundDrawable.draw(canvas);

        canvas.restoreToCount(saveCount);
    }

    /**
     * Draw Progress
     * @param canvas Canvas
     */
    private void drawProgress(Canvas canvas) {
        int saveCount = canvas.getSaveCount();
        canvas.save();

        float width = canvas.getWidth() * mProgress;

        if (width > 0) {
            canvas.clipRect(0, 0, width, canvas.getHeight());

            float sx = width / (float) mSourceDrawable.getIntrinsicWidth();
            float sy = canvas.getHeight() / (float) mSourceDrawable.getIntrinsicHeight();
            canvas.scale(sx, sy);

            mSourceDrawable.draw(canvas);
        }

        canvas.restoreToCount(saveCount);
    }

    /**
     * Implement this to do your drawing.
     *
     * @param canvas the canvas on which the background will be drawn
     */
    @Override
    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawProgress(canvas);
    }
}
