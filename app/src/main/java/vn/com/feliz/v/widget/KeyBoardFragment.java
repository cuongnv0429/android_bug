package vn.com.feliz.v.widget;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import org.greenrobot.eventbus.EventBus;

import vn.com.feliz.p.eventbus.DeleteDigit;
import vn.com.feliz.p.eventbus.EnterEvent;
import vn.com.feliz.p.eventbus.DigitRegister;
import vn.com.feliz.R;

/**
 * Created by Nguyen Thai Son on 2016-11-13.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class KeyBoardFragment extends android.support.v4.app.Fragment {

    private ImageButton one_btn;
    private ImageButton two_btn;
    private ImageButton three_btn;
    private ImageButton four_btn;
    private ImageButton five_btn;
    private ImageButton six_btn;
    private ImageButton seven_btn;
    private ImageButton eight_btn;
    private ImageButton nine_btn;
    private ImageButton zero_btn;
    private ImageButton back_btn;
    private ImageButton done_btn;

    public static StringBuilder sb;

    private onKeyBoardEvent keyboardEventListener;



    private int currentLength;

    public static KeyBoardFragment newInstance(String EditTextValue) {
        KeyBoardFragment fragment = new KeyBoardFragment();
        Bundle bundle = new Bundle();
        bundle.putString("et_value", EditTextValue);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        try {

            keyboardEventListener = (onKeyBoardEvent) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        sb = new StringBuilder(getArguments().getString("et_value"));
        currentLength = sb.length();
        View rootView = inflater.inflate(R.layout.keyboard, container, false);

        one_btn = (ImageButton) rootView.findViewById(R.id.one_btn);
        one_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                EventBus.getDefault().post(new DigitRegister("1"));
            }
        });
        two_btn = (ImageButton) rootView.findViewById(R.id.two_btn);
        two_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("2"));
            }
        });
        three_btn = (ImageButton) rootView.findViewById(R.id.three_btn);
        three_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("3"));

            }
        });
        four_btn = (ImageButton) rootView.findViewById(R.id.four_btn);
        four_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("4"));
            }
        });
        five_btn = (ImageButton) rootView.findViewById(R.id.five_btn);
        five_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("5"));

            }
        });
        six_btn = (ImageButton) rootView.findViewById(R.id.six_btn);
        six_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                EventBus.getDefault().post(new DigitRegister("6"));
            }
        });
        seven_btn = (ImageButton) rootView.findViewById(R.id.seven_btn);
        seven_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("7"));
            }
        });
        eight_btn = (ImageButton) rootView.findViewById(R.id.eight_btn);
        eight_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("8"));

            }
        });
        nine_btn = (ImageButton) rootView.findViewById(R.id.nine_btn);
        nine_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                EventBus.getDefault().post(new DigitRegister("9"));
            }
        });
        zero_btn = (ImageButton) rootView.findViewById(R.id.zero_btn);
        zero_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (sb.length() >= 0)
                    EventBus.getDefault().post(new DigitRegister("0"));
            }
        });
        back_btn = (ImageButton) rootView.findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (sb.length() > 0) {
                   /* currentLength--;
                    sb.deleteCharAt((sb.length()) - 1);
                    keyboardEventListener.backButtonPressed(sb.toString());*/
                    EventBus.getDefault().post(new DeleteDigit());

                }
            }
        });
        back_btn.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {

                currentLength = 0;
                sb = new StringBuilder();
                keyboardEventListener.backLongPressed();
                return false;
            }
        });
        done_btn = (ImageButton) rootView.findViewById(R.id.done);
        done_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //
                //  keyboardEventListener.doneButtonPressed(sb.toString());
                EventBus.getDefault().post(new EnterEvent());
            }
        });
        return rootView;
    }

    public interface onKeyBoardEvent {
        public void numberIsPressed(String total);

        public void doneButtonPressed(String total);

        public void backLongPressed();

        public void backButtonPressed(String total);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

    }
/*   public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }*/

  /*  public void add(String num) {
        currentLength++;
        if (currentLength <= maxLength) {

            sb.append(num);
            keyboardEventListener.numberIsPressed(sb.toString());
        } else
            currentLength--;
    }*/
}