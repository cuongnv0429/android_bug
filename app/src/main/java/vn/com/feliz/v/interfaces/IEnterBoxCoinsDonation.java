package vn.com.feliz.v.interfaces;

import android.view.View;

/**
 * Created by ITV01 on 12/25/16.
 */

public interface IEnterBoxCoinsDonation {
    void enterBoxCoinsDonation(View view, Boolean statusKeyboard);
    void validateCoin(String error);
}
