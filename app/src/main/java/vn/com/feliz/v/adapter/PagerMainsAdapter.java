package vn.com.feliz.v.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import vn.com.feliz.v.fragment.ReviewsScreen;
import vn.com.feliz.v.fragment.ShoppingScreen;
import vn.com.feliz.v.fragment.RootFragmentCoupons;
import vn.com.feliz.v.fragment.SettingScreen;
import vn.com.feliz.v.fragment.CoinsScreen;

/**
 * Created by Nguyen Thai Son on 2016-12-16.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class PagerMainsAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs;

    public PagerMainsAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                RootFragmentCoupons tab1 = new RootFragmentCoupons();
                return tab1;
            case 1:
                CoinsScreen tab2 = new CoinsScreen();
                return tab2;
            case 2:
                ReviewsScreen tab3 = new ReviewsScreen();
                return tab3;
            case 3:
                ShoppingScreen tab4= new ShoppingScreen();
                return tab4;
            case 4:
                SettingScreen tab5 = new SettingScreen();
                return tab5;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            Log.e("PagerMainsAdapter: ", "Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}