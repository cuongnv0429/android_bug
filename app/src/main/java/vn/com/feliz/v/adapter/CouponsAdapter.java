package vn.com.feliz.v.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.R;
import vn.com.feliz.databinding.RvItemCouponBinding;

public class CouponsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<CouponModel.Coupon> mData;
    private int mSelectedPos = -1;
    private OnItemRecyclerListener onItemListener;
    private int mBlackColor;
    private int mGrayColor;

    public static class ViewHolderItem extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * callback when click on button of each item
         */
        private IViewHolderMovie mCallback;
        private RvItemCouponBinding mBinding;

        public ViewHolderItem(RvItemCouponBinding binding, IViewHolderMovie callback) {
            super(binding.getRoot());
            mCallback = callback;
            mBinding = binding;
            binding.rlCouponItemRoot.setOnClickListener(this);
            binding.ivRating.setOnClickListener(this);
        }

        public RvItemCouponBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View v) {
            if (getAdapterPosition() >= 0 ) {
                mCallback.OnClicked(v, ViewHolderItem.this, getAdapterPosition());
            }
        }

        public interface IViewHolderMovie {
            void OnClicked(View v, ViewHolderItem viewHolder, int position);
        }
    }

    public static class ViewHolderLoading extends RecyclerView.ViewHolder{
        public ViewHolderLoading(View itemView) {
            super(itemView);
        }
    }

    public CouponsAdapter(Context context, List<CouponModel.Coupon> data, OnItemRecyclerListener listener) {
        //Picasso.with(context).setIndicatorsEnabled(true);
        mContext = context;
        mData = data;
        if (mData == null) {
            mData = new ArrayList<>();
        }
        onItemListener = listener;

        mBlackColor = ContextCompat.getColor(mContext, android.R.color.black);
        mGrayColor = ContextCompat.getColor(mContext, R.color.couponscreen_text_gray);

        //MainActivity.couponList = mData;
    }

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        if (viewType == 0) {
            RvItemCouponBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.rv_item_coupon, parent, false);
            final ViewHolderItem viewHolder = new ViewHolderItem(binding, new ViewHolderItem.IViewHolderMovie() {
                @Override
                public void OnClicked(View v, final ViewHolderItem vHolder, int position) {
                    if (position >= 0) {
                        onItemListener.onRecyclerViewItemClicked(v, position);
                    }

                }
            });
            return viewHolder;
        }else {
            return new ViewHolderLoading(LayoutInflater.from(parent.getContext()).inflate(R.layout.rv_item_loading, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ViewHolderLoading){
            return;
        }
        final ViewHolderItem holder = (ViewHolderItem) viewHolder;
        holder.getBinding().tvCouponLabel.setText(mData.get(position).getCouponName());
        holder.getBinding().tvCouponName.setText(mData.get(position).getCouponSizeDes());
        holder.getBinding().tvValue.setText(mData.get(position).getCouponTypeDes());
        holder.getBinding().tvExpiredDate.setText(mData.get(position).getCouponExpired());
        holder.getBinding().rbRating.setRating(mData.get(position).getCouponRating());
        holder.getBinding().tvRatingNumber.setText("(" + mData.get(position).getCouponReview() + ")");
        Picasso.with(mContext).load(Uri.parse(mData.get(position).getMediaUrl())).resizeDimen(R.dimen.dp96, R.dimen.dp96).into(holder.getBinding().ivAvatar);

        boolean isCouponStatus = true;
        if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
            isCouponStatus = checkCouponStatus(mData.get(position));
        }
        switch (mData.get(position).getStatus()) {
            case Constant.COUPON_STATUS_NEW://new
                /*holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_organe);*/
                if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
                    holder.getBinding().flAction.setVisibility(View.VISIBLE);
                    holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                    holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                    holder.getBinding().llPrice.setVisibility(View.INVISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setText(""+mData.get(position).getList().size());
                    holder.getBinding().ivStatus.setVisibility(View.VISIBLE);
                    if (isCouponStatus){
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data);
                    }else {
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data_g);
                    }
                }else {
                    holder.getBinding().flAction.setVisibility(View.INVISIBLE);
                }
                //color of text--> black
                holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                holder.getBinding().tvValue.setTextColor(mBlackColor);
                holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);
                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                if (position < mData.size()-1){
                    if (mData.get(position + 1).getStatus() != null) {
                        if (!mData.get(position+1).getStatus().equalsIgnoreCase(Constant.COUPON_STATUS_NEW)) {
                            holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_organe);
                        }
                    }
                }
                break;
            case Constant.COUPON_STATUS_NOT_EXCHANGE_YET://not exchange yet
                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
                    holder.getBinding().flAction.setVisibility(View.VISIBLE);
                    holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                    holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                    holder.getBinding().llPrice.setVisibility(View.INVISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setText(""+mData.get(position).getList().size());
                    holder.getBinding().ivStatus.setVisibility(View.VISIBLE);
                    if (isCouponStatus){
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data);
                    }else {
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data_g);
                    }
                }else {
                    holder.getBinding().flAction.setVisibility(View.INVISIBLE);
                }
                //color of text--> black
                holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                holder.getBinding().tvValue.setTextColor(mBlackColor);
                holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);
                break;
            case Constant.COUPON_STATUS_EXPIRED://expired
                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                holder.getBinding().flAction.setVisibility(View.VISIBLE);
                holder.getBinding().llPrice.setVisibility(View.INVISIBLE);
                holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
                    holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setText(""+mData.get(position).getList().size());
                    holder.getBinding().ivStatus.setVisibility(View.VISIBLE);
                    if (isCouponStatus){
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data);
                    }else {
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data_g);
                    }
                }else {
                    holder.getBinding().ivClose.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.INVISIBLE);
                    holder.getBinding().ivStatus.setVisibility(View.INVISIBLE);
                }
                //color of text--> gray
                if (!isCouponStatus) {
                    holder.getBinding().tvCouponLabel.setTextColor(mGrayColor);
                    holder.getBinding().tvValue.setTextColor(mGrayColor);
                    holder.getBinding().tvExpiredDate.setTextColor(mGrayColor);
                    holder.getBinding().tvRatingNumber.setTextColor(mGrayColor);
                } else {
                    holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                    holder.getBinding().tvValue.setTextColor(mBlackColor);
                    holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                    holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);
                }
                break;
            case Constant.COUPON_STATUS_EXCHANGED://exchanged
                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                holder.getBinding().flAction.setVisibility(View.VISIBLE);
                holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                holder.getBinding().llPrice.setVisibility(View.INVISIBLE);
                if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
                    holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setText(""+mData.get(position).getList().size());
                    holder.getBinding().ivStatus.setVisibility(View.VISIBLE);
                    if (isCouponStatus){
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data);
                    }else {
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data_g);
                    }
                }else {
                    isCouponStatus = false;
                    holder.getBinding().ivExchange.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.INVISIBLE);
                    holder.getBinding().ivStatus.setVisibility(View.INVISIBLE);
                }
                //color of text--> gray
                if (!isCouponStatus) {
                    holder.getBinding().tvCouponLabel.setTextColor(mGrayColor);
                    holder.getBinding().tvValue.setTextColor(mGrayColor);
                    holder.getBinding().tvExpiredDate.setTextColor(mGrayColor);
                    holder.getBinding().tvRatingNumber.setTextColor(mGrayColor);
                } else {
                    holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                    holder.getBinding().tvValue.setTextColor(mBlackColor);
                    holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                    holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);
                }
                break;
            case Constant.COUPON_STATUS_CAN_BUY://can buy EC

                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                if (mData.get(position).getList() != null && mData.get(position).getList().size() > 1) {
                    holder.getBinding().flAction.setVisibility(View.VISIBLE);
                    holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                    holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                    holder.getBinding().llPrice.setVisibility(View.INVISIBLE);
                    //holder.getBinding().tvUnitNumber.setVisibility(View.VISIBLE);
                    //holder.getBinding().tvUnitNumber.setText(""+mData.get(position).getList().size());
                    holder.getBinding().ivStatus.setVisibility(View.VISIBLE);
                    if (isCouponStatus){
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data);
                    }else {
                        holder.getBinding().ivStatus.setImageResource(R.drawable.data_g);
                    }
                }else {
                    holder.getBinding().flAction.setVisibility(View.INVISIBLE);
                }
                //color of text--> black
                holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                holder.getBinding().tvValue.setTextColor(mBlackColor);
                holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);

                break;
            default://can buy by gb$
                holder.getBinding().vLineOrange.setBackgroundResource(R.color.couponscreen_line_gray);
                holder.getBinding().flAction.setVisibility(View.VISIBLE);
                holder.getBinding().ivClose.setVisibility(View.INVISIBLE);
                holder.getBinding().ivExchange.setVisibility(View.INVISIBLE);
                //holder.getBinding().tvUnitNumber.setVisibility(View.INVISIBLE);
                holder.getBinding().ivStatus.setVisibility(View.INVISIBLE);
                holder.getBinding().llPrice.setVisibility(View.VISIBLE);
                holder.getBinding().tvPrice.setVisibility(View.GONE);

                //color of text--> black
                holder.getBinding().tvCouponLabel.setTextColor(mBlackColor);
                holder.getBinding().tvValue.setTextColor(mBlackColor);
                holder.getBinding().tvExpiredDate.setTextColor(mBlackColor);
                holder.getBinding().tvRatingNumber.setTextColor(mBlackColor);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setmData(List<CouponModel.Coupon> coupons) {
        mData = coupons;
    }

    //default have children coupon and return false if have exchange or expired child coupon
    private boolean checkCouponStatus(CouponModel.Coupon coupon){
        for (CouponModel.Coupon c : coupon.getList()){
            if (Constant.COUPON_STATUS_NEW.equals(c.getStatus()) || Constant.COUPON_STATUS_NOT_EXCHANGE_YET.equals(c.getStatus())
                    || Constant.COUPON_STATUS_CAN_BUY.equals(c.getStatus())){
                return true;
            }
        }
        return false;
    }

    public void addCoupon(CouponModel.Coupon coupon){
        if (mData == null) mData = new ArrayList<>();
        mData.add(coupon);
        //MainActivity.couponList = mData;
        notifyItemInserted(mData.size());
    }

    public void addCoupon(List<CouponModel.Coupon> coupons){
        if (mData == null) mData = new ArrayList<>();
        mData.addAll(coupons);
        //MainActivity.couponList = mData;
        notifyItemRangeInserted(mData.size()-coupons.size(), coupons.size());
    }

    public void removeLoadingCoupon(){//simulate a coupon for loading view
        if (mData!=null && mData.size() > 0){
            if (mData.get(mData.size()-1).getViewType()==1){//loading
                mData.remove(mData.size()-1);
                notifyItemRemoved(mData.size()-1);
            }
            //MainActivity.couponList = mData;
        }
    }

    public void clearAdapter(){
        Log.d("CLEAN ADAPTER", "CC");
        if (mData != null) {
            mData.clear();
            //MainActivity.couponList = mData;
            notifyDataSetChanged();
        }
    }

    public List<CouponModel.Coupon> getData() {
        return mData;
    }

    /**
     * get item by id
     */

    public CouponModel.Coupon getCouponById(String coupon_id) {
        for(int i = 0; i < mData.size() ; i++) {
            if (mData.get(i).getCouponId().equals(coupon_id)) {
                return mData.get(i);
            }
        }
        //MainActivity.couponList = mData;
        return null;
    }

    /**
     * Edit item
     */
    public void update(CouponModel.Coupon coupon) {
        for(int i = 0; i < mData.size() ; i++) {
            if (mData.get(i).getCouponId().equals(coupon.getCouponId())) {
                mData.set(i, coupon);
                //MainActivity.couponList = mData;
                break;
            }
        }
    }

    /**
     * Update viewed for items
     */
    public void updateViewed() {
        if(mData != null) {
            for (int i = 0; i < mData.size(); i++) {
                if (mData.get(i) != null && mData.get(i).getStatus()!=null && mData.get(i).getStatus().equals(Constant.COUPON_STATUS_NEW)) {
                    mData.get(i).setStatus(Constant.COUPON_STATUS_NOT_EXCHANGE_YET);
                }
            }
        }
        //MainActivity.couponList = mData;
    }

    public void updateViewedML() {
        if(MainActivity.couponList != null) {
            for (int i = 0; i < MainActivity.couponList.size(); i++) {
                if (MainActivity.couponList.get(i) != null && MainActivity.couponList.get(i).getStatus()!=null && MainActivity.couponList.get(i).getStatus().equals(Constant.COUPON_STATUS_NEW)) {
                    MainActivity.couponList.get(i).setStatus(Constant.COUPON_STATUS_NOT_EXCHANGE_YET);
                }
            }
        }
        //MainActivity.couponList = mData;
    }

    /**
     * Update viewed for items
     */
    public void updateViewedSub(String coupon_id) {
        if(MainActivity.couponList != null) {
            for (int i = 0; i < MainActivity.couponList.size(); i++) {
                if (MainActivity.couponList.get(i) != null) {
                    if(MainActivity.couponList.get(i).getCouponId() != null) {
                        if (MainActivity.couponList.get(i).getCouponId().equals(coupon_id)) {
                            List<CouponModel.Coupon> subcoupons = MainActivity.couponList.get(i).getList();
                            if (subcoupons != null) {
                                for (int j = 0; j < subcoupons.size(); j++) {
                                    if (subcoupons.get(j).getStatus().equals(Constant.COUPON_STATUS_NEW)) {
                                        subcoupons.get(j).setStatus(Constant.COUPON_STATUS_NOT_EXCHANGE_YET);
                                        mData.get(j).setStatus(Constant.COUPON_STATUS_NOT_EXCHANGE_YET);
                                    }
                                }
                                MainActivity.couponList.get(i).setList(subcoupons);
                            }
                        }
                    }
                }
            }
        }
        //MainActivity.couponList = mData;
    }

    /**
     * Update viewed for items
     */
    public void updateExchanged(String coupon_id, String coupon_sub_id) {

        for(int i = 0; i < mData.size() ; i++) {
            if (mData.get(i).getCouponId().equals(coupon_id)) {
                if(coupon_sub_id != null && coupon_sub_id != "") {
                    if (mData.get(i).getList() != null && mData.get(i).getList().size() > 0) {
                        for (int j = 0; j < mData.get(i).getList().size(); j++) {
                            if (mData.get(i).getList().get(i).getCouponId().equals(coupon_sub_id)) {
                                mData.get(i).getList().get(i).setStatus(Constant.COUPON_STATUS_EXCHANGED);
                            }
                        }
                    }
                } else {
                    mData.get(i).setStatus(Constant.COUPON_STATUS_EXCHANGED);
                }
            }
        }
    }

    /**
     * interface to fire event of item
     */
    public interface OnItemRecyclerListener {
        void onRecyclerViewItemClicked(View v, int pos);
    }
}
