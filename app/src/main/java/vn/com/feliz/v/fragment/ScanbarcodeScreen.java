package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.v.widget.CameraPreview;
import vn.com.feliz.v.widget.PhoneEditText;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("deprecation")
public class ScanbarcodeScreen extends BaseFragment implements OnPostResponseListener, OnMenuItemSelected {
    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;
    private ImageScanner scanner;
    @BindView(R.id.cameraPreview)
    FrameLayout mCameraPreview;
    private boolean barcodeScanned = false;
    private boolean previewing = true;
    private String mBarCode;
    private String mBarCodeCoupon;
    public static String mIdCoupons;
    public static int mIdCouponsTotal;
    public static String mIdCouponsSubid;
    private String mToken_user;
    private CouponsPresenter mPresenter;
    private TSnackbar snackbar;
    @BindView(R.id.scan_barcode_root)
    RelativeLayout mRootView;
    private ProgressDialog mProgressDialog;
    @BindView(R.id.main_topbar)
    RelativeLayout mTopbar;
    @BindView(R.id.main_topbar_back)
    ImageView mBack;
    @BindView(R.id.tv_topbar_left)
    TextView mTitleBarLeft;
    @BindView(R.id.main_top_bar_scaner_img)
    ImageView mTitleBarScanerImgRight;
    @BindView(R.id.camera_loading)
    RelativeLayout mCameraLoading;
    @BindView(R.id.camera_loading_spinner)
    ProgressBar mCameraLoadingSpinner;
    private boolean mFromRegister;

    private Handler mHandler;
    private Handler mBarcodePopup;
    private Runnable mBarcodeRun;
    private Dialog mPopupBarcodeDialog;
    private int mWrongBarcode = 0;
    private boolean mIsInputBarcode = false;
    private boolean eventMode = false;
    private int scanTimeout = 20000; // Default 20s

    static {
        try {

            System.loadLibrary("iconv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ScanbarcodeScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new CouponsPresenter(mActivity, this);
        // Set Title
        //EventBus.getDefault().post(new SetTitleMessage(getString(R.string.reviews_Screen)));
        EventBus.getDefault().post(this);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mIdCoupons = bundle.getString(Constant.F_ARG_COUPON_ID);
            mIdCouponsTotal = bundle.getInt(Constant.F_ARG_COUPON_TOTAL);
            mIdCouponsSubid = bundle.getString(Constant.F_ARG_SUB_COUPON_ID);
            mBarCodeCoupon = bundle.getString(Constant.F_ARG_COUPON_BARCODE);
            mFromRegister = bundle.getBoolean("fromRegister", false);
        }
        if (mPresenter.hasUserInfo()) {
            mToken_user = mPresenter.getUserInfo().getToken();
        }
        if(mPresenter.hasMetaInfo() != null) {
            if(mPresenter.hasMetaInfo().getEvent_mode().equals("1")) {
                eventMode = true;
            }

            scanTimeout = Integer.parseInt(mPresenter.hasMetaInfo().getScan_timeout());
            if(scanTimeout <= 0) {
                scanTimeout = 20000; // default 20s
            }
        }
        // Register Events
        /*if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        if(mPopupBarcodeDialog != null) {
            cancelWaitPopupBarcode();
//            mPopupBarcodeDialog.dismiss();
        }
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = null;
        try {
            rootView = inflater.inflate(R.layout.fragment_scan_barcode, container, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        mActivity.mBottom().setVisibility(View.GONE);
        mTopbar.setBackgroundResource(R.color.topbar_scan_bar);
        mBack.setImageDrawable(getResources().getDrawable(R.drawable.btn_back_white_2x));
        mTitleBarLeft.setVisibility(View.VISIBLE);
        mTitleBarLeft.setText(R.string.topbar_scanbar_code);
        mTitleBarScanerImgRight.setVisibility(View.VISIBLE);
        initControls();

        mActivity.lockEventIn(2500);

        mHandler = new Handler();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mCameraLoading != null) {
                    mCameraLoading.setVisibility(View.GONE);
                }

                mCamera = getCameraInstance();

                // Instance barcode scanner
                scanner = new ImageScanner();
                scanner.setConfig(0, Config.X_DENSITY, 3);
                scanner.setConfig(0, Config.Y_DENSITY, 3);
                mPreview = new CameraPreview(mActivity, mCamera, previewCb,
                        autoFocusCB);
                if (mPreview != null) {
                    mCameraPreview.addView(mPreview);
                }
                if(mCamera != null) {
                    mCamera.setPreviewCallback(previewCb);
//                mCamera.autoFocus(autoFocusCB);
                    mCamera.startPreview();
                }
                previewing = true;
            }
        }, 320);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mCameraLoadingSpinner != null) {
                    mCameraLoadingSpinner.setVisibility(View.GONE);
                }
            }
        }, 300);

        startWaitPopupBarcode();

        return rootView;
    }

    private void cancelWaitPopupBarcode() {
        if(mBarcodePopup != null && mBarcodeRun != null) {
            mBarcodePopup.removeCallbacks(mBarcodeRun);
        }
    }

    private void startWaitPopupBarcode() {
        cancelWaitPopupBarcode();

        if(eventMode) {
            mBarcodePopup = new Handler();
            mBarcodeRun = new Runnable() {
                @Override
                public void run() {
                    showInputBarcode();
                }
            };
            mBarcodePopup.postDelayed(mBarcodeRun, scanTimeout);
        }
    }

    private boolean checkWrongBarcode(boolean type) {
        if(!type && eventMode) {
            mWrongBarcode++;
            if (mWrongBarcode < 3) {
                return true;
            } else {
                mWrongBarcode = 0;
                cancelWaitPopupBarcode();
                showInputBarcode();
                return false;
            }
        }
        return true;
    }

    /**
     * setup Widget
     */
    private void initControls() {
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        try {

            autoFocusHandler = new Handler();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * previewCb
     */
    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            try {

                Camera.Parameters parameters = camera.getParameters();
                Camera.Size size = parameters.getPreviewSize();

                Image barcode = new Image(size.width, size.height, "Y800");
                barcode.setData(data);

                int result = scanner.scanImage(barcode);

                if (result != 0) {
                    previewing = false;
                    if(mCamera != null) {
                        mCamera.setPreviewCallback(null);
                        mCamera.stopPreview();
                    }

                    SymbolSet syms = scanner.getResults();
                    for (Symbol sym : syms) {
                        String scanResult = sym.getData().trim();
                        mBarCode = scanResult;
                        if (!mBarCode.isEmpty()) {
                            cancelWaitPopupBarcode();
                            if(mBarCodeCoupon != null) {
                                checkLocalBarcode(mBarCode, false);
                            } else {
                                if(mActivity.isConnected(getContext())) {
                                    mProgressDialog = Utils.showLoading(mActivity);
                                    mPresenter.checkBarcode(mToken_user, mBarCode, mIdCoupons);
                                } else {
                                    mActivity.showNetworkAlert(getView());
                                }
                            }
                            barcodeScanned = true;
                        } else {
                            barcodeScanned = false;
                            if(mCamera != null) {
                                mCamera.setPreviewCallback(previewCb);
                                mCamera.startPreview();
                                mCamera.autoFocus(autoFocusCB);
                            }
                            previewing = true;
                        }


                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    PhoneEditText barcodeInput;
    private void showInputBarcode() {
        previewing = false;
        try {
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
        } catch (Exception e) {

        }

        cancelWaitPopupBarcode();

        try {
            LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View dialogView = inflater.inflate(R.layout.custom_dialog_verify_barcode, null);

            int maxLength = 4;
            barcodeInput = (PhoneEditText) dialogView.findViewById(R.id.verify_barcode_number);
            barcodeInput.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

            mPopupBarcodeDialog = mActivity.dialogConfirm(dialogView, getString(R.string.dialog_confirm_text), getString(R.string.dialog_retry_exit), new MainActivity.DialogCallback() {
                @Override
                public void goit(Dialog dialog) {
                    String code = barcodeInput.getText().toString().trim();
                    checkLocalBarcode(code, true);
                }

                @Override
                public void cancel(Dialog dialog) {
                    dialog.dismiss();
                    barcodeScanned = false;
                    try {
                        mCamera.setPreviewCallback(previewCb);
                        mCamera.startPreview();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    previewing = true;
                    startWaitPopupBarcode();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 3000);
        }
    };

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing && mCamera != null)
                mCamera.autoFocus(autoFocusCB);
        }
    };



    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            Utils.hideLoading(mProgressDialog);
            releaseCamera();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        if(mCamera != null) {
            barcodeScanned = true;
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       /* try {

            if(mCamera ==null){

                mCamera = getCameraInstance();
                mPreview = new CameraPreview(mActivity, mCamera, previewCb,
                        autoFocusCB);
                mCameraPreview.addView(mPreview);
                mCamera.setPreviewCallback(previewCb);
                mCamera.startPreview();
                previewing = true;
                mCamera.autoFocus(autoFocusCB);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @OnClick(R.id.camera_scan_bgr)
    public void continueScan() {
        if (barcodeScanned) {
            try {
                barcodeScanned = false;
                mCamera.setPreviewCallback(previewCb);
                mCamera.startPreview();
                previewing = true;
                startWaitPopupBarcode();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            mCamera.autoFocus(autoFocusCB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.main_topbar_back)
    public void backEventScanbar() {
       /* CouponsDetailScreen fr = CouponsDetailScreen.newInstance(mIdCoupons, mIdCouponsSubid, true);
        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.root_frame, fr, "coupon_detail")
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .commitAllowingStateLoss();
        getChildFragmentManager().executePendingTransactions();*/

        mActivity.getSupportFragmentManager().popBackStack();
        mActivity.lockEventIn(2000);
    }

    private void checkLocalBarcode(String barcode, boolean last) {

        if(barcode.length() < 4) {
            return;
        }

        String tmp_code = "";

        if(last) {
            int start = mBarCodeCoupon.length() - barcode.length();
            tmp_code = mBarCodeCoupon.substring(start, mBarCodeCoupon.length());
        }

        if(mBarCodeCoupon.equals(barcode) || (last && tmp_code.equals(barcode))) {
            if(mPopupBarcodeDialog != null) {
                mPopupBarcodeDialog.dismiss();
            }
            //reset barcode
            InputCodeStoreScreen fragment = new InputCodeStoreScreen();
            Bundle arguments = new Bundle();
            arguments.putString(Constant.F_ARG_COUPON_ID, mIdCoupons);
            arguments.putString(Constant.F_ARG_COUPON_BARCODE, mBarCodeCoupon);
            arguments.putInt(Constant.F_ARG_COUPON_TOTAL, mIdCouponsTotal);
            arguments.putString(Constant.F_ARG_SUB_COUPON_ID, mIdCouponsSubid);
            arguments.putBoolean("fromRegister", mFromRegister);
            fragment.setArguments(arguments);
            mActivity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_frame, fragment, "fragment_input_code")
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
            getChildFragmentManager().executePendingTransactions();
            mBarCode = "";
        } else {
            String err = "Mã vạch không đúng!";
            Utils.vibarte(mActivity);
            if (barcodeInput != null) {
                barcodeInput.setBackgroundResource(R.drawable.bg_error_enter_coin);
            }

            if(checkWrongBarcode(last)) {

                mIsInputBarcode = last;

                //customdialog
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View dialogView = inflater.inflate(R.layout.custom_dialog, null);
                builder.setView(dialogView);
                LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
                TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                tvYes.setText(getString(R.string.dialog_retry_rescan));
                LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
                TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                tvNo.setText(getString(R.string.dialog_retry_exit));
                TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
                contentDialog.setText(err);
                final Dialog dialog = builder.create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.show();
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
//                        if (barcodeScanned && !mIsInputBarcode) {
//                            try {
//                                barcodeScanned = false;
//                                mCamera.setPreviewCallback(previewCb);
//                                mCamera.startPreview();
//                                previewing = true;
//                                mCamera.autoFocus(autoFocusCB);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        startWaitPopupBarcode();
                    }
                });
                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        if(mPopupBarcodeDialog != null) {
                            cancelWaitPopupBarcode();
                            mPopupBarcodeDialog.dismiss();
                        }
                        mActivity.getSupportFragmentManager().popBackStack();
                    }
                });
            }
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mActivity.showNetworkAlert(getView());
        } else {

            if (task.getType() == ApiTaskType.CHECK_BARCODE) {
                if (status == ApiResponseCode.SUCCESS) {

                    //reset barcode
                    InputCodeStoreScreen fragment = new InputCodeStoreScreen();
                    Bundle arguments = new Bundle();
                    arguments.putString(Constant.F_ARG_COUPON_ID, mIdCoupons);
                    arguments.putString(Constant.F_ARG_COUPON_BARCODE, mBarCode);
                    arguments.putInt(Constant.F_ARG_COUPON_TOTAL, mIdCouponsTotal);
                    arguments.putString(Constant.F_ARG_SUB_COUPON_ID, mIdCouponsSubid);
                    arguments.putBoolean("fromRegister", mFromRegister);
                    fragment.setArguments(arguments);
                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, fragment, "fragment_input_code")
                            .addToBackStack(null)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                    mBarCode = "";
                } else {
                    if (checkWrongBarcode(false)) {
                        String err = mPresenter.messageErr();
                        Utils.vibarte(mActivity);
                        //customdialog
                        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
                        builder.setView(dialogView);
                        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
                        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                        tvYes.setText(getString(R.string.dialog_retry_rescan));
                        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
                        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                        tvNo.setText(getString(R.string.dialog_retry_exit));
                        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
                        contentDialog.setText(err);
                        final Dialog dialog = builder.create();
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setCancelable(false);
                        dialog.show();
                        btnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                if (barcodeScanned) {
                                    try {
                                        barcodeScanned = false;
                                        mCamera.setPreviewCallback(previewCb);
                                        mCamera.startPreview();
                                        previewing = true;
                                        mCamera.autoFocus(autoFocusCB);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                startWaitPopupBarcode();
                            }
                        });
                        btnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                                mActivity.getSupportFragmentManager().popBackStack();
                            }
                        });
                    }
                }
            }
        }

        return true;

    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }


    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COUPONS;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        releaseCamera();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
