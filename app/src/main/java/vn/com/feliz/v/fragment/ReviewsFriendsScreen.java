package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.RefreshListFriend;
import vn.com.feliz.p.eventbus.ReloadtListUser;
import vn.com.feliz.p.eventbus.SearchReviewFriend;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_Friends_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;

import static vn.com.feliz.v.fragment.ReviewsScreen.imgBackMain;
import static vn.com.feliz.v.fragment.ReviewsScreen.tvTotalFriend;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsFriendsScreen extends BaseFragment implements OnPostResponseListener, OnLoadMoreListener, OnItemClickListener {
    private ReviewsPrensenter mPrensenter;

    private boolean mIsLoading = false;
    private int visibleThreshold = 6;
    private OnLoadMoreListener mOnLoadMoreListener;
    private List<ReviewAllItem> mReviewFriendsItems = new ArrayList<>();
    private List<ReviewAllItem> mReviewFriendsItemsSum = new ArrayList<>();
    private Reviews_Friends_Adapter mReviewsFriendsAdapter;
    private ListAllReviewsResponse mReviewsResponse;

    public static RecyclerView mRecyclerViewN;
    private String mTokenUser;
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    public static int totalFriend;
    private String keyWordSearch;
    private boolean isSearch;
    @BindView(R.id.tvNodataFriend)
    TextView mTvNoDataFriend;
    @BindView(R.id.swipe_review_friends)
    SwipeRefreshLayout mSwipeReviewFriend;
    @BindView(R.id.btn_retry_friends)
    Button mBtnRetryFriends;
    private ProgressDialog mProgressDialog;

    static final String ARG_PARAM1 = "param1";
    private boolean mFistLoad = false;

    public ReviewsFriendsScreen() {
        // Required empty public constructor
    }

    public static ReviewsFriendsScreen newInstance(boolean first_load) {
        ReviewsFriendsScreen fragment = new ReviewsFriendsScreen();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, first_load);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrensenter = new ReviewsPrensenter(mActivity, this);
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
        }

        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (getArguments() != null) {
            mFistLoad = getArguments().getBoolean(ARG_PARAM1);
        }

        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reviews_friends, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        mRecyclerViewN = (RecyclerView) rootView.findViewById(R.id.recycleReviewsFriends);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewN.setLayoutManager(mLayoutManager);
        mRecyclerViewN.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        setOnLoadMoreListener(this);
//        if(!mFistLoad) {
            mPageOffset = 0;
            setData(mPageOffset, mPageLimit);
//        }
        inItRecycleViewsLoadmore();
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("quick_review_friend")) {
                        if (mRecyclerViewN != null) {
                            mRecyclerViewN.setVisibility(View.GONE);
                        }
                    }

                    if (((String) o).equalsIgnoreCase("back_review_friend")) {
                        back();
                    }
                }
            }
        });
        mSwipeReviewFriend.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset, mPageLimit);
            }
        });

        mBtnRetryFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset, mPageLimit);
            }
        });
        return rootView;
    }

    private void setData(int mOffset, int mLimit) {
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            mPrensenter.getListFriendReviews(mTokenUser, "friend", String.valueOf(mOffset),
                    String.valueOf(mLimit), "");
        }
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        back();
        if (visible) {
            try {
                if(mReviewFriendsItems == null || (mReviewFriendsItems != null && mReviewFriendsItems.size() == 0)) {
                    mPageOffset = 0;
                    setData(mPageOffset, mPageLimit);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    private void back() {
        if (getView() != null) {
            getView().setFocusableInTouchMode(true);
            getView().requestFocus();
            getView().setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            RxBus.getInstance().post("back");
                            return true;
                        }
                        mActivity.lockEventIn(500);
                    }
                    return false;

                }


            });
        }
    }

    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerViewN.getLayoutManager();
        mRecyclerViewN.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading && mOnLoadMoreListener != null) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        back();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        back();
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(mActivity);
    }

    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        switch (task.getType()) {
            case GET_LIST_FRIEND:
                if (mBtnRetryFriends != null) {
                    mBtnRetryFriends.setVisibility(View.GONE);
                }
                if (mSwipeReviewFriend != null) {
                    mSwipeReviewFriend.setVisibility(View.VISIBLE);
                }
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mReviewsResponse = (ListAllReviewsResponse) baseResponse.getData();

                    if (mReviewsResponse != null) {
                        try {
                            mReviewFriendsItems = mReviewsResponse.getListReviews();
                            totalFriend = 0;
                            if (mReviewsResponse.getTotal() != null) {
                                totalFriend = Integer.parseInt(mReviewsResponse.getTotal());
                            }
                            tvTotalFriend.setText("(" + totalFriend + ")");
                            if (mSwipeReviewFriend.isRefreshing()) {
                                mReviewFriendsItemsSum = new ArrayList<>();
                                if (mReviewFriendsItems != null) {
                                    mReviewFriendsItemsSum = mReviewFriendsItems;
                                }
                                setUpRecycleView();
                                loading = true;
                                mSwipeReviewFriend.setRefreshing(false);
                            } else {
                                if (mReviewFriendsItems != null) {
                                    if (loading) {
                                        mReviewFriendsItemsSum = mReviewFriendsItems;
                                        setUpRecycleView();
                                    } else if (!loading) {
                                        loading = true;
                                        mReviewFriendsItemsSum.addAll(mReviewFriendsItems);
                                        mReviewsFriendsAdapter.notifyDataSetChanged();
                                    }
                                } else if (mReviewFriendsItems == null && isSearch) {
                                    mReviewFriendsItemsSum.clear();
                                    mReviewsFriendsAdapter.notifyDataSetChanged();
                                    mSwipeReviewFriend.setVisibility(View.GONE);
                                    mTvNoDataFriend.setVisibility(View.VISIBLE);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (mReviewFriendsItemsSum.size() > 0) {
                        mTvNoDataFriend.setVisibility(View.GONE);
                        mSwipeReviewFriend.setVisibility(View.VISIBLE);
                    } else {
                        mSwipeReviewFriend.setVisibility(View.GONE);
                        mTvNoDataFriend.setVisibility(View.VISIBLE);
                    }
                }
        }
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        if (task.getThrowable() != null) {
            if (task.getType() == ApiTaskType.GET_LIST_FRIEND) {
                Utils.hideLoading(mProgressDialog);
                mSwipeReviewFriend.setRefreshing(false);
                mReviewFriendsItemsSum = new ArrayList<>();
                mReviewFriendsItems = new ArrayList<>();
                if (mReviewsFriendsAdapter != null) {
                    mReviewsFriendsAdapter.notifyDataSetChanged();
                }
                mSwipeReviewFriend.setVisibility(View.GONE);
                mTvNoDataFriend.setVisibility(View.VISIBLE);
                mBtnRetryFriends.setVisibility(View.VISIBLE);
            }
        }
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }

    @Override
    public void onLoadMore() {
        mReviewFriendsItemsSum.add(null);
        mReviewsFriendsAdapter.notifyItemInserted(mReviewFriendsItemsSum.size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Remove loading item
                if (mReviewFriendsItemsSum.size() > 0) {
                    mReviewFriendsItemsSum.remove(mReviewFriendsItemsSum.size() - 1);
                    mReviewsFriendsAdapter.notifyItemRemoved(mReviewFriendsItemsSum.size());
                }
                if (isSearch) {
                    isSearch = false;
                    requestGetListAllSearchReviewNextPage();
                } else {
                    requestGetListAllReviewNextPage();
                }


            }
        }, 1000);
    }

    /**
     * request list
     */
    private void requestGetListAllSearchReviewNextPage() {
        mPrensenter.getListMeReviewsSearch(mTokenUser, "friend", String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), keyWordSearch);
    }

    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerViewN != null) {
            if (mReviewFriendsItemsSum == null) {
                mReviewFriendsItemsSum = new ArrayList<>();
            }
            mReviewsFriendsAdapter = new Reviews_Friends_Adapter(mActivity, mReviewFriendsItemsSum);
            mRecyclerViewN.setAdapter(mReviewsFriendsAdapter);
        }
    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {
        if (mPrensenter.hasUserInfo()) {
            mTokenUser = mPrensenter.getUserInfo().getToken();
            mPrensenter.getListFriendReviews(mTokenUser, "friend", String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), "");
        }
    }

    @Subscribe
    public void getEventSearchFriend(SearchReviewFriend searchReviewMe) {
        if (getView() != null) {
            if (isFragmentUIActive()) {
                getView().setFocusableInTouchMode(true);
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                mActivity.showDialogExitApp(mActivity);
                                return true;
                            }
                            mActivity.lockEventIn(500);
                        }
                        return false;

                    }


                });
            }
        }
        try {
            isSearch = true;
            loading = true;
            mPageOffset = 0;
            keyWordSearch = searchReviewMe.getKeyWord();
            mPrensenter.getListFriendReviews(mTokenUser, "friend", String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), keyWordSearch);
            inItRecycleViewsLoadmore();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(View view, int position) {

        ReviewsListProductUserScreen reviewsListProductUserScreen = new ReviewsListProductUserScreen();
        Bundle bundle = new Bundle();

        if (mReviewFriendsItemsSum.get(position).getUser_id() != null) {
            bundle.putString(Constant.F_ARG_USER_ID, mReviewFriendsItemsSum.get(position).getUser_id());
        }
        if (mReviewFriendsItemsSum.get(position).getFullname() != null) {
            bundle.putString(Constant.F_ARG_USER_NAME, mReviewFriendsItemsSum.get(position).getFullname());
        }
        reviewsListProductUserScreen.setArguments(bundle);
        android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.rlNewReview, reviewsListProductUserScreen, "reviewsListProductUserScreen");
        fragmentTransaction.commit();
        ReviewsScreen.mRlNewReview.setVisibility(View.VISIBLE);
    }

    /**
     * @param refreshListFriend
     */
    @Subscribe
    public void listenerReloadListMe(RefreshListFriend refreshListFriend) {
        mRecyclerViewN.setVisibility(View.VISIBLE);
        imgBackMain.setVisibility(View.VISIBLE);
    }

    @Subscribe
    public void listenerReloadListUser(ReloadtListUser reloadtListUser) {
        mRecyclerViewN.setVisibility(View.VISIBLE);
        imgBackMain.setVisibility(View.VISIBLE);
    }

    /**
     * set enable/disable loadMore
     */
    public void setLoaded() {
        mIsLoading = false;
    }

    @Subscribe
    public void feFreshData(refreshDataReview refreshData) {
        if (refreshData.getTypeScreen().toLowerCase().equals("review_friend") && isFragmentUIActive()) {
            loading = true;
            mPageOffset = 0;
            setData(mPageOffset, mPageLimit);

        }
    }

    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }
}
