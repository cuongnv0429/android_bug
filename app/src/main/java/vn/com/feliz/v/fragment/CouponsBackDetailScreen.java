package vn.com.feliz.v.fragment;


import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.MapView;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.databinding.FragmentCouponBackDetailScreenBinding;
import vn.com.feliz.gbservice1.api.GetCouponDetailService;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.LocationHomeExchange;
import vn.com.feliz.m.response.ShopModel;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.p.eventbus.refreshData;
import vn.com.feliz.v.activity.BaseActivity;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.adapter.CouponsBackAdapter;
import vn.com.feliz.v.adapter.GooglePlacesAutocompleteAdapter;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;


public class CouponsBackDetailScreen extends BaseFragment implements OnMenuItemSelected, View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnPostResponseListener{
    private String TAG = CouponsBackDetailScreen.class.getSimpleName();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private Location mLastLocation;
    private FragmentCouponBackDetailScreenBinding mBinding;
    private List<ShopModel.Shop> mShops;
    private String mCouponId;
    private String mCouponSubId;
    private CouponModel.Coupon mCoupon;
    private CouponDetailReceiver mReceiver;
    private boolean mVideoPlayingEarlier;
    private ProgressDialog mProgressDialog;
    private Long mLastUpdate = System.currentTimeMillis();
    private GooglePlacesAutocompleteAdapter googlePlacesAutocompleteAdapter;
    private CouponsPresenter couponsPresenter;
    private TSnackbar snackbar;
    private TextWatcher mTextWatcher;
    LocationHomeExchange location;
    private boolean isCheck;
    private boolean isShowKeyBoard = false;
    private String mAdress = "";
    private boolean isSendStatusLock = false;
    String list_shop_message = "";
    String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    public CouponsBackDetailScreen() {
        // Required empty public constructor
    }

    public static CouponsBackDetailScreen newInstance(List<ShopModel.Shop> shops, boolean videoPlaying) {
        CouponsBackDetailScreen fragment = new CouponsBackDetailScreen();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, (Serializable) shops);
        args.putBoolean(ARG_PARAM3, videoPlaying);
        fragment.setArguments(args);
        return fragment;
    }

    public static CouponsBackDetailScreen newInstance(String couponId, String couponSubId) {
        CouponsBackDetailScreen fragment = new CouponsBackDetailScreen();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM2, couponId);
        args.putString(ARG_PARAM4, couponSubId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //EventBus.getDefault().post(new SetTitleMessage(getString(R.string.coupons_Screen)));
        // Register Events

        if(MainActivity.fromPushMap_coupon_id != null) {
            if (!BaseActivity.hasPermissions(mActivity, PERMISSIONS_LOCATION)) {
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                    checkAndRequestPermissionsLoaction();
                }
            } else {
                if (!Utils.isLocationEnabled(mActivity)) {
                    Utils.displayPromptForEnablingGPS(mActivity);
                }
            }
            MainActivity.fromPushMap_coupon_id = null;

        }

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        EventBus.getDefault().post(this);
        if (getArguments() != null) {
            mShops = (List<ShopModel.Shop>) getArguments().getSerializable(ARG_PARAM1);
            mCouponId = getArguments().getString(ARG_PARAM2);
            mCouponSubId = getArguments().getString(ARG_PARAM4);
            mVideoPlayingEarlier = getArguments().getBoolean(ARG_PARAM3);
            if (getArguments().containsKey("list_shop_message")) {
                list_shop_message = getArguments().getString("list_shop_message");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupon_back_detail_screen, container, false);
            mBinding.mainTopbar.mainTopBarTitle.setText(R.string.coupons_Screen);
            mBinding.mainTopbar.mainTopbarBackLn.setOnClickListener(this);
            mBinding.rvLocation.setLayoutManager(new LinearLayoutManager(getContext()));
            initView();
            mBinding.txtNoPlaceExchangeGift.setVisibility(View.GONE);
            if (mShops == null) {
                createGoogleApiClient();
                mBinding.relativeCouponBackDetail.setVisibility(View.GONE);
                if (mBinding.txtNoPlaceExchangeGift != null) {
                    mBinding.txtNoPlaceExchangeGift.setVisibility(View.VISIBLE);
                    mBinding.txtNoPlaceExchangeGift.setText(list_shop_message);
                }
            } else {
                mBinding.relativeCouponBackDetail.setVisibility(View.VISIBLE);
                mBinding.rvLocation.setAdapter(new CouponsBackAdapter(mActivity, mShops, null));
                createGoogleApiClient();
            }
        }
        return mBinding.getRoot();
    }

    private void initView() {
        mBinding.relativeCouponBackDetail.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                mBinding.relativeCouponBackDetail.getWindowVisibleDisplayFrame(rect);
                int screenHeight = mBinding.relativeCouponBackDetail.getRootView().getHeight();
                int heightDifference = screenHeight - rect.bottom + rect.top;
                if (heightDifference > screenHeight * 0.15) {
                    if (!isShowKeyBoard) {
                        isShowKeyBoard = true;
                        mBinding.edtSearchGiftExchageHome.removeTextChangedListener(mTextWatcher);
                        mBinding.edtSearchGiftExchageHome.setText("");
                        mBinding.btnConfirm.setVisibility(View.GONE);
                        mBinding.edtSearchGiftExchageHome.addTextChangedListener(mTextWatcher);
                    }
                } else {
                    if (isShowKeyBoard) {
                        isShowKeyBoard = false;
                        mBinding.edtSearchGiftExchageHome.removeTextChangedListener(mTextWatcher);
                    }
                }

            }
        });
        location = new LocationHomeExchange();
        couponsPresenter = new CouponsPresenter(mActivity, this);
        googlePlacesAutocompleteAdapter = new  GooglePlacesAutocompleteAdapter(mActivity,
                R.layout.item_gift_exchange_home);
        mBinding.lsvLocation.setAdapter(googlePlacesAutocompleteAdapter);
        mBinding.lsvLocation.setTextFilterEnabled(true);
        mTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equalsIgnoreCase("")) {
                    mBinding.btnConfirm.setVisibility(View.GONE);
                    if (googlePlacesAutocompleteAdapter != null) {
                        googlePlacesAutocompleteAdapter.removeData();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equalsIgnoreCase("")) {
                    if (googlePlacesAutocompleteAdapter != null) {
                        googlePlacesAutocompleteAdapter.removeData();
                    }
                    mBinding.btnConfirm.setVisibility(View.GONE);
                } else {
                    if (googlePlacesAutocompleteAdapter != null) {
                        googlePlacesAutocompleteAdapter.getFilter().filter(s.toString());
                    }
                }
            }
        };
        mBinding.sbText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hideKeyboardDefaultOfDevices();
                if (isChecked) {
                    mBinding.edtSearchGiftExchageHome.setVisibility(View.VISIBLE);
                    mBinding.lsvLocation.setVisibility(View.VISIBLE);
                } else {
                    mBinding.edtSearchGiftExchageHome.setVisibility(View.GONE);
                    mBinding.lsvLocation.setVisibility(View.GONE);
                }
            }
        });
        mBinding.edtSearchGiftExchageHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyBoard(mBinding.edtSearchGiftExchageHome);
                if (googlePlacesAutocompleteAdapter != null) {
                    googlePlacesAutocompleteAdapter.removeData();
                }
            }
        });

        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location != null) {
                    submitGiftHomeExchange(location);
                }
            }
        });

        mBinding.lsvLocation.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final AutocompletePrediction prediction = ((AutocompletePrediction)googlePlacesAutocompleteAdapter.getItem(position));
                mBinding.edtSearchGiftExchageHome.removeTextChangedListener(mTextWatcher);
                mBinding.edtSearchGiftExchageHome.setText(prediction.getFullText(null));
                hideKeyboardDefaultOfDevices();
                googlePlacesAutocompleteAdapter.removeData();
                Places.GeoDataApi.getPlaceById(mGoogleApiClient,
                        prediction.getPlaceId())
                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(PlaceBuffer places) {
                                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                    final Place myPlace = places.get(0);
                                    Geocoder geocoder = new Geocoder(mActivity, Locale.getDefault());
                                    try {
                                        List<Address> addressList = geocoder.getFromLocation(myPlace.getLatLng().latitude,
                                                myPlace.getLatLng().longitude, 1);
                                        Address address = addressList.get(0);
                                        if (address != null) {
                                            location = parseJsonLocate(new Gson().toJson(address));
                                            location.setAddress(prediction.getFullText(null).toString());
                                            if (!mAdress.equalsIgnoreCase(location.getAddress())) {
                                                mBinding.btnConfirm.setClickable(true);
                                                mBinding.btnConfirm.setBackgroundColor(getResources().getColor(R.color.cl_text_box));
                                                mBinding.btnConfirm.setVisibility(View.VISIBLE);
                                            } else {
                                                mBinding.btnConfirm.setVisibility(View.GONE);
                                            }
                                        }

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                }
                                places.release();
                            }
                        });
            }
        });
    }

    private void submitGiftHomeExchange(LocationHomeExchange location) {
        if (couponsPresenter != null) {
            if (mBinding.sbText.isChecked()) {
                isSendStatusLock = false;
                isCheck = true;
                mProgressDialog = Utils.showLoading(mActivity);
                couponsPresenter.submitGiftHomeExchange(couponsPresenter.getUserInfo().getToken(),
                        mCouponId, mCouponSubId, "1", location.getAddress(), location.getCity_name(), location.getDistrict_name(),
                        location.getWard_name());
            }
        }
    }

    private LocationHomeExchange parseJsonLocate(String json) {
        LocationHomeExchange locationHomeExchange = new LocationHomeExchange();
        try {
            JSONObject jsonObject = new JSONObject(json);
            if (jsonObject.has("mAddressLines")) {
                JSONObject mAddressLines = jsonObject.getJSONObject("mAddressLines");
                if (mAddressLines.has("0")) {
                    locationHomeExchange.setAddress(mAddressLines.getString("0"));
                }
                if (mAddressLines.has("1")) {
                    locationHomeExchange.setWard_name(mAddressLines.getString("1"));
                }
                if (mAddressLines.has("2")) {
                    locationHomeExchange.setDistrict_name(mAddressLines.getString("2"));
                }
                if (mAddressLines.has("3")) {
                    locationHomeExchange.setCity_name(mAddressLines.getString("3"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return locationHomeExchange;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideKeyboardDefaultOfDevices();
        mReceiver = new CouponDetailReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(getActivity().getPackageName());
        getActivity().registerReceiver(mReceiver, filter);
    }

    @Override
    public void onResume() {
        super.onResume();
        alertGPS();
    }

    public void alertGPS() {
        if (!Utils.isLocationEnabled(mActivity)) {
            alertGPSNotitle();
        } else {

            if (mBinding.rvLocation.getAdapter() != null) {
                for (MapView m : ((CouponsBackAdapter) mBinding.rvLocation.getAdapter()).getMapViews()) {
                    m.onResume();
                }
            }
        }
    }

    public void alertGPSNotitle() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(mActivity.getString(R.string.message_action_gb_skip)); // De sau
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(mActivity.getString(R.string.message_action_gb_retry)); // Vao config
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(mActivity.getString(R.string.message_location_gb_latlng));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
                dialog.dismiss();
            }
        });
    }

    private void hideKeyboardDefaultOfDevices() {
        try {
            InputMethodManager inputMethod = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethod.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showKeyBoard(View view) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COUPONS;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mBinding.rvLocation.getAdapter() != null) {
            for (MapView m : ((CouponsBackAdapter)mBinding.rvLocation.getAdapter()).getMapViews()) {
                m.onLowMemory();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isCheck != mBinding.sbText.isChecked()) {
            isSendStatusLock = true;
            if (!mBinding.sbText.isChecked()) {
                couponsPresenter.submitGiftHomeExchange(couponsPresenter.getUserInfo().getToken(),
                        mCouponId, mCouponSubId, "0",
                        mBinding.edtSearchGiftExchageHome.getText().toString(), "", "",
                        "");
            } else {
                couponsPresenter.submitGiftHomeExchange(couponsPresenter.getUserInfo().getToken(),
                        mCouponId, mCouponSubId, "1",
                        mBinding.edtSearchGiftExchageHome.getText().toString(), "", "",
                        "");
            }
        }
        if (mBinding.rvLocation.getAdapter() != null) {
            for (MapView m : ((CouponsBackAdapter)mBinding.rvLocation.getAdapter()).getMapViews()) {
                m.onPause();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mShops == null) {
            if (mReceiver != null) {
                getActivity().unregisterReceiver(mReceiver);
            }
        }
        if (mBinding.rvLocation.getAdapter() != null) {
            for (MapView m : ((CouponsBackAdapter)mBinding.rvLocation.getAdapter()).getMapViews()) {
                m.onDestroy();
            }
        }
        if (mGoogleApiClient != null){
            if(mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v == null) return;
        switch (v.getId()) {
            case R.id.main_topbar_back_ln:
                Utils.hideSoftKeyboard(mActivity);
                if (mShops != null) {
                    if (mVideoPlayingEarlier) {
                        CouponsDetailScreen back = ((CouponsDetailScreen)getActivity().getSupportFragmentManager().findFragmentByTag("coupon_detail"));
                        if (back != null) {
                            back.playVideoIfCan();
                        }
                    }
                    if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    } else {
                        mActivity.getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .addToBackStack(null)
                                .commitAllowingStateLoss();
                        getChildFragmentManager().executePendingTransactions();
                    }
                }else {
                    if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                        getActivity().getSupportFragmentManager().popBackStack();
                    } else {
                        mActivity.getSupportFragmentManager()
                                .beginTransaction()
                                .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                .addToBackStack(null)
                                .commitAllowingStateLoss();
                        getChildFragmentManager().executePendingTransactions();
                    }

                }
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        googlePlacesAutocompleteAdapter.setGoogleApiClient(mGoogleApiClient);
        getLocation();
    }

    public void getLocation() {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            //get coupon available update
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        if (mLastLocation == null) {
            alertGPSNotitle();
        } else {
            getCouponDetail(mCouponId, mCouponSubId);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        alertGPS();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        alertGPS();
    }

    @Override
    public void onLocationChanged(Location location) {
//        try {
//            if (Utils.getDistance(mLastLocation, location) > 50 && mLastUpdate < System.currentTimeMillis() - 60000) {
//
////                if(mCoupon.getShops()!=null && mCoupon.getShops().size() > 0) {
////                    for (int i = 0; i<mCoupon.getShops().size(); i++) {
////                        String distance = Utils.getDistance2(mLastLocation, mCoupon.getShops().get(i).getLatitude(), mCoupon.getShops().get(i).getLongitude());
////                        mCoupon.getShops().get(i).setDistance(distance);
////                    }
////                }
////
////                CouponsBackAdapter adapter = ((CouponsBackAdapter) mBinding.rvLocation.getAdapter());
////                adapter.setData(mCoupon.getShops());
////                adapter.notifyDataSetChanged();
//
//                mLastLocation = location;
//                mLastUpdate = System.currentTimeMillis();
//                getCouponDetail(mCouponId, mCouponSubId);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    private void createGoogleApiClient(){
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void getCouponDetail(String couponId, String couponSubId){
        if (Utils.isNetworkAvailable(getContext())) {
            CouponsDetailScreen.updateLocationDeatail = true;
            mProgressDialog = Utils.showLoading(mActivity);
            Intent i = new Intent(getContext(), GetCouponDetailService.class);
            i.putExtra("coupon_id", couponId);
            i.putExtra("coupon_sub_id", couponSubId);
            i.putExtra("location", mLastLocation);
            getActivity().startService(i);
        }else {
            //Utils.showNoInternetMessage(getContext(), null);
        }
    }

    private void confirmExChangeHome(String smsConfirm) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.profile_dialog_ok));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        btnNo.setVisibility(View.GONE);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(smsConfirm);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBinding.btnConfirm.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });

    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        switch (task.getType()) {
            case SUBMIT_HOME_EXCHANGE:
                BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                Utils.hideLoading(mProgressDialog);
                if (baseResponse.getCode() == 200) {
                    mAdress = location.getAddress();
                    if (baseResponse.getData() != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(new Gson().toJson(baseResponse.getData()));
                            String message = "";
                            if (jsonObject.has("message")) {
                                message = jsonObject.getString("message");
                            }
                            if (!isSendStatusLock) {
                                confirmExChangeHome(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    if (mProgressDialog != null) {
                        Utils.hideLoading(mProgressDialog);
                    }
                }
                break;
        }
        return true;
    }

    public class CouponDetailReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Utils.hideLoading(mProgressDialog);
            final int connect_status = intent.getIntExtra(Constant.CONNECT_STATUS, 200);
            if(connect_status == Constant.CANNOT_CONNECT_SERVER) {
                mActivity.showNetworkAlert(getView());
            } else {
                final int action = intent.getIntExtra(Constant.ACTION, 0);
                switch (action) {
                    case Constant.ACTION_GET_COUPON_DETAIL:
                        CouponModel couponModel = (CouponModel) intent.getSerializableExtra("coupon");
                        if (couponModel != null) {
                            if (couponModel.getCode().equals(Constant.CODE_SUCCESS)) {
                                if(couponModel.getCoupon() != null) {
                                    mCoupon = couponModel.getCoupon();
                                    if (mCoupon.getShops() != null) {
                                        mShops = mCoupon.getShops();
                                    }
                                    //set gift exchange home
                                    switch (mCoupon.getCouponExchangeHomeStatus()) {
                                        case 0:
                                            mBinding.linearGiftExchangeHome.setVisibility(View.VISIBLE);
                                            mBinding.sbText.setChecked(false);
                                            isCheck = false;
                                            mBinding.edtSearchGiftExchageHome.setVisibility(View.GONE);
                                            mBinding.lsvLocation.setVisibility(View.GONE);
                                            mBinding.edtSearchGiftExchageHome.removeTextChangedListener(mTextWatcher);
                                            mBinding.edtSearchGiftExchageHome.setText(mCoupon.getCouponExchangeHomeAddress());
                                            mAdress = mCoupon.getCouponExchangeHomeAddress();
                                            break;
                                        case 1:
                                            isCheck = true;
                                            mBinding.edtSearchGiftExchageHome.removeTextChangedListener(mTextWatcher);
                                            mBinding.edtSearchGiftExchageHome.setText(mCoupon.getCouponExchangeHomeAddress());
                                            mBinding.linearGiftExchangeHome.setVisibility(View.VISIBLE);
                                            mBinding.sbText.setChecked(true);
                                            mBinding.edtSearchGiftExchageHome.setVisibility(View.VISIBLE);
                                            mBinding.lsvLocation.setVisibility(View.VISIBLE);
                                            mAdress = mCoupon.getCouponExchangeHomeAddress();
                                            break;
                                        case 2:
                                            mBinding.linearGiftExchangeHome.setVisibility(View.GONE);
                                            break;
                                    }
                                    if (mShops == null) {
                                        mBinding.relativeCouponBackDetail.setVisibility(View.GONE);
                                        if (mBinding.txtNoPlaceExchangeGift != null) {
                                            mBinding.txtNoPlaceExchangeGift.setVisibility(View.VISIBLE);
                                            mBinding.txtNoPlaceExchangeGift.setText(mCoupon.getListShopMessage());
                                        }
                                    } else {
                                        mBinding.relativeCouponBackDetail.setVisibility(View.VISIBLE);
                                        mBinding.txtNoPlaceExchangeGift.setVisibility(View.GONE);
                                    }
                                    mBinding.rvLocation.setAdapter(new CouponsBackAdapter(getContext(), mShops, null));
                                } else {
                                    // trường hợp user không có coupon này
                                    mBinding.relativeCouponBackDetail.setVisibility(View.GONE);
                                    if (mBinding.txtNoPlaceExchangeGift != null) {
                                        mBinding.txtNoPlaceExchangeGift.setVisibility(View.VISIBLE);
                                        mBinding.txtNoPlaceExchangeGift.setText(getResources().getString(R.string.text_no_coupon));
                                    }
                                }
                            } else {
                                Utils.showMessageNoTitle(CouponsBackDetailScreen.this.getContext(), couponModel.getMessage(), null);
                            }
                        }
                        break;
                }
            }
        }
    }
    @Subscribe
    public void feFreshData(refreshData refreshData) {
//        if(refreshData.getNameFragment().toLowerCase().equals("detailbackcoupon")) {
//// noi reload data
//            if(mShops == null) getCouponDetail(mCouponId, mCouponSubId);
//            mBinding.rvLocation.getAdapter().notifyDataSetChanged();
//        }
    }

    private boolean checkAndRequestPermissionsLoaction() {
        int contactpermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.GET_ACCOUNTS);
        int locationpermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (contactpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), Constant.REQUEST_PERMISSIONS_LOCATION);
            }
            return false;
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQUEST_PERMISSIONS_LOCATION:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }else {
                    Utils.displayPromptForEnablingGPS(mActivity);
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
