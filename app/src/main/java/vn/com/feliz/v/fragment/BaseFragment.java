package vn.com.feliz.v.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;

import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.R;

import butterknife.Unbinder;


/**
 * Created by macbook on 4/11/16.
 */
public abstract class BaseFragment
        extends Fragment
        implements OnResponseListener {

    /**
     * fragment's activity
     */
    MainActivity mActivity;
    /**
     * Check Fragment is Resumed
     */
    private boolean mIsResumed = false;
    /**
     * Dialog Button Bar
     */
    private static View mDialogButtonsBar;
    /**
     * Resource Unbinder
     */
    Unbinder mUnbinder;
    /**
     * The Dialog
     */
    private Dialog mDialog;

    /**
     * Called when a fragment is first attached to its context.
     * {@link #onCreate(Bundle)} will be called after this.
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) context;
        mActivity.setCurrentFragment(this);
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
        mIsResumed = true;
    }

    public void onFragmentResume() {

    }

    /**
     * Show Loading Dialog
     *
     * @param message int
     */
    protected void showLoading(@StringRes int message) {
        mDialog = Utils.showProgressDialog(mActivity, message);
    }

    /**
     * Hide Current Loading Dialog
     */
    protected void hideLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    public boolean onBackPressed() {
        return false;
    }

    /**
     * Get Resumed Value
     *
     * @return TRUE if Fragment is resumed
     */
    public boolean isFragmentResumed() {
        return mIsResumed;
    }

    /**
     * Receive Response
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Task is Finished and Execute next Task
     */
    @Override
    public final boolean onResponse(ApiTask task, int status) {
        if (this instanceof OnPostResponseListener) {
            OnPostResponseListener listener = (OnPostResponseListener) this;

            if (listener.willProcess(task, status)) {
                return listener.onPostResponse(task, status);
            }
        }

        return onProcessResponse(task, status);
    }

    /**
     * Will Process on Child Classes
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Response is Process on Child Classes
     */
    @Override
    public boolean willProcess(ApiTask task, int status) {
        return status == ApiResponseCode.SUCCESS ||
                status != ApiResponseCode.CANNOT_CONNECT_TO_SERVER;
    }

    /**
     * Local Process Response
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Task is Finished and Execute next Task
     */
    private boolean onProcessResponse(ApiTask task, int status) {
        hideLoading();

        switch (status) {
            case ApiResponseCode.CANNOT_CONNECT_TO_SERVER:
                break;

            default:
                break;
        }

        return true;
    }

    /**
     * Retry When Not Connect to Server
     *
     * @param task ApiTask
     */
 /*   private void showRetryDialog(final ApiTask task) {
        Utils.showConfirmDialog(mActivity,
                R.string.dialog_title_attention, R.string.dialog_message_no_connect,
                R.string.dialog_message_no_connect_no, null,
                R.string.dialog_message_no_connect_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        task.execute();
                    }
                }, this);
    }*/


    /**
     * Called when the Fragment is no longer resumed.  This is generally
     * tied to {@link Activity#onPause() Activity.onPause} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onPause() {
        mIsResumed = false;
        super.onPause();
    }

    /**
     * Called when the view previously created by {@link #onCreateView} has
     * been detached from the fragment.  The next time the fragment needs
     * to be displayed, a new view will be created.  This is called
     * after {@link #onStop()} and before {@link #onDestroy()}.  It is called
     * <em>regardless</em> of whether {@link #onCreateView} returned a
     * non-null view.  Internally it is called after the view's state has
     * been saved but before it has been removed from its parent.
     */
    @Override
    public void onDestroyView() {
        if (mDialogButtonsBar != null && mDialogButtonsBar.getParent() != null) {
            ((ViewGroup) mDialogButtonsBar.getParent()).removeView(mDialogButtonsBar);
        }
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
        super.onDestroyView();
    }

    public boolean popFragment() {
        boolean isPop = false;
        FragmentManager fm = getChildFragmentManager();
        if (fm.getBackStackEntryCount() > 1) {
            isPop = true;
            getChildFragmentManager().popBackStack();
        }
        return isPop;
    }

    public Fragment getCurrentFragment() {
        return getChildFragmentManager().findFragmentById(
                R.id.root_frame);
    }



    /**
     * @return count in backtrack
     */
    public int getCountBackStack() {
        return getChildFragmentManager().getBackStackEntryCount();
    }

    public void clearStack() {
/*
* Here we are clearing back stack fragment entries
*/
        int backStackEntry = mActivity.getSupportFragmentManager().getBackStackEntryCount();

        if (backStackEntry > 0) {
            for (int i = 0; i < backStackEntry; i++) {
                mActivity.getSupportFragmentManager().popBackStackImmediate();
            }
        }

/*
* Here we are removing all the fragment that are shown here
*/
        if (mActivity.getSupportFragmentManager().getFragments() != null && mActivity.getSupportFragmentManager().getFragments().size() > 0) {
            for (int i = 0; i < mActivity.getSupportFragmentManager().getFragments().size(); i++) {
                Fragment mFragment = mActivity.getSupportFragmentManager().getFragments().get(i);
                if (mFragment != null) {
                    mActivity.getSupportFragmentManager().beginTransaction().remove(mFragment).commit();
                }
            }
        }
    }
}
