package vn.com.feliz.v.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.Subscriber;
import rx.android.app.AppObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.NetworkStateReceiver;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.Profile;
import vn.com.feliz.m.ProfileConfirmInfo;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.UserInfo;
import vn.com.feliz.m.response.RequestCodeRespone;
import vn.com.feliz.m.response.VerifyResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.DataManager;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.LoginPresenter;
import vn.com.feliz.p.RegisterPresenter;
import vn.com.feliz.p.eventbus.HidSearchTopBar;
import vn.com.feliz.p.eventbus.LockEvent;
import vn.com.feliz.p.eventbus.RequestFocus;
import vn.com.feliz.p.eventbus.SetTitleMessage;
import vn.com.feliz.p.eventbus.refreshData;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.PagerMainsAdapter;
import vn.com.feliz.v.fragment.BaseFragment;
import vn.com.feliz.v.fragment.CouponsBackDetailScreen;
import vn.com.feliz.v.fragment.CouponsDetailScreen;
import vn.com.feliz.v.fragment.CouponsScreen;
import vn.com.feliz.v.fragment.ReviewsDetailProductScreen;
import vn.com.feliz.v.fragment.ReviewsScreen;
import vn.com.feliz.v.fragment.SettingScreen;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.v.widget.CustomViewPager;
import vn.com.feliz.v.widget.PhoneEditText;


public class MainActivity extends BaseActivity implements OnPostResponseListener, NetworkStateReceiver.NetworkStateReceiverListener {
    public static final String PREFS_NAME = Constant.MYPREFSFILE;
    RegisterPresenter mPresenter;
    @BindView(R.id.main_top_bar_title)
    TextView mTitle;
    @BindView(R.id.main_bottom_bar)
    FrameLayout mBottomBar;
    @BindView(R.id.img_tab_coupons)
    ImageView mCoupons;
    @BindView(R.id.img_tab_coins)
    ImageView mCoins;
    @BindView(R.id.img_tab_review)
    ImageView mReviews;
    @BindView(R.id.img_tab_shopping)
    ImageView mShopping;
    @BindView(R.id.img_tab_setting)
    ImageView mSetting;
    @BindView(R.id.main_topbar_back)
    ImageView mBackBtn;
    @BindView(R.id.main_top_bar_coupons)
    TextView mTitleCouponsDetail;
    @BindView(R.id.main_topbar)
    RelativeLayout mTopBar;
    @BindView(R.id.tv_topbar_left)
    TextView mTitleBarLeft;
    @BindView(R.id.main_top_bar_scaner_img)
    ImageView mTitleBarScanerImgRight;
    @BindView(R.id.rl_container)
    RelativeLayout mContainerView;
    private SharedPreferences mSettings;
    public static boolean isFirstRun;
    private boolean isChangeMobile;
    private TSnackbar snackbar;
    private NetworkStateReceiver mNetworkStateReceiver;
    private boolean isDisConect;
    @BindView(R.id.reviews_pager_main)
    CustomViewPager mViewPager;
    @BindView(R.id.v_overlay)
    View mViewOverlayBottomBar;
    public static String couponsIdFirst;
    public static DatabaseReference mFirebaseDatabase;
    public static FirebaseDatabase mFirebaseInstance;
    protected static ValueEventListener mFireBaseRef;
    public static List<CouponModel.Coupon> couponList;
    public static Boolean reload_list = false;
    public static Boolean reload_detail = false;
    public static Boolean not_from_list = false;
    public static Boolean from_sub_list = false;
    public static boolean isScreenEnterCodeOfShop = false;
    public static boolean isFirstLoad = true;
    public static CallbackManager callbackManager;
    public ProgressDialog mProgressDialog;
    private Context mContext;
    private Dialog mDialog;
    private String mClickVerifySector;
    private String mPhoneNumber = "";
    public static boolean isFromPushMap = false;
    public static boolean isFromPush = false;
    public static String fromPushMap_coupon_id;
    public static String fromPushMap_sub_coupon_id;
    public static int fromPushType = 0;
    public String currentPhone = "";
    private CompositeSubscription mComposite;
    Profile profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        FacebookSdk.sdkInitialize(getApplicationContext());
        //check nextwork connect
        try {
            mNetworkStateReceiver = new NetworkStateReceiver();
            mNetworkStateReceiver.addListener(this);
            this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Exception e) {
            e.printStackTrace() ;
        }
        //initialize LoginPresenter
        mPresenter = new RegisterPresenter(this, this);
        //check first run
        mSettings = getSharedPreferences(PREFS_NAME, 0);
        // isFirstRun = mSettings.getBoolean(Constant.FIRST_RUN, true);
        try {

            if (mPresenter.getUserInfo().getLogin_type().equals(Constant.NEW_LOGIN)) {
                isFirstRun = true;
            } else if (mPresenter.getUserInfo().getLogin_type().equals(Constant.OLD_LOGIN)) {
                isFirstRun = false;
            }

            String referreCode = Utils.getReferreCode(getApplicationContext());
            Log.e("REFERE YES", referreCode);
            if (referreCode != null & !referreCode.equals("")) {
                mPresenter.submitReferreCode(mPresenter.getUserInfo().getToken(), referreCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //son edit 08/12/16
      /*  isFirstRun = mSettings.getBoolean(Constant.FIRST_RUN, true);*/
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main_content);
        // Bind all listeners
        mUnbinder = ButterKnife.bind(this);

        // RegisterActivity Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //setup view pager
        final PagerMainsAdapter adapter = new PagerMainsAdapter
                (getSupportFragmentManager(), 5);
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(5);
        mViewPager.setPagingEnabled(false);
        //check first run time;

        //if(mFirebaseInstance == null) {
        if(mPresenter.getUserInfo()!=null) {
            Log.e("mMainActivity:SonDB4", mPresenter.getUserInfo().toString());
            initFirebase(mPresenter.getUserInfo().getUid());
//            initFirebase(mPresenter.getUserInfo().getUid());

            if(mPresenter.getUserInfo().getAllow_ads() != null && mPresenter.getUserInfo().getAllow_ads().equals("1")) {
                Intent intent = new Intent();
                // BYME
                // intent.setClass(this, LockScreenService.class);
                //startService(intent);
            } else {
                Intent intent = new Intent();
                //BYME
                // intent.setClass(this, LockScreenService.class);
                //stopService(intent);
            }
        }
        //}

        checkRunTime();
//        listenerForceUpdate(mPresenter.getUserInfo().getUid());
        //check wifi
        if (!Utils.isNetworkAvailable(this)) {
            //setup snackBar
            snackbar = TSnackbar
                    .make(mContainerView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_LONG);
            Utils.showMessageTopbar(snackbar, getString(R.string.wifi_speed), this);
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            this.unregisterReceiver(mNetworkStateReceiver);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * check first run time
     */
    private void checkRunTime() {
        BaseApplication mBaseApplication = (BaseApplication) getApplication();
        Tracker mTracker = mBaseApplication.getDefaultTracker();
        SharedPreferences pre = getSharedPreferences
                (Constant.MYPREFSFILE_MOBILE_CHANGE, MODE_PRIVATE);
        isChangeMobile = pre.getBoolean(Constant.IS_CHANGE, false);
        //reset flag change mobile from profile
        SharedPreferences.Editor editorMObile = pre.edit();
        editorMObile.putBoolean(Constant.IS_CHANGE, false);
        editorMObile.apply();
        //if first run
        if ((isFirstRun)) {
            Intent i = this.getIntent();
            if (i != null) {
                try {
                    ProfileConfirmInfo mProfile = (ProfileConfirmInfo) i.getSerializableExtra(Constant.FACEBOOK_OBJECT);
                    couponsIdFirst = mProfile.getCoupons_id();
                    mViewPager.setCurrentItem(0);
                    mCoupons.setSelected(true);
                    mSetting.setSelected(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }//if change mobile
        else if (isChangeMobile) {
            //reset flag
            isChangeMobile = pre.getBoolean(Constant.IS_CHANGE, false);
            //mContainer.setVisibility(View.GONE);
            mCoupons.setSelected(false);
            mSetting.setSelected(true);
            mViewPager.setCurrentItem(4, false);
            setMenuItemSelectedNew(OnMenuItemSelected.SELECT_SETTING);
        } else {
            Intent i = getIntent();

            if (i != null) {
                int code = -1;
                String ads_id = "";
                if (i.getExtras() != null) {
                    if (i.getExtras().containsKey("push_type")) {
                        try {
                            //background and kill app
                            code = Integer.parseInt(i.getExtras().getString("push_type"));
                        } catch (NumberFormatException ex) {
                            //foreground
                            code = i.getExtras().getInt("push_type");
                        }
                    }

                    if (i.getExtras().containsKey("ads_id")) {
                        ads_id = i.getExtras().getString("ads_id");
                    }
                }
                if (code != -1) {
                    mTracker.enableAdvertisingIdCollection(true);
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory(Constant.CATEGORY_ANALYTISC_PUSH_NOTIFICATIONS)
                            .setAction(Constant.EVENT_ANALYTISC_NOTIFICATION_CLICK + "_" + code + "_" + ads_id)
                            .build());

                }
                Log.e("Main", code + "__");
                switch (code) {
                    case 15:
                        final String appPackageName = getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                        MainActivity.this.finish();
                        break;
                }
                if (code == 2 || code == 3 || code == 4) {
                  /*  getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.main_content_container, new SettingScreen())
                            .commit();*/
                    isFromPush = true;
                    setMenuItemSelectedNew(OnMenuItemSelected.SELECT_SETTING);
                    mViewPager.setCurrentItem(4, false);
                } else if(code == 6 || code == 7) {
                    setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COUPONS);
                    mViewPager.setCurrentItem(0);
                    isFirstRun = true;
                    couponsIdFirst = i.getStringExtra("coupon_id");
                    if(code == 6) {
                        fromPushType = code;
                    }
                } else if (code == 5) {
                    //mContainer.setVisibility(View.VISIBLE);
//                    getSupportFragmentManager()
//                            .beginTransaction()
//                            .add(R.id.container_detail, CouponsBackDetailScreen.newInstance(i.getStringExtra("coupon_id"), i.getStringExtra("coupon_sub_id")))
//                            .commit();
                    setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COUPONS);
                    mViewPager.setCurrentItem(0);
                    isFromPushMap = true;
                    fromPushMap_coupon_id = i.getStringExtra("coupon_id");
                    fromPushMap_sub_coupon_id = i.getStringExtra("coupon_sub_id");
                } else {
                    //if run seconds time
                    // mContainer.setVisibility(View.GONE);
                  /*  getSupportFragmentManager()
                            .beginTransaction()
                            .add(R.id.main_content_container, CouponsScreen.newInstance(null))
                            .commit();*/
                    if (code == 9) {
                        setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COINS);
                        mViewPager.setCurrentItem(1);
                    } else {
                        setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COUPONS);
                        mViewPager.setCurrentItem(0);
                        if (code == 12) {
                            isFromPushMap = false;
                            isFirstRun = false;
                            couponList = null;
//                        getSupportFragmentManager()
//                                .beginTransaction()
//                                .replace(R.id.root_frame, CouponsScreen.newInstance(null))
//                                .addToBackStack(null)
//                                .setTransition(FragmentTransaction.TRANSIT_NONE)
//                                .commit();
                        }
                    }

                }
            } else {
            /*    //if run seconds time
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.main_content_container, CouponsScreen.newInstance(null))
                        .commit();*/
                setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COUPONS);
                mViewPager.setCurrentItem(0);
            }
        }

        }

    private void listenerForceUpdate(final String mUid) {
        mComposite = new CompositeSubscription();
        mComposite.add(AppObservable.bindActivity(MainActivity.this,
                Observable.create(new Observable.OnSubscribe<Profile>() {
                    @Override
                    public void call(final Subscriber<? super Profile> subscriber) {
                        DatabaseReference ref = FirebaseDatabase.getInstance()
                                .getReferenceFromUrl(Constant.FIREBASE_URL);
                        ref.child("user").child(mUid).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                String json = new Gson().toJson(dataSnapshot.getValue());
                                JSONObject jsonObject = null;
                                try {
                                    jsonObject = new JSONObject(json);
                                    JSONObject jsonProfile = jsonObject.getJSONObject("profile");
                                    String force_update = "";
                                    if (jsonProfile.has("force_update")) {
                                        force_update = jsonProfile.getString("force_update");
                                    }
                                    if (force_update.equalsIgnoreCase("1") || force_update.equalsIgnoreCase("2")) {
                                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                            }
                        });
                    }
                }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(DataManager.getInstance(MainActivity.this).getmScheduler())
                .subscribe(new Subscriber<Profile>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onStart() {
                        super.onStart();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Profile profileRespone) {
                    }
                }));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }
    /**
     * get event lock from adapter Expanable Interest
     */
    @Subscribe
    public void lockEvent(LockEvent mLockEvent) {
        lockEventIn(Constant.LOCK_EVENT_TIME);
    }

    /**
     * Check Login facebook
     *
     * @return isLoggedIn
     */
    public boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoginPresenter p = new LoginPresenter(this, this);

        if(p.getUserInfo() != null) {
            if (p.getUserInfo().getToken() == "") {
                gotoLogin();
            }
        } else {
            Log.e("Back to splash", "User null");
            // Back to splash
            Intent i = new Intent(MainActivity.this, SplashScreen.class);
            startActivity(i);
            // close this activity
            finish();
        }

        if(mPresenter.getUserInfo()!=null && mFirebaseInstance == null) {
            initFirebase(mPresenter.getUserInfo().getUid());
        }

        // RegisterActivity Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //check wifi
        if (!Utils.isNetworkAvailable(this)) {
            //setup snackBar
            snackbar = TSnackbar
                    .make(mContainerView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_LONG);
            Utils.showMessageTopbar(snackbar, getString(R.string.wifi_speed), this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("my TEST", "ONRESUME");
    }

    /**
     * Set Title top bar
     *
     * @param message String
     */
    @Subscribe
    public void setTitle(SetTitleMessage message) {
        mTitle.setText(message.getmTitleText());
    }

    //setupdate tabnew
    public void setMenuItemSelectedNew(int screen) {
        mCoupons.setSelected(false);
        mCoins.setSelected(false);
        mReviews.setSelected(false);
        mShopping.setSelected(false);
        mSetting.setSelected(false);

        switch (screen) {
            case OnMenuItemSelected.SELECT_COUPONS:
                mCoupons.setSelected(true);
                break;
            case OnMenuItemSelected.SELECT_COINS:
                mCoins.setSelected(true);
                break;
            case OnMenuItemSelected.SELECT_REVIEWS:
                mReviews.setSelected(true);
                break;
            case OnMenuItemSelected.SELECT_SHOPPING:
                mShopping.setSelected(true);
                break;
            case OnMenuItemSelected.SELECT_SETTING:
                mSetting.setSelected(true);
                break;
            case OnMenuItemSelected.SELECT_SETTING_CONFIRM_REGISTER:
                mBottomBar.setVisibility(View.GONE);

            default:
                throw new ArrayIndexOutOfBoundsException();
        }
    }

    @OnClick(R.id.v_overlay)
    void clickBottomBar() {
        Log.e("bottom_bar", "bottom_bar");
        //send event to Coins,Review and Setting Screen
        RxBus.getInstance().post("bottom_bar");
    }

    /**
     * click tab setting
     */
    @OnClick(R.id.img_tab_setting)
    void clickTabSetting() {
        if (mViewPager.getCurrentItem() != 4) {
            mViewPager.setCurrentItem(4, false);
            setMenuItemSelectedNew(OnMenuItemSelected.SELECT_SETTING);
        }
    }

    /**
     * click tab coins
     */
    @OnClick(R.id.img_tab_coins)
    void clickTabCoins() {
        if (mViewPager.getCurrentItem() != 1) {
            mViewPager.setCurrentItem(1, false);
            setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COINS);
        }
    }

    /**
     * click tab img_tab_review
     */
    @OnClick(R.id.img_tab_review)
    void clickTabReviews() {

        if (mViewPager.getCurrentItem() != 2) {
            if(MainActivity.isFirstLoad) {
                MainActivity.isFirstLoad = false;
                EventBus.getDefault().post(new refreshDataReview("review_friend"));
            }
            EventBus.getDefault().post(new refreshDataReview("review_all"));
            EventBus.getDefault().post(new refreshDataReview("review_me"));
            mViewPager.setCurrentItem(2, false);
            setMenuItemSelectedNew(OnMenuItemSelected.SELECT_REVIEWS);
            EventBus.getDefault().post(new RequestFocus());
        }
    }

    /**
     * click tab img_tab_review
     */
    @OnClick(R.id.img_tab_shopping)
    void clickTabShopping() {
        if (mViewPager.getCurrentItem() != 3) {
            mViewPager.setCurrentItem(3, false);
            setMenuItemSelectedNew(OnMenuItemSelected.SELECT_SHOPPING);
        }
    }

    /**
     * click tab coupons
     */
    @OnClick(R.id.img_tab_coupons)
    void clickTabCoupons() {

        if(not_from_list) {
            not_from_list = false;
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                    .addToBackStack(null)
                    .setTransition(FragmentTransaction.TRANSIT_NONE)
                    .commit();
        }
        mViewPager.setCurrentItem(0, false);
        setMenuItemSelectedNew(OnMenuItemSelected.SELECT_COUPONS);
    }

    /**
     * Back Pressed
     *
     * @param btn View
     */
    @OnClick({R.id.main_topbar_back_ln, R.id.main_topbar_back})
    public void onBack(View btn) {
        Fragment fragment = getSupportFragmentManager()
                .findFragmentById(R.id.root_frame);

        if (fragment != null) {
            //fragment instanceof CouponsDetailScreen ||
            if (fragment instanceof CouponsBackDetailScreen) {
                Log.i("e onBack >>>", "Coupon detail");
                //getSupportFragmentManager().popBackStack();
                mTitleCouponsDetail.setText("");
                mTitleCouponsDetail.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Check Back Button Visible
     *
     * @param fragment Current Screen
     */
    @Subscribe
    public void checkItemsVisible(BaseFragment fragment) {
        mBottomBar.setVisibility(View.VISIBLE);
    }

    PhoneEditText code;
    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Utils.hideLoading(mProgressDialog);
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            showNetworkAlert(mContainerView);
        } else {

            if (task.getType() == ApiTaskType.REQUEST_CODE) {
                if (status == ApiResponseCode.SUCCESS || status == ApiResponseCode.REQUEST_EXISTS) {
                    try {
                        BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                        RequestCodeRespone mRequestCode = new RequestCodeRespone();
                        mRequestCode = (RequestCodeRespone) baseResponse.getData();
                        Log.d("sssDEBUG", "mRequestCode " + mRequestCode.getPhone());

                        mDialog.dismiss();
                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        View dialogView = inflater.inflate(R.layout.custom_dialog_verify_phone, null);
                        dialogView.findViewById(R.id.verify_phone_number).setVisibility(View.GONE);
                        TextView verify_phone_dialog_title = (TextView) dialogView.findViewById(R.id.verify_phone_dialog_title);
                        if (mRequestCode != null) {
                            verify_phone_dialog_title.setText("Mã xác thực đã được gửi đến số điện thoại " + mRequestCode.getPhone() +
                                    ", hãy nhập mã vào đây");
                        }

                        code = (PhoneEditText) dialogView.findViewById(R.id.verify_code_number);
                        code.setVisibility(View.VISIBLE);
                        code.setFocusable(true);
                        code.setFocusableInTouchMode(true);
                        int maxLength = 4;
                        code.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

                        dialogConfirm(dialogView,
                                getString(R.string.dialog_confirm_text),
                                getString(R.string.dialog_retry_exit),
                                new DialogCallback() {
                                    @Override
                                    public void goit(Dialog dialog) {
                                        String code = ((PhoneEditText) dialog.findViewById(R.id.verify_code_number)).getText().toString().trim();
                                        UserInfo userInfo = mPresenter.getUserInfo();
                                        if(userInfo != null) {
                                            mProgressDialog = Utils.showLoading(mContext);
                                            mPresenter.verifyCode(code, Constant.TYPE_REQUEST, userInfo.getToken(), mPhoneNumber);
                                        }
                                    }
                                    @Override
                                    public void cancel(Dialog dialog) {
                                        dialog.dismiss();
                                    }
                                });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (phone_number != null) {
                        phone_number.setBackgroundResource(R.drawable.edt_background_error);
                    }
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    snackbar = TSnackbar
                            .make(mContainerView, " " + baseResponse.getMessage(), TSnackbar.LENGTH_LONG);
                    Utils.showMessageTopbarNextWork(snackbar, baseResponse.getMessage(), this);
                    Utils.vibarte(MainActivity.this);
                }
            } else if (task.getType() == ApiTaskType.VERIFY_CODE) {

                BaseResponse baseResponse = (BaseResponse) task.getResponse().body();

                if (status == ApiResponseCode.SUCCESS) {

                    VerifyResponse verifyResponse = (VerifyResponse) baseResponse.getData();

                    mDialog.dismiss();
                    mPresenter.updateIsVerify("1");

                    if(verifyResponse!= null && verifyResponse.getPhone() != null) {
                        mPresenter.updatePhone(verifyResponse.getPhone());
                    } else {
                        mPresenter.updatePhone(mPhoneNumber);
                    }
                    snackbar = TSnackbar
                            .make(mContainerView, " " + getString(R.string.verify_phone_success), TSnackbar.LENGTH_LONG);
                    Utils.showMessageTopbarNextWork(snackbar, getString(R.string.verify_phone_success), this);
                    if(mClickVerifySector.equals("coin")) {
                        findViewById(R.id.txt_coins_donation).performClick();
                    } else if(mClickVerifySector.equals("buycoupon")) {
                        findViewById(R.id.btn_gift).performClick();
                    }

                } else if(status == ApiResponseCode.CODE_NOT_MATCH) {
                    if (code != null) {
                        code.setBackgroundResource(R.drawable.edt_background_error);
                    }
                    snackbar = TSnackbar
                            .make(mContainerView, " " + baseResponse.getMessage(), TSnackbar.LENGTH_LONG);
                    Utils.showMessageTopbarNextWork(snackbar, baseResponse.getMessage(), this);
                    Utils.vibarte(MainActivity.this);
                }
            }
        }

        return true;
    }

    /**
     * Destroy all fragments and loaders.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {

            if (mNetworkStateReceiver != null) {
                mNetworkStateReceiver.removeListener(this);
            }
            if (EventBus.getDefault().isRegistered(this)) {
                EventBus.getDefault().unregister(this);
            }
            //reset flag change mobile from profile
            SharedPreferences pre = getSharedPreferences
                    (Constant.MYPREFSFILE_MOBILE_CHANGE, MODE_PRIVATE);
            SharedPreferences.Editor editorMObile = pre.edit();
            editorMObile.putBoolean(Constant.IS_CHANGE, false);
            editorMObile.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TextView getTitleCouponsDetail() {
        return mTitleCouponsDetail;
    }

    /**
     * @return mBottom
     */
    public FrameLayout mBottom() {
        return mBottomBar;
    }


    /**
     * @return mTitleBarLeft
     */
    public TextView mTitleLeft() {
        return mTitleBarLeft;
    }

    private void backShowDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(message);
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                MainActivity.isScreenEnterCodeOfShop = false;
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.popBackStack();
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Log.e("onBackPressed", fragmentManager.getBackStackEntryCount() + "__");
        if (fragmentManager.getBackStackEntryCount() < 1) {
            if (this instanceof BaseActivity) {
                ((BaseActivity) this).showDialogExitApp(this);
            }
        } else {
            Log.e("not_from_list", not_from_list + "");
            if (mViewOverlayBottomBar != null) {
                mViewOverlayBottomBar.setVisibility(View.GONE);
            }
            if (isScreenEnterCodeOfShop) {//the enter code to gift screen
                backShowDialog(getString(R.string.dialog_change));
            } else if (!not_from_list && mViewPager.getCurrentItem() == 0) {//the screen is in gift list
                ((BaseActivity) this).showDialogExitApp(this);
            } /*else if (fragmentManager.getBackStackEntryCount() == 1 && mViewPager.getCurrentItem() == 2){//the review screen
                ((BaseActivity) this).showDialogExitApp(this);
            } */
            else if(mViewPager.getCurrentItem() == 4 && fragmentManager.getBackStackEntryCount() == 1){
                ((BaseActivity) this).showDialogExitApp(this);
            }
            if(not_from_list&&mViewPager.getCurrentItem()==0&&fragmentManager.getBackStackEntryCount()==1){
                ((BaseActivity) this).showDialogExitApp(this);
            } else {
                if(!(mViewPager.getCurrentItem() == 2)&&not_from_list) {
                    fragmentManager.popBackStack();
                }
            }

        }

//        Fragment fragment = getSupportFragmentManager()
//                .findFragmentById(R.id.root_frame);
//        if (fragment instanceof SearchPlaceScreen) {
//            Log.i("MainActivity", "popping backstack");
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    getSupportFragmentManager().popBackStack();
//                }
//            }, Constant.TIME_DELAY_HIDE_KEY_BOARD);
//        } else {
//            Log.i("MainActivity", "nothing on backstack, calling super");
//            super.onBackPressed();
//        }
    }

    @Override
    public void networkAvailable() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
        if (isDisConect) {
            Fragment fragment = getSupportFragmentManager()
                    .findFragmentById(R.id.root_frame);
            if (fragment instanceof SettingScreen) {
                EventBus.getDefault().post(new refreshData("settingfragment"));
            } else if (fragment instanceof CouponsScreen) {
                EventBus.getDefault().post(new refreshData("couponsscreen"));
            } else if (fragment instanceof CouponsDetailScreen) {
                EventBus.getDefault().post(new refreshData("detailcoupon"));
            } else if (fragment instanceof CouponsBackDetailScreen) {
                EventBus.getDefault().post(new refreshData("detailbackcoupon"));
            }
            isDisConect = false;
        }
    }

    @Override
    public void networkUnavailable() {
        //setup snackBar
        snackbar = TSnackbar
                .make(mContainerView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_LONG);
        Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
        isDisConect = true;
    }

    public View getmViewOverlayBottomBar() {
        return mViewOverlayBottomBar;
    }

    public void initFirebase(String uid) {

        mFirebaseInstance = FirebaseDatabase.getInstance();

        mFirebaseDatabase = mFirebaseInstance.getReferenceFromUrl(Constant.FIREBASE_URL).child("user").child(uid);

        mFireBaseRef = mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String json = new Gson().toJson(dataSnapshot.getValue());
                profile = new Profile();
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    JSONObject jsonProfile = jsonObject.getJSONObject("profile");
                    profile = new Gson().fromJson(jsonProfile.toString(), Profile.class);
                    RxBus.getInstance().post(profile);

                    Log.e("a TOTOTO", jsonProfile.getString("device_id") +" - "+ Utils.getDeviceId(getApplicationContext()));

                    /* Process some key */

                    if(!getIn_form_login()) {
                        if (jsonProfile.has("device_id")) {
                            if (!jsonProfile.getString("device_id").equals(Utils.getDeviceId(getApplicationContext()))) {
                                Log.e("mMainActivity:SonDB1", jsonProfile.getString("device_id") + "-" + Utils.getDeviceId(getApplicationContext()));
                                gotoLogin();
                            }
                        }
                    } else {
                        setIn_form_login(false);
                    }

                    if(jsonProfile.has("force_update")) {
                        if(jsonProfile.getString("force_update").equals("1") || jsonProfile.getString("force_update").equals("2")) {
                            MainActivity.couponList = null;
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
//                            String message = "";
//                            if(jsonProfile.has("force_update_message")) {
//                                message = jsonProfile.getString("force_update_message");
//                            }
//
//                            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
//                            builder.setMessage(message)
//                                    .setCancelable(false)
//                                    .setPositiveButton("Cập nhật", new DialogInterface.OnClickListener() {
//                                        public void onClick(DialogInterface dialog, int id) {
//                                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                            try {
//                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                                            } catch (android.content.ActivityNotFoundException anfe) {
//                                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                                            }
//                                        }
//                                    });
//                            AlertDialog alert = builder.create();
//                            alert.setCanceledOnTouchOutside(false);
//                            alert.setCancelable(false);
//                            alert.requestWindowFeature(getWindow().FEATURE_NO_TITLE);
//                            alert.show();
                        }
                    }

                } catch (JSONException e) {
                    Log.e("a ROMROM", "xc");
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    protected void gotoLogin() {
        LoginPresenter p = new LoginPresenter(this, this);
        p.clearInvalidToken();
        Log.e("mMainActivity:SonDB2", "gotoLogin");

        mFirebaseDatabase.removeEventListener(mFireBaseRef);

        setIn_form_login(true);

        Utils.showMessageNoTitle(this, getString(R.string.login_from_different_device_alert), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.couponList = null;
                Intent in = new Intent(getApplicationContext(), LoginActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(in);
                overridePendingTransition(0, R.anim.pull_in_left);
                dialogInterface.dismiss();
            }
        });
    }

    public void gotoReviewDetail(ReviewAllItem mReviewAllItem) {

        Bundle bundle = new Bundle();

        bundle.putString(Constant.F_ARG_REFER_FROM_COUPON, "1");

        bundle.putString(Constant.F_ARG_PRODUCT_IMG, mReviewAllItem.getProduct_image());
        bundle.putString(Constant.F_ARG_PRODUCT_NAME, mReviewAllItem.getProduct_name());
        bundle.putString(Constant.F_ARG_PRODUCT_CONTENT, mReviewAllItem.getContent());
        bundle.putString(Constant.F_ARG_PRODUCT_RATING, mReviewAllItem.getProduct_rating());
        if(mReviewAllItem.getListStart() != null) {
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_5, mReviewAllItem.getListStart().getFive());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_4, mReviewAllItem.getListStart().getFour());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_3, mReviewAllItem.getListStart().getThree());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_2, mReviewAllItem.getListStart().getTwo());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_1, mReviewAllItem.getListStart().getOne());
        }
        bundle.putString(Constant.F_ARG_PRODUCT_REVIEWS, mReviewAllItem.getProduct_review());
        bundle.putString(Constant.F_ARG_PRODUCT_ID, mReviewAllItem.getProduct_id());
        EventBus.getDefault().post(new HidSearchTopBar());
        ReviewsDetailProductScreen reviewsDetailProductScreen = new ReviewsDetailProductScreen();
        reviewsDetailProductScreen.setArguments(bundle);
        setMenuItemSelectedNew(OnMenuItemSelected.SELECT_REVIEWS);
        mViewPager.setCurrentItem(2, false);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.rlNewReview, reviewsDetailProductScreen, "ReviewsDetailProductScreen")
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
        //Send event to ReviewAll Screen and ReviewMe Screen
        RxBus.getInstance().post("reload_data_review_detail");
        ReviewsScreen.mRlNewReview.setVisibility(View.VISIBLE);
    }

    public void setCurrentTab(int Tab) {
        mViewPager.setCurrentItem(Tab, false);
        setMenuItemSelectedNew(OnMenuItemSelected.SELECT_REVIEWS);
    }
    //cuongnv
    public int getCurrentTab() {
        if (mViewPager != null) {
            return mViewPager.getCurrentItem();
        }
        return 0;
    }

    PhoneEditText phone_number;
    public void showVerifyPhone(final Context context, String sector) {
        mContext = context;
        mClickVerifySector = sector;
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog_verify_phone, null);
        phone_number = (PhoneEditText) dialogView.findViewById(R.id.verify_phone_number);
        int maxLength = 11;
        phone_number.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});

        UserInfo userInfo = mPresenter.getUserInfo();
        if(userInfo != null) {
            phone_number.setText(userInfo.getPhone().replace("+84", "0").replaceAll("\\s+", ""));
        }

        dialogConfirm(dialogView,
                getString(R.string.dialog_confirm_text),
                getString(R.string.dialog_retry_exit),
                new DialogCallback() {
                    @Override
                    public void goit(Dialog dialog) {
                        String phone = ((PhoneEditText) dialog.findViewById(R.id.verify_phone_number)).getText().toString().trim();
                        currentPhone = phone;
                        UserInfo userInfo = mPresenter.getUserInfo();
                        if(userInfo != null) {
                            mPhoneNumber = phone;
                            mProgressDialog = Utils.showLoading(mContext);
                            mPresenter.requestCode(userInfo.getToken(), phone, Constant.TYPE_REQUEST);
                        }

                    }

                    @Override
                    public void cancel(Dialog dialog) {
                        dialog.dismiss();
                    }
                });
    }

    public Dialog dialogConfirm(View dialogView, String textYes, String textNo, final DialogCallback callback) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(textYes);
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(textNo);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);

        mDialog = builder.create();
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mDialog.show();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.goit(mDialog);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.cancel(mDialog);
            }
        });

        return mDialog;
    }

    public interface DialogCallback {
        void goit(Dialog dialog);
        void cancel(Dialog dialog);
    }

}