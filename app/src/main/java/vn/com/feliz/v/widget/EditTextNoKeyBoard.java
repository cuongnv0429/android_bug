package vn.com.feliz.v.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;


/**
 * Created by Nguyen Thai Son on 2016-11-13.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class EditTextNoKeyBoard extends EditText {

    public EditTextNoKeyBoard(Context context) {
        super(context);
    }

    public EditTextNoKeyBoard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public EditTextNoKeyBoard(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onCheckIsTextEditor() {
        // TODO Auto-generated method stub
        return false;
    }

    private EditTextNoKeyBoard.KeyImeChange keyImeChangeListener;

    public void setKeyImeChangeListener(EditTextNoKeyBoard.KeyImeChange listener) {
        keyImeChangeListener = listener;
    }

    public interface KeyImeChange {
        public void onKeyIme(int keyCode, KeyEvent event);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (keyImeChangeListener != null) {
            keyImeChangeListener.onKeyIme(keyCode, event);
        }
        return false;
    }
}