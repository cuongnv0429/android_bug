package vn.com.feliz.v.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.util.regex.Pattern;

import butterknife.Unbinder;
import io.realm.Realm;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.eventbus.CancleGps;
import vn.com.feliz.v.fragment.BaseFragment;


/**
 * Created by macbook on 4/11/16.
 */
public abstract class BaseActivity extends AppCompatActivity implements OnResponseListener, Runnable {
    int PERMISSION_ALL = 1;
    String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};
    /**
     * Resource Unbinder
     */
    Unbinder mUnbinder;
    /**
     * Realm Instance
     */
    Realm mRealm;

    int mStatusBarHeight;
    /**
     * Navigation Bar Height
     */
    int mNavigationBarHeight;
    /**
     * Current Fragment
     */
    private BaseFragment mCurrentFragment;
    /**
     * Is Events Locked?
     */
    boolean mIsEventsLocked;
    /**
     * Handler Instance
     */
    Handler mHandler;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private static final String TAG = "Contacts";
    /**
     * Application Info
     */
    ApplicationInfo mApplicationInfo;

    /**
     * Activity is created
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      /*  if (!isLocationEnabled(this)) {
            displayPromptForEnablingGPS(this);
        }*/

        // Get a Realm instance for this thread
        mRealm = Realm.getDefaultInstance();
        mHandler = new Handler();
        //active lockscreen service
        if (Utils.getLockScreenStatus(this)) {
            Intent intent = new Intent();
            // BYME
            // intent.setClass(this, LockScreenService.class);
            //startService(intent);
        }

        Intent startingIntent = getIntent();
        if (startingIntent != null) {

            if (Intent.ACTION_VIEW.equals(startingIntent.getAction())) {
                Uri uri = startingIntent.getData();
                String code = uri.getQueryParameter("gift_code");
                if(code != null && code.equals("")) {
                    Utils.saveReferreCode(getApplicationContext(), code);
                }
            }

            int update_app = startingIntent.getIntExtra("update_app", 0); // Retrieve the id
            if(update_app == 1) {
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }
    }

    /**
     * @return
     * @throws InterruptedException
     * @throws IOException
     */
    public boolean isConnected() throws InterruptedException, IOException {
        String command = "ping -c 1 google.com";
        return (Runtime.getRuntime().exec(command).waitFor() == 0);
    }

    /**
     * check gps
     *
     * @param context
     * @return
     */
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }

    //check speed network
    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        }

        return false;

    }

    /**
     * show dialog confirm enable gps
     *
     * @param activity
     */
    public static void displayPromptForEnablingGPS(final Activity activity) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        final String action = Settings.ACTION_LOCATION_SOURCE_SETTINGS;
        final String message = "Gps đang tắt. Vui lòng bật lên để sử dụng app";

        builder.setMessage(message)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                activity.startActivity(new Intent(action));
                                d.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface d, int id) {
                                d.cancel();
                                EventBus.getDefault().post(new CancleGps(Constant.SPlASH_NAME));
                            }
                        });
        builder.create().show();
    }

    /**
     * request permission version android 6.0
     *
     * @param context
     * @param permissions
     * @return
     */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Starts executing the active part of the class' code. This method is
     * called when a thread is started that has been created with a class which
     * implements {@code Runnable}.
     */
    @Override
    public void run() {
        mIsEventsLocked = false;
    }

    /**
     * Setup Overlay Screen
     */
    void setupOverlayScreen() {
        // Default Values
        mStatusBarHeight = 0;
        mNavigationBarHeight = 0;

        // Get Status bar Height
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Resources resources = getResources();

            boolean hasSoftNavigationBar = hasSoftKeys();

            int statusHeightId = resources.getIdentifier(
                    "status_bar_height", "dimen", "android");
            int navigationHeightId = resources.getIdentifier(
                    "navigation_bar_height", "dimen", "android");

            if (statusHeightId > 0) {
                mStatusBarHeight = getResources().getDimensionPixelSize(statusHeightId);
            }
            if (navigationHeightId > 0 && hasSoftNavigationBar) {
                mNavigationBarHeight = resources.getDimensionPixelSize(navigationHeightId);
            }
        }
    }

    /**
     * Has Soft Navigation Bar
     *
     * @return True/False
     */
    public boolean hasSoftKeys() {
        boolean hasSoftwareKeys;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display d = getWindowManager().getDefaultDisplay();

            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);

            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;

            hasSoftwareKeys = (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
        } else {
            boolean hasMenuKey = ViewConfiguration.get(this).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            hasSoftwareKeys = !hasMenuKey && !hasBackKey;
        }

        return hasSoftwareKeys;
    }

    /**
     * Get Base Handler
     *
     * @return Handler
     */
    public Handler getHandler() {
        return mHandler;
    }

    /**
     * Show Status Bar
     */
    public void showStatusBar() {
        //if (Build.VERSION.SDK_INT < 16) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        /*} else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
    }

    /**
     * Called to process touch screen events.  You can override this to
     * intercept all touch screen events before they are dispatched to the
     * window.  Be sure to call this implementation for touch screen events
     * that should be handled normally.
     *
     * @param ev The touch screen event.
     * @return boolean Return true if this event was consumed.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return !mIsEventsLocked && super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return !mIsEventsLocked && super.dispatchKeyEvent(event);
    }

    public void setKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.dermaclara",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    /**
     * Hide Status Bar
     */
    public void hideStatusBar() {
        //if (Build.VERSION.SDK_INT < 16) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        /*} else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }*/
    }

    /**
     * Lock All Event in Time
     *
     * @param duration long
     */
    public void lockEventIn(long duration) {
        mIsEventsLocked = true;

        try {
            mHandler.removeCallbacks(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler.postDelayed(this, duration);
    }

    /**
     * Get Status Bar Height
     *
     * @return int
     */
    public int getStatusBarHeight() {
        return mStatusBarHeight;
    }

    /**
     * Get Navigation Bar Height
     *
     * @return int
     */
    public int getNavigationBarHeight() {
        return mNavigationBarHeight;
    }

    /**
     * Get Navigation Width
     *
     * @return width
     */
    int getNavigationWidth() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return (Math.min(metrics.widthPixels, metrics.heightPixels) * 5) / 6;
    }

    /**
     * Ser Current Fragment
     *
     * @param fragment BaseFragment
     */
    public void setCurrentFragment(BaseFragment fragment) {
        mCurrentFragment = fragment;
    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {

    }

    /**
     * Receive Response
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Task is Finished and Execute next Task
     */
    @Override
    public final boolean onResponse(ApiTask task, int status) {

        Log.e("e NETOWRK1", this.toString());

        OnPostResponseListener listener = (OnPostResponseListener) this;

        if (this instanceof OnPostResponseListener) {

            if (listener.willProcess(task, status)) {
                return listener.onPostResponse(task, status);
            }
        }

        return listener.onPostResponse(task, status);
    }

    /**
     * Will Process on Child Classes
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Response is Process on Child Classes
     */
    @Override
    public boolean willProcess(ApiTask task, int status) {
        return status == ApiResponseCode.SUCCESS ||
                status != ApiResponseCode.CANNOT_CONNECT_TO_SERVER;
    }

    /**
     * Local Process Response
     *
     * @param task   ApiTask
     * @param status int
     * @return True if Task is Finished and Execute next Task
     */
    private boolean onProcessResponse(ApiTask task, int status) {
        // hideLoading();

        switch (status) {
            case ApiResponseCode.CANNOT_CONNECT_TO_SERVER:
                //   showRetryDialog(task);
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * animation hide image
     *
     * @param img ImageView
     */
    public void fadeOutAndHideImage(final ImageView img) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(3000);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                img.setVisibility(View.INVISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }

    /**
     * animation hide image
     *
     * @param img ImageView
     */
    public void fadeOutAndHideLayout(final RelativeLayout img) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(10);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                img.setVisibility(View.INVISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }

    /**
     * animation hide image
     *
     * @param img ImageView
     */
    public void fadeIntAndShowLayout(final RelativeLayout img) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(10);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                img.setVisibility(View.INVISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeOut);
    }

    /**
     * animation show image
     *
     * @param img ImageView
     */
    public void fadeInAndShowImage(final ImageView img) {
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new AccelerateInterpolator());
        fadeIn.setDuration(3000);

        fadeIn.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                img.setVisibility(View.VISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });

        img.startAnimation(fadeIn);
    }

    /**
     * check speed internet
     *
     * @return boolean
     */
    public boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
//                        try {
//                            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
//                            urlc.setRequestProperty("User-Agent", "Test");
//                            urlc.setRequestProperty("Connection", "close");
//                            urlc.setConnectTimeout(500); //choose your own timeframe
//                            urlc.setReadTimeout(500); //choose your own timeframe
//                            urlc.connect();
//                            int networkcode2 = urlc.getResponseCode();
//                            return (urlc.getResponseCode() == 200);
//                        } catch (IOException e) {
//                            return (false);  //connectivity exists, but no internet.
//                        }
                    }

        }
        return false;
    }
    public void showDialogExitApp(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
        contentDialog.setText(getString(R.string.alert_exit_app));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
               moveTaskToBack(true);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
    /**
     * Destroy all fragments and loaders.
     */
    @Override
    protected void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
            mUnbinder = null;
        }
        super.onDestroy();
    }

    public boolean checkAndAlertNetWork(View view) {
        if(!isConnected(getApplicationContext())) {
            if(view != null) {
                TSnackbar snackbar = TSnackbar
                        .make(view, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
            }
            return false;
        } else {
            return true;
        }
    }

    public void showNetworkAlert(View view) {
        if(view != null) {
            TSnackbar snackbar = TSnackbar
                    .make(view, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
        }
    }

    public void showFireBaseError(View view) {
        if(view != null) {
            TSnackbar snackbar = TSnackbar
                    .make(view, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_LONG);
          //  Utils.showMessageTopbarNextWork(snackbar, getString(R.string.firebase_error), this);
        }
    }

    // Flag to known login auto or handle: not check device_id from firebase when login handle (because not syn online);
    private static Boolean in_form_login = false;

    public static String removeAccent(String s) {
        if (s == null) {
            return "";
        }
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("");
    }


    public static Boolean getIn_form_login() {
        return in_form_login;
    }

    public static void setIn_form_login(Boolean type) {
        in_form_login = type;
    }
}
