package vn.com.feliz.v.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.v.fragment.AdsImageFragment;

public class AdsPagerAdapter extends FragmentStatePagerAdapter {

    private List<AdsImageFragment> mData;

    public AdsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        return mData.get(position);
    }

    @Override
    public int getCount() {
        return mData==null?0:mData.size();
    }

    public void setData(List<AdsImageFragment> data){
        mData = data;
        if (mData == null) mData = new ArrayList<>();
    }
    public void addData(AdsImageFragment data){
        if (mData == null) mData = new ArrayList<>();
        mData.add(data);
        notifyDataSetChanged();
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            Log.e("AdsPagerAdapter: ", "Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}