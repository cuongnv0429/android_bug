package vn.com.feliz.v.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.databinding.FragmentAdsImageBinding;

public class AdsImageFragment extends Fragment{
    private static final String URL = "url";
    private static final String SIZE = "size";
    private static final String HAVE_DOTS = "have_dots";
    private FragmentAdsImageBinding mBinding;
    private String mUrl;
    private int mSize;
    private boolean mHaveDots;
    private Context mContext;
    private OnAdsImageDotClicked mListener;
    private List<Integer> mTouchs;


    public AdsImageFragment() {
    }

    public static AdsImageFragment newInstance(String url, int size, boolean haveDots) {
        AdsImageFragment fragment = new AdsImageFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putInt(SIZE, size);
        args.putBoolean(HAVE_DOTS, haveDots);
        fragment.setArguments(args);
        return fragment;
    }

    //cuongnv
    public static AdsImageFragment newInstance(String url, int size, boolean haveDots, List<Integer> ads) {
        AdsImageFragment fragment = new AdsImageFragment();
        Bundle args = new Bundle();
        args.putString(URL, url);
        args.putInt(SIZE, size);
        args.putIntegerArrayList("ads", (ArrayList<Integer>) ads);
        args.putBoolean(HAVE_DOTS, haveDots);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUrl = getArguments().getString(URL);
        mSize = getArguments().getInt(SIZE);
        mHaveDots = getArguments().getBoolean(HAVE_DOTS, false);
        if (getArguments().containsKey("ads")) {
            mTouchs = (List<Integer>) getArguments().getSerializable("ads");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ads_image, container, false);

        if (!TextUtils.isEmpty(mUrl)) {
            mContext = getContext();
            mBinding.pbAdsImages.setVisibility(View.VISIBLE);
            Picasso.with(getContext()).load(mUrl).resize(mSize, mSize).into(mBinding.ivAds, new Callback() {
                @Override
                public void onSuccess() {
                    mBinding.pbAdsImages.setVisibility(View.GONE);
                    if (mHaveDots) {
                        if (mTouchs != null) {
                            for (int i = 0; i < 3; i++) {
                                try  {
                                    random3Dots(mContext, mSize, mTouchs.get(i));
                                } catch (Exception ex) {
                                    random3Dots(mContext, mSize, i + 2);
                                }
                            }
                        } else {
                            for (int i = 0; i < 3; i++) {
                                random3Dots(mContext, mSize, i + 2);
                            }
                        }

                    }
                }

                @Override
                public void onError() {

                }
            });
        }

        return mBinding.getRoot();
    }

    private void random3Dots(Context context, int size, int positionDot) {
        Log.e("random3Dots", "random3Dots");
        if(context == null) {
            return;
        }
        View dot = new View(context);
        dot.setId(R.id.v_dot);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(100,100);
        SecureRandom sr = new SecureRandom();
//        int posW = sr.nextInt(size-100-100)+100;
//        int posH = sr.nextInt(size-100-100)+100;
        int row = (positionDot - 1) / 3;
        int column = (positionDot - 1) % 3;
        int marginLeft = column * (size /3) + ((size / 6));
        int marginTop = row * (size /3) + ((size / 6));
        lp.setMargins(marginLeft, marginTop, 0, 0);
        dot.setLayoutParams(lp);
        dot.setBackgroundResource(R.drawable.light);
        mBinding.flSlideImage.addView(dot);
        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onDotClicked(v);
                mBinding.flSlideImage.removeView(v);
            }
        });
    }

    public OnAdsImageDotClicked getListener() {
        return mListener;
    }

    public void setListener(OnAdsImageDotClicked mListener) {
        this.mListener = mListener;
    }

    public interface OnAdsImageDotClicked{
        void onDotClicked(View v);
    }
}