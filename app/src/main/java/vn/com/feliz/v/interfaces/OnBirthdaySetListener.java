package vn.com.feliz.v.interfaces;

import java.util.Calendar;

/**
 * Copyright Innotech Vietnam
 *
 */
public interface OnBirthdaySetListener {
    /**
     * @param year        The year that was set.
     * @param monthOfYear The month that was set (0-11) for compatibility
     *                    with {@link Calendar}.
     * @param dayOfMonth  The day of the month that was set.
     */
    void onBirthdaySet(int year, int monthOfYear, int dayOfMonth);
}
