package vn.com.feliz.v.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.p.eventbus.positionProductByFriend;
import vn.com.feliz.v.interfaces.ItemTouchHelperAdapter;
import vn.com.feliz.v.widget.ReviewsScreenHolder.LoadingViewHolder;
import vn.com.feliz.v.widget.ReviewsScreenHolder.ReviewsProductbyFriendHolder;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class Reviews_Product_byFriend_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        ItemTouchHelperAdapter {

    private List<ReviewAllItem> mReviewAllItemList;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;


    public Reviews_Product_byFriend_Adapter(Context context, List<ReviewAllItem> mReviewAllItemList) {
        this.mReviewAllItemList = mReviewAllItemList;
        this.context = context;
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //    Collections.swap(mMessageList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        //   mMessageList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        // Collections.swap(mMessageList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    @Override
    public int getItemViewType(int position) {
        return mReviewAllItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reviews_me, parent, false);
            return new ReviewsProductbyFriendHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ReviewsProductbyFriendHolder) {
            final ReviewAllItem mMessage = mReviewAllItemList.get(position);
            final ReviewsProductbyFriendHolder mReviewsAllHolder = (ReviewsProductbyFriendHolder) holder;
          //  mReviewsAllHolder.item_reviews_name.setTextSize(7 * context.getResources().getDisplayMetrics().density);
            if(mMessage.getProduct_name()!=null) {
                mReviewsAllHolder.item_reviews_name.setText(mMessage.getProduct_name());
            }
         //   mReviewsAllHolder.product_rating.setTextSize(7 * context.getResources().getDisplayMetrics().density);
            mReviewsAllHolder.product_rating.setText("("+mMessage.getProduct_review()+")");
            if (mMessage.getProduct_rating() != null) {
                mReviewsAllHolder.rb_rating_all.setRating(Float.parseFloat(mMessage.getProduct_rating()));
            }
           // mReviewsAllHolder.product_review.setTextSize(6 * context.getResources().getDisplayMetrics().density);
            if (mMessage.getUsefull() == null) {
                mReviewsAllHolder.product_review.setText("0 "+context.getString(R.string.product_review_me_count_zero));
            } else {
                mReviewsAllHolder.product_review.setText(mMessage.getUsefull() +" "+
                        context.getString(R.string.product_review_me_count_zero));
            }
           // mReviewsAllHolder.created_at.setTextSize(5 * context.getResources().getDisplayMetrics().density);
            if(mMessage.getCreated_at()!=null){
                mReviewsAllHolder.created_at.setText(mMessage.getCreated_at());
                mReviewsAllHolder.created_at.setTypeface(Typeface.DEFAULT);
            }
            if(mMessage.getContent()!=null) {
                if (mMessage.getContent().length() >= Constant.MAX_LENGTH_CONTENT_REVIEW) {
                    mReviewsAllHolder.tvShowMore_me.setVisibility(View.VISIBLE);
                    if (!mMessage.isExpand()) {
                        mReviewsAllHolder.content.setText(mMessage.getContent().substring(0, Constant.MAX_LENGTH_CONTENT_REVIEW) + "...");
                        mReviewsAllHolder.tvShowMore_me.setText(R.string.view_more);
                        mReviewsAllHolder.tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_expand_xx, 0, 0, 0);
                    } else {
                        mReviewsAllHolder.content.setText(mMessage.getContent());
                        mReviewsAllHolder.tvShowMore_me.setText(R.string.view_less);
                        mReviewsAllHolder.tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_less_xx, 0, 0, 0);
                    }
                } else {
                    mReviewsAllHolder.content.setText(mMessage.getContent());
                    mReviewsAllHolder.tvShowMore_me.setVisibility(View.GONE);
                }

            }

           /* Glide.with(context).load(mMessage.getProduct_image())
                    .error(R.drawable.loading_new)
                    .centerCrop()
                    .override(96,96)
                    .centerCrop()
                    .crossFade()
                    .placeholder(R.drawable.loading_new).into(mReviewsAllHolder.img_coupon_review_all);*/
            Picasso.with(context).load(Uri.parse(mMessage.getProduct_image())).
                    resizeDimen(R.dimen.dp_0, R.dimen.dp96).onlyScaleDown().placeholder(R.drawable.holder_img).
                    into(mReviewsAllHolder.img_coupon_review_all);
           // mReviewsAllHolder.item_reviews_left_rate.setText(mMessage.getRateTitle());
            mReviewsAllHolder.main_content_me.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().post(new positionProductByFriend(position));
                }
            });

            mReviewsAllHolder.tvShowMore_me.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mMessage.isExpand()) {
                        mReviewsAllHolder.content.setText(mMessage.getContent());
                        mReviewsAllHolder.tvShowMore_me.setText(R.string.view_less);
                        mReviewsAllHolder.tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_less_xx, 0, 0, 0);
                        mMessage.setExpand(true);
                    } else {
                        mReviewsAllHolder.content.setText(mMessage.getContent().substring(0, Constant.MAX_LENGTH_CONTENT_REVIEW) + "...");
                        mReviewsAllHolder.tvShowMore_me.setText(R.string.view_more);
                        mReviewsAllHolder.tvShowMore_me.setCompoundDrawablesWithIntrinsicBounds(R.drawable.icons_expand_xx, 0, 0, 0);
                        mMessage.setExpand(false);
                    }
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }

    @Override
    public int getItemCount() {
        return mReviewAllItemList == null ? 0 : mReviewAllItemList.size();
    }

    public void remove(int position) {
        mReviewAllItemList.remove(position);
        notifyItemRemoved(position);
    }


}