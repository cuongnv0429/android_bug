package vn.com.feliz.v.widget.ReviewsScreenHolder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import vn.com.feliz.R;
import vn.com.feliz.v.interfaces.ItemTouchHelperViewHolder;
import vn.com.feliz.v.widget.AutoResizeTextView;


/**
 * Created by Mr Son on 2016-08-01.
 */
public class ReviewsAllHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {
    public TextView item_reviews_name;
    public AutoResizeTextView product_rating,created_at;
    public TextView content;
    public ImageView img_coupon_review_all;
    public RatingBar rb_rating_all;
   // public LinearLayout mLinearLayoutMain;

    public ReviewsAllHolder(View view) {
        super(view);
        item_reviews_name = (TextView) view.findViewById(R.id.item_reviews_name);
        product_rating = (AutoResizeTextView) view.findViewById(R.id.product_rating);
        content = (TextView) view.findViewById(R.id.content);
        created_at = (AutoResizeTextView) view.findViewById(R.id.created_at);
        img_coupon_review_all = (ImageView) view.findViewById(R.id.img_coupon_review_all);
        rb_rating_all = (RatingBar) view.findViewById(R.id.rb_rating_all);
     //   mLinearLayoutMain = (LinearLayout) view.findViewById(R.id.ln_main_message_item);
        view.setOnClickListener(this);
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }

    @Override
    public void onClick(View view) {

    }
}
