package vn.com.feliz.v.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.v.interfaces.ItemTouchHelperAdapter;
import vn.com.feliz.v.widget.ReviewsScreenHolder.LoadingViewHolder;
import vn.com.feliz.v.widget.ReviewsScreenHolder.ReviewsAllHolder;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class Reviews_All_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        ItemTouchHelperAdapter {

    private List<ReviewAllItem> mReviewAllItemList;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ReviewsAllHolder mReviewsAllHolder;

    public Reviews_All_Adapter(Context context, List<ReviewAllItem> mReviewAllItemList) {
        this.mReviewAllItemList = mReviewAllItemList;
        this.context = context;
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //    Collections.swap(mMessageList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        //   mMessageList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        // Collections.swap(mMessageList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    @Override
    public int getItemViewType(int position) {
        return mReviewAllItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reviews_all, parent, false);
            return new ReviewsAllHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReviewsAllHolder) {
            ReviewAllItem mMessage = mReviewAllItemList.get(position);
            mReviewsAllHolder = (ReviewsAllHolder) holder;
            //   mReviewsAllHolder.item_reviews_name.setTextSize(7 * context.getResources().getDisplayMetrics().density);
            if (mMessage.getProduct_name() != null) {
                String msg = mMessage.getProduct_name();
                Log.e("msg", msg);
                mReviewsAllHolder.item_reviews_name.setText(msg);
            }
            // mReviewsAllHolder.product_rating.setTextSize(7 * context.getResources().getDisplayMetrics().density);
            if (mMessage.getProduct_review() != null) {
                mReviewsAllHolder.product_rating.setText("(" + mMessage.getProduct_review() + ")");
            }
            if (mMessage.getContent() != null) {
                mReviewsAllHolder.content.setText(mMessage.getContent());
            }
            if (mMessage.getProduct_rating() != null) {

                mReviewsAllHolder.rb_rating_all.setRating(Float.parseFloat(mMessage.getProduct_rating()));
            }

            if (mMessage.getCreated_at() != null && mMessage.getIs_sponsor().equals("0")) {
                mReviewsAllHolder.created_at.setText(mMessage.getCreated_at());
                mReviewsAllHolder.created_at.setTextColor(context.getResources().getColor(R.color.item_review_content));
                mReviewsAllHolder.created_at.setTypeface(Typeface.DEFAULT);
            } else if (mMessage.getIs_sponsor().equals("1")) {
                mReviewsAllHolder.created_at.setText(R.string.item_reviews_all_rate);
                mReviewsAllHolder.created_at.setTextColor(context.getResources().getColor(R.color.item_reviews_all_left_rate));
                mReviewsAllHolder.created_at.setTypeface(Typeface.DEFAULT);
            }

            Picasso.with(context).load(Uri.parse(mMessage.getProduct_image())).
                    resizeDimen(R.dimen.dp_0, R.dimen.dp96).onlyScaleDown().placeholder(R.drawable.holder_img).
                    into(mReviewsAllHolder.img_coupon_review_all);
            // mReviewsAllHolder.item_reviews_left_rate.setText(mMessage.getRateTitle());
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }

    @Override
    public int getItemCount() {
        return mReviewAllItemList == null ? 0 : mReviewAllItemList.size();
    }

    public void remove(int position) {
        mReviewAllItemList.remove(position);
        notifyItemRemoved(position);
    }


}