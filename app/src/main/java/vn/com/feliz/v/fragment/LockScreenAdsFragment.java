package vn.com.feliz.v.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.common.Utils;
import vn.com.feliz.databinding.FragmentLockScreenAdsBinding;
import vn.com.feliz.m.AdsModel;
import vn.com.feliz.v.adapter.AdsPagerAdapter;

public class LockScreenAdsFragment extends Fragment implements View.OnClickListener,
        View.OnTouchListener, AdsImageFragment.OnAdsImageDotClicked, SeekBar.OnSeekBarChangeListener,
        MediaPlayer.OnSeekCompleteListener{
    private static final String ARG_PARAM1 = "param1";

    private AdsModel mAdsModel;
    private FragmentLockScreenAdsBinding mBinding;
    private Drawable mDotNormal;
    private Drawable mDotActive;
    private int dotCount = 0;
    private MediaPlayer mMediaPlayer;
    private boolean isMute;
    private int mPagerPosition;
    private int mDeviceWidth;
    private boolean mEndVideo;
    private OnAdsItemClick mListener;
    private boolean isLookedAt;
    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        public void run() {
            if (mBinding.vpAds.getAdapter() != null) {
                if (mPagerPosition >= mBinding.vpAds.getAdapter().getCount()) {
                    mPagerPosition = 0;
                    if (mListener != null && !isLookedAt){
                        isLookedAt = true;
                        mListener.onLookedAtAds(mAdsModel.getAds().getAdsId());
                    }
                } else {
                    mPagerPosition ++;
                }
                mBinding.vpAds.setCurrentItem(mPagerPosition, true);
                handler.postDelayed(runnable, 2000);
            }
        }
    };
    private Context mContext;

    private Handler mHandlerVideo = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                if (mBinding.vvAds.isPlaying()) {
                    int position = mBinding.vvAds.getCurrentPosition();
                    int duration = mBinding.vvAds.getDuration();
                    int progress = position * 100 / (duration == 0 ? 1 : duration);
                    mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                    mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(position));
                    mBinding.tvEndProgress.setText(Utils.convertLongTimeToString(duration));
                    mBinding.sbVideoProgress.setProgress(progress);
                    mBinding.sbVideoProgress.setSecondaryProgress(mBinding.vvAds.getBufferPercentage());
                    if (position >= duration-350) {
                        Log.d("HoangNM", "pause here.....");
                        if (mMediaPlayer != null) {
                            Log.d("HoangNM", "pause.....");
                            if (mListener != null && !isLookedAt){
                                isLookedAt = true;
                                mListener.onLookedAtAds(mAdsModel.getAds().getAdsId());
                            }
                            mMediaPlayer.pause();
                            mEndVideo = true;
                            mHandlerVideo.removeMessages(1);
                            mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                            mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(duration));
                            mBinding.tvEndProgress.setText(Utils.convertLongTimeToString(duration));
                            mBinding.sbVideoProgress.setProgress(100);
                        }

                    }else {
                        mHandlerVideo.sendEmptyMessageDelayed(1, 100);
                    }
                }
            }
        }
    };


    public LockScreenAdsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param ads Parameter 1.
     * @return A new instance of fragment LockScreenAdsFragment.
     */
    public static LockScreenAdsFragment newInstance(AdsModel ads) {
        LockScreenAdsFragment fragment = new LockScreenAdsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, ads);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAdsModel = (AdsModel) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_lock_screen_ads, container, false);
        mBinding.ivStaticAds.setVisibility(View.VISIBLE);
        mBinding.ivAdsMore.setOnClickListener(this);
        mBinding.ivAdsClose.setOnClickListener(this);
        mBinding.ivAdsRepeat.setOnClickListener(this);
        mBinding.ivAdsSend.setOnClickListener(this);
        mBinding.ivPlayVideo.setOnClickListener(this);
        mBinding.ivSpeaker.setOnClickListener(this);
        //mBinding.llAds.setOnClickListener(this);
        mBinding.sbVideoProgress.setOnSeekBarChangeListener(this);
        mBinding.vvAds.setOnTouchListener(this);
        mBinding.vpAds.setOnTouchListener(this);
        mDotNormal = getActivity().getResources().getDrawable(R.drawable.lockscreen_ads_dot_normal);
        mDotActive = getActivity().getResources().getDrawable(R.drawable.lockscreen_ads_dot_active);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int mDeviceWidth = displayMetrics.widthPixels;

        ViewGroup.LayoutParams p = mBinding.llAdsAction.getLayoutParams();
        p.height = mDeviceWidth/7;
        mBinding.llAdsAction.setLayoutParams(p);
        dotCount = 0;
        showAds();
        return mBinding.getRoot();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mMediaPlayer != null) {
            try {
                if (mMediaPlayer.isPlaying()) {
                    mBinding.vvAds.pause();
                    mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                    mHandlerVideo.removeMessages(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.e("onDestroyView", "onDestroyView");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.iv_play_video:
                Log.e("video", mBinding.vvAds.isPlaying() + "__" + mEndVideo);
                if (mBinding.vvAds.isPlaying()) {
                    mBinding.vvAds.pause();
                    mBinding.ivPlayVideo.setImageResource(R.drawable.play_video);
                    mHandlerVideo.removeMessages(1);
                } else {
                    if (mEndVideo) {
                        if (mMediaPlayer != null) {
                            mEndVideo = false;
                            try {
                                mMediaPlayer.seekTo(0);
                                mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                                mBinding.vvAds.start();
                                mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    } else {
                        mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                        mBinding.vvAds.start();
                        mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                    }
                }
                break;
            case R.id.iv_speaker:
                toogleVolumn();
                break;
            default:
                if (mListener != null) mListener.onAdsItemClicked(view);
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.vv_Ads:
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    toogleVolumn();
                }
                break;
            case R.id.vp_ads:
                if (handler != null) {
                    handler.removeCallbacks(runnable);
                }
                break;
        }
        return false;
    }

    @Override
    public void onDotClicked(View v) {
        dotCount ++;
//        Log.e("onDotClicked: ", dotCount + "__");
        if (dotCount == 9){
            dotCount = 0;
            //slide automatically after all dot clicked
            handler.postDelayed(runnable, 2000);
            if (mListener != null) mListener.onAdsItemClicked(v);
            if (mListener != null && !isLookedAt){
                isLookedAt = true;
                mAdsModel.getAds().setStatus("2");
                mListener.onLookedAtAds(mAdsModel.getAds().getAdsId());
            }
        }else if (dotCount == 6) {
            if (mAdsModel.getAds().getTouch().size() > 6) {
                List<Integer> mTouchPoints = new ArrayList<>();
                mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(6, 9));
                AdsImageFragment f3 = AdsImageFragment
                        .newInstance(mAdsModel.getAds().getMediaUrlList().get(2), mDeviceWidth, true,
                                mTouchPoints);
                f3.setListener(this);
                ((AdsPagerAdapter)mBinding.vpAds.getAdapter()).addData(f3);
                mBinding.vpAds.setCurrentItem(2, true);//transition to page with index = 2
            } else {
                List<Integer> mTouchPoints = new ArrayList<>();
                mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(0, 3));
                AdsImageFragment f3 = AdsImageFragment.newInstance(mAdsModel.getAds().getMediaUrlList().get(2),
                        mDeviceWidth, true, mTouchPoints);
                f3.setListener(this);
                ((AdsPagerAdapter)mBinding.vpAds.getAdapter()).addData(f3);
                mBinding.vpAds.setCurrentItem(2, true);//transition to page with index = 2
            }

        }else if (dotCount == 3){
            if (mAdsModel.getAds().getTouch().size() > 3) {
                List<Integer> mTouchPoints = new ArrayList<>();
                mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(3, 6));
                AdsImageFragment f2 = AdsImageFragment
                        .newInstance(mAdsModel.getAds().getMediaUrlList().get(1), mDeviceWidth, true, mTouchPoints);
                f2.setListener(this);
                ((AdsPagerAdapter)mBinding.vpAds.getAdapter()).addData(f2);
                mBinding.vpAds.setCurrentItem(1, true);//transition to page with index = 1
            } else {
                List<Integer> mTouchPoints = new ArrayList<>();
                mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(0, 3));
                AdsImageFragment f2 = AdsImageFragment
                        .newInstance(mAdsModel.getAds().getMediaUrlList().get(1), mDeviceWidth, true, mTouchPoints);
                f2.setListener(this);
                ((AdsPagerAdapter)mBinding.vpAds.getAdapter()).addData(f2);
                mBinding.vpAds.setCurrentItem(1, true);//transition to page with index = 1
            }

        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandlerVideo.removeMessages(1);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //mHandlerVideo.sendEmptyMessage(1);
        mBinding.vvAds.seekTo((int) (seekBar.getProgress() / 100f * mBinding.vvAds.getDuration()));
    }

    @Override
    public void onStop() {
        super.onStop();

        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }




    @Override
    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            if (mEndVideo) {
                mBinding.tvCurrentProgress.setText(Utils.convertLongTimeToString(0));
                mBinding.sbVideoProgress.setProgress(0);
                mediaPlayer.start();
                mBinding.ivPlayVideo.setImageResource(R.drawable.pause_video);
                mEndVideo = false;
            }
            mHandlerVideo.sendEmptyMessage(1);
        }
    }

    private void toogleVolumn(){
        if (mMediaPlayer != null) {
            if (mBinding.vvAds.isPlaying()) {
                if (isMute) {
                    mMediaPlayer.setVolume(1f, 1f);
                    mBinding.ivSpeaker.setImageResource(R.drawable.l1);
                } else {
                    mMediaPlayer.setVolume(0f, 0f);
                    mBinding.ivSpeaker.setImageResource(R.drawable.l2);
                }
                isMute = !isMute;
            }

        }
    }

    private void showAds(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        mDeviceWidth = displayMetrics.widthPixels;
        ViewGroup.LayoutParams lp = mBinding.flAdsContent.getLayoutParams();
        // enable/disable 'xem them' button
        mBinding.ivAdsMore.setSelected(true);
        lp.height = mDeviceWidth;
        mBinding.flAdsContent.setLayoutParams(lp);
        mBinding.ivAdsMore.setEnabled(!TextUtils.isEmpty(mAdsModel.getAds().getLink()));
//        Log.e("showAds", mAdsModel.getAds().getMediaType() + "__" + mAdsModel.getAds().getAdsType());
        switch (mAdsModel.getAds().getMediaType()) {
            case "1"://1 image[
                mBinding.ivStaticAds.setVisibility(View.VISIBLE);
                mContext = getContext();
                mBinding.adsDescription.setTextColor(getResources().getColor(R.color.item_review_content));
                mBinding.adsDescription.setText("Nhận thêm gb$ khi tương tác bằng cách chạm vào 3 điểm sáng hay click Xem Thêm");
                mBinding.adsDescription.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(mAdsModel.getAds().getMediaUrl()).into(mBinding.ivStaticAds, new Callback() {
                    @Override
                    public void onSuccess() {
                        if (mListener != null && !isLookedAt){
                            isLookedAt = true;
                            mListener.onLookedAtAds(mAdsModel.getAds().getAdsId());
                        }
                        if (mAdsModel.getAds().getAdsType().equals("2") && !mAdsModel.getAds().getStatus().equals("2")) {
                            List<Integer> mTouchPoints = new ArrayList<>();
                            mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(0, 3));
                            for (int i = 0; i < 3; i++) {
                                try  {
                                    random3Dots(mContext, mDeviceWidth, mTouchPoints.get(i));
                                } catch (Exception ex) {
                                    random3Dots(mContext, mDeviceWidth, i + 2);
                                }
                            }
                        }
                    }
                    @Override
                    public void onError() {

                    }
                });
                break;
            case "2"://3 images
                mBinding.adsDescription.setTextColor(getResources().getColor(R.color.item_review_content));
                mBinding.adsDescription.setText("Nhận thêm gb$ khi tương tác bằng cách chạm vào 3 điểm sáng hay click Xem Thêm");
                mBinding.adsDescription.setVisibility(View.VISIBLE);
                mBinding.flImagesAds.setVisibility(View.VISIBLE);
                AdsPagerAdapter adapter = new AdsPagerAdapter(LockScreenAdsFragment.this.getChildFragmentManager());
                List<AdsImageFragment> fragments = new ArrayList<>();
                boolean showDots = !mAdsModel.getAds().getStatus().equals("2");
                if (mAdsModel.getAds().getAdsType().equals("2")) {//random 3 dots
                    if (!showDots) {
                        for (int i = 0; i < 3; i++) {
                            AdsImageFragment f1 = AdsImageFragment
                                    .newInstance(mAdsModel.getAds().getMediaUrlList().get(i), mDeviceWidth, showDots);
                            f1.setListener(this);
                            fragments.add(f1);
                        }
                        handler.postDelayed(runnable, 2000);
                    } else {
                        List<Integer> mTouchPoints = new ArrayList<>();
                        mTouchPoints.addAll(0, mAdsModel.getAds().getTouch().subList(0, 3));

                        AdsImageFragment f1 = AdsImageFragment
                                .newInstance(mAdsModel.getAds().getMediaUrlList().get(0), mDeviceWidth, showDots, mTouchPoints);
                        f1.setListener(this);
                        fragments.add(f1);
                    }


                }else {
                    AdsImageFragment f1 = AdsImageFragment
                            .newInstance(mAdsModel.getAds().getMediaUrlList().get(0), 0, false);
                    f1.setListener(this);
                    fragments.add(f1);
                    AdsImageFragment f2 = AdsImageFragment
                            .newInstance(mAdsModel.getAds().getMediaUrlList().get(1), 0, false);
                    f2.setListener(this);
                    fragments.add(f2);
                    AdsImageFragment f3 = AdsImageFragment
                            .newInstance(mAdsModel.getAds().getMediaUrlList().get(2), 0, false);
                    f3.setListener(this);
                    fragments.add(f3);
                    handler.postDelayed(runnable, 2000);
                }
                adapter.setData(fragments);
                mBinding.vpAds.addOnPageChangeListener(new LockScreenAdsFragment.PagerListener());
                mBinding.vpAds.setAdapter(adapter);
                mBinding.vpAds.setOffscreenPageLimit(2);

                break;
            case "3"://video
                if (mAdsModel.getAds().getMediaUrl() == null) {
                    Toast.makeText(getContext(), "Không có video để hiển thị.", Toast.LENGTH_LONG).show();
                    break;
                }
                mBinding.adsDescription.setVisibility(View.GONE);
                mBinding.rlVideoAds.setVisibility(View.VISIBLE);
                mBinding.vvAds.setVideoURI(Uri.parse(mAdsModel.getAds().getMediaUrl()));
                mContext = getContext();
                mBinding.vvAds.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mBinding.pbAdsWaiting.setVisibility(View.GONE);
                        mMediaPlayer = mediaPlayer;
                        mediaPlayer.start();
                        mediaPlayer.seekTo(0);
                        mMediaPlayer.setVolume(0f, 0f);
                        mMediaPlayer.setOnSeekCompleteListener(LockScreenAdsFragment.this);
                        isMute = true;
                        mHandlerVideo.sendEmptyMessageDelayed(1, 1000);
                    }
                });
                mBinding.vvAds.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mBinding.vvAds.seekTo(mBinding.vvAds.getDuration());
                    }
                });
                break;
        }
    }

    private void random3Dots(Context context, int size, int positionDot) {
        if(context == null) {
            return;
        }
        View dot = new View(context);
        dot.setId(R.id.v_dot);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(100,100);
        SecureRandom sr = new SecureRandom();
//        int posW = sr.nextInt(size-100-100)+100;
//        int posH = sr.nextInt(size-100-100)+100;
        int row = (positionDot - 1) / 3;
        int column = (positionDot - 1) % 3;
        int marginLeft = column * (size /3) + ((size / 6));
        int marginTop = row * (size /3) + ((size / 6));
        lp.setMargins(marginLeft, marginTop, 0, 0);
        dot.setLayoutParams(lp);
        dot.setBackgroundResource(R.drawable.light);
        mBinding.flAdsContent.addView(dot);
        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dotCount ++;
//                Log.e("random3Dots: ", dotCount + "__");
                if (dotCount >= 3){
                    dotCount = 0;
                    //TODO
                    //updateAds("5", "0", "");
                    if (mListener != null) mListener.onAdsItemClicked(v);
                }
                mBinding.flAdsContent.removeView(v);
            }
        });
    }

    public OnAdsItemClick getListener() {
        return mListener;
    }

    public void setListener(OnAdsItemClick mListener) {
        this.mListener = mListener;
    }

    private class PagerListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotActive);
                        mBinding.vDot1.setBackground(mDotNormal);
                        mBinding.vDot2.setBackground(mDotNormal);
                    }else {
                        mBinding.vDot0.setBackgroundDrawable(mDotActive);
                        mBinding.vDot1.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot2.setBackgroundDrawable(mDotNormal);
                    }
                    break;
                case 1:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotNormal);
                        mBinding.vDot1.setBackground(mDotActive);
                        mBinding.vDot2.setBackground(mDotNormal);
                    }else {
                        mBinding.vDot0.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot1.setBackgroundDrawable(mDotActive);
                        mBinding.vDot2.setBackgroundDrawable(mDotNormal);
                    }
                    break;
                case 2:
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
                        mBinding.vDot0.setBackground(mDotNormal);
                        mBinding.vDot1.setBackground(mDotNormal);
                        mBinding.vDot2.setBackground(mDotActive);
                    }else {
                        mBinding.vDot0.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot1.setBackgroundDrawable(mDotNormal);
                        mBinding.vDot2.setBackgroundDrawable(mDotActive);
                    }
                    break;
            }
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    public interface OnAdsItemClick{
        void onAdsItemClicked(View v);
        void onLookedAtAds(String adsId);
    }
}
