package vn.com.feliz.v.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.greenrobot.eventbus.EventBus;

import io.realm.Realm;
import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.LoginAuth;
import vn.com.feliz.common.NetworkStateReceiver;
import vn.com.feliz.common.Utils;
import vn.com.feliz.gbreceiver.Referrer;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.LoginModel;
import vn.com.feliz.m.response.DeviceInfo;
import vn.com.feliz.m.response.LoginResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.OnResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.LoginPresenter;
import vn.com.feliz.p.RegisterPresenter;
import vn.com.feliz.services.LCService;
import vn.com.feliz.v.widget.LocationProvider;

/**
 * Created by Nguyen Thai Son on 2016-11-08.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class SplashScreen extends BaseActivity implements OnResponseListener, NetworkStateReceiver.NetworkStateReceiverListener, OnPostResponseListener {
    public SplashScreen() {

    }

    TextView mSplashGoldBeetleBottom;
    ImageView mSplashGoldBeetleSlogan;
    private LoginPresenter mLoginPresenter;
    private String mTokenUser = "";
    private RegisterPresenter getmRegisterPresenter;
    private double mLat;
    private double mLong;
    private String mDeviceId;
    DeviceInfo mDeviceInfo;
    private LocationProvider mLocationProvider;
    private RelativeLayout mRootView;
    private TSnackbar snackbar;
    //private String mCurrentToken;
    private NetworkStateReceiver mNetworkStateReceiver;
    private boolean isNextWorkDisConnect;
    //check connect
    private boolean isConnect;
    public static final String MYPREFSFILE_WELLCOME = Constant.MYPREFSFILE;
    private LinearLayout mSplashView;
    private LinearLayout mWellcomeView;
    private TextView mConfirmWellcome;
    private ImageView imgGibi;
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    private boolean firstRun;
    SharedPreferences settings;
    private ImageView imgSplash;
    private Referrer mReferrer;
    private LoginModel mLoginModel;
    private ProgressBar mSplashLoading;

    private FirebaseAuth mAuth;
    int Measuredwidth = 0;
    int Measuredheight = 0;
    Point size = new Point();
    WindowManager w = getWindowManager();

    private FirebaseAuth mFireBaseAuth;

    private LoginAuth mLoginAuth;

    private boolean mIsInEvent = false;
    private Handler mHandlerMoveScreen;
    private Runnable mRunnableMoveScreen;
    BaseApplication mBaseApplication;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBaseApplication = (BaseApplication) getApplication();
        mTracker = mBaseApplication.getDefaultTracker();
        //run firs time
        //4A:90:A5:EA:AB:FA:2E:F3:5E:11:5A:AD:0D:6E:96:19:E6:27:98:67

        Log.e("MD5 >>: ", Utils.md5("123456abcd"));

        //Set up our Lockscreen
        //makeFullScreen();
        startService(new Intent(this,LCService.class));

        //setContentView(R.layout.activity_main);

        mRealm = Realm.getDefaultInstance();
        // Register Events
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_splash);
        // Set Fonts
        BaseApplication application = BaseApplication.getInstance();
        mSplashGoldBeetleBottom = (TextView) findViewById(R.id.tv_splash_gold_beetle_bottom);
        mSplashGoldBeetleBottom.setTypeface(application.getFontRegular());
        mSplashGoldBeetleSlogan = (ImageView) findViewById(R.id.tv_splash_gold_beetle);
        mWellcomeView = (LinearLayout) findViewById(R.id.rl_wellcome);
        mSplashView = (LinearLayout) findViewById(R.id.ln_splash);
        mSplashLoading = (ProgressBar) findViewById(R.id.splash_login_loading);

        imgGibi = (ImageView) findViewById(R.id.img_gibi_default);
        imgSplash = (ImageView) findViewById(R.id.imgSplash);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(mSplashGoldBeetleSlogan);
        Glide.with(this).load(R.drawable.splash_new).into(imageViewTarget);
        //check nextwork connect create
        try {
            mNetworkStateReceiver = new NetworkStateReceiver();
            mNetworkStateReceiver.addListener(this);
            this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            mReferrer = new Referrer();
            this.registerReceiver(mReferrer, null);

        } catch (Exception e) {
            e.printStackTrace();
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        Measuredwidth = displayMetrics.widthPixels;

        mRootView = (RelativeLayout) findViewById(R.id.rlSplash_container);
        mWellcomeView.setVisibility(View.VISIBLE);
        imgSplash.getLayoutParams().width = Measuredwidth;
        Glide.with(this).load("http://cdn.goldbeetle.vn/event-tet/first/first.jpg")
                .error(R.drawable.loading_new)
                .centerCrop()
                .crossFade()
                .placeholder(R.drawable.loading_new).into(imgSplash);

        settings = getSharedPreferences(MYPREFSFILE_WELLCOME, 0);
        firstRun = settings.getBoolean(Constant.WELLCOME, true);

//        if (ContextCompat.checkSelfPermission(this,
//                Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.READ_PHONE_STATE)) {
//                runTimeSplash();
//            } else {
//                ActivityCompat.requestPermissions(this,
//                        new String[]{Manifest.permission.READ_PHONE_STATE},
//                        Constant.REQUEST_PERMISSIONS_UUID);
//            }
//        } else {
//            runTimeSplash();
//        }

        runTimeSplash();
    }
    /**
     * A simple method that sets the screen to fullscreen.  It removes the Notifications bar,
     *   the Actionbar and the virtual keys (if they are on the phone)
     */
    public void makeFullScreen() {
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        if(Build.VERSION.SDK_INT < 19) { //View.SYSTEM_UI_FLAG_IMMERSIVE is only on API 19+
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        } else {
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

//    @Override
//    public void onBackPressed() {
//        return; //Do nothing!
//    }

    public void unlockScreen(View view) {
        //Instead of using finish(), this totally destroys the process
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    /**
     * runTimeSplash
     */
    private void runTimeSplash() {

        mDeviceId = Utils.generateUUID(getApplicationContext());
        Utils.saveDeviceId(this, mDeviceId);

        mFireBaseAuth = FirebaseAuth.getInstance();

        getmRegisterPresenter = new RegisterPresenter(this, this);
        getmRegisterPresenter.getMetaDataRequest();
        mLoginPresenter = new LoginPresenter(this, this);
        try {

            if (firstRun) {
                mConfirmWellcome = (TextView) findViewById(R.id.btn_confirm_wellcome);
                mSplashView.setVisibility(View.GONE);

                SharedPreferences.Editor editor = settings.edit(); // Open the editor for our settings
                editor.putBoolean(Constant.WELLCOME, false); // It is no longer the first run
                editor.apply();
                mHandlerMoveScreen = new Handler();
                mRunnableMoveScreen = new Runnable() {
                    @Override
                    public void run() {
                        if (mConfirmWellcome != null) {
                            mConfirmWellcome.callOnClick();
                        }
                    }
                };
                mHandlerMoveScreen.postDelayed(mRunnableMoveScreen, 4000);

                mConfirmWellcome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Utils.isNetworkAvailable(getApplicationContext()) && mDeviceId != null) {
                            mSplashView.setVisibility(View.VISIBLE);
                            mWellcomeView.setVisibility(View.GONE);
                            imgGibi.setVisibility(View.GONE);
                            mSplashGoldBeetleSlogan.setVisibility(View.VISIBLE);
                            if (mRunnableMoveScreen != null) {
                                if (mHandlerMoveScreen != null) {
                                    mHandlerMoveScreen.removeCallbacks(mRunnableMoveScreen);
                                    mHandlerMoveScreen = null;
                                }

                            }
                            gotoLogin();
                        }
                    }
                });


            } else {
                mSplashView.setVisibility(View.VISIBLE);
                mWellcomeView.setVisibility(View.GONE);
                imgGibi.setVisibility(View.VISIBLE);
                mSplashGoldBeetleSlogan.setVisibility(View.GONE);
                gotoLogin();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check next work
        if (!isConnected(getApplicationContext())) {
            showNetworkAlert(mRootView);
        }
    }

//    private void connect() {
//        if (!isConnect) {
//            mLocationProvider = new LocationProvider(this, this);
//            mLocationProvider.connect();
//            isConnect = true;

//        }
//
//    }
/*
    private void connect() {
        if (!isConnect) {
            mLocationProvider = new LocationProvider(this, this);
            mLocationProvider.connect();
            isConnect = true;
        }
    }*/

    @Override
    protected void onPause() {
        super.onPause();
        try {
            if (mLocationProvider != null) {
                mLocationProvider.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    /**
     * gotoLogin
     */
    private void gotoLogin() {

        mLoginAuth = new LoginAuth(this);

        if (isConnected(getApplicationContext())) {
            mLoginPresenter = new LoginPresenter(this, this);

            setIn_form_login(true);
            //requestLogin
            if (mLoginPresenter.hasUserInfo() && mLoginPresenter.getUserInfo() != null
                    && mLoginPresenter.getUserInfo().getToken() != null && !mLoginPresenter.getUserInfo().getToken().equals("")) {
                Log.e("gotoLogin", "exists");
                mSplashLoading.setVisibility(View.VISIBLE);
                mLoginAuth.nextStepLoginFB(mLoginCallback);
            } else {
                Log.e("gotoLogin", "new");
                //register new user
                new Handler().postDelayed(new Runnable() {

                    /*713cda826320a67e617595bc3de5486c
                     * Showing splash screen with a timer. This will be useful when you
                     * want to show case your app logo / company
                     */
                    @Override
                    public void run() {
                        Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, R.anim.pull_in_left);
                        // close this activity
                        finish();
                    }
                }, Constant.SPLASH_TIME_OUT);
            }
        } else {
            isNextWorkDisConnect = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (mNetworkStateReceiver != null) {
            mNetworkStateReceiver.removeListener(this);
            this.unregisterReceiver(mNetworkStateReceiver);
        }
        if (mReferrer != null) {
            this.unregisterReceiver(mReferrer);
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        mSplashLoading.setVisibility(View.GONE);
        Log.e("SpalshScreen", status + "__");
//        Log.e("SpalshScreen", new Gson().toJson(task) + "__");
        if (status == Constant.CANNOT_CONNECT_SERVER) {
            showNetworkAlert(mRootView);
            Log.d("Login error", "Retry in 5s");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    gotoLogin();
                }
            }, 20000);
        } else {
            if (getIntent().getExtras() != null) {
                int code = 0;
                String ads_id = "";
                if (getIntent().getExtras().containsKey("push_type")) {
                    try {
                        //background and kill app
                        code = Integer.parseInt(getIntent().getExtras().getString("push_type"));
                    } catch (NumberFormatException ex) {
                        //foreground
                        code = getIntent().getExtras().getInt("push_type");
                    }
                    ads_id = getIntent().getExtras().getString("ads_id");
                }
//                if (code == 15) {
//                    mTracker.enableAdvertisingIdCollection(true);
//                    mTracker.send(new HitBuilders.EventBuilder()
//                            .setCategory(Constant.CATEGORY_ANALYTISC_PUSH_NOTIFICATIONS)
//                            .setAction(Constant.EVENT_ANALYTISC_NOTIFICATION_CLICK + "_" + code + "_" + ads_id)
//                            .build());
//                    final String appPackageName = getPackageName();
//                    try {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//                    }
//                    finish();
//                    return true;
//                }
            }
            if (task.getType() == ApiTaskType.LOGIN) {
                if (status == ApiResponseCode.SUCCESS) {

                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    LoginResponse loginResponse = (LoginResponse) baseResponse.getData();

                    if(!mLoginModel.getFbID().equals(loginResponse.getFacebookId())) {
                        fbLoginOtherAction();
                    } else {
                        Utils.saveToken(this, loginResponse.getToken());
                        Intent i = new Intent(SplashScreen.this, MainActivity.class);
                        if (getIntent().getExtras() != null) {
                            i.putExtras(getIntent().getExtras());
                        }
                        startActivity(i);
                        overridePendingTransition(0, R.anim.pull_in_left);
                        // close this activity
                        finish();
                    }

                } else //if login not success
                {
                    // token timeout or register new user
                    new Handler().postDelayed(new Runnable() {

                        /*
                         * Showing splash screen with a timer. This will be useful when you
                         * want to show case your app logo / company
                         */
                        @Override
                        public void run() {
                            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                            startActivity(i);
                            overridePendingTransition(0, R.anim.pull_in_left);
                            // close this activity
                            finish();
                        }
                    }, 100);

                }
            }
        }
        return true;
    }

    @Override
    public void networkAvailable() {
        Log.e("networkAvailable", ">>");
        mSplashLoading.setVisibility(View.VISIBLE);
        if (isNextWorkDisConnect) {
            if (snackbar != null) {
                snackbar.dismiss();
            }
         /*   mLocationProvider = new LocationProvider(this, this);
            mLocationProvider.connect();*/
            isNextWorkDisConnect = false;
            try {
//                mDeviceId = Settings.Secure.getString(this.getContentResolver(),
//                        Settings.Secure.ANDROID_ID);
//                Utils.saveDeviceId(this, mDeviceId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            runTimeSplash();
        }
    }

    @Override
    public void networkUnavailable() {
        snackbar = TSnackbar
                .make(mRootView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_INDEFINITE);
        Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
        isNextWorkDisConnect = true;
        mSplashLoading.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQUEST_PERMISSIONS_UUID:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        runTimeSplash();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    snackbar = TSnackbar
                            .make(mRootView, " " + getString(R.string.not_alow_permission), TSnackbar.LENGTH_INDEFINITE);
                    Utils.showMessageTopbarNextWork(snackbar, getString(R.string.not_alow_permission), this);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    private LoginActivity.LoginCallback mLoginCallback = new LoginActivity.LoginCallback() {
        @Override
        public void processFB(LoginModel loginModel) {
            mLoginModel = loginModel;
            processAccount();
        }

        @Override
        public void fbLoginOther() {
            mSplashLoading.setVisibility(View.GONE);
            fbLoginOtherAction();
        }

        @Override
        public void cancelFB() {
            mSplashLoading.setVisibility(View.GONE);
            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(i);
            overridePendingTransition(0, R.anim.pull_in_left);
            // close this activity
            finish();
        }

        @Override
        public void errorFB(String error) {

            snackbar = TSnackbar
                    .make(mSplashView, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), SplashScreen.this);

            LoginManager.getInstance().logOut();
            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(i);
            overridePendingTransition(0, R.anim.pull_in_left);
            // close this activity
            finish();
        }
    };

    private void processAccount() {
        mLoginModel.setFirebaseUID(mLoginModel.getFbID() + Constant.FIREBASE_EMAIL_PREFIX);
        mLoginModel.setFirebasePassword(Utils.md5(mLoginModel.getFbID()));
        // Start SingIn and check account exist
        Log.e(">>>>>>>a ", mLoginModel.getFirebasePassword());
        Log.e(">>>>>>>b ", mLoginModel.getFirebaseUID());
        fireBaseLogin();
    }

    private void fireBaseLogin() {
        Log.e(">>>>>>>a1 ", mLoginModel.getFirebasePassword());
        Log.e(">>>>>>>b1 ", mLoginModel.getFirebaseUID());
        try {
            mFireBaseAuth.signInWithEmailAndPassword(mLoginModel.getFirebaseUID(), mLoginModel.getFirebasePassword()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {

                    mSplashLoading.setVisibility(View.GONE);

                    if (task.getException() != null) {
                        task.getException().printStackTrace();
                        task.getException();

                        snackbar = TSnackbar
                                .make(mRootView, getString(R.string.firebase_error), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, getString(R.string.firebase_error), SplashScreen.this);

                        Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, R.anim.pull_in_left);
                        // close this activity
                        finish();
                    }
                    if (task.isSuccessful()) {
                        Log.e(">>>>>>>x2x", "4");
                        FirebaseUser firebaseUser = task.getResult().getUser();
                        mLoginModel.setFirebaseUID(firebaseUser.getUid());
                        if (mLoginPresenter.getUserInfo().isValid()) {
                            mTokenUser = mLoginPresenter.getUserInfo().getToken();
                        } else {
                            mTokenUser = Utils.getToken(SplashScreen.this);
                        }
                        mSplashView.setVisibility(View.VISIBLE);

                        mLoginPresenter.loginRequest(mTokenUser, mLoginModel.getAccessToken());
                    }
                }
            });
        } catch (Exception e) {
            mSplashLoading.setVisibility(View.GONE);
            mLoginPresenter.submitErrorLoginFB((e.getMessage().toString()));
            snackbar = TSnackbar
                    .make(mRootView, getString(R.string.firebase_error), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.firebase_error), this);
            e.printStackTrace();

            Intent i = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(i);
            overridePendingTransition(0, R.anim.pull_in_left);
            // close this activity
            finish();
        }
    }

    private void fbLoginOtherAction() {
        dialogConfirm(
                getString(R.string.login_facebook_login_other_account),
                getString(R.string.login_facebook_retry),
                getString(R.string.login_facebook_register_new),
                new LoginActivity.DialogCallback() {
                    @Override
                    public void goit(Dialog dialog) {
                        dialog.dismiss();
                        mSplashLoading.setVisibility(View.GONE);
                        mLoginAuth.nextStepLoginFB(mLoginCallback);
                    }

                    @Override
                    public void cancel(Dialog dialog) {
                        dialog.dismiss();
                        LoginManager.getInstance().logOut();

                        Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                        startActivity(i);
                        overridePendingTransition(0, R.anim.pull_in_left);
                        // close this activity
                        finish();
                    }
                }
        );
    }

    public void dialogConfirm(String message, String textYes, String textNo, final LoginActivity.DialogCallback callback) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
        tvYes.setText(textYes);
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
        tvNo.setText(textNo);
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);

        contentDialog.setText(message);

        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.goit(dialog);
            }
        });

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.cancel(dialog);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LoginAuth.callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}