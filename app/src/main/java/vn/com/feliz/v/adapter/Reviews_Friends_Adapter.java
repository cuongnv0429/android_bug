package vn.com.feliz.v.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import vn.com.feliz.R;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.v.interfaces.ItemTouchHelperAdapter;
import vn.com.feliz.v.widget.ReviewsScreenHolder.LoadingViewHolder;
import vn.com.feliz.v.widget.ReviewsScreenHolder.ReviewsFriendsHolder;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class Reviews_Friends_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements
        ItemTouchHelperAdapter {

    private List<ReviewAllItem> mReviewFriendsItemList;
    private Context context;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ReviewsFriendsHolder mReviewsFriendsHolder;

    public Reviews_Friends_Adapter(Context context, List<ReviewAllItem> mReviewFriendsItemList) {
        this.mReviewFriendsItemList = mReviewFriendsItemList;
        this.context = context;
    }


    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        //    Collections.swap(mMessageList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        //   mMessageList.remove(position);
        notifyItemRemoved(position);
    }

    public void swap(int firstPosition, int secondPosition) {
        // Collections.swap(mMessageList, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    @Override
    public int getItemViewType(int position) {
        return mReviewFriendsItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reviews_friend, parent, false);
            return new ReviewsFriendsHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading_item, parent, false);
            return new LoadingViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReviewsFriendsHolder) {
            ReviewAllItem mFriendsItem = mReviewFriendsItemList.get(position);
            mReviewsFriendsHolder = (ReviewsFriendsHolder) holder;
          //  mReviewsFriendsHolder.item_reviews_friend_name.setTextSize(7 * context.getResources().getDisplayMetrics().density);
            mReviewsFriendsHolder.item_reviews_friend_name.setText(mFriendsItem.getFullname());
            String danhgia = context.getString(R.string.friend_dangia);
        //    mReviewsFriendsHolder.user_review.setTextSize(6 * context.getResources().getDisplayMetrics().density);
            if(mFriendsItem.getUser_review()!=null|| mFriendsItem.getUser_review().equals("")) {
                mReviewsFriendsHolder.user_review.setText(danhgia + "(" + mFriendsItem.getUser_review() + ")");
            }else {
                mReviewsFriendsHolder.user_review.setText(danhgia + "(" + 0 + ")");
            }
           // mReviewsFriendsHolder.tvDate.setTextSize(6 * context.getResources().getDisplayMetrics().density);
            mReviewsFriendsHolder.tvDate.setText(mFriendsItem.getCreated_at());
            mReviewsFriendsHolder.tvConttent.setText(mFriendsItem.getContent());
            Picasso.with(context).load(Uri.parse(mFriendsItem.getAvatar())).
                    resizeDimen(R.dimen.fb_avatar_large, R.dimen.fb_avatar_large).placeholder(R.drawable.holder_img).
                    into(mReviewsFriendsHolder.imgAvatarFriendReview);

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }

    @Override
    public int getItemCount() {
        return mReviewFriendsItemList == null ? 0 : mReviewFriendsItemList.size();
    }

    public void remove(int position) {
        mReviewFriendsItemList.remove(position);
        notifyItemRemoved(position);
    }


}