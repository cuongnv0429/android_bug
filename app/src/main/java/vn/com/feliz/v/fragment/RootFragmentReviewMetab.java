package vn.com.feliz.v.fragment;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vn.com.feliz.R;

/**
 * Created by Nguyen Thai Son on 2016-12-24.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RootFragmentReviewMetab extends BaseFragment {
    public RootFragmentReviewMetab() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.root_fragment_review_me, container, false);
        /* Inflate the layout for this fragment */
       /* FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.root_view_me_new, new ReviewsMeScreen());
        transaction.commit();*/
        android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ReviewsMeScreen fragment = new ReviewsMeScreen();
        fragmentTransaction.add(R.id.root_view_me_new, fragment);
        fragmentTransaction.commit();

 //getChildFragmentManager().beginTransaction().replace(R.id.root_view_me_new, new ReviewsMeScreen()).addToBackStack(null).commit();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {

        }
    }

}
