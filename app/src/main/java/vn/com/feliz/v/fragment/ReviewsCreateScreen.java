package vn.com.feliz.v.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.eventbus.RequestFocus;
import vn.com.feliz.v.activity.BaseActivity;
import vn.com.feliz.v.widget.VerticalScrollview;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsCreateScreen extends BaseFragment implements OnPostResponseListener, View.OnKeyListener, ViewTreeObserver.OnGlobalLayoutListener {
    private String mNameProduct;
    private String mRatingvalue;
    private String mReview;
    @BindView(R.id.edtNameProduct)
    EditText mEdtNameProduct;
    @BindView(R.id.rbNewReview)
    RatingBar mRatingBar;
    @BindView(R.id.edtComment)
    EditText mComment;
    @BindView(R.id.rlRoot_create_review)
    LinearLayout mRlRoot;
    @BindView(R.id.scroll)
    VerticalScrollview mScrollView;
    /*  @BindView(R.id.edtCommentLn)
      ScrollView mScrollViewChild;*/
    private TSnackbar snackbar;
    @BindView(R.id.vTemp)
    View mTView;
    @BindView(R.id.tvProductName)
    TextView mProductName;
    @BindView(R.id.tvRating)
    TextView tvRating;
    @BindView(R.id.tvReview)
    TextView tvReview;
    @BindView(R.id.btnScanbarRv)
    TextView mScanbar;
    private CountDownTimer waitTimer;
    final Handler handler = new Handler();
    private SharedPreferences sharedPrefCr;
    private boolean isFirst;
    private boolean isKeyboardOpen = false;
    public ReviewsCreateScreen() {
        // Required empty public constructor
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().post(this);
        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }


    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_new_review, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        sharedPrefCr = mActivity.getSharedPreferences("reviewcreate", Context.MODE_PRIVATE);


        if (sharedPrefCr.getString("mNameProduct", "") != null) {

            mNameProduct = sharedPrefCr.getString("mNameProduct", "");
            mEdtNameProduct.setText(mNameProduct);


        }
        if (sharedPrefCr.getString("mRatingvalue", "") != null && sharedPrefCr.getString("mRatingvalue", "") != "") {
            mRatingvalue = sharedPrefCr.getString("mRatingvalue", "");
            mRatingBar.setRating(Float.parseFloat(mRatingvalue));
        }
        if (sharedPrefCr.getString("mComment", "") != null && sharedPrefCr.getString("mComment", "") != "") {
            mReview = sharedPrefCr.getString("mComment", "");
            mComment.setText(mReview);

        }
        SharedPreferences.Editor editor = sharedPrefCr.edit();
        editor.putString("mNameProduct", "");
        editor.putString("mRatingvalue", "");
        editor.putString("mComment", "");
        editor.commit();

        //
        mRlRoot.getViewTreeObserver().addOnGlobalLayoutListener(this);
        mTView.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    if (((String) o).equalsIgnoreCase("back_scan_barcode_review")) {
                        if (getView() != null) {
                            backEventSoftKey();
                        }
                    }
                }
            }
        });
        return rootView;
    }

    /**
     * backEventSoftKey
     */
    private void backEventSoftKey() {
        try {
            if (isFragmentUIActive()) {
                Log.e("createScreen", "createScreen");
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                SharedPreferences sharedPref = mActivity.getSharedPreferences("reviewcreate", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString("mNameProduct", mEdtNameProduct.getText().toString());
                                editor.putString("mRatingvalue", String.valueOf(mRatingBar.getRating()));
                                editor.putString("mComment", mComment.getText().toString());
                                editor.commit();
                                onBack();
                                return true;
                            }
                        }
                        return false;
                    }


                });
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        backEventSoftKey();
    }


    @Subscribe
    public void requestFocusKeyback(RequestFocus requestFocus) {
        backEventSoftKey();
    }


    @OnClick(R.id.imgBackReview)
    public void onBack() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        builder.setView(dialogView);
        LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
        TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);

        tvYes.setText(getString(R.string.dialog_retry_yes));
        LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
        TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);

        tvNo.setText(getString(R.string.dialog_retry_no));
        TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);

        contentDialog.setText(getString(R.string.dialog_review_new));
        final Dialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences sharedPref = mActivity.getSharedPreferences("reviewcreate", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("mNameProduct", mEdtNameProduct.getText().toString());
                editor.putString("mRatingvalue", String.valueOf(mRatingBar.getRating()));
                editor.putString("mComment", mComment.getText().toString());
                editor.commit();
                if (mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsCreateScreen") != null) {
                    mActivity.getSupportFragmentManager().beginTransaction()
                            .remove(mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsCreateScreen")).commit();
                }
                mActivity.getSupportFragmentManager().popBackStack(R.id.rlNewReview, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                mActivity.getSupportFragmentManager().executePendingTransactions();
                Fragment fragment = mActivity.getSupportFragmentManager().findFragmentById(R.id.rlNewReview);
                //create review from ReviewsListProductUserScreen
                if (fragment == null) {
                    return;
                }
                if (fragment instanceof ReviewsListProductUserScreen) {
                    // send event back from CreateReviewScreen to ReviewsListProductUserScreen
                    RxBus.getInstance().post("refresh_listener_back_press");
                } else if ( fragment instanceof ReviewsDetailProductScreen) {
                    // send event back from CreateReviewScreen to ReviewsDetailProductScreen
                    RxBus.getInstance().post("refresh_listener_back_press_detail");
                }else if (fragment instanceof ReviewsDetailProductByUserScreen) {
                    RxBus.getInstance().post("refresh_listener_back_press_detail_product");
                } else {
                    RxBus.getInstance().post("back_review_all");
                    ReviewsScreen.mRlNewReview.setVisibility(View.GONE);
                }

            }
        });
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

    }

    /**
     * get Value reviews
     */
    private void getValueReview() {
        mNameProduct = mEdtNameProduct.getText().toString();
        mRatingvalue = String.valueOf(mRatingBar.getRating());
        mReview = mComment.getText().toString();
    }

    /**
     * validate input
     */
    private void validateInput() {
        if (mEdtNameProduct.getText().toString().trim().equals("") || mEdtNameProduct.getText() == null) {
            //setup snackBar
            snackbar = TSnackbar
                    .make(mRlRoot, " " + getString(R.string.validate_review_name_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.validate_review_name_product), mActivity);
            Utils.vibarte(mActivity);
            mEdtNameProduct.requestFocus();
        } else if (mRatingBar.getRating() <= 0) {
            snackbar = TSnackbar
                    .make(mRlRoot, " " + getString(R.string.validate_review_rating_bar_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.validate_review_rating_bar_product), mActivity);
            Utils.vibarte(mActivity);
        } else if (mComment.getText().toString().trim().equals("") || mComment.getText() == null) {
            snackbar = TSnackbar
                    .make(mRlRoot, " " + getString(R.string.validate_review_rating_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.validate_review_rating_product), mActivity);
            Utils.vibarte(mActivity);
            mComment.requestFocus();
        } else {
            getValueReview();
            ScanbarcodeReviewScreen mReviewScreen = new ScanbarcodeReviewScreen();
            Bundle arguments = new Bundle();
            arguments.putString(Constant.F_ARG_PRODUCT_NAME_REVIEWS_CREATE, mNameProduct);
            arguments.putString(Constant.F_ARG__REVIEWS_RATING_VALUE_CREATE, mRatingvalue);
            arguments.putString(Constant.F_ARG_COMMENT_CREATE, mReview);
            mReviewScreen.setArguments(arguments);
            FragmentTransaction transaction = getFragmentManager()
                    .beginTransaction();
            transaction.addToBackStack(null);
            transaction.add(R.id.rlNewReview, mReviewScreen, "ScanbarcodeReviewScreen");
            transaction.commitAllowingStateLoss();
//            Fragment fragment = mActivity.getSupportFragmentManager().findFragmentByTag("ReviewsCreateScreen");
//            if (fragment != null) {
//                mActivity.getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
//            }

        }
    }

    @OnClick(R.id.btnScanbarRv)
    public void scanBarcode() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!BaseActivity.hasPermissions(mActivity, Constant.PERMISSIONSCAMERA)) {
                checkAndRequestPermissionsCamera();
            } else {
                validateInput();
            }
        } else {
            validateInput();
        }
    }

    /**
     * check permission camera
     *
     * @return
     */
    private boolean checkAndRequestPermissionsCamera() {
        int writepermission = ContextCompat.checkSelfPermission(mActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), Constant.REQUEST_PERMISSIONS_CAMERA);
            }
            return false;
        }
        return true;
    }

    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }


    @Override
    public void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(mActivity);

    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        Utils.hideSoftKeyboard(mActivity);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getView() != null) {
            backEventSoftKey();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getView() != null) {
            backEventSoftKey();
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        return true;
    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        return super.willProcess(task, status);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            Utils.hideSoftKeyboard(mActivity);
        }
        return false;
    }

    @Override
    public void onGlobalLayout() {
        //check keyboard show/hide
        Rect r = new Rect();
        if (mRlRoot != null) {
            mRlRoot.getWindowVisibleDisplayFrame(r);
            int screenHeight = mRlRoot.getRootView().getHeight();
            // r.bottom is the position above soft keypad or device button.
            // if keypad is shown, the r.bottom is smaller than that before.
            int keypadHeight = screenHeight - r.bottom;

            if (keypadHeight > screenHeight * 0.15) {
                if (mEdtNameProduct.isFocused() && mComment.getText().toString().trim().equals("")) {
                    mComment.setHint(getString(R.string.edtReviewHint));
                }
                // keyboard is opened
                mTView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, keypadHeight + 10000));
                isFirst = true;

            } else {
                if (!mComment.getText().toString().trim().equals("")) {
                    mTView.setLayoutParams(new RelativeLayout.LayoutParams(0, 10000));
                } else {
                    mTView.setLayoutParams(new RelativeLayout.LayoutParams(0, 0));
                }
                if (isFirst) {
                    isFirst = false;
                    backEventSoftKey();
                }


            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Constant.REQUEST_PERMISSIONS_CAMERA:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    validateInput();
                } else {
                    snackbar = TSnackbar
                            .make(getView(), " " + getString(R.string.not_alow_permission), TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbarNextWork(snackbar, getString(R.string.not_alow_permission), mActivity);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
