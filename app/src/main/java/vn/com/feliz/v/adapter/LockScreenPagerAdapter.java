package vn.com.feliz.v.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import vn.com.feliz.v.fragment.LockScreenFragment;
import vn.com.feliz.v.fragment.LockScreenSideFragment;

public class LockScreenPagerAdapter extends FragmentStatePagerAdapter {
    public LockScreenPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return new LockScreenFragment();
            default:
                return new LockScreenSideFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try{
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException){
            Log.e("LockScreenPager: ", "Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}
