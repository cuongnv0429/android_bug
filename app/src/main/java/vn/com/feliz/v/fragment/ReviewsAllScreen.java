package vn.com.feliz.v.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.functions.Action1;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.m.response.ListAllReviewsResponse;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.data.RxBus;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.BackListAll;
import vn.com.feliz.p.eventbus.RefreshListMe;
import vn.com.feliz.p.eventbus.SearchReviewAll;
import vn.com.feliz.p.eventbus.refreshDataReview;
import vn.com.feliz.v.adapter.Reviews_All_Adapter;
import vn.com.feliz.v.interfaces.listviewcustom.OnItemClickListener;
import vn.com.feliz.v.interfaces.listviewcustom.OnLoadMoreListener;
import vn.com.feliz.v.interfaces.listviewcustom.SwipeableItemClickListener;

import static vn.com.feliz.v.fragment.ReviewsScreen.imgBackMain;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReviewsAllScreen extends BaseFragment implements OnPostResponseListener, OnItemClickListener, OnLoadMoreListener{
    private Reviews_All_Adapter mReviewsAllAdapter;
    @BindView(R.id.recycleReviewsAll)
    RecyclerView mRecyclerView;
    @BindView(R.id.swipe_review_all)
    SwipeRefreshLayout mSwipeReviewAll;
    @BindView(R.id.btn_retry_all)
    Button mBtnRetry;
    private List<ReviewAllItem> mReviewAllItemList = new ArrayList<>();
    private List<ReviewAllItem> mReviewAllItemListSum = new ArrayList<>();
    private OnLoadMoreListener mOnLoadMoreListener;
    private ReviewsPrensenter mPrensenter;
    private String mTokenUser;
    //TransactionInfo
    private int mPageOffset = 0;
    private int mPageLimit = 10;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private ListAllReviewsResponse mReviewAllResponse;
    public static int totalAll;
    private ProgressDialog mProgressDialog;
    boolean isSearch;
    private boolean isBack;
    private String keyWordSearch;
    public static TextView mtvNodataAll;

    static final String ARG_PARAM1 = "param1";
    private boolean mFistLoad = false;
    //check load data not yet ?
    private boolean isLoad = false;

    public ReviewsAllScreen() {
        // Required empty public constructor
    }

    public static ReviewsAllScreen newInstance(boolean first_load) {
        ReviewsAllScreen fragment = new ReviewsAllScreen();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM1, first_load);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Called to do initial creation of a fragment.  This is called after
     * {@link #onAttach(Activity)} and before
     * {@link #onCreateView(LayoutInflater, ViewGroup, Bundle)}.
     * <p/>
     * <p>Note that this can be called while the fragment's activity is
     * still in the process of being created.  As such, you can not rely
     * on things like the activity's content view hierarchy being initialized
     * at this point.  If you want to do work once the activity itself is
     * created, see {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>Any restored child fragments will be created before the base
     * <code>Fragment.onCreate</code> method returns.</p>
     *
     * @param savedInstanceState If the fragment is being re-created from
     *                           a previous saved state, this is the state.
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // mPresenter = new LoginPresenter(mActivity, this);
        mPrensenter = new ReviewsPrensenter(mActivity, this);

        // Register Events
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        if (getArguments() != null) {
            mFistLoad = getArguments().getBoolean(ARG_PARAM1);
        }

        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
    }

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_reviews_all, container, false);
        // Bind Resources
        mUnbinder = ButterKnife.bind(this, rootView);
        //setup recycleView
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnItemTouchListener(new SwipeableItemClickListener(mActivity, this));
        mtvNodataAll = (TextView) rootView.findViewById(R.id.tvNodataAll);
        setOnLoadMoreListener(this);
        //set Data first
        if(!mFistLoad) {
            isLoad = true;
            mPageOffset = 0;
            setData(mPageOffset + "", mPageLimit + "");
        }
        inItRecycleViewsLoadmore();
        mBtnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLoad = true;
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset + "", mPageLimit + "");
            }
        });
        mSwipeReviewAll.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isLoad = true;
                isSearch = false;
                mPageOffset = 0;
                setData(mPageOffset + "", mPageLimit + "");
            }
        });
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof String) {
                    //Load data when no data
                    if (((String) o).equalsIgnoreCase("reload_data_review_detail")) {
                        if (!isLoad) {
                            isLoad = true;
                            isSearch = false;
                            mPageOffset = 0;
                            setData(mPageOffset + "", mPageLimit + "");
//                            inItRecycleViewsLoadmore();
                        }
                    }
                    if (((String) o).equalsIgnoreCase("back_review_all")) {
                        if (getView() != null) {
                            refreshBack();

                        }
                    }
                }
            }
        });
        return rootView;
    }

    /**
     * inItRecycleView
     */
    private void inItRecycleViewsLoadmore() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);


                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mPageOffset = mPageOffset + mPageLimit;
                            mOnLoadMoreListener.onLoadMore();

                        }
                    }
                }
            }
        });
    }

    /**
     * setup recycle view
     */
    private void setUpRecycleView() {
        if (mRecyclerView != null) {
            mReviewsAllAdapter = new Reviews_All_Adapter(mActivity, mReviewAllItemListSum);
            mRecyclerView.setAdapter(mReviewsAllAdapter);
        }
    }

    /**
     * set Data first load
     */
    private void setData(String mOffset, String mLimit) {
        if (mPrensenter.hasUserInfo()) {
          /*  mProgressDialog = Utils.showLoading(getContext());*/
            mTokenUser = mPrensenter.getUserInfo().getToken();
            //API get ListReviews
            //   mProgressDialog = Utils.showLoading(mActivity);
            mPrensenter.getListAllReviews(mTokenUser, "all", mOffset,
                    mLimit);
        }
        if (mReviewsAllAdapter != null) {
            setUpRecycleView();
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        Log.e("setMenuVisibility","setMenuVisibility");
        refreshBack();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e("setUserVisibleHint","setUserVisibleHint");
        refreshBack();
    }

    /**
     * set enable/disable loadMore
     */


    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        switch (task.getType()) {
            case GET_LIST_ALL:
                mBtnRetry.setVisibility(View.GONE);
                mSwipeReviewAll.setVisibility(View.VISIBLE);
                if (status == ApiResponseCode.SUCCESS) {
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    mReviewAllResponse = (ListAllReviewsResponse) baseResponse.getData();
                    try {
                        mReviewAllItemList = mReviewAllResponse.getListReviews();
                        totalAll = 0;
                        if (mReviewAllResponse.getTotal() != null) {
                            totalAll = Integer.parseInt(mReviewAllResponse.getTotal());
                            ReviewsScreen.tvTotal.setText("(" + String.valueOf(totalAll) + ")");
                        }
                        if (mSwipeReviewAll.isRefreshing()) {
                            mReviewAllItemListSum = new ArrayList<>();
                            if (mReviewAllItemList != null) {
                                mReviewAllItemListSum = mReviewAllItemList;
                            }
                            setUpRecycleView();
                            mSwipeReviewAll.setRefreshing(false);
                            loading = true;
                        } else {
                            if (mReviewAllItemList != null && !isBack) {
                                if (loading) {
                                    mReviewAllItemListSum = mReviewAllItemList;
                                    setUpRecycleView();
                                } else if (!loading && !isBack) {
                                    loading = true;
                                    mReviewAllItemListSum.addAll(mReviewAllItemList);
                                    mReviewsAllAdapter.notifyDataSetChanged();
                                }

                            } else if (mReviewAllItemList == null && isSearch) {
                                mReviewAllItemListSum.clear();
                                mReviewsAllAdapter.notifyDataSetChanged();
                                mtvNodataAll.setVisibility(View.VISIBLE);
                            }
                        }
                        isBack = false;
                    } catch (Exception e) {
                        if (mReviewAllItemListSum.size() > 0) {
                            mtvNodataAll.setVisibility(View.GONE);
                        } else {
                            mtvNodataAll.setVisibility(View.VISIBLE);
                        }
                        e.printStackTrace();
                    }

                }
                if (mReviewAllItemListSum.size() > 0) {
                    mtvNodataAll.setVisibility(View.GONE);
                } else {
                    mtvNodataAll.setVisibility(View.VISIBLE);
                }
                break;

        }
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        Utils.hideSoftKeyboard(mActivity);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void refreshBack() {
        if (getView() != null) {
            if (isFragmentUIActive()) {
                Log.e("backAll", "backAll");
                getView().setFocusableInTouchMode(true);
                getView().requestFocus();
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                mActivity.showDialogExitApp(mActivity);
                                return true;
                            }
                            mActivity.lockEventIn(500);
                        }
                        return false;

                    }


                });
            }
        }
    }

    @Subscribe
    public void backListener(BackListAll backListAll) {
        mRecyclerView.setVisibility(View.VISIBLE);
        imgBackMain.setVisibility(View.GONE);
        isBack = true;
        Utils.hideSoftKeyboard(mActivity);
    }

    @Subscribe
    public void listenerReloadListAll(RefreshListMe refreshListMe) {
        mRecyclerView.setVisibility(View.VISIBLE);
        imgBackMain.setVisibility(View.VISIBLE);
        mRecyclerView.smoothScrollBy(0, 2);

    }

    @Override
    public boolean willProcess(ApiTask task, int status) {
        if (task.getThrowable() != null) {
            if (task.getType() == ApiTaskType.GET_LIST_ALL) {
                Utils.hideLoading(mProgressDialog);
                mSwipeReviewAll.setRefreshing(false);
                mReviewAllItemList = new ArrayList<>();
                mReviewAllItemListSum = new ArrayList<>();
                if (mReviewsAllAdapter != null) {
                    mReviewsAllAdapter.notifyDataSetChanged();
                }
                mSwipeReviewAll.setVisibility(View.GONE);
                mtvNodataAll.setVisibility(View.VISIBLE);
                mBtnRetry.setVisibility(View.VISIBLE);
            }
        }
        return super.willProcess(task, status);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
       /* if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }*/
    }


    @Override
    public void onItemClick(View view, int position) {
        Utils.hideSoftKeyboard(mActivity);
        isBack = false;
        ReviewsDetailProductScreen reviewsDetailProductScreen = new ReviewsDetailProductScreen();
        Bundle bundle = new Bundle();

        bundle.putString(Constant.F_ARG_PRODUCT_IMG, mReviewAllItemListSum.get(position).getProduct_image());
        bundle.putString(Constant.F_ARG_PRODUCT_NAME, mReviewAllItemListSum.get(position).getProduct_name());
        bundle.putString(Constant.F_ARG_PRODUCT_CONTENT, mReviewAllItemListSum.get(position).getContent());
        bundle.putString(Constant.F_ARG_PRODUCT_RATING, mReviewAllItemListSum.get(position).getProduct_rating());
        if (mReviewAllItemListSum.get(position).getListStart() != null) {
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_5, mReviewAllItemListSum.get(position).getListStart().getFive());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_4, mReviewAllItemListSum.get(position).getListStart().getFour());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_3, mReviewAllItemListSum.get(position).getListStart().getThree());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_2, mReviewAllItemListSum.get(position).getListStart().getTwo());
            bundle.putString(Constant.F_ARG_PRODUCT_RATING_1, mReviewAllItemListSum.get(position).getListStart().getOne());
        }
        bundle.putString(Constant.F_ARG_PRODUCT_REVIEWS, mReviewAllItemListSum.get(position).getProduct_review());
        bundle.putString(Constant.F_ARG_PRODUCT_ID, mReviewAllItemListSum.get(position).getProduct_id());
        reviewsDetailProductScreen.setArguments(bundle);

        mActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.rlNewReview, reviewsDetailProductScreen, "ReviewsDetailProductScreen")
                .addToBackStack(null)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commitAllowingStateLoss();
        getChildFragmentManager().executePendingTransactions();
        ReviewsScreen.mRlNewReview.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadMore() {
        mReviewAllItemListSum.add(null);
        mReviewsAllAdapter.notifyItemInserted(mReviewAllItemListSum.size() - 1);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Remove loading item
                if (mReviewAllItemListSum.size() > 0) {
                    mReviewAllItemListSum.remove(mReviewAllItemListSum.size() - 1);
                    mReviewsAllAdapter.notifyItemRemoved(mReviewAllItemListSum.size());
                }
                if (isSearch) {
                    isSearch = false;
                    requestGetListAllSearchReviewNextPage();
                } else {
                    requestGetListAllReviewNextPage();
                }

            }
        }, 1000);
    }

    /**
     * request list
     */
    private void requestGetListAllReviewNextPage() {
        mPrensenter.getListAllReviews(mTokenUser, Constant.TYPE_REVIEWS, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit));
    }

    /**
     * request list
     */
    private void requestGetListAllSearchReviewNextPage() {
        mPrensenter.getListAllReviewsSearch(mTokenUser, Constant.TYPE_REVIEWS, String.valueOf(mPageOffset),
                String.valueOf(mPageLimit), keyWordSearch);
    }

    /**
     * set load more listener
     *
     * @param mOnLoadMoreListener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    @Subscribe
    public void getEventSearch(SearchReviewAll searchReviewAll) {
        if (getView() != null) {
            if (isFragmentUIActive()) {
                getView().setFocusableInTouchMode(true);
                getView().setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN) {
                            if (keyCode == KeyEvent.KEYCODE_BACK) {
                                mActivity.showDialogExitApp(mActivity);
                                return true;
                            }
                            mActivity.lockEventIn(500);
                        }
                        return false;

                    }


                });
            }
        }
        isLoad = true;
        isSearch = true;
        loading = true;
        mPageOffset = 0;
        keyWordSearch = searchReviewAll.getKeySearch().trim();
        if(mTokenUser != null) {
            mPrensenter.getListAllReviewsSearch(mTokenUser, Constant.TYPE_REVIEWS, String.valueOf(mPageOffset),
                    String.valueOf(mPageLimit), keyWordSearch);
            inItRecycleViewsLoadmore();
        }
    }

    @Subscribe
    public void feFreshData(refreshDataReview refreshData) {
        if (refreshData.getTypeScreen().toLowerCase().equals("review_all") && !isLoad && isFragmentUIActive()) {
            refreshBack();
            isLoad = true;
            loading = true;
            mPageOffset = 0;
            setData(mPageOffset + "", mPageLimit + "");
            inItRecycleViewsLoadmore();
        }
    }

    public boolean isFragmentUIActive() {
        return isAdded() && !isDetached() && !isRemoving();
    }
}
