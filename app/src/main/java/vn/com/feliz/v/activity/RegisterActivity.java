package vn.com.feliz.v.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.Set;

import vn.com.feliz.R;
import vn.com.feliz.application.BaseApplication;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.NetworkStateReceiver;
import vn.com.feliz.common.Utils;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.network.services.ApiTaskType;
import vn.com.feliz.p.RegisterPresenter;
import vn.com.feliz.p.eventbus.RegisterFirebase;
import vn.com.feliz.v.widget.KeyBoardFragment;
import vn.com.feliz.v.widget.PhoneEditText;


public class RegisterActivity extends BaseActivity implements NetworkStateReceiver.NetworkStateReceiverListener, View.OnClickListener, FacebookCallback<LoginResult>,
        KeyBoardFragment.onKeyBoardEvent, OnPostResponseListener, TextWatcher, ViewTreeObserver.OnGlobalLayoutListener {
    private PhoneEditText mPhone, mFacebookEdt;
    private Button mConfirm, mRegisterFaceBook;
    private LinearLayout mConfirmView, mFaceBookRegisterView;
    private ImageView mGooldbeele, mGooldbeeleFaceBook;
    //facebook
    private CallbackManager mCallbackManagerFacebook;
    private LoginButton mLoginButtonFacebook;
    private String mPhoneNumber;
    private boolean isChangeMobile;
    private RegisterPresenter mRegisterPresenter;
    private URL profile_pic;
    private String name;
    private String email;
    private String birthday;
    private String ID_Facebook;
    private String gender;
    private String adress;
    private FirebaseAuth mAuth;
    public String mUid = "";
    private RelativeLayout mScroll;
    private String mPassWordFireBase;
    String emailFirebase = "";
    private String mUrlAvt;
    private ProgressDialog mProgressDialog;
    private ImageView mTerm;
    private TSnackbar snackbar;
    private RelativeLayout mTopbar;
    private TextView mTitleBarText;
    private ImageView mImgBack;
    private NetworkStateReceiver mNetworkStateReceiver;
    private String accessToken;
    public static String messageChangeMobile;
    private  RelativeLayout mRlLoading;
    private boolean mFirebaseLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_register);
        //check nextwork connect create
        try {
            mNetworkStateReceiver = new NetworkStateReceiver();
            mNetworkStateReceiver.addListener(this);
            this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAuth = FirebaseAuth.getInstance();
        mRegisterPresenter = new RegisterPresenter(RegisterActivity.this, this);
        mPhone = (PhoneEditText) findViewById(R.id.edt_phone);
        mFacebookEdt = (PhoneEditText) findViewById(R.id.edt_facebook);
        mPhone.setImeActionLabel("Enter", KeyEvent.KEYCODE_ENTER);
        mFacebookEdt.setImeActionLabel("Enter", KeyEvent.KEYCODE_ENTER);
        mRegisterFaceBook = (Button) findViewById(R.id.btn_confirm_facebook);
        mFaceBookRegisterView = (LinearLayout) findViewById(R.id.facebook_ln);
        mGooldbeeleFaceBook = (ImageView) findViewById(R.id.img_gb_ic_facebook);
        mLoginButtonFacebook = (LoginButton) findViewById(R.id.login_button);
        mConfirm = (Button) findViewById(R.id.btn_confirm);
        mConfirmView = (LinearLayout) findViewById(R.id.mainLayout);
        mGooldbeele = (ImageView) findViewById(R.id.img_gb_ic);
        // mScroll = (ScrollView) findViewById(R.id.scroll_register);
        mTerm = (ImageView) findViewById(R.id.activity_term);
        mRegisterFaceBook.setEnabled(false);
        mConfirm.setEnabled(false);
        mTopbar = (RelativeLayout) findViewById(R.id.rl_top_bar_back_verify_code);
        mTitleBarText = (TextView) findViewById(R.id.activity_register_title_bar_verify_code);
        mImgBack = (ImageView) findViewById(R.id.main_topbar_back_register_change_profile);
        mScroll = (RelativeLayout) findViewById(R.id.rl_container_register);
        mRlLoading = (RelativeLayout) findViewById(R.id.rlLoading);
        //mRlLoading.setVisibility(View.GONE);
        //set Event click
        setEvent();
        // setup keyboard hide when create activity
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        // Set Fonts
        BaseApplication application = BaseApplication.getInstance();
        mConfirm.setTypeface(application.getFontLight());
        mRegisterFaceBook.setTypeface(application.getFontLight());
        //eventbus
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //if  is change mobile{
        SharedPreferences pre = getSharedPreferences
                (Constant.MYPREFSFILE_MOBILE_CHANGE, MODE_PRIVATE);
        isChangeMobile = pre.getBoolean(Constant.IS_CHANGE, false);
        // if change number phone from profile
        if (isChangeMobile) {
            mTopbar.setVisibility(View.VISIBLE);
            mTitleBarText.setText(getString(R.string.register_change_number_phone));
            mTerm.setVisibility(View.INVISIBLE);
        } else {
            //default
            mTopbar.setVisibility(View.INVISIBLE);
            mTerm.setVisibility(View.VISIBLE);


            //check notification settings
            Set<String> allowedApps = NotificationManagerCompat.getEnabledListenerPackages(this);
            if (allowedApps != null && allowedApps.contains(getPackageName())) {
                Log.d("HoangNM", "Granted the permission");
            } else {
                Utils.showMessageNoTitle(this, "Bạn cần cho phép bật tính năng truy cập thông báo.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                            Intent ii = new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS");
                            startActivity(ii);
                            dialogInterface.dismiss();
                        } else {
                            try {
                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                intent.setData(Uri.parse("package:" + getApplicationContext().getPackageName()));
                                startActivity(intent);
                                dialogInterface.dismiss();
                            } catch (ActivityNotFoundException ex) {
                                Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
                                startActivity(intent);
                                dialogInterface.dismiss();
                            }
                        }
                    }
                });
            }
         /*   //check nextwork connect
            try {
                mNetworkStateReceiver = new NetworkStateReceiver();
                mNetworkStateReceiver.addListener(this);
                this.registerReceiver(mNetworkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            } catch (Exception e) {
                e.printStackTrace();
            }*/
           /*try {

                if(hasNavBar(this)) {
                    // Do whatever you need to do, this device has a navigation bar
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)mKey_board_containerPhone.getLayoutParams();
                    params.setMargins(0, 0, 0, 240);
                    mKey_board_containerPhone.setLayoutParams(params);

                    LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)mKey_board_container.getLayoutParams();
                    params1.setMargins(0, 0, 0,330);
                    mKey_board_container.setLayoutParams(params1);
                }
        }catch (Exception e){
               e.printStackTrace();
           }*/
        }

        // temp code
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        String currentHomePackage = resolveInfo.activityInfo.packageName;
        Log.d("HoangNM", "current home: " + currentHomePackage);*/
        mProgressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        mConfirmView.getViewTreeObserver().addOnGlobalLayoutListener(this);

    }

    /**
     * @param fbID
     */
    private void getUID(final String fbID) {
    }

    protected void signupFirebase() {
        // Remove register
    }

    protected void onResume() {
        super.onResume();
        Log.e("mLoginInRegister", "1");
        try {
            this.getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    /**
     * set On listener
     */
    private void setEvent() {
        mPhone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EnterEvent();
                return true;
            }
        });
        mFacebookEdt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                EnterEvent();
                return true;
            }
        });

        mCallbackManagerFacebook = CallbackManager.Factory.create();
        //request ReadPermissions Facebook
        mLoginButtonFacebook.setReadPermissions(Arrays.asList(
                Constant.PUBLIC_PROFILE, Constant.EMAIL, Constant.USER_BIRTHDAY, Constant.USER_FRIENDS, "user_location"

        ));
 /*     "user_actions.books",  "user_actions.fitness", "user_actions.music",
                "user_actions.news",
                "user_actions.video"
                , "user_education_history",
                "user_events", "user_games_activity",
                "user_hometown",
                "user_location",
                "user_relationship_details",
                "user_religion_politics", "user_status", "user_work_history"*/
        mLoginButtonFacebook.registerCallback(mCallbackManagerFacebook, this);

        mConfirm.setOnClickListener(this);
        mRegisterFaceBook.setOnClickListener(this);
        mPhone.setOnClickListener(this);
        mFacebookEdt.setOnClickListener(this);
        mConfirmView.setOnClickListener(this);
        mFaceBookRegisterView.setOnClickListener(this);
        mPhone.addTextChangedListener(this);
        mFacebookEdt.addTextChangedListener(this);
        mTerm.setOnClickListener(this);
        mImgBack.setOnClickListener(this);
        try {
            mFacebookEdt.setFilters(new InputFilter[]
                    {new InputFilter.LengthFilter(mRegisterPresenter.hasMetaInfo().getVerify_code_limit())});
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * enter event
     */
    private void EnterEvent() {
        if (mConfirmView.getVisibility() == View.VISIBLE) {
            try {
                //get phone number put to ProfileOBJ
                mPhoneNumber = mPhone.getText().toString().trim();
                if (mPhoneNumber.length() > 0) {
                    Utils.hideSoftKeyboard(this);

                    //register first
                    if (isChangeMobile) {
                        String token_user = mRegisterPresenter.getUserInfo().getToken();
                        mProgressDialog = Utils.showLoading(RegisterActivity.this);
                        mRegisterPresenter.requestCode(token_user, mPhoneNumber, Constant.TYPE_REQUEST);
                    } else {
                        //register first
                        mProgressDialog = Utils.showLoading(RegisterActivity.this);
                        mRegisterPresenter.requestCode("", mPhoneNumber, Constant.TYPE_REQUEST);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            if (mFacebookEdt.getText().length() == mRegisterPresenter.hasMetaInfo().getVerify_code_limit()) {
                mRegisterPresenter.verifyCode(mFacebookEdt.getText().toString().trim(), Constant.TYPE_REQUEST);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_confirm:
                lockEventIn(1500);
                //  mConfirm.setEnabled(false);
                // mConfirm.setBackgroundColor(getResources().getColor(R.color.btn_color_ccc));
                try {
                    //get phone number put to ProfileOBJ
                    mPhoneNumber = mPhone.getText().toString().trim();
                    if (isChangeMobile) {
                        String token_user = mRegisterPresenter.getUserInfo().getToken();
                        Log.e("register", token_user);
                        mProgressDialog = Utils.showLoading(RegisterActivity.this);
                        mRegisterPresenter.requestCode(token_user, mPhoneNumber, Constant.TYPE_REQUEST);

                    } else {
                        mProgressDialog = Utils.showLoading(RegisterActivity.this);
                        mRegisterPresenter.requestCode("", mPhoneNumber, Constant.TYPE_REQUEST);

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.btn_confirm_facebook:
                lockEventIn(1500);
                mProgressDialog = Utils.showLoading(RegisterActivity.this);
                //mRlLoading.setVisibility(View.VISIBLE);
                mRegisterPresenter.verifyCode(mFacebookEdt.getText().toString().trim(), Constant.TYPE_REQUEST);
                break;
            case R.id.activity_term:
                try {
                    final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                    dialog.setContentView(R.layout.fragment_terms_conditions_screen);
                    dialog.setTitle(getString(R.string.term_title));
                    WebView mTextContent = (WebView) dialog.findViewById(R.id.textContent);

                    String textContent;
                    textContent = "<html><body><p align=\"justify\">";
                    textContent += mRegisterPresenter.hasMetaInfo().getTerm();
                    textContent += "</p></body></html>";
                    // mTextContent.loadData(textContent, "text/html", "utf-8");
                    mTextContent.getSettings().setJavaScriptEnabled(true);
                    mTextContent.loadDataWithBaseURL("", textContent, "text/html", "UTF-8", "");
                    Button btnCancel = (Button) dialog.findViewById(R.id.btn_confirm_term_condition);
                    BaseApplication application = BaseApplication.getInstance();
//                    btnCancel.setTypeface(application.getFontHelvetical());
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case R.id.main_topbar_back_register_change_profile:

                // TODO Auto-generated method stub
                //back when change profile
                if (mConfirmView.getVisibility() == View.VISIBLE && isChangeMobile) {
                    SharedPreferences mSettings_mobile;
                    mSettings_mobile = this.getSharedPreferences(Constant.MYPREFSFILE_MOBILE_CHANGE, 0);
                    SharedPreferences.Editor editorMobile = mSettings_mobile.edit();
                    editorMobile.clear();
                    editorMobile.putBoolean(Constant.IS_CHANGE, true);
                    editorMobile.commit();
//                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
//                    startActivity(i);
                    finish();
                }//back when create new user
                if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.custom_dialog, null);
                    builder.setView(dialogView);
                    LinearLayout btnYes = (LinearLayout) dialogView.findViewById(R.id.btnYes);
                    TextView tvYes = (TextView) dialogView.findViewById(R.id.tvYes);
                    tvYes.setText(getString(R.string.dialog_retry_yes));
                    LinearLayout btnNo = (LinearLayout) dialogView.findViewById(R.id.btnNo);
                    TextView tvNo = (TextView) dialogView.findViewById(R.id.tvNo);
                    tvNo.setText(getString(R.string.dialog_retry_no));
                    TextView contentDialog = (TextView) dialogView.findViewById(R.id.contentDialog);
                    contentDialog.setText(getString(R.string.dialog_change_mobile_veryficode));
                    final Dialog dialog = builder.create();
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCancelable(false);
                    dialog.show();
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            mFacebookEdt.getText().clear();
                            mFaceBookRegisterView.setVisibility(View.GONE);
                            mConfirmView.setVisibility(View.VISIBLE);
                            if (isChangeMobile) {
                                mTopbar.setVisibility(View.VISIBLE);
                                mTitleBarText.setText(getString(R.string.register_change_number_phone));
                                mTerm.setVisibility(View.INVISIBLE);
                            } else {
                                //default
                                mTopbar.setVisibility(View.INVISIBLE);
                                mTerm.setVisibility(View.VISIBLE);


                            }
                        }
                    });
                    btnNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });
                 /*   Utils.showRetryDialogVeryficodeRegister(this,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    mFacebookEdt.getText().clear();
                                    mFaceBookRegisterView.setVisibility(View.GONE);
                                    mConfirmView.setVisibility(View.VISIBLE);
                                    if (isChangeMobile) {
                                        mTopbar.setVisibility(View.VISIBLE);
                                        mTitleBarText.setText(getString(R.string.register_change_number_phone));
                                        mTerm.setVisibility(View.INVISIBLE);
                                    } else {
                                        //default
                                        mTopbar.setVisibility(View.INVISIBLE);
                                        mTerm.setVisibility(View.VISIBLE);


                                    }
                                }
                            },
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    // Todo cancel all pending task
                                }
                            });*/


                }

        }


    }


    /**
     * showHideIcon loadding
     */
    private void showIcon() {
        if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            fadeInAndShowImage(mGooldbeeleFaceBook);
            //  mGooldbeeleFaceBook.setVisibility(View.VISIBLE);
        } else if (mConfirmView.getVisibility() == View.VISIBLE) {
            fadeInAndShowImage(mGooldbeele);
        }
    }

    /**
     * showHideIcon loadding
     */
    private void hideIcon() {
        if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            fadeOutAndHideImage(mGooldbeeleFaceBook);
        } else if (mConfirmView.getVisibility() == View.VISIBLE) {
            fadeOutAndHideImage(mGooldbeele);
        }
    }


    /**
     * CallBack log in facebook
     *
     * @param loginResult loginResult
     */
    @Override
    public void onSuccess(LoginResult loginResult) {
        //mRlLoading.setVisibility(View.GONE);
        try {

            accessToken = loginResult.getAccessToken()
                    .getToken();

            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object,
                                                GraphResponse response) {
                            try {
                                String id = null;
                                id = object.getString("id");
                                if (id != null) {
                                    ID_Facebook = id;
                                } else {
                                    Profile profile = Profile.getCurrentProfile();
                                    id = profile.getId();
                                    ID_Facebook = id;
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                name = object.getString(Constant.NAME);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                email = object.getString(Constant.EMAIL);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                gender = object.getString(Constant.GENDER);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                birthday = object.getString(Constant.BIRTHDAY);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject locationObj = object.getJSONObject("location");
                                adress = locationObj.getString("name");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                JSONObject pictureObj = object.getJSONObject("picture");
                                JSONObject data = pictureObj.getJSONObject("data");
                                mUrlAvt = data.getString("url");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //skip birthday field
                            if (birthday == null) {
                                birthday = "";
                            }
                            //get uid
                            //mRlLoading.setVisibility(View.VISIBLE);
                            EventBus.getDefault().post(new RegisterFirebase(ID_Facebook));
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString(Constant.FIELDS,
                    "id,name,email,gender,birthday,picture,location");
            request.setParameters(parameters);
            request.executeAsync();

        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Subscribe
    public void register(RegisterFirebase email) {
        if (email.getIdFb() != null) {
            emailFirebase = email.getIdFb();
            Log.e("register", emailFirebase);
        } else {
            emailFirebase = Utils.md5(""+Math.random()) + "-unk";
        }
        Log.e("register", emailFirebase);
        getUID(emailFirebase);
    }

    @Override
    public void onCancel() {
        Log.v("LoginActivity", "cancle login");
    }

    @Override
    public void onError(FacebookException error) {
        try {
            Log.v("LoginActivity", error.getCause().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode,
                                    Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        mCallbackManagerFacebook.onActivityResult(requestCode, responseCode, data);

    }

    @Override
    public void numberIsPressed(String total) {
        //if facebook register view is visible
        checkVisibleView(total);
    }

    @Override
    public void doneButtonPressed(String total) {

    }

    @Override
    public void backLongPressed() {
        //if facebook register view is visible
        if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            mFacebookEdt.setText(Constant.KEY_NULL);
        }  //if facebook register view is not visible
        else if (mConfirmView.getVisibility() == View.VISIBLE) {
            mPhone.setText(Constant.KEY_NULL);
        }
    }

    @Override
    public void backButtonPressed(String total) {
        checkVisibleView(total);
    }

    /**
     * '
     * Check view facebook register or phone visible
     *
     * @param total total
     */
    private void checkVisibleView(String total) {
        if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            mFacebookEdt.setText(total);
        } else if (mConfirmView.getVisibility() == View.VISIBLE) {
            mPhone.setText(total);
        }
    }


    @Override
    public void onBackPressed() {
        //back when change profile
        if (mConfirmView.getVisibility() == View.VISIBLE && isChangeMobile) {
            SharedPreferences mSettings_mobile;
            mSettings_mobile = this.getSharedPreferences(Constant.MYPREFSFILE_MOBILE_CHANGE, 0);
            SharedPreferences.Editor editorMobile = mSettings_mobile.edit();
            editorMobile.clear();
            editorMobile.putBoolean(Constant.IS_CHANGE, true);
            editorMobile.commit();
//            Intent i = new Intent(RegisterActivity.this, MainActivity.class);
//            startActivity(i);
            finish();
        }//back when create new user
        if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {

            Utils.showRetryDialogVeryficodeRegister(this,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            mFacebookEdt.getText().clear();
                            mFaceBookRegisterView.setVisibility(View.GONE);
                            mConfirmView.setVisibility(View.VISIBLE);
                            if (isChangeMobile) {
                                mTopbar.setVisibility(View.VISIBLE);
                                mTitleBarText.setText(getString(R.string.register_change_number_phone));
                                mTerm.setVisibility(View.INVISIBLE);
                            } else {
                                //default
                                mTopbar.setVisibility(View.INVISIBLE);
                                mTerm.setVisibility(View.VISIBLE);
                            }
                        }
                    },
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // Todo cancel all pending task
                        }
                    });
        }

    }

    @Override
    protected void onPause() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
        Utils.hideSoftKeyboard(this);
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        Log.e("", task.getType() + "");
        Utils.hideLoading(mProgressDialog);
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            showNetworkAlert(mScroll);
        } else {

            if (task.getType() == ApiTaskType.REQUEST_CODE) {
                Utils.hideLoading(mProgressDialog);
                if (snackbar != null) {
                    snackbar.dismiss();
                }
                if (status == ApiResponseCode.SUCCESS || status == ApiResponseCode.REQUEST_EXISTS) {
                    mFaceBookRegisterView.setVisibility(View.VISIBLE);
                    mConfirmView.setVisibility(View.GONE);

                    //show topBar
                    mTopbar.setVisibility(View.VISIBLE);
                    //default view
                    mTitleBarText.setText(getString(R.string.register_facebook_title_bar));
                    //reset flag keyboard show hide
                    //if change profile;
                    if (isChangeMobile) {
                        mTitleBarText.setText(getString(R.string.register_change_number_phone));
                        mRegisterFaceBook.setText(getString(R.string.register_facbook_btn_change_mobile));
                    } else {
                        //TODO
                    }
                } else {
                    String err = mRegisterPresenter.messageErr();
                    //setup snackBar
                    snackbar = TSnackbar
                            .make(mScroll, " " + err, TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, err, this);
                    Utils.vibarte(this);
                    if (mPhone != null) {
                        mPhone.setBackgroundResource(R.drawable.edt_background_error);
                    }
                }
            } else if (task.getType() == ApiTaskType.VERIFY_CODE) {
                if (status == ApiResponseCode.SUCCESS) {
                    Utils.hideLoading(mProgressDialog);
                    //change profile
                    if (isChangeMobile) {
                        String token_user = mRegisterPresenter.getUserInfo().getToken();
                        mRegisterPresenter.changeProfile("", token_user, mPhoneNumber, "",
                                Constant.PROGRESS_STATUS, "", "");
                        mRegisterPresenter.updatePhone(mPhoneNumber);
                        mRegisterPresenter.updateIsVerify("1");
                    } else {
                        if (accessToken != null) {
                            LoginManager.getInstance().logOut();
                            mLoginButtonFacebook.performClick();
                        } else {
                            mLoginButtonFacebook.performClick();
                        }
                    }
                    //    }
                } else {
                    //mRlLoading.setVisibility(View.GONE);
                    Utils.hideLoading(mProgressDialog);
                    String err = mRegisterPresenter.messageErr();
                    //setup snackBar
                    snackbar = TSnackbar
                            .make(mScroll, " " + err, TSnackbar.LENGTH_SHORT);
                    Utils.showMessageTopbar(snackbar, err, this);
                    Utils.vibarte(this);
                    if (mFacebookEdt != null) {
                        mFacebookEdt.setBackgroundResource(R.drawable.edt_background_error);
                    }
                }
            } else if (task.getType() == ApiTaskType.REGISTER) {
                // Register not using in this activity
                // From 17-01-2017 by Thomas
            } else if (task.getType() == ApiTaskType.CHANGE_PROFILE) {
                Utils.hideLoading(mProgressDialog);
                if (status == ApiResponseCode.SUCCESS) {
                    messageChangeMobile = mRegisterPresenter.messageErr();
                    SharedPreferences mSettings_mobile;
                    mSettings_mobile = this.getSharedPreferences(Constant.MYPREFSFILE_MOBILE_CHANGE, 0);
                    SharedPreferences.Editor editorMobile = mSettings_mobile.edit();
                    editorMobile.clear();
                    editorMobile.putBoolean(Constant.IS_CHANGE, true);
                    editorMobile.commit();
                    Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(i);
                    overridePendingTransition(0, R.anim.pull_in_left);
                    // close this activity
                    finish();
                } else {
                    Utils.vibarte(this);

                }
            }
        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (mConfirmView.getVisibility() == View.VISIBLE) {

            //disable button
            if (s.toString().trim().length() == 0) {
                mConfirm.setEnabled(false);
                mConfirm.setBackgroundColor(getResources().getColor(R.color.btn_color_ccc));
            } else {
                mConfirm.setEnabled(true);
                mConfirm.setBackgroundColor(getResources().getColor(R.color.btn_confirm_background));
            }

        } else if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {


            //  Utils.vibarte(this);
            if (s.toString().trim().length() > Constant.MAX_LENGTH) {
                Utils.vibarte(this);
            }
            //disable button
            if (s.toString().trim().length() == 0) {
                mRegisterFaceBook.setEnabled(false);
                mRegisterFaceBook.setBackgroundColor(getResources().getColor(R.color.btn_color_ccc));
            } else if (s.toString().trim().length() == Constant.MAX_LENGTH) {
                mRegisterFaceBook.setEnabled(true);
                mRegisterFaceBook.setBackgroundColor(getResources().getColor(R.color.btn_confirm_background));
            } else {
                mRegisterFaceBook.setEnabled(false);
                mRegisterFaceBook.setBackgroundColor(getResources().getColor(R.color.btn_color_ccc));
            }
        }

    }

    @Override
    public void afterTextChanged(Editable s) {
        int position = s.length();
        if (mConfirmView.getVisibility() == View.VISIBLE) {
            Editable etext = mPhone.getText();
            // Selection.setSelection(etext, position);
        } else if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
            Editable etext = mFacebookEdt.getText();
            // Selection.setSelection(etext, position);
        }
    }


    /**
     * Destroy all fragments and loaders.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mNetworkStateReceiver != null) {
            mNetworkStateReceiver.removeListener(this);
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if(mProgressDialog!=null){
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void networkAvailable() {
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    @Override
    public void networkUnavailable() {
        //setup snackBar
        snackbar = TSnackbar
                .make(mScroll, " " + getString(R.string.wifi_speed), TSnackbar.LENGTH_SHORT);
        Utils.showMessageTopbarNextWork(snackbar, getString(R.string.wifi_speed), this);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        boolean handleReturn = super.dispatchTouchEvent(ev);

        View view = getCurrentFocus();

        int x = (int) ev.getX();
        int y = (int) ev.getY();

        if (view instanceof EditText) {
            View innerView = getCurrentFocus();

            if (ev.getAction() == MotionEvent.ACTION_UP &&
                    !getLocationOnScreen((EditText) innerView).contains(x, y)) {

                InputMethodManager input = (InputMethodManager)
                        getSystemService(INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(getWindow().getCurrentFocus()
                        .getWindowToken(), 0);
            }
        }

        return handleReturn;
    }

    /**
     * @param mEditText
     * @return
     */
    protected Rect getLocationOnScreen(EditText mEditText) {
        Rect mRect = new Rect();
        int[] location = new int[2];

        mEditText.getLocationOnScreen(location);

        mRect.left = location[0];
        mRect.top = location[1];
        mRect.right = location[0] + mEditText.getWidth();
        mRect.bottom = location[1] + mEditText.getHeight();

        return mRect;
    }

    @Override
    public void onGlobalLayout() {
        //check keyboard show/hide
        Rect r = new Rect();
        mConfirmView.getWindowVisibleDisplayFrame(r);
        int screenHeight = mConfirmView.getRootView().getHeight();
        // r.bottom is the position above soft keypad or device button.
        // if keypad is shown, the r.bottom is smaller than that before.
        int keypadHeight = screenHeight - r.bottom;

        if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.

            // keyboard is closed
            new Handler().postDelayed(new Runnable() {

                /*713cda826320a67e617595bc3de5486c
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */
                @Override
                public void run() {
                    showIcon();
                }
            }, 0);

            if (mConfirmView.getVisibility() == View.VISIBLE) {
               /* if(isChangeMobile){
                    mTopbar.setVisibility(View.INVISIBLE);
                }*/
                View linearLayout = findViewById(R.id.mainConfirm);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
                params.setMargins(0, 0, 0, keypadHeight + 300);
                linearLayout.setLayoutParams(params);
                linearLayout.requestLayout();

            } else if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {

                View linearLayout = findViewById(R.id.mainFacebookLogin);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
                if (isChangeMobile) {
                    params.setMargins(0, 0, 0, keypadHeight + 300);
                } else {
                    params.setMargins(0, 0, 0, keypadHeight + 300);
                }
                linearLayout.setLayoutParams(params);
                linearLayout.requestLayout();
            }
        } else {
            // keyboard is opened
            if (mConfirmView.getVisibility() == View.VISIBLE) {

            /*    if(isChangeMobile){
                    mTopbar.setVisibility(View.VISIBLE);
                }*/
                View linearLayout = findViewById(R.id.mainConfirm);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
                params.setMargins(0, 0, 0, 0);
                linearLayout.setLayoutParams(params);
                linearLayout.requestLayout();
                new Handler().postDelayed(new Runnable() {

                    /*713cda826320a67e617595bc3de5486c
                     * Showing splash screen with a timer. This will be useful when you
                     * want to show case your app logo / company
                     */
                    @Override
                    public void run() {
                        hideIcon();
                    }
                }, 0);

            } else if (mFaceBookRegisterView.getVisibility() == View.VISIBLE) {
                //  mTopbar.setVisibility(View.VISIBLE);
                View linearLayout = findViewById(R.id.mainFacebookLogin);
                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
                params.setMargins(0, 0, 0, 0);
                linearLayout.setLayoutParams(params);
                linearLayout.requestLayout();
                new Handler().postDelayed(new Runnable() {

                    /*713cda826320a67e617595bc3de5486c
                     * Showing splash screen with a timer. This will be useful when you
                     * want to show case your app logo / company
                     */
                    @Override
                    public void run() {
                        hideIcon();
                    }
                }, 0);

            }

        }
    }
}