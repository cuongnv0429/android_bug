package vn.com.feliz.v.widget.ReviewsScreenHolder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import vn.com.feliz.v.widget.AutoResizeTextView;
import vn.com.feliz.v.interfaces.ItemTouchHelperViewHolder;
import vn.com.feliz.R;


/**
 * Created by Mr Son on 2016-08-01.
 */
public class ReviewsFriendsHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {
    public TextView item_reviews_friend_name,tvDate,user_review;
    public AutoResizeTextView tvConttent;
    public ImageView imgAvatarFriendReview;
   // public LinearLayout mLinearLayoutMain;

    public ReviewsFriendsHolder(View view) {
        super(view);
        item_reviews_friend_name = (TextView) view.findViewById(R.id.name_friends);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        user_review = (TextView) view.findViewById(R.id.user_review);
        tvConttent = (AutoResizeTextView) view.findViewById(R.id.tvConttent);
        imgAvatarFriendReview = (ImageView) view.findViewById(R.id.imgAvatarFriendReview);
     //   mLinearLayoutMain = (LinearLayout) view.findViewById(R.id.ln_main_message_item);
        view.setOnClickListener(this);
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }

    @Override
    public void onClick(View view) {

    }
}
