package vn.com.feliz.v.widget;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import vn.com.feliz.R;

public class BbTextView extends TextView{
    public final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public BbTextView(Context context) {
        super(context);
        applyCustomFont(context, null);
    }

    public BbTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context, attrs);
    }

    public BbTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BbTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context, attrs);
    }

    private void applyCustomFont(Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.GbTextView);

        int fontName = attributeArray.getInt(R.styleable.GbTextView_font, 0);
        int textStyle = Typeface.NORMAL;
        if (!isInEditMode()) {
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }
        Typeface customFont = selectTypeface(context, fontName, textStyle);
        setTypeface(customFont);

        attributeArray.recycle();
    }

    private Typeface selectTypeface(Context context, int fontName, int textStyle) {
        if (fontName == 0){//helvetical font
            return null;
//            switch (textStyle) {
//                case Typeface.BOLD: // bold
//                    return Typeface.createFromAsset(context.getAssets(), "HelveticaNeueUltraLight.ttf");
//
//                case Typeface.ITALIC: // italic
//                    return Typeface.createFromAsset(context.getAssets(), "HelveticaNeueUltraLight.ttf");
//
//                case Typeface.BOLD_ITALIC: // bold italic
//                    return Typeface.createFromAsset(context.getAssets(), "HelveticaNeueUltraLight.ttf");
//
//                case Typeface.NORMAL: // regular
//                default:
//                    return Typeface.createFromAsset(context.getAssets(), "HelveticaNeueUltraLight.ttf");
//            }
        } else if (fontName == 1){
            switch (textStyle) {
                case Typeface.BOLD: // bold
                    return Typeface.createFromAsset(context.getAssets(), "Aria.ttf");

                case Typeface.ITALIC: // italic
                    return Typeface.createFromAsset(context.getAssets(), "Aria.ttf");

                case Typeface.BOLD_ITALIC: // bold italic
                    return Typeface.createFromAsset(context.getAssets(), "Aria.ttf");

                case Typeface.NORMAL: // regular
                default:
                    return Typeface.createFromAsset(context.getAssets(), "Aria.ttf");
            }
        } else {
            // no matching font found
            // return null so Android just uses the standard font (Roboto)
            return null;
        }
    }
}
