package vn.com.feliz.v.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.androidadvance.topsnackbar.TSnackbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import vn.com.feliz.R;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.databinding.FragmentCouponsScreenBinding;
import vn.com.feliz.gbservice1.api.GetCouponListService;
import vn.com.feliz.gbservice1.api.SubmitCouponStatusService;
import vn.com.feliz.m.BaseResponse;
import vn.com.feliz.m.CouponListModel;
import vn.com.feliz.m.CouponModel;
import vn.com.feliz.m.ReviewAllItem;
import vn.com.feliz.network.OnPostResponseListener;
import vn.com.feliz.network.services.ApiResponseCode;
import vn.com.feliz.network.services.ApiTask;
import vn.com.feliz.p.CouponsPresenter;
import vn.com.feliz.p.ReviewsPrensenter;
import vn.com.feliz.p.eventbus.refreshData;
import vn.com.feliz.v.activity.MainActivity;
import vn.com.feliz.v.adapter.CouponsAdapter;
import vn.com.feliz.v.interfaces.OnMenuItemSelected;
import vn.com.feliz.v.widget.CustomViewPager;
public class CouponsScreen extends BaseFragment implements OnMenuItemSelected,
        CouponsAdapter.OnItemRecyclerListener, View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, OnPostResponseListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private FragmentCouponsScreenBinding mBinding;
    private CouponListReceiver mReceiver;
    public List<CouponModel.Coupon> mCouponList;
    public CouponModel.Coupon mParentCoupon;
    public CouponModel.Coupon mParentCouponTmp;
    private boolean mCanBack;
    private boolean mRequesting;
    private boolean mEndPage;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest = LocationRequest.create();
    private Location mLastLocation;
    private TSnackbar snackbar;
    private long mIdOfFragment = 0;
    private ProgressDialog mProgressDialog;
    private CouponsAdapter couponAdapter;
    private static boolean is_updated_viewed = false;
    private long lastScroll = System.currentTimeMillis();
    private final int SCROLL_DELAY = 800;
    private boolean mOnLoadMore = false;
    private CouponsPresenter couponsPresenter;
    private Dialog mDialog;
    private boolean mShowAlert = false;
    private String mMessageAlert = "";

    public static boolean onBackFromDetail = false;
    public static int onBacCouponDetailIndex = 0;
    private int mLastFirstVisibleItem;
    private boolean pauseScroll = false;
    private int mScrollOffset;
    @BindView(R.id.dialog_select_product_1)
    LinearLayout mProduct1;
    @BindView(R.id.dialog_select_product_2)
    LinearLayout mProduct2;
    @BindView(R.id.dialog_select_product_3)
    LinearLayout mProduct3;
    @BindView(R.id.reviews_pager_main)
    CustomViewPager mViewPager;

    public static boolean isLoad;

    public CouponsScreen() {
        // Required empty public constructor
    }

    public static CouponsScreen newInstance(CouponModel.Coupon parentCoupon) {
        return CouponsScreen.newInstance(parentCoupon, false);
    }

    public static CouponsScreen newInstance(boolean alert, String message) {
        CouponsScreen fragment = new CouponsScreen();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM4, alert);
        args.putString(ARG_PARAM5, message);
        fragment.setArguments(args);
        return fragment;
    }

    public static CouponsScreen newInstance(CouponModel.Coupon parentCoupon, boolean canBack) {
        CouponsScreen fragment = new CouponsScreen();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, parentCoupon);
        args.putBoolean(ARG_PARAM2, canBack);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isLoad = true;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        if (getArguments() != null) {
            mParentCoupon = (CouponModel.Coupon) getArguments().getSerializable(ARG_PARAM1);
            mCanBack = getArguments().getBoolean(ARG_PARAM2, false);
            mShowAlert = getArguments().getBoolean(ARG_PARAM4, false);
            mMessageAlert = getArguments().getString(ARG_PARAM5);
        }
        //  mActivity.mBottom().setVisibility(View.VISIBLE);
        mProgressDialog = new ProgressDialog(mActivity, R.style.AppCompatAlertDialogStyle);
        //Picasso.with(getActivity()).setIndicatorsEnabled(true);

        MainActivity.not_from_list = false;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_coupons_screen, container, false);
        /*View decorView = getActivity().getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(uiOptions);*/
        mBinding.rvCoupons.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.rvCoupons.addOnScrollListener(new EndlessScrollListener());
        mBinding.mainTopbar.mainTopBarTitle.setText(R.string.coupons_Screen);
        //mBinding.icLoaddingCoupons.setImageResource(R.drawable.loadding_ic_gif);
        Log.d("HoangNM", "onCreateView......");
        if (mParentCoupon != null) {
            Log.i("mCouponScreen:SonDB2", mParentCoupon.toString());
        } else {
            Log.i("mCouponScreen:SonDB2", "null");
        }
        if (mIdOfFragment == 0) {
            mIdOfFragment = Calendar.getInstance().getTimeInMillis();
        }
        if (mCanBack) {
            mBinding.mainTopbar.mainTopbarBackLn.setOnClickListener(this);
        } else {
            mBinding.mainTopbar.mainTopbarBackLn.setVisibility(View.INVISIBLE);
        }

        if (MainActivity.couponList != null) {
            reLoadAdapter();
        }

        if (MainActivity.reload_list && MainActivity.couponList != null) {
            MainActivity.reload_list = false;
            //mProgressDialog = Utils.showLoading(mActivity);
            MainActivity.couponList = null;
            //getCouponList(0);
        }
        mBinding.swipeCoupon.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mRequesting = false;
                mEndPage = false;
                pauseScroll = false;
                mOnLoadMore = false;
                mProgressDialog = Utils.showLoading(mActivity);
                MainActivity.couponList = null;
                getCouponList(0);
            }
        });

        mBinding.btnRetryCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mProgressDialog = Utils.showLoading(mActivity);
                mRequesting = false;
                mEndPage = false;
                pauseScroll = false;
                mOnLoadMore = false;
                mProgressDialog = Utils.showLoading(mActivity);
                MainActivity.couponList = null;
                getCouponList(0);
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        createGoogleApiClient();
        if (mParentCoupon == null) {
            mReceiver = new CouponListReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(getActivity().getPackageName());
            getActivity().registerReceiver(mReceiver, filter);
        }

        if(mShowAlert) {
            mShowAlert = false;
            mMessageAlert = getResources().getString(R.string.alert_coupon_buy_success);
            try {
                snackbar = TSnackbar
                        .make(getView(), mMessageAlert, TSnackbar.LENGTH_SHORT);
                Utils.showMessageTopbar(snackbar, mMessageAlert, mActivity);
                Utils.vibarte(mActivity);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getType() {
        return OnMenuItemSelected.SELECT_COUPONS;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.not_from_list = false;
    }

    public void onFragmentResume() {
        Log.i("COUPON LIST", "IS RESUME 1a");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("HoangNM", "on Destroy coupon screen.");
        if (mParentCoupon == null && mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
        if(mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (mRequesting) {//check to stop service
            Log.d("HoangNM", "stop get coupon list service");
            Intent i = new Intent(getContext(), GetCouponListService.class);
            getActivity().stopService(i);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        if (v == null) return;
        switch (v.getId()) {
            case R.id.main_topbar_back_ln:
                Log.e("main_topbar_back_ln", "main_topbar_back_ln");
                // getActivity().getSupportFragmentManager().popBackStack();
                mActivity.getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.root_frame, CouponsScreen.newInstance(null))
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commitAllowingStateLoss();
                getChildFragmentManager().executePendingTransactions();
                break;
        }
    }

    private static boolean mIsRecyclerItemClicked;

    @Override
    public void onRecyclerViewItemClicked(View v, int pos) {

        CouponModel.Coupon coupon;

        switch (v.getId()) {
            case R.id.rl_coupon_item_root:
                //fix click twice rapidly, we should stop and wait 700ms
                if (mIsRecyclerItemClicked) return;
                mIsRecyclerItemClicked = true;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mIsRecyclerItemClicked = false;
                    }
                }, 700);

                CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
                coupon = adapter.getData().get(pos);

                // Update viewed status for all data
                if(!is_updated_viewed) {
                    adapter.updateViewed();
                    adapter.notifyDataSetChanged();
                } else {
                    is_updated_viewed = true;
                }

                onBacCouponDetailIndex = pos;

                if (!coupon.getStatus().equals(Constant.COUPON_STATUS_CAN_BUY) && coupon.getList() != null &&
                        coupon.getList().size() > 1) {//more than 1 unit and status=1,2,3,4

                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, CouponsSubScreen.newInstance(coupon, true), "coupon_sub_list")
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                    onFragmentResume();
                } else {
                    MainActivity.reload_detail = false;
                    MainActivity.reload_list = false;
                    mActivity.getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.root_frame, CouponsDetailScreen.newInstance(coupon), "coupon_detail")
                            .addToBackStack(null)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commitAllowingStateLoss();
                    getChildFragmentManager().executePendingTransactions();
                }
                break;
            case R.id.iv_rating:
                coupon = ((CouponsAdapter) mBinding.rvCoupons.getAdapter()).getData().get(pos);
                viewDetailReview(coupon);
                break;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            Log.i("mCouponScreen:SonDB3", "Connected");
//            if (ContextCompat.checkSelfPermission(getContext(),
//                    Manifest.permission.ACCESS_FINE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED) {
//                //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//            } else {
//                //get coupon available update
//                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//            }
//            if (mLastLocation == null) {
//                mLastLocation = new Location("dummyprovider");
//                mLastLocation.setLatitude(0);
//                mLastLocation.setLongitude(0);
//            }
            if (MainActivity.couponList == null) {//yes this is first level
                mProgressDialog = Utils.showLoading(mActivity);
                Log.d("request api", "2");
                getCouponList(0);
                // mBinding.icLoaddingCoupons.setVisibility(View.VISIBLE);
                // mProgressDialog = Utils.showLoading(mActivity);
            } else {
                reLoadAdapter();
                Log.i("request api", "3");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("HoangNM", "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("HoangNM", "onConnectionFailed");
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    private void createGoogleApiClient() {
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }

    private void getCouponList(int offset) {
        if (mRequesting) return;
//        if (Utils.isNetworkAvailable(getContext())) {
            Intent i = new Intent(getContext(), GetCouponListService.class);
            i.putExtra("fragment_id", mIdOfFragment);
            i.putExtra("offset", offset);
            i.putExtra("location", mLastLocation);
            getActivity().startService(i);
            mRequesting = true;
//        } else {
            //Utils.showNoInternetMessage(getContext(), null);
//        }
    }

    public class CouponListReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final long fragmentId = intent.getLongExtra("fragment_id", 0);
            final int offset = intent.getIntExtra("offset", 0);
            final int connect_status = intent.getIntExtra("connect_status", 0);
            final int action = intent.getIntExtra(Constant.ACTION, 0);
            pauseScroll = false;
            if (connect_status == Constant.CANNOT_CONNECT_SERVER) {
                if (action == Constant.ACTION_GET_COUPON_LIST) {
                    Utils.hideLoading(mProgressDialog);
                    mBinding.swipeCoupon.setRefreshing(false);
                    MainActivity.couponList = null;
                    mCouponList = new ArrayList<>();
                    CouponsAdapter adapter = new CouponsAdapter(getContext(), mCouponList, CouponsScreen.this);
                    mBinding.rvCoupons.setAdapter(adapter);
                    couponAdapter = adapter;

                    mBinding.swipeCoupon.setVisibility(View.GONE);
                    mBinding.txtNoDataCoupon.setVisibility(View.VISIBLE);
                    mBinding.btnRetryCoupon.setVisibility(View.VISIBLE);
                }
            } else {
                mBinding.swipeCoupon.setVisibility(View.VISIBLE);
                mBinding.txtNoDataCoupon.setVisibility(View.GONE);
                mBinding.btnRetryCoupon.setVisibility(View.GONE);
            }
            if (fragmentId != mIdOfFragment) return;
            Utils.hideLoading(mProgressDialog);
            switch (action) {
                case Constant.ACTION_GET_COUPON_LIST:
                    mRequesting = false;
                    //mBinding.icLoaddingCoupons.setVisibility(View.GONE);
                    Utils.hideLoading(mProgressDialog);

                    if(mOnLoadMore) {
                        CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
                        if (adapter != null) {
                            adapter.removeLoadingCoupon();
                            mOnLoadMore = false;
                        }
                    }

                    if(connect_status == Constant.CANNOT_CONNECT_SERVER) {
                        mBinding.swipeCoupon.setRefreshing(false);
                        mActivity.showNetworkAlert(getView());
                    } else {
                        CouponListModel couponList = (CouponListModel) intent.getSerializableExtra("coupon_list");
                        if (couponList != null) {
                            switch (couponList.getCode()) {
                                case Constant.CODE_SUCCESS:
                                    List<CouponModel.Coupon> coupons = couponList.getCouponList().getCouponList();

                                    mCouponList = coupons;
                                    if (mBinding.tvEmpty != null) {
                                        mBinding.tvEmpty.setVisibility(View.GONE);
                                    }

                                    if (offset == 0 || mBinding.rvCoupons.getAdapter() == null ||
                                            (mBinding.rvCoupons.getAdapter() != null && mBinding.rvCoupons.getAdapter().getItemCount() == 0) ||
                                            mBinding.swipeCoupon.isRefreshing()) {//first time
                                        mBinding.swipeCoupon.setRefreshing(false);
                                        if (coupons.size() > 0) {//available data in first time
                                            //Collections.sort(coupons, new CustomComparator());
                                            CouponsAdapter adapter = new CouponsAdapter(getContext(), coupons, CouponsScreen.this);
                                            mBinding.rvCoupons.setAdapter(adapter);
                                            couponAdapter = adapter;

                                            //start service to update coupon status
                                            Intent i = new Intent(CouponsScreen.this.getContext(), SubmitCouponStatusService.class);
                                            i.putExtra("location", mLastLocation);
                                            CouponsScreen.this.getActivity().startService(i);

                                            MainActivity.couponList = adapter.getData();
                                        } else {
                                            mEndPage = true;
                                            mBinding.tvEmpty.setVisibility(View.VISIBLE);
                                        }
                                        Log.e("coupon 1", coupons.size() + "__");
                                    } else {
                                        CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
                                        if (coupons.size() > 0) {
                                            Log.e("coupon", coupons.size() + "__");
                                            adapter.removeLoadingCoupon();
                                            adapter.addCoupon(coupons);
                                        } else {
                                            mEndPage = true;
                                            adapter.removeLoadingCoupon();
                                        }
                                        MainActivity.couponList = adapter.getData();
                                    }
                                    break;
                                case Constant.CODE_ERROR_EXCEPTION:
//                            if(mParentCouponTmp != null && mParentCoupon == null) {
//                                mParentCoupon = mParentCouponTmp;
//                            }
                                    RecyclerView.Adapter adapter = mBinding.rvCoupons.getAdapter();
                                    if (adapter != null && adapter instanceof CouponsAdapter) {
                                        ((CouponsAdapter) adapter).removeLoadingCoupon();
                                    }
                                    break;
                                default:
//                            if(mParentCouponTmp != null && mParentCoupon == null) {
//                                mParentCoupon = mParentCouponTmp;
//                            }
                                    //remove loading view if exist
                                    Utils.showMessageNoTitle(CouponsScreen.this.getContext(), couponList.getMessage(), null);
                                    break;
                            }
                        }
                    }

                    break;
            }
        }
    }

    private void reLoadAdapter() {
        mBinding.rvCoupons.setVisibility(View.GONE);
        updateViewedML();
        if (MainActivity.couponList == null) {
            MainActivity.couponList = new ArrayList<>();
        }
        mBinding.rvCoupons.setAdapter(new CouponsAdapter(getContext(), MainActivity.couponList, CouponsScreen.this));
        mBinding.rvCoupons.setVisibility(View.VISIBLE);
    }

    private void updateViewedML() {
        if(MainActivity.couponList != null) {
            for (int i = 0; i < MainActivity.couponList.size(); i++) {
                if (MainActivity.couponList.get(i) != null && MainActivity.couponList.get(i).getStatus()!=null && MainActivity.couponList.get(i).getStatus().equals(Constant.COUPON_STATUS_NEW)) {
                    MainActivity.couponList.get(i).setStatus(Constant.COUPON_STATUS_NOT_EXCHANGE_YET);
                }
            }
        }
    }

    private void loadMore() {
//        if (!Utils.isNetworkAvailable(mActivity)) {
//            return;
//        }

        if (mRequesting || mEndPage) {
        } else {

            if (!pauseScroll && !mOnLoadMore) {
                lastScroll = System.currentTimeMillis();

                CouponsAdapter adapter = ((CouponsAdapter) mBinding.rvCoupons.getAdapter());
                mScrollOffset = adapter.getItemCount();

                if (lastScroll > System.currentTimeMillis() - SCROLL_DELAY) {
                    pauseScroll = true;
                    mOnLoadMore = true;
                    //add view loading
                    CouponModel.Coupon coupon = new CouponModel.Coupon();
                    coupon.setViewType(1);
                    adapter.addCoupon(coupon);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("request api", "loadmore delay");
                            getCouponList(mScrollOffset);
                        }
                    }, 1000);
                } else {
                    Log.d("request api", "loadmore now");
                    //add view loading
                    mOnLoadMore = true;
                    CouponModel.Coupon coupon = new CouponModel.Coupon();
                    coupon.setViewType(1);
                    adapter.addCoupon(coupon);
                    getCouponList(mScrollOffset);
                }
            }
        }

    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            try {
                if (CouponsDetailScreen.mMediaPlayer != null) {
                    CouponsDetailScreen.mMediaPlayer.pause();
                    CouponsDetailScreen.mMediaPlayer.setVolume(0f, 0f);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (snackbar != null) {
            snackbar.dismiss();
        }
    }

    class EndlessScrollListener extends RecyclerView.OnScrollListener {
        public EndlessScrollListener() {
            super();
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (dy > 0) //check for scroll down
            {
                int totalItem = recyclerView.getLayoutManager().getItemCount();
                int lastVisibleItem = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                if (!mRequesting && lastVisibleItem == totalItem - 1) {
                    // Scrolled to bottom. Do something here.
                    loadMore();
                }
            }
        }
    }

    private class CustomComparator implements Comparator<CouponModel.Coupon> {
        @Override
        public int compare(CouponModel.Coupon obj1, CouponModel.Coupon obj2) {
            if (obj1.getStatus().compareTo(obj2.getStatus()) == 0) {//equal
                return obj1.getCouponExpiredDate().compareTo(obj2.getCouponExpiredDate());
            }
            return obj1.getStatus().compareTo(obj2.getStatus());
        }
    }

    /**
     * network
     */
    @Subscribe
    public void feFreshData(refreshData refreshData) {
        if (refreshData.getNameFragment().toLowerCase().equals("couponsscreen")) {
            //Toast.makeText(getContext(), "Refresh after lost network.", Toast.LENGTH_LONG).show();
            // noi reload data
            RecyclerView.Adapter adapter = mBinding.rvCoupons.getAdapter();
            if (adapter != null && adapter instanceof CouponsAdapter) {
                ((CouponsAdapter) mBinding.rvCoupons.getAdapter()).clearAdapter();
            }
            mEndPage = false;
            mRequesting = false;
            //mProgressDialog = Utils.showLoading(mActivity);
            Log.d("request api", "feFreshdata");
            if (mIdOfFragment == 0) {
                mIdOfFragment = Calendar.getInstance().getTimeInMillis();
            }
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.hide();
                }
            }
            mProgressDialog = Utils.showLoading(mActivity);
            getCouponList(0);
            //mBinding.icLoaddingCoupons.setVisibility(View.VISIBLE);

        }
    }

    private void viewDetailReview(final CouponModel.Coupon coupon) {
        if(coupon == null) {
            return;
        }

        List<CouponModel.Coupon.Product> products = coupon.getListProduct();

        int mNumberProduct = 0;
        if(products != null && products.size() > 0) {
            mNumberProduct = products.size();
        }

        if(mNumberProduct == 0) {
            snackbar = TSnackbar
                    .make(getView(),  getString(R.string.coupon_not_have_product), TSnackbar.LENGTH_SHORT);
            Utils.showMessageTopbar(snackbar, getString(R.string.coupon_not_have_product), mActivity);
            return;
        }

        if(mNumberProduct == 1) {
            gotoProductReview(Integer.parseInt(products.get(0).getId()));
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowmanager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        windowmanager.getDefaultDisplay().getMetrics(displayMetrics);
        int mDeviceWidth = displayMetrics.widthPixels - 50;

        int partWidth = (mDeviceWidth / 3) - 10;

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        LayoutInflater li = LayoutInflater.from(mActivity);
        final View myView = li.inflate(R.layout.dialog_select_product_view_review, null);

        View product1 = myView.findViewById(R.id.dialog_select_product_1);
        View product2 = myView.findViewById(R.id.dialog_select_product_2);
        View product3 = myView.findViewById(R.id.dialog_select_product_3);

        // Reset all view to invisible
        product1.setVisibility(View.GONE);
        product2.setVisibility(View.GONE);
        product3.setVisibility(View.GONE);

        builder.setView(myView);
        mDialog = builder.create();
        mDialog.show();

        int imageWidth = partWidth;
        int imageHeight = partWidth * 2;

        ViewGroup.LayoutParams p = product1.getLayoutParams();
        p.width = imageWidth;
        p.height = imageHeight;

        product1.setLayoutParams(p);
        product2.setLayoutParams(p);
        product3.setLayoutParams(p);

        if(mNumberProduct >= 1) {
            product1.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_1);
            Picasso.with(getContext()).load(products.get(0).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product1.setId(Integer.parseInt(products.get(0).getId()));

            product1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });

            product1.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot1).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 2) {
            product2.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_2);
            Picasso.with(getContext()).load(coupon.getListProduct().get(1).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product2.setId(Integer.parseInt(products.get(1).getId()));

            product2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product2.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot2).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
        if(mNumberProduct >= 3) {
            product3.setVisibility(View.VISIBLE);
            ImageView v = (ImageView) myView.findViewById(R.id.product_picture_3);
            Picasso.with(getContext()).load(coupon.getListProduct().get(2).getImage())
                    .resize(imageWidth, imageHeight).onlyScaleDown()
                    .into(v);

            product3.setId(Integer.parseInt(products.get(2).getId()));

            product3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                    gotoProductReview(v.getId());
                }
            });
            product3.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org_ck);
                            break;
                        case MotionEvent.ACTION_UP:
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    myView.findViewById(R.id.v_introduction_dot3).setBackgroundResource(R.drawable.check_org);
                                }
                            }, 500);
                            break;
                    }
                    return false;
                }
            });
        }
    }

    private void gotoProductReview(int product_id) {
        Log.e("product_id", product_id + "__");
        if(mDialog != null) {
            mDialog.dismiss();
        }

        if(mActivity.checkAndAlertNetWork(getView())) {
            ReviewsPrensenter mReviewPresenter = new ReviewsPrensenter(mActivity, this);

            if (mReviewPresenter.hasUserInfo()) {
                mProgressDialog = Utils.showLoading(getContext());
                String mTokenUser = mReviewPresenter.getUserInfo().getToken();
                mReviewPresenter.getListAllReviewByProductPresenta(mTokenUser, "" + product_id, "", "", "product");
            }
        }
    }

    @Override
    public boolean onPostResponse(ApiTask task, int status) {
        if (mProgressDialog != null) {
            Utils.hideLoading(mProgressDialog);
        }
        if(status == Constant.CANNOT_CONNECT_SERVER) {
            mActivity.showNetworkAlert(getView());
        } else {
            switch (task.getType()) {
                case GET_LIST_REVIEW_BY_PRODUCT_ONLY:
                    BaseResponse baseResponse = (BaseResponse) task.getResponse().body();
                    if (status == ApiResponseCode.SUCCESS) {
                        ReviewAllItem mReviewAllItem = (ReviewAllItem) baseResponse.getData();
                        mActivity.gotoReviewDetail(mReviewAllItem);
                    } else {
                        snackbar = TSnackbar
                                .make(getView(), baseResponse.getMessage(), TSnackbar.LENGTH_SHORT);
                        Utils.showMessageTopbar(snackbar, baseResponse.getMessage(), mActivity);
                        Utils.vibarte(mActivity);
                    }
                    break;
            }
        }
        return false;
    }

}