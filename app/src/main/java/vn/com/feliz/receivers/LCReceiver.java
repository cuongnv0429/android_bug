package vn.com.feliz.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.util.Log;

import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.gbservice1.api.GetAdsListService;
import vn.com.feliz.m.AdsListModel;
import vn.com.feliz.m.response.PushResponse;
import vn.com.feliz.v.activity.LockScreenActivity;


public class LCReceiver extends BroadcastReceiver {

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("onReceive", "onReceive");

        mContext = context;

        String action = intent.getAction();

        boolean lockscreen_enabled = Utils.getLockScreenStatus(mContext);

        //If the screen was just turned on or it just booted up, start your Lock Activity
        if(lockscreen_enabled && (action.equals(Intent.ACTION_SCREEN_ON) || action.equals(Intent.ACTION_BOOT_COMPLETED)))
        {
            if(Utils.isNetworkAvailable(mContext)) {
                PushResponse pushResponse = Utils.getAds(mContext);
                if (pushResponse == null) {
                    long last_get = Utils.getLastGetAds(mContext);
                    if(last_get < (System.currentTimeMillis() - 5000)) { //47019
                        try {
                            Utils.setLastGetAds(mContext);
                            GetAdsListReceiver mReceiver = new GetAdsListReceiver();
                            IntentFilter filter = new IntentFilter();
                            filter.addAction(mContext.getPackageName());
                            mContext.getApplicationContext().registerReceiver(mReceiver, filter);
                            Intent i = new Intent(mContext, GetAdsListService.class);
                            mContext.startService(i);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            appendLog(ex);
                        }

                    }
                } else {
                    showLockScreen();
                }
            }
        }
    }

    public void appendLog(Exception ex)
    {
        PrintWriter pw;
        try {
            pw = new PrintWriter(
                    new FileWriter(Environment.getExternalStorageDirectory()+"/rt.txt", true));
            ex.printStackTrace(pw);
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showLockScreen() {
        Intent i = new Intent(mContext, LockScreenActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
    }

    class GetAdsListReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int action = intent.getIntExtra(Constant.ACTION, 0);
            AdsListModel mAdsModel;
            switch (action) {
                case Constant.ACTION_GET_ADS_LIST:
                    mAdsModel = (AdsListModel) intent.getSerializableExtra("ads");
                    if (mAdsModel != null) {
                        if (mAdsModel.getCode().equals(Constant.CODE_SUCCESS) && mAdsModel.getAdsList() != null) {
                            Log.e("ADS LIST", mAdsModel.getAdsList().toString());
                            Gson g = new Gson();
                            if(mAdsModel.getAdsList() != null && mAdsModel.getAdsList().size() > 0) {
                                boolean have_ads = false;
                                Utils.clearAds(mContext);
                                for (int i = 0; i < mAdsModel.getAdsList().size(); i++) {
                                    if(mAdsModel.getAdsList().get(i) != null) {
                                        if (mAdsModel.getAdsList().get(i).getPush_type() != null) {
                                            Utils.saveAds(mContext, g.toJson(mAdsModel.getAdsList().get(i)));
                                            have_ads = true;
                                        }
                                    }
                                }
                                if(have_ads) {
                                    showLockScreen();
                                }
                            }
                        }
                    }
                    break;
            }
        }
    }
}
