package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;

public class SubmitPushStatusService extends BaseService {
    public SubmitPushStatusService() {
        this("");
    }

    public SubmitPushStatusService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String url = Constant.DOMAIN + "coupon/submit-push-status";
            Location location = intent.getParcelableExtra("location");
            String couponId = intent.getStringExtra("coupon_id");
            int status = intent.getIntExtra("status", 0);
            HttpURLConnection connection = getConnection(url, RequestMethod.POST, location);
            writeStream(connection.getOutputStream(), "token="+ Utils.getToken(getApplicationContext())+"&coupon_id="+couponId+"&status="+status);
            int statusCode = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (statusCode == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "push status submit: " + result);
            /*AdsModel model = g.fromJson(result, AdsModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("ads", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_SUBMIT_ADS);
            sendBroadcast(i);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
