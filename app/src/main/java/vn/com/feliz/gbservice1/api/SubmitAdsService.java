package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONObject;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.AdsModel;

public class SubmitAdsService extends BaseService {
    public SubmitAdsService() {
        this("");
    }

    public SubmitAdsService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String adsId = intent.getStringExtra("ads_id");
            String status = intent.getStringExtra("status");
            String repeatTime = intent.getStringExtra("repeat_time");
            String shareLink = intent.getStringExtra("share_link");
            String url = Constant.DOMAIN + "ads/submit-status";
            JSONObject object = new JSONObject();
            object.put("token", Utils.getToken(getApplicationContext()));
            object.put("ads_id", adsId);
            object.put("status", status);
            object.put("repeat_time", repeatTime);
            object.put("share_link", shareLink);
            HttpURLConnection connection = getConnection(url, BaseService.RequestMethod.POST);
            writeStream(connection.getOutputStream(), object.toString());
            int statusCode = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (statusCode == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "stream submit: " + result);
            AdsModel model = g.fromJson(result, AdsModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("ads", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_SUBMIT_ADS);
            sendBroadcast(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
