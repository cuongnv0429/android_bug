package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.CouponModel;

public class GetCouponDetailService extends BaseService {

    public GetCouponDetailService() {
        this("");
    }

    public GetCouponDetailService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String couponId = intent.getStringExtra("coupon_id");
            String couponSubId = intent.getStringExtra("coupon_sub_id");
            Location location = intent.getParcelableExtra("location");
            String url = Constant.DOMAIN + "coupon/detail";
            String params = "token="+ Utils.getToken(getApplicationContext())+"&coupon_id="+couponId;
            if (couponSubId != null) params += "&coupon_sub_id="+couponSubId;
            HttpURLConnection connection = getConnection(url, RequestMethod.POST, location);
            writeStream(connection.getOutputStream(), params);
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().setDateFormat(Constant.DATE_FORMAT).create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "get coupon detail: " + result);
            CouponModel model = g.fromJson(result, CouponModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("coupon", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_COUPON_DETAIL);
            sendBroadcast(i);
        } catch (Exception e) {
            e.printStackTrace();
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("connect_status", Constant.CANNOT_CONNECT_SERVER);
            sendBroadcast(i);
        }
    }


}
