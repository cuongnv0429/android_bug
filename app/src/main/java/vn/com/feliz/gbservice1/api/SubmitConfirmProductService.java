package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.BaseModel;

public class SubmitConfirmProductService extends BaseService {
    public SubmitConfirmProductService() {
        this("");
    }

    public SubmitConfirmProductService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String couponId = intent.getStringExtra("coupon_id");
            List<String> listProductId = intent.getStringArrayListExtra("list_product_id");
            String url = Constant.DOMAIN + "coupon/submit-confirm-product";
            JSONObject object = new JSONObject();
            object.put("token", Utils.getToken(getApplicationContext()));
            object.put("coupon_id", couponId);
            object.put("list_product_id", new JSONArray(listProductId));
            HttpURLConnection connection = getConnection(url, RequestMethod.POST);
            writeStream(connection.getOutputStream(), object.toString());
            int statusCode = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (statusCode == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "stream submit: " + result);
            BaseModel model = g.fromJson(result, BaseModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("base", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_SUBMIT_CONFIRM_PRODUCT);
            sendBroadcast(i);
        } catch (Exception e) {
            e.printStackTrace();
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("connect_status", Constant.CANNOT_CONNECT_SERVER);
            sendBroadcast(i);
        }
    }
}
