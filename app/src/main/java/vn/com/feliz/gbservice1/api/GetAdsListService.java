package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.AdsListModel;
import vn.com.feliz.m.AdsModel;

public class GetAdsListService extends BaseService {

    public GetAdsListService() {
        this("");
    }

    public GetAdsListService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String url = Constant.DOMAIN + "ads/list";
            String device_id = Utils.generateUUID(getApplicationContext());
            HttpURLConnection connection = getConnection(device_id, url, RequestMethod.POST, null);
            writeStream(connection.getOutputStream(), "");
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "get ads stream: " + result);
            AdsListModel model = g.fromJson(result, AdsListModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("ads", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_ADS_LIST);
            sendBroadcast(i);
        } catch (Exception e) {
            Intent i = new Intent(getApplicationContext().getPackageName());
            AdsListModel model = new AdsListModel();
            model.setCode("500");
            i.putExtra("ads", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_ADS_LIST);
            sendBroadcast(i);
            e.printStackTrace();
        }
    }


}
