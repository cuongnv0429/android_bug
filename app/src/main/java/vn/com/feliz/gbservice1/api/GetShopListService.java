package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.response.ShopModel;

public class GetShopListService extends BaseService {

    public GetShopListService() {
        this("");
    }

    public GetShopListService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String url = Constant.DOMAIN + "shop/list";
            HttpURLConnection connection = getConnection(url, RequestMethod.POST);
            writeStream(connection.getOutputStream(), "token=" + Utils.getToken(getApplicationContext()) + "&coupon_id=1");
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "get shop list: " + result);
            ShopModel model = g.fromJson(result, ShopModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("shop", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_SHOP_LIST);
            sendBroadcast(i);
        } catch (Exception e) {
            e.printStackTrace();
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("connect_status", Constant.CANNOT_CONNECT_SERVER);
            sendBroadcast(i);
        }
    }


}
