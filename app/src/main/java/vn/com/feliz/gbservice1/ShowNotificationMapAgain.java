package vn.com.feliz.gbservice1;


import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class ShowNotificationMapAgain extends IntentService implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;

    public ShowNotificationMapAgain(){
        this("");
    }

    public ShowNotificationMapAgain(String name) {
        super(name);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createGoogleApiClient();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        if (ContextCompat.checkSelfPermission(getApplicationContext(),
//                Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//            //requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
//        } else {
//            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//
//            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
//        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
    }

    /*private void checkShowNotificationAgain(NotificationModel notification, long time) {
        if (notification.getRepeatTimes() <= 0) return;
        if (mLastLocation == null) return;
        Location notificationLocation = new Location(notification.getKey());
        notificationLocation.setLatitude(notification.getLatitude());
        notificationLocation.setLongitude(notification.getLongitude());
        if (mLastLocation.distanceTo(notificationLocation) > Constant.NOTIFICATION_RADIUS) return;
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                NotificationModel notification = (NotificationModel)msg.getData().getSerializable("notification");
                Intent intent = new Intent(LockScreenFragment.this.getContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(LockScreenFragment.this.getContext(), 0 *//* request code *//*, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                Bundle b = new Bundle();
                b.putInt("push_type", notification.getPushType());
                b.putString("coupon_id", notification.getCouponId());
                b.putString("gb_code", notification.getGbCode());
                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                android.support.v7.app.NotificationCompat.Builder notificationBuilder = (android.support.v7.app.NotificationCompat.Builder)
                        new android.support.v7.app.NotificationCompat.Builder(LockScreenFragment.this.getContext())
                                .setSmallIcon(R.drawable.icons_notifi_x)
                                .setContentTitle(notification.getTitle())
                                .setContentText(notification.getDescription())
                                .setAutoCancel(true)
                                .setSound(defaultSoundUri)
                                .setContentIntent(pendingIntent)
                                .setExtras(b);

                NotificationManager notificationManager =
                        (NotificationManager) LockScreenFragment.this.getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(notification.getId(), notificationBuilder.build());
                return false;
            }
        });
        Message message = Message.obtain();
        message.what = notification.getId();
        Bundle b = new Bundle();
        notification.setRepeatTimes(notification.getRepeatTimes()-1);
        b.putSerializable("notification", notification);
        message.setData(b);
        handler.sendMessageDelayed(message, time);
    }*/

    private void createGoogleApiClient(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(30 * 1000);
        mLocationRequest.setFastestInterval(5 * 1000);
        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            mGoogleApiClient.connect();
        }
    }
}
