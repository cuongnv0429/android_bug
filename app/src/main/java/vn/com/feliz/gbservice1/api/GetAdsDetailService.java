package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.m.AdsModel;
import vn.com.feliz.common.Utils;

public class GetAdsDetailService extends BaseService {

    public GetAdsDetailService() {
        this("");
    }

    public GetAdsDetailService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String adsId = intent.getStringExtra("ads_id");
            String description = intent.getStringExtra("ads_description");
            String url = Constant.DOMAIN + "ads/detail";
            HttpURLConnection connection = getConnection(url, BaseService.RequestMethod.POST);
            writeStream(connection.getOutputStream(), "token=" + Utils.getToken(getApplicationContext()) + "&ads_id="+adsId);
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d(">>>>>> Load ads", "Detail > "+adsId);
            Log.d("HoangNM", "get ads stream: " + result);
            AdsModel model = g.fromJson(result, AdsModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("ads", model);
            i.putExtra("ads_id", adsId);
            i.putExtra("ads_description", description);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_ADS);
            sendBroadcast(i);
        } catch (Exception e) {
            Intent i = new Intent(getApplicationContext().getPackageName());
            AdsModel model = new AdsModel();
            String adsId = intent.getStringExtra("ads_id");
            model.setCode("500");
            i.putExtra("ads", model);
            i.putExtra("ads_id", adsId);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_ADS);
            sendBroadcast(i);
            e.printStackTrace();
        }
    }


}
