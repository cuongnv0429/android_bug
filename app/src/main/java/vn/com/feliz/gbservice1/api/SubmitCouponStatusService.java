package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;

public class SubmitCouponStatusService extends BaseService {
    public SubmitCouponStatusService() {
        this("");
    }

    public SubmitCouponStatusService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String url = Constant.DOMAIN + "coupon/submit-status";
            Location location = intent.getParcelableExtra("location");
            HttpURLConnection connection = getConnection(url, RequestMethod.POST, location);
            writeStream(connection.getOutputStream(), "token="+ Utils.getToken(getApplicationContext()));
            int statusCode = connection.getResponseCode();
            Gson g = new GsonBuilder().create();
            String result;
            if (statusCode == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "stream coupon submit: " + result);
            /*AdsModel model = g.fromJson(result, AdsModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("ads", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_SUBMIT_ADS);
            sendBroadcast(i);*/
        } catch (Exception e) {
            e.printStackTrace();
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("connect_status", Constant.CANNOT_CONNECT_SERVER);
            sendBroadcast(i);
        }
    }
}
