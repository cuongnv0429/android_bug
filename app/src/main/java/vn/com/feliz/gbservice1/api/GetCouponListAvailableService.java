package vn.com.feliz.gbservice1.api;

import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.HttpURLConnection;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.CouponAvailableModel;

public class GetCouponListAvailableService extends BaseService {

    public GetCouponListAvailableService() {
        this("");
    }

    public GetCouponListAvailableService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            String url = Constant.DOMAIN + "coupon/list-available";
            HttpURLConnection connection = getConnection(url, RequestMethod.POST);
            writeStream(connection.getOutputStream(), "token="+ Utils.getToken(getApplicationContext()));
            int status = connection.getResponseCode();
            Gson g = new GsonBuilder().setDateFormat(Constant.DATE_FORMAT).create();
            String result;
            if (status == HttpURLConnection.HTTP_OK) {
                result = readStream(connection.getInputStream());
            } else {
                result = readStream(connection.getErrorStream());
            }
            Log.d("HoangNM", "get coupon available list available: " + result);
            CouponAvailableModel model = g.fromJson(result, CouponAvailableModel.class);
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("coupon_available", model);
            i.putExtra(Constant.ACTION, Constant.ACTION_GET_COUPON_AVAILABLE_LIST);
            sendBroadcast(i);
        } catch (Exception e) {
            e.printStackTrace();
            Intent i = new Intent(getApplicationContext().getPackageName());
            i.putExtra("connect_status", Constant.CANNOT_CONNECT_SERVER);
            sendBroadcast(i);
        }
    }


}
