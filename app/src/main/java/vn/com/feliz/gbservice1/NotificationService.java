package vn.com.feliz.gbservice1;


import android.annotation.TargetApi;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import vn.com.feliz.common.Constant;
import vn.com.feliz.m.NotificationModel;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class NotificationService extends NotificationListenerService {

    private String TAG = this.getClass().getSimpleName();
    private NLServiceReceiver nlservicereciver;
    @Override
    public void onCreate() {
        super.onCreate();
//        Log.d(TAG, "onCreate...");
        nlservicereciver = new NLServiceReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("hoangnm.receiver.service");
        registerReceiver(nlservicereciver,filter);
    }

    @Override

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(nlservicereciver);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
//        Log.d("HoangNM", TAG + " CONNECTED");
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
//        Log.d("HoangNM", TAG + " DISCONNECTED");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        //Log.i(TAG,"onNotificationPosted...");
        //Log.i(TAG,"ID :" + sbn.getId() + "\t" + sbn.getNotification().tickerText + "\t" + sbn.getPackageName());
        // DISABLED PUSH
//        if (!TextUtils.isEmpty(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE)) &&
//                !TextUtils.isEmpty(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT))) {
//            Intent i = new Intent(getApplicationContext().getPackageName());
//            i.putExtra(Constant.ACTION, Constant.ACTION_GET_SYSTEM_NOTIFICATION_POSTED);
//            i.putExtra("notification_event", formatNotification(sbn));
//            sendBroadcast(i);
//        }
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
//        Log.i(TAG,"onNotificationRemoved...");
//        Log.e("", new Gson().toJson(sbn));
        Intent i = new  Intent(getApplicationContext().getPackageName());
        i.putExtra(Constant.ACTION, Constant.ACTION_REMOVE_SYSTEM_NOTIFICATION);
        i.putExtra("notification_event", formatNotification(sbn));
        sendBroadcast(i);
    }

    class NLServiceReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if(intent.getStringExtra("command").equals("clear_all")){
                    NotificationService.this.cancelAllNotifications();
                } else {
                    if (intent.getStringExtra("command").equals("clear_one")) {
                        NotificationModel notification = (NotificationModel) intent.getSerializableExtra("notification");
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                            NotificationService.this.cancelNotification(notification.getPackageName(), notification.getTag(), notification.getId());
                        } else {
                            NotificationService.this.cancelNotification(notification.getKey());
                        }
                    } else {
//                        if (intent.getStringExtra("command").equals("list")) {
//                            Intent i = new Intent(getApplicationContext().getPackageName());
//                            StatusBarNotification[] statusBarNotification = NotificationService.this.getActiveNotifications();
//                            if (statusBarNotification == null) return;
//                            List<NotificationModel> notifications = new ArrayList<>();
//                            //for (StatusBarNotification sbn : statusBarNotification) {
//                            for (int j = statusBarNotification.length - 1; j >= 0; j--) {
//                                String title = statusBarNotification[j].getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString();
//                                String text = statusBarNotification[j].getNotification().extras.getCharSequence(Notification.EXTRA_TEXT).toString();
//                                if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(text)) {
//                                    notifications.add(formatNotification(statusBarNotification[j]));
//                                }
//                            }
//                            i.putExtra("notification_event", (Serializable) notifications);
//                            i.putExtra(Constant.ACTION, Constant.ACTION_GET_SYSTEM_NOTIFICATION_LIST);
//                            sendBroadcast(i);
//                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
                //Toast.makeText(getApplicationContext(), "comand: " + intent.getStringExtra("command"), Toast.LENGTH_LONG).show();
//                Log.d(TAG, "Command "+intent.getStringExtra("command"));
            }
        }
    }

    private NotificationModel formatNotification(StatusBarNotification sbn){
        NotificationModel model = new NotificationModel();
        model.setTitle(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE) + "");
        model.setDescription(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT) + "");
        model.setPackageName(sbn.getPackageName());
        model.setIcon(sbn.getNotification().icon);
        model.setPostTime(sbn.getPostTime());
        model.setId(sbn.getId());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            model.setTag(sbn.getTag());
        } else {
            model.setKey(sbn.getKey());
        }
        model.setPushType(sbn.getNotification().extras.getInt("push_type", 0));
        model.setRepeatTimes(sbn.getNotification().extras.getInt("repeat_times", 0));
        model.setCouponId(sbn.getNotification().extras.getString("coupon_id", ""));
        model.setGbCode(sbn.getNotification().extras.getString("gb_code", ""));
        model.setLatitude(sbn.getNotification().extras.getDouble("latitude", 0));
        model.setLongitude(sbn.getNotification().extras.getDouble("longitude", 0));
//        Log.d(TAG, "notification: " + (sbn.getNotification().extras != null?sbn.getNotification().extras.toString():"null"));
        return model;
    }

}
