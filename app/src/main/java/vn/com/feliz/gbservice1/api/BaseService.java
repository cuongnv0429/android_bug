package vn.com.feliz.gbservice1.api;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import vn.com.feliz.BuildConfig;
import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;

public class BaseService  extends IntentService {

    public enum RequestMethod {
        GET,
        POST,
        DELETE,
        PUT
    }
    public BaseService() {
        this("");
    }
    public BaseService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    protected HttpURLConnection getConnection(String url, RequestMethod method) throws Exception {
        Location location = new Location("dummyprovider");
        location.setLatitude(0);
        location.setLongitude(0);
        return getConnection(url, method, location);
    }
    protected HttpURLConnection getConnection(String url, RequestMethod method, Location location) throws Exception {
        String device_id = Utils.getDeviceId(getApplicationContext());
        return getConnection(device_id, url, method, location);
    }

    protected HttpURLConnection getConnection(String device_id, String url, RequestMethod method, Location location) throws Exception {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setConnectTimeout(Constant.CONNECTION_TIMEOUT);
        connection.setReadTimeout(Constant.CONNECTION_TIMEOUT);
        connection.setRequestProperty("Accept-Charset", "UTF-8");
        connection.setRequestMethod(method.name());
        connection.setDoInput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("X-DEVICE-ID", device_id);
        connection.setRequestProperty("X-OS-TYPE", "ANDROID");
        connection.setRequestProperty("X-OS-VERSION", "Android " + Build.VERSION.RELEASE);
        connection.setRequestProperty("X-API-ID", "ID_ITV_GB");
        connection.setRequestProperty("X-API-KEY", "KEY_ITV_GB");
        connection.setRequestProperty("X-APP-VERSION", BuildConfig.VERSION_NAME);
        if(location == null) {
            connection.setRequestProperty("X-LATITUDE", "0");
            connection.setRequestProperty("X-LONGITUDE", "0");
        } else {
            connection.setRequestProperty("X-LATITUDE", "" + location.getLatitude());
            connection.setRequestProperty("X-LONGITUDE", "" + location.getLongitude());
        }
        if(FirebaseInstanceId.getInstance().getToken()!=null) {
            connection.setRequestProperty("X-PUSH-TOKEN",  FirebaseInstanceId.getInstance().getToken());
            Log.e("ePUSH_TOKEN", FirebaseInstanceId.getInstance().getToken());
        } else {
            Log.e("ePUSH_TOKEN", "NO TOKEN");
        }
        return connection;
    }

    protected String readStream(InputStream in) throws IOException {
        StringBuilder result = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        while (true) {
            String line = br.readLine();
            if (line == null) {
                return result.toString();
            }
            result.append(line);
            result.append('\n');
        }
    }

    protected void writeStream(OutputStream out, String data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
        writer.write(data);
        writer.flush();
        writer.close();
        out.close();
    }
}
