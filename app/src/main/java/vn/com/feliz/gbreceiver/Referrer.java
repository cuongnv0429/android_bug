package vn.com.feliz.gbreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import vn.com.feliz.common.Utils;

public class Referrer extends BroadcastReceiver {
    public Referrer() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle extras = intent.getExtras();
        String referrerString = extras.getString("referrer");
        Log.e("mREFERE>>x", referrerString);
        if(referrerString != null && referrerString != "") {
            try {
                Uri uri = Uri.parse("http://localhost/?"+referrerString);
                if (uri != null) {
                    String gift_code = uri.getQueryParameter("gift_code");
                    if (gift_code != null && !gift_code.equals("")) {
                        Log.e("mREFERE>>y", gift_code);
                        Utils.saveReferreCode(context, gift_code);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}