package vn.com.feliz.gbreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import vn.com.feliz.common.Constant;
import vn.com.feliz.common.Utils;
import vn.com.feliz.m.PushModel;

public class LockScreenReceiver extends BroadcastReceiver {

    private static final String FIRE_BASE_URL = Constant.FIREBASE_URL;
    public static DatabaseReference mFirebaseDatabase;
    public static FirebaseDatabase mFirebaseInstance;
    protected static ValueEventListener mFireBaseRef;
    protected static ChildEventListener mChildEventListener;
    protected static ValueEventListener mValueEventListener;
    public static List<PushModel> mPushList = new ArrayList<>();
    private PushModel mPushModel;
    private boolean mFireBaseInitialed = false;
    private long firebaseTimeout;
    private int mCount = 0;
    private int initItemTotal = 0;

    public LockScreenReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("LockScreenFragment","onReceive lockscreen ACTION_SCREEN_OFF");
            /*Intent lockIntent = new Intent("android.intent.lockscreen");
            lockIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(lockIntent);*/
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("LockScreenFragment","onReceive lockscreen ACTION_SCREEN_ON");

            //connectFireBase(context);

            // do other things if you need
        } else if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            // do other things if you need
        }
    }

    private void connectFireBase(final Context context) {
        String device_id = Utils.generateUUID(context);
        Log.e("Device ID", device_id);

        if(mFirebaseDatabase != null && firebaseTimeout < System.currentTimeMillis()) {
            mFirebaseDatabase = null;
            mChildEventListener = null;
            mCount = 0;
            mFireBaseInitialed = false;
        }
        if(mFirebaseDatabase != null) {
            startLockscreen(context);
            return;
        }

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReferenceFromUrl(FIRE_BASE_URL).child("ads").child(device_id);

        mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(">>>>> CHANGE DATA", dataSnapshot.toString());

                Utils.clearAds(context);
                mPushList = new ArrayList<>();

                String json = new Gson().toJson(dataSnapshot.getValue());

                try {
                    JSONObject jobj = new JSONObject(json);
                    Iterator<String> iter = jobj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {

                            JSONObject object = jobj.getJSONObject(key);

                            mPushModel = new PushModel();

                            if(object.has("push_type")) {
                                mPushModel.setPush_type(object.getString("push_type"));
                            }
                            if(object.has("ads_id")) {
                                mPushModel.setAds_id(object.getString("ads_id"));
                            }
                            if(object.has("description")) {
                                mPushModel.setDescription(object.getString("description"));
                            }
                            if(object.has("coupon_id")) {
                                mPushModel.setCoupon_id(object.getString("coupon_id"));
                            }
                            if(object.has("coupon_sub_id")) {
                                mPushModel.setCoupon_sub_id(object.getString("coupon_sub_id"));
                            }
                            if(object.has("post_time")) {
                                mPushModel.setPost_time(object.getString("post_time"));
                            }
                            if(object.has("next_show_time")) {
                                mPushModel.setNext_show_time(object.getString("next_show_time"));
                            }

                            addData(context, mPushModel);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    JSONArray object = new JSONArray(json);
                    initItemTotal = 0;
                    for(int i = 0; i<object.length(); i++) {
                        try {
                            try {
                                JSONObject jo = object.getJSONObject(i);
                                Log.e(">>", jo.toString());
                                mPushModel = new PushModel();
                                if(jo.has("push_type")) {
                                    mPushModel.setPush_type(jo.getString("push_type"));
                                }
                                if(jo.has("ads_id")) {
                                    mPushModel.setAds_id(jo.getString("ads_id"));
                                }
                                if(jo.has("description")) {
                                    mPushModel.setDescription(jo.getString("description"));
                                }
                                if(jo.has("coupon_id")) {
                                    mPushModel.setCoupon_id(jo.getString("coupon_id"));
                                }
                                if(jo.has("coupon_sub_id")) {
                                    mPushModel.setCoupon_sub_id(jo.getString("coupon_sub_id"));
                                }
                                if(jo.has("post_time")) {
                                    mPushModel.setPost_time(jo.getString("post_time"));
                                }
                                if(jo.has("next_show_time")) {
                                    mPushModel.setNext_show_time(jo.getString("next_show_time"));
                                }

                                addData(context, mPushModel);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                startLockscreen(context);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                mFirebaseDatabase = null;
            }
        });
    }

    private void addData(Context context, PushModel pushModel) {
        if(pushModel == null) {
            return;
        }

        if (
                pushModel.getPush_type().equals("2")
                        || pushModel.getPush_type().equals("3")
                        || pushModel.getPush_type().equals("5")
                        || pushModel.getPush_type().equals("6")
                        || pushModel.getPush_type().equals("7")
                        || pushModel.getPush_type().equals("8")
                        || pushModel.getPush_type().equals("10")
                )
        {
            if (pushModel.getPush_type() != null) {
                if(checkPushAdsExist(pushModel.getAds_id())) {
                    Utils.deleteAds(context, pushModel.getAds_id());
                    Log.e(">>>>> Ads add: ", "IS EXIST ADS ID: "+pushModel.getAds_id());
                }

                Gson g = new Gson();
                Utils.saveAds(context, g.toJson(pushModel));
                mPushList.add(pushModel);
            } else {
                Log.e(">>>>> Ads add: ", "IS PUSH TYPE NULL");
            }
        } else {
            Log.e(">>>>> Ads add: ", "IS PUSH TYPE OUT OF TYPE");
        }
    }

    private void startLockscreen(Context context) {
        Log.e(">>>>Check Ads:", "startLockscreen");
        if(Utils.getAds(context) != null) {
            Intent lockIntent = new Intent("android.intent.lockscreen");
            lockIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            context.startActivity(lockIntent);
        }
    }

    private boolean checkPushAdsExist(String ads_id) {
        if(mPushList != null) {
            for(int i= 0; i<mPushList.size(); i++) {
                if(mPushList.get(i).getAds_id().equals(ads_id)) {
                    return true;
                }
            }
        }
        return false;
    }
}