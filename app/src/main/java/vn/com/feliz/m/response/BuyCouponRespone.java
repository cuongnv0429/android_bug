package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;
/**
 * Created by ITV01 on 12/27/16.
 */

public class BuyCouponRespone {
    @SerializedName("coupon_id")
    @Expose
    private String coupon_id;

    @SerializedName("coupon_sub_id")
    @Expose
    private String coupon_sub_id;

    @SerializedName("coupon_type")
    @Expose
    private String coupon_type;

    @SerializedName("media_type")
    @Expose
    private String media_type;
    @SerializedName("media_url")
    @Expose
    private String media_url;

    @SerializedName("video_url")
    @Expose
    private String video_url;

    @SerializedName("media_url_list")
    @Expose
    private List<String> media_url_list;

    @SerializedName("coupon_name")
    @Expose
    private String coupon_name;

    @SerializedName("coupon_size_des")
    @Expose
    private String coupon_size_des;

    @SerializedName("coupon_type_des")
    @Expose
    private String coupon_type_des;

    @SerializedName("coupon_rating")
    @Expose
    private float coupon_rating;

    @SerializedName("coupon_coin")
    @Expose
    private String coupon_coin;

    @SerializedName("coupon_coin_des")
    @Expose
    private String coupon_coin_des;

    @SerializedName("coupon_review")
    @Expose
    private String coupon_review;

    @SerializedName("coupon_barcode")
    @Expose
    private String coupon_barcode;

    @SerializedName("coupon_expired")
    @Expose
    private String coupon_expired;

    @SerializedName("coupon_expired_date")
    @Expose
    private String coupon_expired_date;

    @SerializedName("coupon_expired_day")
    @Expose
    private String coupon_expired_day;

    @SerializedName("coupon_expired_day_des")
    @Expose
    private String coupon_expired_day_des;

    @SerializedName("coupon_exchange_date")
    @Expose
    private String coupon_exchange_date;

    @SerializedName("coupon_exchange_day")
    @Expose
    private String coupon_exchange_day;

    @SerializedName("coupon_exchange_date_des")
    @Expose
    private String coupon_exchange_date_des;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("quantity")
    @Expose
    private int quantity;

    @SerializedName("list")
    @Expose
    private List<BuyCouponRespone> list;

    @SerializedName("list_product")
    @Expose
    private List<Product> list_product;

    @SerializedName("list_exchange_shop")
    @Expose
    private List<Shop> list_exchange_shop;

    @SerializedName("share_link")
    @Expose
    private String share_link;

    @SerializedName("share_buy_link")
    @Expose
    private String share_buy_link;

    @SerializedName("share_title")
    @Expose
    private String share_title;

    @SerializedName("share_buy_title")
    @Expose
    private String share_buy_title;

    @SerializedName("is_invite")
    @Expose
    private String is_invite;

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public void setCoupon_type(String coupon_type) {
        this.coupon_type = coupon_type;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public List<String> getMedia_url_list() {
        return media_url_list;
    }

    public void setMedia_url_list(List<String> media_url_list) {
        this.media_url_list = media_url_list;
    }

    public String getCoupon_name() {
        return coupon_name;
    }

    public void setCoupon_name(String coupon_name) {
        this.coupon_name = coupon_name;
    }

    public String getCoupon_size_des() {
        return coupon_size_des;
    }

    public void setCoupon_size_des(String coupon_size_des) {
        this.coupon_size_des = coupon_size_des;
    }

    public String getCoupon_type_des() {
        return coupon_type_des;
    }

    public void setCoupon_type_des(String coupon_type_des) {
        this.coupon_type_des = coupon_type_des;
    }

    public float getCoupon_rating() {
        return coupon_rating;
    }

    public void setCoupon_rating(float coupon_rating) {
        this.coupon_rating = coupon_rating;
    }

    public String getCoupon_coin() {
        return coupon_coin;
    }

    public void setCoupon_coin(String coupon_coin) {
        this.coupon_coin = coupon_coin;
    }

    public String getCoupon_coin_des() {
        return coupon_coin_des;
    }

    public void setCoupon_coin_des(String coupon_coin_des) {
        this.coupon_coin_des = coupon_coin_des;
    }

    public String getCoupon_review() {
        return coupon_review;
    }

    public void setCoupon_review(String coupon_review) {
        this.coupon_review = coupon_review;
    }

    public String getCoupon_barcode() {
        return coupon_barcode;
    }

    public void setCoupon_barcode(String coupon_barcode) {
        this.coupon_barcode = coupon_barcode;
    }

    public String getCoupon_expired() {
        return coupon_expired;
    }

    public void setCoupon_expired(String coupon_expired) {
        this.coupon_expired = coupon_expired;
    }

    public String getCoupon_expired_date() {
        return coupon_expired_date;
    }

    public void setCoupon_expired_date(String coupon_expired_date) {
        this.coupon_expired_date = coupon_expired_date;
    }

    public String getCoupon_expired_day() {
        return coupon_expired_day;
    }

    public void setCoupon_expired_day(String coupon_expired_day) {
        this.coupon_expired_day = coupon_expired_day;
    }

    public String getCoupon_exchange_date() {
        return coupon_exchange_date;
    }

    public void setCoupon_exchange_date(String coupon_exchange_date) {
        this.coupon_exchange_date = coupon_exchange_date;
    }

    public String getCoupon_exchange_day() {
        return coupon_exchange_day;
    }

    public void setCoupon_exchange_day(String coupon_exchange_day) {
        this.coupon_exchange_day = coupon_exchange_day;
    }

    public String getCoupon_expired_day_des() {
        return coupon_expired_day_des;
    }

    public void setCoupon_expired_day_des(String coupon_expired_day_des) {
        this.coupon_expired_day_des = coupon_expired_day_des;
    }

    public String getCoupon_exchange_date_des() {
        return coupon_exchange_date_des;
    }

    public void setCoupon_exchange_date_des(String coupon_exchange_date_des) {
        this.coupon_exchange_date_des = coupon_exchange_date_des;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<BuyCouponRespone> getList() {
        return list;
    }

    public void setList(List<BuyCouponRespone> list) {
        this.list = list;
    }

    public List<Product> getList_product() {
        return list_product;
    }

    public void setList_product(List<Product> list_product) {
        this.list_product = list_product;
    }

    public List<Shop> getList_exchange_shop() {
        return list_exchange_shop;
    }

    public void setList_exchange_shop(List<Shop> list_exchange_shop) {
        this.list_exchange_shop = list_exchange_shop;
    }

    public String getShare_link() {
        return share_link;
    }

    public void setShare_link(String share_link) {
        this.share_link = share_link;
    }

    public String getShare_buy_link() {
        return share_buy_link;
    }

    public void setShare_buy_link(String share_buy_link) {
        this.share_buy_link = share_buy_link;
    }

    public String getShare_title() {
        return share_title;
    }

    public void setShare_title(String share_title) {
        this.share_title = share_title;
    }

    public String getShare_buy_title() {
        return share_buy_title;
    }

    public void setShare_buy_title(String share_buy_title) {
        this.share_buy_title = share_buy_title;
    }

    public String getIs_invite() {
        return is_invite;
    }

    public void setIs_invite(String is_invite) {
        this.is_invite = is_invite;
    }

    public class Product {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("image")
        @Expose
        private String image;

        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public class Shop {
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("distance")
        @Expose
        private String distance;

        @SerializedName("latitude")
        @Expose
        private double latitude;

        @SerializedName("longitude")
        @Expose
        private double longitude;

        @SerializedName("status")
        @Expose
        private int status;

        @SerializedName("status_des")
        @Expose
        private String statusDes;
    }


}
