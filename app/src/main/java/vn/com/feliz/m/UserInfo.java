package vn.com.feliz.m;

import android.util.Log;

import io.realm.RealmObject;

/**
 * Copyright Innotech Vietnam
 *
 */
public class UserInfo extends RealmObject {

    private String userId;
    private String fullname;
    private String username;
    private String email;
    private String firebase_password;
    private String token;
    private String login_type;
    private String uid;
    private double latitude;
    private double longitude;
    private String allow_ads;
    private String is_verify;
    private String access_token;
    private String phone;
    private String fb_id;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirebase_password() {
        return firebase_password;
    }

    public void setFirebase_password(String firebase_password) {
        this.firebase_password = firebase_password;
    }

    public String getToken() {
        Log.d("sssDEBUG",""+token);
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setLatitude(double n) {
        this.latitude = n;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLongitude(double n) {
        this.longitude = n;
    }
    public double getLongitude() {
        return longitude;
    }

    public void setAllow_ads(String t) {
        this.allow_ads = t;
    }
    public String getAllow_ads() {
        return allow_ads;
    }

    public void setIs_verify(String status) {
        this.is_verify = status;
    }
    public String getIs_verify() {
        return is_verify;
    }

    public void setFb_id(String id) {
        this.fb_id = id;
    }
    public String getFb_id() {
        return fb_id;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
    public String getAccess_token() {
        return access_token;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }
}
