package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nguyen Thai Son on 2016-11-15.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */

public class QuestionDailyResponse {
    @SerializedName("question_id")
    @Expose
    private String question_id;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("answers")
    @Expose
    private List<AnswerResponse> listAnser = new ArrayList<AnswerResponse>();

    public List<AnswerResponse> getListAnser() {
        return listAnser;
    }

    public void setListAnser(List<AnswerResponse> listAnser) {
        this.listAnser = listAnser;
    }

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}
