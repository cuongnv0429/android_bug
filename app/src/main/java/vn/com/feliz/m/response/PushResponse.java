package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class PushResponse implements Serializable {
    @SerializedName("push_type")
    @Expose
    private String push_type;

    @SerializedName("ads_id")
    @Expose
    private String ads_id;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("post_time")
    @Expose
    private String post_time;

    @SerializedName("next_show_time")
    @Expose
    private String next_show_time;

    @SerializedName("coupon_id")
    @Expose
    private String coupon_id;

    @SerializedName("coupon_sub_id")
    @Expose
    private String coupon_sub_id;

    @SerializedName("gb_code")
    @Expose
    private String gb_code;

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getAds_id() {
        return ads_id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPush_type(String push_type) {
        this.push_type = push_type;
    }

    public String getPush_type() {
        return push_type;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }

    public String getPost_time() {
        return post_time;
    }

    public void setNext_show_time(String next_show_time) {
        this.next_show_time = next_show_time;
    }

    public String getNext_show_time() {
        return next_show_time;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setGb_code(String gb_code) {
        this.gb_code = gb_code;
    }

    public String getGb_code() {
        return gb_code;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }
}
