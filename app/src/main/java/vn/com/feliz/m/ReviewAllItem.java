package vn.com.feliz.m;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import vn.com.feliz.m.response.ListStartResponse;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class ReviewAllItem {
    @SerializedName("review_id")
    @Expose
    private String review_id;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("product_name")
    @Expose
    private String product_name;
    @SerializedName("product_image")
    @Expose
    private String product_image;
    @SerializedName("product_rating")
    @Expose
    private String product_rating;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("is_sponsor")
    @Expose
    private String is_sponsor;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("list_stars")
    @Expose
    private ListStartResponse listStart;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("product_id")
    @Expose
    private String product_id;
    @SerializedName("user_review")
    @Expose
    private String user_review;
    @SerializedName("usefull")
    @Expose
    private String usefull;
   @SerializedName("product_review")
    @Expose
    private String product_review;
   @SerializedName("is_review")
    @Expose
    private String is_review;

    private boolean isExpand = false;

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getUsefull() {
        return usefull;
    }

    public void setUsefull(String usefull) {
        this.usefull = usefull;
    }

    public String getIs_review() {
        return is_review;
    }

    public void setIs_review(String is_review) {
        this.is_review = is_review;
    }

    public String getUser_review() {
        return user_review;
    }

    public void setUser_review(String user_review) {
        this.user_review = user_review;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_rating() {
        return product_rating;
    }

    public void setProduct_rating(String product_rating) {
        this.product_rating = product_rating;
    }

    public String getProduct_review() {
        return product_review;
    }

    public void setProduct_review(String product_review) {
        this.product_review = product_review;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIs_sponsor() {
        return is_sponsor;
    }

    public void setIs_sponsor(String is_sponsor) {
        this.is_sponsor = is_sponsor;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public ListStartResponse getListStart() {
        return listStart;
    }

    public void setListStart(ListStartResponse listStart) {
        this.listStart = listStart;
    }
}
