package vn.com.feliz.m;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PushModel implements Serializable{
    private String push_type;
    private String ads_id;
    private String description;
    private String post_time;
    private String coupon_id;
    private String coupon_sub_id;
    private String next_show_time;

    public void setPush_type(String push_type) {
        this.push_type = push_type;
    }

    public String getPush_type() {
        return push_type;
    }

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getAds_id() {
        return ads_id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPost_time(String post_time) {
        this.post_time = post_time;
    }
    public String getPost_time() {
        return post_time;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }

    public void setNext_show_time(String next_show_time) {
        this.next_show_time = next_show_time;
    }

    public String getNext_show_time() {
        return next_show_time;
    }
}
