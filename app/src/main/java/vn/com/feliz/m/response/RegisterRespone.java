package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Thai Son on 2016-11-14.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RegisterRespone {
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firebase_password")
    @Expose
    private String firebase_password;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("coupon_id")
    @Expose
    private String coupon_id;
    @SerializedName("coupon_sub_id")
    @Expose
    private String coupon_sub_id;
    @SerializedName("login_type")
    @Expose
    private String login_type;

    @SerializedName("uid")
    @Expose
    private String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirebase_password() {
        return firebase_password;
    }

    public void setFirebase_password(String firebase_password) {
        this.firebase_password = firebase_password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
