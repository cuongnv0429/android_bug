package vn.com.feliz.m;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import vn.com.feliz.m.response.ShopModel;

public class CouponModel extends BaseModel implements Serializable {
    @SerializedName("data")
    private Coupon coupon;

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public static class Coupon implements Serializable {
        @SerializedName("coupon_id")
        private String couponId;
        @SerializedName("coupon_sub_id")
        private String couponSubId;
        @SerializedName("coupon_type")
        private String couponType;
        @SerializedName("media_type")
        private String mediaType;
        @SerializedName("media_url")
        private String mediaUrl;
        @SerializedName("video_url")
        private String videoUrl;
        @SerializedName("media_url_list")
        private List<String> mediaUrlList;
        @SerializedName("coupon_name")
        private String couponName;
        @SerializedName("coupon_size_des")
        private String couponSizeDes;
        @SerializedName("coupon_type_des")
        private String couponTypeDes;
        @SerializedName("coupon_rating")
        private float couponRating;
        @SerializedName("coupon_coin")
        private String couponCoin;
        @SerializedName("coupon_coin_des")
        private String couponCoinDes;
        @SerializedName("coupon_review")
        private String couponReview;
        @SerializedName("coupon_barcode")
        private String couponBarCode;
        @SerializedName("coupon_expired")
        private String couponExpired;
        @SerializedName("coupon_expired_date")
        private Date couponExpiredDate;
        @SerializedName("coupon_expired_day")
        private String couponExpiredDay;
        @SerializedName("coupon_exchange_date")
        private String couponExchangeDate;
        @SerializedName("coupon_exchange_home_status")
        private int couponExchangeHomeStatus;
        @SerializedName("coupon_exchange_home_address")
        private String couponExchangeHomeAddress;
        @SerializedName("coupon_exchange_day")
        private String couponExchangeDay;
        @SerializedName("status")
        private String status;
        @SerializedName("quantity")
        private int quantity;
        @SerializedName("list")
        private List<Coupon> list;
        @SerializedName("list_product")
        private List<Product> listProduct;
        @SerializedName("list_exchange_shop")
        private List<ShopModel.Shop> shops;
        @SerializedName("share_link")
        private String shareLink;
        @SerializedName("share_buy_link")
        private String shareBuyLink;
        @SerializedName("share_title")
        private String shareTitle;
        @SerializedName("share_buy_title")
        private String shareBuyTitle;
        @SerializedName("is_invite")
        private String isInvite;
        @SerializedName("is_review")
        private String isReview;
        @SerializedName("is_escommerce")
        private String isEscommerce;
        @SerializedName("escommerce_link")
        private String escommerceLink;
        @SerializedName("list_shop_message")
        private String listShopMessage;
        @SerializedName("escomercial_text")
        private String esComericalText;
        @SerializedName("escomercial_popup_text")
        private String esComericalPopupText;


        public String getEsComericalText() {
            return esComericalText;
        }

        public void setEsComericalText(String esComericalText) {
            this.esComericalText = esComericalText;
        }

        public String getEsComericalPopupText() {
            return esComericalPopupText;
        }

        public void setEsComericalPopupText(String esComericalPopupText) {
            this.esComericalPopupText = esComericalPopupText;
        }

        public String getListShopMessage() {
            return listShopMessage;
        }

        public void setListShopMessage(String listShopMessage) {
            this.listShopMessage = listShopMessage;
        }

        private int viewType;//type on listview, not real data

        public String getCouponId() {
            return couponId;
        }

        public void setCouponId(String couponId) {
            this.couponId = couponId;
        }

        public String getCouponSubId() {
            return couponSubId;
        }

        public void setCouponSubId(String couponSubId) {
            this.couponSubId = couponSubId;
        }

        public String getCouponName() {
            return couponName;
        }

        public void setCouponName(String couponName) {
            this.couponName = couponName;
        }

        public List<Product> getListProduct() {
            return listProduct;
        }

        public void setListProduct(List<Product> listProduct) {
            this.listProduct = listProduct;
        }

        public List<ShopModel.Shop> getShops() {
            return shops;
        }

        public void setShops(List<ShopModel.Shop> shops) {
            this.shops = shops;
        }

        public String getCouponType() {
            return couponType;
        }

        public void setCouponType(String couponType) {
            this.couponType = couponType;
        }

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getMediaUrl() {
            return mediaUrl;
        }

        public void setMediaUrl(String mediaUrl) {
            this.mediaUrl = mediaUrl;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public List<String> getMediaUrlList() {
            return mediaUrlList;
        }

        public void setMediaUrlList(List<String> mediaUrlList) {
            this.mediaUrlList = mediaUrlList;
        }

        public String getCouponSizeDes() {
            return couponSizeDes;
        }

        public void setCouponSizeDes(String couponSizeDes) {
            this.couponSizeDes = couponSizeDes;
        }

        public String getCouponTypeDes() {
            return couponTypeDes;
        }

        public void setCouponTypeDes(String couponTypeDes) {
            this.couponTypeDes = couponTypeDes;
        }

        public float getCouponRating() {
            return couponRating;
        }

        public void setCouponRating(float couponRating) {
            this.couponRating = couponRating;
        }

        public String getCouponCoin() {
            return couponCoin;
        }

        public void setCouponCoin(String couponCoin) {
            this.couponCoin = couponCoin;
        }

        public String getCouponCoinDes() {
            return couponCoinDes;
        }

        public void setCouponCoinDes(String couponCoinDes) {
            this.couponCoinDes = couponCoinDes;
        }

        public String getCouponReview() {
            return couponReview;
        }

        public void setCouponReview(String couponReview) {
            this.couponReview = couponReview;
        }

        public String getCouponBarCode() {
            return couponBarCode;
        }

        public void setCouponBarCode(String couponBarCode) {
            this.couponBarCode = couponBarCode;
        }

        public String getCouponExpired() {
            return couponExpired;
        }

        public void setCouponExpired(String couponExpired) {
            this.couponExpired = couponExpired;
        }


        public Date getCouponExpiredDate() {
            return couponExpiredDate;
        }

        public void setCouponExpiredDate(Date couponExpiredDate) {
            this.couponExpiredDate = couponExpiredDate;
        }

        public String getCouponExpiredDay() {
            return couponExpiredDay;
        }

        public void setCouponExpiredDay(String couponExpiredDay) {
            this.couponExpiredDay = couponExpiredDay;
        }

        public String getCouponExchangeDate() {
            return couponExchangeDate;
        }

        public void setCouponExchangeDate(String couponExchangeDate) {
            this.couponExchangeDate = couponExchangeDate;
        }

        public String getCouponExchangeDay() {
            return couponExchangeDay;
        }

        public void setCouponExchangeDay(String couponExchangeDay) {
            this.couponExchangeDay = couponExchangeDay;
        }

        public int getCouponExchangeHomeStatus() {
            return couponExchangeHomeStatus;
        }

        public void setCouponExchangeHomeStatus(int couponExchangeHomeStatus) {
            this.couponExchangeHomeStatus = couponExchangeHomeStatus;
        }

        public String getCouponExchangeHomeAddress() {
            return couponExchangeHomeAddress;
        }

        public void setCouponExchangeHomeAddress(String couponExchangeHomeAddress) {
            this.couponExchangeHomeAddress = couponExchangeHomeAddress;
        }

        public String getShareLink() {
            return shareLink;
        }

        public void setShareLink(String shareLink) {
            this.shareLink = shareLink;
        }

        public String getShareBuyLink() {
            return shareBuyLink;
        }

        public void setShareBuyLink(String shareBuyLink) {
            this.shareBuyLink = shareBuyLink;
        }

        public String getShareTitle() {
            return shareTitle;
        }

        public void setShareTitle(String shareTitle) {
            this.shareTitle = shareTitle;
        }

        public String getShareBuyTitle() {
            return shareBuyTitle;
        }

        public void setShareBuyTitle(String shareBuyTitle) {
            this.shareBuyTitle = shareBuyTitle;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getQuantity() {
            return quantity;
        }

        public void setQuantity(int quantity) {
            this.quantity = quantity;
        }

        public List<Coupon> getList() {
            return list;
        }

        public void setList(List<Coupon> list) {
            this.list = list;
        }

        public int getViewType() {
            return viewType;
        }

        public void setViewType(int viewType) {
            this.viewType = viewType;
        }

        public String getIsInvite() {
            return isInvite;
        }

        public void setIsInvite(String isInvite) {
            this.isInvite = isInvite;
        }

        public String getIsReview() {
            return isReview;
        }

        public void setIsReview(String isReview) {
            this.isReview = isReview;
        }

        public String getIsEscommerce() {
            return isEscommerce;
        }

        public void setIsEscommerce(String isEscommerce) {
            this.isEscommerce = isEscommerce;
        }

        public void setEscommerceLink(String url) {
            this.escommerceLink = url;
        }
        public String getEscommerceLink() {
            return escommerceLink;
        }

        public class Product implements Serializable{
            @SerializedName("id")
            private String id;
            @SerializedName("image")
            private String image;
            @SerializedName("name")
            private String name;
            @SerializedName("rating")
            private String rating;
            @SerializedName("review")
            private String review;
            @SerializedName("barcode")
            private String barcode;
            @SerializedName("is_review")
            private String is_review;

            public String getRating() {
                return rating;
            }

            public void setRating(String rating) {
                this.rating = rating;
            }

            public String getReview() {
                return review;
            }

            public void setReview(String review) {
                this.review = review;
            }

            public String getBarcode() {
                return barcode;
            }

            public void setBarcode(String barcode) {
                this.barcode = barcode;
            }

            public String getIs_review() {
                return is_review;
            }

            public void setIs_review(String is_review) {
                this.is_review = is_review;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
