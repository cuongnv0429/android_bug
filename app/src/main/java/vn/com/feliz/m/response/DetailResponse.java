package vn.com.feliz.m.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import vn.com.feliz.m.CouponModel;

public class DetailResponse  implements Serializable {
@SerializedName("coupon_id")
private String couponId;
    @SerializedName("coupon_sub_id")
    private String couponSubId;
    @SerializedName("coupon_type")
    private String couponType;
    @SerializedName("media_type")
    private String mediaType;
    @SerializedName("media_url")
    private String mediaUrl;
    @SerializedName("video_url")
    private String videoUrl;
    @SerializedName("media_url_list")
    private List<String> mediaUrlList;
    @SerializedName("coupon_name")
    private String couponName;
    @SerializedName("coupon_size_des")
    private String couponSizeDes;
    @SerializedName("coupon_type_des")
    private String couponTypeDes;
    @SerializedName("coupon_rating")
    private float couponRating;
    @SerializedName("coupon_coin")
    private String couponCoin;
    @SerializedName("coupon_coin_des")
    private String couponCoinDes;
    @SerializedName("coupon_review")
    private String couponReview;
    @SerializedName("coupon_barcode")
    private String couponBarCode;
    @SerializedName("coupon_expired")
    private String couponExpired;
    @SerializedName("coupon_expired_date")
    private Date couponExpiredDate;
    @SerializedName("coupon_expired_day")
    private String couponExpiredDay;
    @SerializedName("coupon_exchange_date")
    private String couponExchangeDate;
    @SerializedName("coupon_exchange_day")
    private String couponExchangeDay;
    @SerializedName("status")
    private String status;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("list")
    private List<CouponModel.Coupon> list;
    @SerializedName("list_product")
    private List<CouponModel.Coupon.Product> listProduct;
    @SerializedName("list_exchange_shop")
    private List<ShopModel.Shop> shops;
    @SerializedName("share_link")
    private String shareLink;
    @SerializedName("share_buy_link")
    private String shareBuyLink;
    @SerializedName("share_title")
    private String shareTitle;
    @SerializedName("share_buy_title")
    private String shareBuyTitle;
    @SerializedName("is_invite")
    private String isInvite;

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponSubId() {
        return couponSubId;
    }

    public void setCouponSubId(String couponSubId) {
        this.couponSubId = couponSubId;
    }

    public String getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public List<String> getMediaUrlList() {
        return mediaUrlList;
    }

    public void setMediaUrlList(List<String> mediaUrlList) {
        this.mediaUrlList = mediaUrlList;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getCouponSizeDes() {
        return couponSizeDes;
    }

    public void setCouponSizeDes(String couponSizeDes) {
        this.couponSizeDes = couponSizeDes;
    }

    public String getCouponTypeDes() {
        return couponTypeDes;
    }

    public void setCouponTypeDes(String couponTypeDes) {
        this.couponTypeDes = couponTypeDes;
    }

    public float getCouponRating() {
        return couponRating;
    }

    public void setCouponRating(float couponRating) {
        this.couponRating = couponRating;
    }

    public String getCouponCoin() {
        return couponCoin;
    }

    public void setCouponCoin(String couponCoin) {
        this.couponCoin = couponCoin;
    }

    public String getCouponCoinDes() {
        return couponCoinDes;
    }

    public void setCouponCoinDes(String couponCoinDes) {
        this.couponCoinDes = couponCoinDes;
    }

    public String getCouponReview() {
        return couponReview;
    }

    public void setCouponReview(String couponReview) {
        this.couponReview = couponReview;
    }

    public String getCouponBarCode() {
        return couponBarCode;
    }

    public void setCouponBarCode(String couponBarCode) {
        this.couponBarCode = couponBarCode;
    }

    public String getCouponExpired() {
        return couponExpired;
    }

    public void setCouponExpired(String couponExpired) {
        this.couponExpired = couponExpired;
    }

    public Date getCouponExpiredDate() {
        return couponExpiredDate;
    }

    public void setCouponExpiredDate(Date couponExpiredDate) {
        this.couponExpiredDate = couponExpiredDate;
    }

    public String getCouponExpiredDay() {
        return couponExpiredDay;
    }

    public void setCouponExpiredDay(String couponExpiredDay) {
        this.couponExpiredDay = couponExpiredDay;
    }

    public String getCouponExchangeDate() {
        return couponExchangeDate;
    }

    public void setCouponExchangeDate(String couponExchangeDate) {
        this.couponExchangeDate = couponExchangeDate;
    }

    public String getCouponExchangeDay() {
        return couponExchangeDay;
    }

    public void setCouponExchangeDay(String couponExchangeDay) {
        this.couponExchangeDay = couponExchangeDay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public List<CouponModel.Coupon> getList() {
        return list;
    }

    public void setList(List<CouponModel.Coupon> list) {
        this.list = list;
    }

    public List<CouponModel.Coupon.Product> getListProduct() {
        return listProduct;
    }

    public void setListProduct(List<CouponModel.Coupon.Product> listProduct) {
        this.listProduct = listProduct;
    }

    public List<ShopModel.Shop> getShops() {
        return shops;
    }

    public void setShops(List<ShopModel.Shop> shops) {
        this.shops = shops;
    }

    public String getShareLink() {
        return shareLink;
    }

    public void setShareLink(String shareLink) {
        this.shareLink = shareLink;
    }

    public String getShareBuyLink() {
        return shareBuyLink;
    }

    public void setShareBuyLink(String shareBuyLink) {
        this.shareBuyLink = shareBuyLink;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareBuyTitle() {
        return shareBuyTitle;
    }

    public void setShareBuyTitle(String shareBuyTitle) {
        this.shareBuyTitle = shareBuyTitle;
    }

    public String getIsInvite() {
        return isInvite;
    }

    public void setIsInvite(String isInvite) {
        this.isInvite = isInvite;
    }
}
