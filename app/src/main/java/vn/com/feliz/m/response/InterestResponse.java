package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nguyen Thai Son on 2016-11-15.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class InterestResponse implements Serializable{
    @SerializedName("group_id")
    @Expose
    private String group_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("list")
    @Expose
    private List<InterestResponseItem> list = new ArrayList<InterestResponseItem>();

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<InterestResponseItem> getList() {
        return list;
    }

    public void setList(List<InterestResponseItem> list) {
        this.list = list;
    }
}
