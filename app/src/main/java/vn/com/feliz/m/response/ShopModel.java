package vn.com.feliz.m.response;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import vn.com.feliz.m.BaseModel;

public class ShopModel extends BaseModel implements Serializable{

    @SerializedName("data")
    private List<Shop> shop;

    public List<Shop> getShop() {
        return shop;
    }

    public void setShop(List<Shop> shop) {
        this.shop = shop;
    }

    public class Shop implements Serializable{
        @SerializedName("id")
        private String id;
        @SerializedName("name")
        private String name;
        @SerializedName("distance")
        private String distance;
        @SerializedName("latitude")
        private double latitude;
        @SerializedName("longitude")
        private double longitude;
        @SerializedName("status")
        private int status;
        @SerializedName("status_des")
        private String statusDes;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getStatusDes() {
            return statusDes;
        }

        public void setStatusDes(String statusDes) {
            this.statusDes = statusDes;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Shop)) return false;

            Shop shop = (Shop) o;

            if (Double.compare(shop.getLatitude(), getLatitude()) != 0) return false;
            if (Double.compare(shop.getLongitude(), getLongitude()) != 0) return false;
            if (getStatus() != shop.getStatus()) return false;
            if (getId() != null ? !getId().equals(shop.getId()) : shop.getId() != null)
                return false;
            if (getName() != null ? !getName().equals(shop.getName()) : shop.getName() != null)
                return false;
            return getDistance() != null ? getDistance().equals(shop.getDistance()) : shop.getDistance() == null && (getStatusDes() != null ? getStatusDes().equals(shop.getStatusDes()) : shop.getStatusDes() == null);

        }

        @Override
        public int hashCode() {
            int result;
            long temp;
            result = getId() != null ? getId().hashCode() : 0;
            result = 31 * result + (getName() != null ? getName().hashCode() : 0);
            result = 31 * result + (getDistance() != null ? getDistance().hashCode() : 0);
            temp = Double.doubleToLongBits(getLatitude());
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            temp = Double.doubleToLongBits(getLongitude());
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            result = 31 * result + getStatus();
            result = 31 * result + (getStatusDes() != null ? getStatusDes().hashCode() : 0);
            return result;
        }
    }
}
