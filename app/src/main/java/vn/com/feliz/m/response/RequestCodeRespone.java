package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Thai Son on 2016-11-14.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class RequestCodeRespone {

    public RequestCodeRespone() {
        this.verify_code = "";
        this.phone = "";
    }

    @SerializedName("verify_code")
    @Expose
    private String verify_code;

    @SerializedName("phone")
    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVerify_code() {
        return verify_code;
    }

    public void setVerify_code(String verify_code) {
        this.verify_code = verify_code;
    }
}
