package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Copyright Innotech Vietnam
 * Created by Mr son on 6/24/2016.
 */
public class LoginResponse {
    @SerializedName("login_type")
    @Expose
    private String login_type;
    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("uid")
    @Expose
    private String uid;

    @SerializedName("facebook_id")
    @Expose
    private String facebookId;

    @SerializedName("is_verify")
    @Expose
    private String isVerify;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("allow_ads")
    @Expose
    private String allow_ads;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }

    public void setFacebookId(String id) {
        this.facebookId = id;
    }
    public String getFacebookId() {
        return facebookId;
    }

    /**
     * @return The token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token The token
     */
    public void setToken(String token) {
        this.token = token;
    }

    public void setIsVerify(String status) {
        this.isVerify = status;
    }
    public String getIsVerify() {
        return isVerify;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }
    public void setAllow_ads(String allow_ads) {
        this.allow_ads = allow_ads;
    }
    public String getAllow_ads() {
        return allow_ads;
    }
}
