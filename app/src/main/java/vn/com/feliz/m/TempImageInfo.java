package vn.com.feliz.m;

import io.realm.RealmObject;

/**
 * Created by thanhlong on 6/25/16.
 */
public class TempImageInfo extends RealmObject {

    private String path;


    /**
     * Get Image Path
     * @return Path
     */
    public String getPath() {
        return path;
    }

    /**
     * Set Image Path
     * @param path Path
     */
    public void setPath(String path) {
        this.path = path;
    }


}
