package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thanhlong on 7/3/16.
 */
public class QuestionDailyListResponse implements Serializable {
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("list")
    @Expose
    private List<QuestionDailyResponse> list = new ArrayList<QuestionDailyResponse>();

    /**
     *
     * @return
     * The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     *
     * @param total
     * The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }


    public List<QuestionDailyResponse> getList() {
        return list;
    }

    public void setList(List<QuestionDailyResponse> list) {
        this.list = list;
    }
}
