package vn.com.feliz.m;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class AddReviewResponse {
    @SerializedName("review_product_id")
    @Expose
    private String review_product_id;
    @SerializedName("deleted")
    @Expose
    private String deleted;

    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("product_id")
    @Expose
    private String product_id;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("created_at")
    @Expose
    private String created_at;
    @SerializedName("helpful_count")
    @Expose
    private String helpful_count;
    @SerializedName("is_share")
    @Expose
    private String is_share;
    @SerializedName("fullname")
    @Expose
    private String fullname;
    @SerializedName("product_name")
    @Expose
    private String product_name;
    @SerializedName("media_url")
    @Expose
    private String media_url;
    @SerializedName("product_rating")
    @Expose
    private String product_rating;
    @SerializedName("product_review")
    @Expose
    private String product_review;
    @SerializedName("is_review0")
    @Expose
    private String is_review0;
    @SerializedName("is_review1")
    @Expose
    private String is_review1;
    @SerializedName("sponsor")
    @Expose
    private String sponsor;

    public String getReview_product_id() {
        return review_product_id;
    }

    public void setReview_product_id(String review_product_id) {
        this.review_product_id = review_product_id;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getHelpful_count() {
        return helpful_count;
    }

    public void setHelpful_count(String helpful_count) {
        this.helpful_count = helpful_count;
    }

    public String getIs_share() {
        return is_share;
    }

    public void setIs_share(String is_share) {
        this.is_share = is_share;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getProduct_rating() {
        return product_rating;
    }

    public void setProduct_rating(String product_rating) {
        this.product_rating = product_rating;
    }

    public String getProduct_review() {
        return product_review;
    }

    public void setProduct_review(String product_review) {
        this.product_review = product_review;
    }

    public String getIs_review0() {
        return is_review0;
    }

    public void setIs_review0(String is_review0) {
        this.is_review0 = is_review0;
    }

    public String getIs_review1() {
        return is_review1;
    }

    public void setIs_review1(String is_review1) {
        this.is_review1 = is_review1;
    }

    public String getSponsor() {
        return sponsor;
    }

    public void setSponsor(String sponsor) {
        this.sponsor = sponsor;
    }
}
