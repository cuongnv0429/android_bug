package vn.com.feliz.m;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Copyright Innotech Vietnam
 * Created by Mrson on 6/24/2016.
 */
public class BaseResponseAddReview<T> {
    @SerializedName("data")
    @Expose
    private String data;

    public BaseResponseAddReview(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
