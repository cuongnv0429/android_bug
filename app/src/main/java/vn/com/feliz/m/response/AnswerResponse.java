package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nguyen Thai Son on 2016-11-15.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class AnswerResponse implements Serializable{
    @SerializedName("answer_id")
    @Expose
    private String answer_id;
    @SerializedName("value")
    @Expose
    private String value;

    public String getAnswer_id() {
        return answer_id;
    }

    public void setAnswer_id(String answer_id) {
        this.answer_id = answer_id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
