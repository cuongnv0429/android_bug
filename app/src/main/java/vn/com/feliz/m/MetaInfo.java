package vn.com.feliz.m;

import io.realm.RealmObject;

/**
 * Created by Nguyen Thai Son on 2016-11-18.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class MetaInfo extends RealmObject {
    private int verify_code_limit;
    private String term;
    private String app_intro;
    private String condition;
    private String app_info;
    private String ads_review;
    private String event_mode;
    private String scan_timeout;
    private String force_update;
    private String force_version;
    private String force_message;
    private String game_link;
    private String game_image_url;
    private String game_message;

    public String getGame_link() {
        return game_link;
    }

    public void setGame_link(String game_link) {
        this.game_link = game_link;
    }

    public String getGame_image_url() {
        return game_image_url;
    }

    public void setGame_image_url(String game_image_url) {
        this.game_image_url = game_image_url;
    }

    public String getGame_message() {
        return game_message;
    }

    public void setGame_message(String game_message) {
        this.game_message = game_message;
    }

    public String getForce_message() {
        return force_message;
    }

    public void setForce_message(String force_message) {
        this.force_message = force_message;
    }

    public String getForce_version() {
        return force_version;
    }

    public void setForce_version(String force_version) {
        this.force_version = force_version;
    }

    public String getForce_update() {
        return force_update;
    }

    public void setForce_update(String force_update) {
        this.force_update = force_update;
    }

    public int getVerify_code_limit() {
        return verify_code_limit;
    }

    public void setVerify_code_limit(int verify_code_limit) {
        this.verify_code_limit = verify_code_limit;
    }
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getApp_intro() {
        return app_intro;
    }

    public void setApp_intro(String app_intro) {
        this.app_intro = app_intro;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getApp_info() {
        return app_info;
    }

    public void setApp_info(String app_info) {
        this.app_info = app_info;
    }

    public void setAds_review(String ads_review) {
        this.ads_review = ads_review;
    }
    public String getAds_review() {
        return ads_review;
    }

    public void setEvent_mode(String event_mode) {
        this.event_mode = event_mode;
    }
    public String getEvent_mode() {
        return event_mode;
    }

    public void setScan_timeout(String scan_timeout) {
        this.scan_timeout = scan_timeout;
    }
    public String getScan_timeout() {
        return scan_timeout;
    }
}