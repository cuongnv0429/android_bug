package vn.com.feliz.m.response;

/**
 * Created by ITV01 on 2/24/17.
 */

public class NotificationRespone {
    private String push_id;
    private String push_type;
    private String coupon_id;
    private String coupon_sub_id;
    private String ads_id;
    private String push_title;
    private String push_content;
    private String push_status;

    public NotificationRespone() {
    }

    public String getPush_id() {
        return push_id;
    }

    public void setPush_id(String push_id) {
        this.push_id = push_id;
    }

    public String getPush_type() {
        return push_type;
    }

    public void setPush_type(String push_type) {
        this.push_type = push_type;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getAds_id() {
        return ads_id;
    }

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getPush_title() {
        return push_title;
    }

    public void setPush_title(String push_title) {
        this.push_title = push_title;
    }

    public String getPush_content() {
        return push_content;
    }

    public void setPush_content(String push_content) {
        this.push_content = push_content;
    }

    public String getPush_status() {
        return push_status;
    }

    public void setPush_status(String push_status) {
        this.push_status = push_status;
    }
}
