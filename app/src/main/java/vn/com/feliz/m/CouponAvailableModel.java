package vn.com.feliz.m;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CouponAvailableModel extends BaseModel implements Serializable{

    @SerializedName("data")
    private CouponAvailable couponAvailable;

    public CouponAvailable getCouponAvailable() {
        return couponAvailable;
    }

    public void setCouponAvailable(CouponAvailable couponAvailable) {
        this.couponAvailable = couponAvailable;
    }

    public class CouponAvailable implements Serializable {
        @SerializedName("total")
        private int total;
        @SerializedName("list")
        private List<CouponModel.Coupon> coupons;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<CouponModel.Coupon> getCoupons() {
            return coupons;
        }

        public void setCoupons(List<CouponModel.Coupon> coupons) {
            this.coupons = coupons;
        }
    }
}
