package vn.com.feliz.m;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AdsModel implements Serializable {
    @SerializedName("code")
    private String code;
    @SerializedName("status")
    private String status;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private Ads ads;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Ads getAds() {
        return ads;
    }

    public void setAds(Ads ads) {
        this.ads = ads;
    }

    public class Ads implements Serializable{
        @SerializedName("ads_id")
        private String adsId;
        @SerializedName("media_url")
        private String mediaUrl;
        @SerializedName("media_url_list")
        private List<String> mediaUrlList;
        @SerializedName("ads_type")
        private String adsType;
        @SerializedName("media_type")
        private String mediaType;
        @SerializedName("link")
        private String link;
        @SerializedName("status")
        private String status;
        @SerializedName("share_title")
        private String shareTitle;
        @SerializedName("share_link")
        private String shareLink;

        @SerializedName("description")
        private String description;

        //cuongnv
        @SerializedName("touch")
        private List<Integer> touch;

        @SerializedName("is_browser")
        private String isBrowser;

        public String getAdsId() {
            return adsId;
        }

        public void setAdsId(String adsId) {
            this.adsId = adsId;
        }

        public String getMediaUrl() {
            return mediaUrl;
        }

        public void setMediaUrl(String mediaUrl) {
            this.mediaUrl = mediaUrl;
        }

        public List<String> getMediaUrlList() {
            return mediaUrlList;
        }

        public void setMediaUrlList(List<String> mediaUrlList) {
            this.mediaUrlList = mediaUrlList;
        }

        public String getAdsType() {
            return adsType;
        }

        public void setAdsType(String adsType) {
            this.adsType = adsType;
        }

        public String getMediaType() {
            return mediaType;
        }

        public void setMediaType(String mediaType) {
            this.mediaType = mediaType;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public void setShareLink(String link) {
            this.shareLink = link;
        }
        public String getShareLink() {
            return shareLink;
        }
        public void setShareTitle(String title) {
            this.shareTitle = title;
        }
        public String getShareTitle() {
            return shareTitle;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public List<Integer> getTouch() {
            return touch;
        }

        public void setTouch(List<Integer> touch) {
            this.touch = touch;
        }

        public String getIsBrowser() {
            return isBrowser;
        }

        public void setIsBrowser(String isBrowser) {
            this.isBrowser = isBrowser;
        }
    }
}
