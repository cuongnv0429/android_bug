package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nguyen Thai Son on 2016-12-19.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class AddReviewUploadResponse {
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("bar_code")
    @Expose
    private String bar_code;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("point")
    @Expose
    private String point;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("is_share")
    @Expose
    private String is_share;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getBar_code() {
        return bar_code;
    }

    public void setBar_code(String bar_code) {
        this.bar_code = bar_code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIs_share() {
        return is_share;
    }

    public void setIs_share(String is_share) {
        this.is_share = is_share;
    }
}
