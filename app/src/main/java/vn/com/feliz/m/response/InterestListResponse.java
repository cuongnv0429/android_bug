package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class InterestListResponse implements Serializable {
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("list")
    @Expose
    private List<InterestResponse> list = new ArrayList<InterestResponse>();

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<InterestResponse> getList() {
        return list;
    }

    public void setList(List<InterestResponse> list) {
        this.list = list;
    }


}
