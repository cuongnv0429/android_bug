package vn.com.feliz.m;

/**
 * Created by ITV01 on 12/26/16.
 */

public class Profile {
    private String phone;
    private String fullname;
    private int current_coin;
    private int save_coin;
    private int gift_coin;
    private int used_coin;
    private String uid;
    private String device_id;
    private int total_notifications;
    private String is_noti_coupon;
    private String is_noti_coin;
    private String is_noti_review;
    private String is_noti_setting;

    public Profile() {
        this.phone = "";
        this.fullname = "";
        this.current_coin = 0;
        this.save_coin = 0;
        this.gift_coin = 0;
        this.used_coin = 0;
        this.uid = "";
        this.device_id = "";
        this.total_notifications = 0;
        this.is_noti_coupon = "0";
        this.is_noti_coin = "0";
        this.is_noti_review = "0";
        this.is_noti_setting = "0";
    }

    public int getTotal_notifications() {
        return total_notifications;
    }

    public void setTotal_notifications(int total_notifications) {
        this.total_notifications = total_notifications;
    }

    public String getIs_noti_coupon() {
        return is_noti_coupon;
    }

    public void setIs_noti_coupon(String is_noti_coupon) {
        this.is_noti_coupon = is_noti_coupon;
    }

    public String getIs_noti_coin() {
        return is_noti_coin;
    }

    public void setIs_noti_coin(String is_noti_coin) {
        this.is_noti_coin = is_noti_coin;
    }

    public String getIs_noti_review() {
        return is_noti_review;
    }

    public void setIs_noti_review(String is_noti_review) {
        this.is_noti_review = is_noti_review;
    }

    public String getIs_noti_setting() {
        return is_noti_setting;
    }

    public void setIs_noti_setting(String is_noti_setting) {
        this.is_noti_setting = is_noti_setting;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getCurrent_coin() {
        return current_coin;
    }

    public void setCurrent_coin(int current_coin) {
        this.current_coin = current_coin;
    }

    public int getSave_coin() {
        return save_coin;
    }

    public void setSave_coin(int save_coin) {
        this.save_coin = save_coin;
    }

    public int getGift_coin() {
        return gift_coin;
    }

    public void setGift_coin(int gift_coin) {
        this.gift_coin = gift_coin;
    }

    public int getUsed_coin() {
        return used_coin;
    }

    public void setUsed_coin(int used_coin) {
        this.used_coin = used_coin;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }
}
