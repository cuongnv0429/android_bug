package vn.com.feliz.m;


import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NotificationModel implements Serializable {

    private int id;
    private String ads_id;
    private String title;
    private String description;
    private String packageName;
    private long postTime;
    private int icon;
    private int pushType;
    private String couponId;
    private String couponSubId;
    private String key;
    private String tag;
    private String gbCode;
    private int repeatTimes;
    private double latitude;
    private double longitude;
    private long next_show_time;
    private List<CouponModel.Coupon.Product> productList = new ArrayList<>();
    private CouponModel.Coupon coupon = new CouponModel.Coupon();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NotificationModel)) return false;

        NotificationModel that = (NotificationModel) o;

        if (getId() != that.getId()) return false;
        //if (getPostTime() != that.getPostTime()) return false;
        if (getIcon() != that.getIcon()) return false;
        if (getPushType() != that.getPushType()) return false;
        if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null)
            return false;
        return getPackageName() != null ? getPackageName().equals(that.getPackageName()) : that.getPackageName() == null && (getCouponId() != null ? getCouponId().equals(that.getCouponId()) : that.getCouponId() == null);

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getPackageName() != null ? getPackageName().hashCode() : 0);
        //result = 31 * result + (int) (getPostTime() ^ (getPostTime() >>> 32));
        result = 31 * result + getIcon();
        result = 31 * result + getPushType();
        result = 31 * result + (getCouponId() != null ? getCouponId().hashCode() : 0);
        return result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public long getPostTime() {
        return postTime;
    }

    public void setPostTime(long postTime) {
        this.postTime = postTime;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getPushType() {
        return pushType;
    }

    public void setPushType(int pushType) {
        this.pushType = pushType;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public String getKey() {
        return key;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void setKey(String key) {
        this.key = key;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getGbCode() {
        return gbCode;
    }

    public void setGbCode(String gbCode) {
        this.gbCode = gbCode;
    }

    public int getRepeatTimes() {
        return repeatTimes;
    }

    public void setRepeatTimes(int repeatTimes) {
        this.repeatTimes = repeatTimes;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setNext_show_time(long next_show_time) {
        this.next_show_time = next_show_time;
    }

    public long getNext_show_time() {
        return next_show_time;
    }

    public void setAds_id(String ads_id) {
        this.ads_id = ads_id;
    }

    public String getAds_id() {
        return ads_id;
    }

    public void setCouponSubId(String couponSubId) {
        this.couponSubId = couponSubId;
    }

    public String getCouponSubId() {
        return couponSubId;
    }

    public void setProductList(List<CouponModel.Coupon.Product> productList) {
        this.productList = productList;
    }

    public List<CouponModel.Coupon.Product> getProductList() {
        return productList;
    }

    public void setCoupon(CouponModel.Coupon coupon) {
        this.coupon = coupon;
    }

    public CouponModel.Coupon getCoupon() {
        return coupon;
    }
}


