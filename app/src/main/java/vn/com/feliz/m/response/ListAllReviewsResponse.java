package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import vn.com.feliz.m.ReviewAllItem;

public class ListAllReviewsResponse  {
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("list")
    @Expose
    private List<ReviewAllItem> listReviews = new ArrayList<ReviewAllItem>();

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<ReviewAllItem> getListReviews() {
        return listReviews;
    }

    public void setListReviews(List<ReviewAllItem> listReviews) {
        this.listReviews = listReviews;
    }
}
