package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * {
 * "code": "200",
 * "status": "success",
 * "message": "Success",
 * "data": {
 * "app_version": "1.0",
 * "os_type": "ios",
 * "device_push_token": "",
 * "device_id": "123456",
 * "os_version": "9.0",
 * "latitude": "",
 * "longitude": "",
 * "fullname": "Thái Sơn",
 * "email": "thaisonm360@gmail.com",
 * "phone": "0971034679",
 * "avatar": "http://graph.facebook.com/710250405798668/picture?type=large",
 * "birthday": "09/07/1991",
 * "uid": "123456",
 * "firebase_password": "123456",
 * "address": "",
 * "gender": "male",
 * "created_at": "2016-11-15 14:23:35",
 * "updated_at": "2016-11-15 14:23:35",
 * "token": "82eb74e85342365e86f4693629ad3535",
 * "token_expired_at": "2016-11-22 14:23:50"
 * }
 * }
 */
public class ProfileRespone {

    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("allow_ads")
    @Expose
    private String allow_ads;
    @SerializedName("size_ads")
    @Expose
    private String size_ads;

    @SerializedName("facebook_id")
    @Expose
    private String facebookId;

    @SerializedName("is_verify")
    @Expose
    private String isVerify;

    public String getAllow_ads() {
        return allow_ads;
    }

    public void setAllow_ads(String allow_ads) {
        this.allow_ads = allow_ads;
    }

    public String getSize_ads() {
        return size_ads;
    }

    public void setSize_ads(String size_ads) {
        this.size_ads = size_ads;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setIsVerify(String status) {
        this.isVerify = status;
    }
    public String getIsVerify() {
        return isVerify;
    }
    public void setFacebookId(String id) {
        this.facebookId = id;
    }
    public String getFacebookId() {
        return facebookId;
    }
}
