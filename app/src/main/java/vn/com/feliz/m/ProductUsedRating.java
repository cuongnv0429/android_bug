package vn.com.feliz.m;

import java.io.Serializable;

/**
 * Created by ITV01 on 1/9/17.
 */

public class ProductUsedRating implements Serializable{
    private String id;
    private String image;
    private String name;
    private Boolean statusCheck;
    private String is_review;
    private String barcode;

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getIs_review() {
        return is_review;
    }

    public void setIs_review(String is_review) {
        this.is_review = is_review;
    }

    public Boolean getStatusCheck() {
        return statusCheck;
    }

    public void setStatusCheck(Boolean statusCheck) {
        this.statusCheck = statusCheck;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
