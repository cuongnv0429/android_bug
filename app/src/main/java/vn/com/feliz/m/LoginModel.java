package vn.com.feliz.m;

/**
 * Created by user on 1/16/17.
 */

public class LoginModel {
    private String accessToken;
    private String fbID;
    private String firebaseUID;
    private String firebasePassword;
    private String name;
    private String email;
    private String gender;
    private String birthday;
    private String avatar;
    private String address;

    private String phone;

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public String getAccessToken() {
        return accessToken;
    }

    public void setFbID(String fbID) {
        this.fbID = fbID;
    }
    public String getFbID() {
        return fbID;
    }

    public void setFirebaseUID(String firebaseUID) {
        this.firebaseUID = firebaseUID;
    }
    public String getFirebaseUID() {
        return firebaseUID;
    }

    public void setFirebasePassword(String password) {
        this.firebasePassword = password;
    }
    public String getFirebasePassword() {
        return firebasePassword;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getEmail() {
        return email;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender() {
        return gender;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String getBirthday() {
        return birthday;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public String getAddress() {
        return address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPhone() {
        return phone;
    }

}
