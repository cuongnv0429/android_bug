package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Nguyen Thai Son on 2016-11-15.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class MetaResponse implements Serializable {
    @SerializedName("img_url")
    @Expose
    private String img_url;
    @SerializedName("video_url")
    @Expose
    private String video_url;
    @SerializedName("term")
    @Expose
    private String term;

    @SerializedName("app_intro")
    @Expose
    private String app_intro;
    @SerializedName("app_info")
    @Expose
    private String app_info;
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("verify_code_limit")
    @Expose
    private String verify_code_limit;
    @SerializedName("default_country_code")
    @Expose
    private String default_country_code;

    @SerializedName("ads_review")
    @Expose
    private String ads_review;

    @SerializedName("event_mode")
    @Expose
    private String event_mode;

    @SerializedName("scan_timeout")
    @Expose
    private String scan_timeout;

    @SerializedName("force_update")
    @Expose
    private String force_update;

    @SerializedName("force_version")
    @Expose
    private String force_version;

    @SerializedName("force_message")
    @Expose
    private String force_message;

    @SerializedName("game_link")
    @Expose
    private String game_link;

    @SerializedName("game_image_url")
    @Expose
    private String game_image_url;

    @SerializedName("game_message")
    @Expose
    private String game_message;

    public String getGame_link() {
        return game_link;
    }

    public void setGame_link(String game_link) {
        this.game_link = game_link;
    }

    public String getGame_image_url() {
        return game_image_url;
    }

    public void setGame_image_url(String game_image_url) {
        this.game_image_url = game_image_url;
    }

    public String getGame_message() {
        return game_message;
    }

    public void setGame_message(String game_message) {
        this.game_message = game_message;
    }

    public String getForce_message() {
        return force_message;
    }

    public void setForce_message(String force_message) {
        this.force_message = force_message;
    }

    public String getForce_version() {
        return force_version;
    }

    public void setForce_version(String force_version) {
        this.force_version = force_version;
    }

    public String getForce_update() {
        return force_update;
    }

    public void setForce_update(String force_update) {
        this.force_update = force_update;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getTerm() {
        return term;
    }

    public String getApp_info() {
        return app_info;
    }

    public void setApp_info(String app_info) {
        this.app_info = app_info;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getApp_intro() {
        return app_intro;
    }

    public void setApp_intro(String app_intro) {
        this.app_intro = app_intro;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getVerify_code_limit() {
        return verify_code_limit;
    }

    public void setVerify_code_limit(String verify_code_limit) {
        this.verify_code_limit = verify_code_limit;
    }

    public String getDefault_country_code() {
        return default_country_code;
    }

    public void setDefault_country_code(String default_country_code) {
        this.default_country_code = default_country_code;
    }

    public void setAds_review(String ads_review) {
        this.ads_review = ads_review;
    }
    public String getAds_review() {
        return ads_review;
    }

    public void setEvent_mode(String event_mode) {
        this.event_mode = event_mode;
    }
    public String getEvent_mode() {
        return event_mode;
    }

    public void setScan_timeout(String scan_timeout) {
        this.scan_timeout = scan_timeout;
    }
    public String getScan_timeout() {
        return scan_timeout;
    }
}