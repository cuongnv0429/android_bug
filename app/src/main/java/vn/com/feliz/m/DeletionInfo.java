package vn.com.feliz.m;

import io.realm.RealmObject;

/**
 * Created by Nguyen Thai Son on 2017-01-05.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class DeletionInfo extends RealmObject {

    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

