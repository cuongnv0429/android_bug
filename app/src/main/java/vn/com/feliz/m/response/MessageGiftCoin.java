package vn.com.feliz.m.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ITV01 on 12/27/16.
 */

public class MessageGiftCoin {
    @SerializedName("request_id")
    @Expose
    private String request_id;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("message")
    @Expose
    private String message;

    public MessageGiftCoin() {
        this.request_id = "";
        this.title = "";
        this.message = "";
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
