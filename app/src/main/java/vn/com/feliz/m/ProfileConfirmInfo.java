package vn.com.feliz.m;

import java.io.Serializable;

/**
 * Created by Nguyen Thai Son on 2016-11-11.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class ProfileConfirmInfo implements Serializable {
    private String birthday;
    private String mobile;
    private String adress;
    private String gender;
    private String userName;
    private String coupons_id;
    private String is_verify;


    public ProfileConfirmInfo(String birthday, String mobile, String adress, String gender, String userName, String is_verify) {
        this.birthday = birthday;
        this.mobile = mobile;
        this.adress = adress;
        this.gender = gender;
        this.userName = userName;
        this.is_verify = is_verify;
    }

    public String getCoupons_id() {
        return coupons_id;
    }

    public void setCoupons_id(String coupons_id) {
        this.coupons_id = coupons_id;
    }

    public ProfileConfirmInfo() {
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setIsVerify(String is_verify) {
        this.is_verify = is_verify;
    }
    public String getIsVerify() {
        return is_verify;
    }
}
