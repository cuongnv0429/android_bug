package vn.com.feliz.m;

/**
 * Created by ITV01 on 1/13/17.
 */

public class LocationHomeExchange {
    private String token;
    private String coupon_id;
    private String coupon_sub_id;
    private String status;
    private String address;
    private String city_name;
    private String district_name;
    private String ward_name;

    public LocationHomeExchange() {
        this.token = "";
        this.coupon_id = "";
        this.coupon_sub_id = "";
        this.status = "";
        this.address = "";
        this.city_name = "";
        this.district_name = "";
        this.ward_name = "";
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_sub_id() {
        return coupon_sub_id;
    }

    public void setCoupon_sub_id(String coupon_sub_id) {
        this.coupon_sub_id = coupon_sub_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getDistrict_name() {
        return district_name;
    }

    public void setDistrict_name(String district_name) {
        this.district_name = district_name;
    }

    public String getWard_name() {
        return ward_name;
    }

    public void setWard_name(String ward_name) {
        this.ward_name = ward_name;
    }
}
