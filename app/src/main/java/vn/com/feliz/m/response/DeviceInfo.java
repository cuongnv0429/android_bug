package vn.com.feliz.m.response;

import io.realm.RealmObject;

/**
 * Created by Nguyen Thai Son on 2016-11-18.
 * InnoTech Viet nam
 * sonntt079@gmail.com
 */
public class DeviceInfo extends RealmObject {

    private String mLat;
    private String mLong;
    private String mDeviceId;
    private String mOSVersion;
    private String mTokenPush;
    private String city;
    private String area;


    public String getmOSVersion() {
        return mOSVersion;
    }

    public void setmOSVersion(String mOSVersion) {
        this.mOSVersion = mOSVersion;
    }

    public String getmTokenPush() {
        return mTokenPush;
    }

    public void setmTokenPush(String mTokenPush) {
        this.mTokenPush = mTokenPush;
    }

    public String getmDeviceId() {
        return mDeviceId;
    }

    public void setmDeviceId(String mDeviceId) {
        this.mDeviceId = mDeviceId;
    }

    public String getmLat() {
        return mLat;
    }

    public void setmLat(String mLat) {
        this.mLat = mLat;
    }

    public String getmLong() {
        return mLong;
    }

    public void setmLong(String mLong) {
        this.mLong = mLong;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }



}
