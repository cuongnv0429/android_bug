package vn.com.feliz.m;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CouponListModel extends BaseModel implements Serializable {
    @SerializedName("data")
    private CouponList couponList;

    public CouponList getCouponList() {
        return couponList;
    }

    public void setCouponList(CouponList couponList) {
        this.couponList = couponList;
    }

    public class CouponList implements Serializable{
        @SerializedName("total")
        private int total;
        @SerializedName("list")
        private List<CouponModel.Coupon> couponList;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<CouponModel.Coupon> getCouponList() {
            return couponList;
        }

        public void setCouponList(List<CouponModel.Coupon> couponList) {
            this.couponList = couponList;
        }
    }
}
