package vn.com.feliz.m;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import vn.com.feliz.m.response.PushResponse;

public class AdsListModel extends BaseModel implements Serializable {
    @SerializedName("data")
    private List<PushResponse> adsList;

    public List<PushResponse> getAdsList() {
        return adsList;
    }

    public void setAdsList(List<PushResponse> adsList) {
        this.adsList = adsList;
    }
}
